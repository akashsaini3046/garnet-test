@isTest
private with sharing class BookingPDFControllerTest {
    @testSetup static void setup() {
        List<Booking__c> bookings = TestDataUtility.getBookingWithChildren('DP', 'EQUIP', 1);
    }

    @isTest static void bookingPDFTest() {
        Test.startTest();
        Booking__c booking = [SELECT Id, Name from Booking__c LIMIT 1];
        PageReference pageRef = Page.BookingRecordPdf;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',booking.id);
        pageRef.getParameters().put('bookingName',booking.Name);
        ApexPages.StandardController sc = new ApexPages.standardController(booking);
        BookingPDFController bookingPDFCtrl = new BookingPDFController();
        System.assert(pageRef.getUrl().contains('apex/bookingrecordpdf'), 'URL contains Booking Record PDF');
        Test.stopTest();
    }
    
    @isTest static void bookingPDFBBLUKTest() {
        Test.startTest();
        Booking__c booking = [SELECT Id, Name from Booking__c LIMIT 1];
        FreightDetail__c freight = [SELECT Cargo_Type__c FROM FreightDetail__c LIMIT 1];
        freight.Cargo_Type__c = 'BBLUK';
        update freight;
        PageReference pageRef = Page.BookingRecordPdf;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',booking.id);
        pageRef.getParameters().put('bookingName',booking.Name);
        ApexPages.StandardController sc = new ApexPages.standardController(booking);
        BookingPDFController bookingPDFCtrl = new BookingPDFController();
        System.assert(pageRef.getUrl().contains('apex/bookingrecordpdf'), 'URL contains Booking Record PDF');
        Test.stopTest();
    }
    
    @isTest static void bookingPDFAUTOTest() {
        Test.startTest();
        Booking__c booking = [SELECT Id, Name from Booking__c LIMIT 1];
        FreightDetail__c freight = [SELECT Cargo_Type__c FROM FreightDetail__c LIMIT 1];
        freight.Cargo_Type__c = 'AUTO';
        update freight;
        PageReference pageRef = Page.BookingRecordPdf;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',booking.id);
        pageRef.getParameters().put('bookingName',booking.Name);
        ApexPages.StandardController sc = new ApexPages.standardController(booking);
        BookingPDFController bookingPDFCtrl = new BookingPDFController();
        System.assert(pageRef.getUrl().contains('apex/bookingrecordpdf'),  'URL contains Booking Record PDF');
        Test.stopTest();
    }
    
    @isTest static void downloadPDFTest() {
        Test.startTest();
        Booking__c booking = [SELECT Id, Name from Booking__c LIMIT 1];
        PageReference pageRef = Page.BookingRecordPdf;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id',booking.id);
        pageRef.getParameters().put('bookingName',booking.Name);
        ApexPages.StandardController sc = new ApexPages.standardController(booking);
        BookingPDFController bookingPDFCtrl = new BookingPDFController();
        PageReference newPageRef = bookingPDFCtrl.downloadPDF();
        System.assert(newPageRef.getUrl().contains('apex/BookingRecordPDF'), 'URL contains Booking Record PDF');
        Test.stopTest();
    }

}