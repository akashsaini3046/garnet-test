@isTest private class CustomerCommunity_FileController_Test {
    
    
    @testSetup static void TestShareUser(){
        
        Account testAccount = TestDataUtility.createAccount('Nagarro',null,null,False,1)[0];
        
        DescribeFieldResult describeState = Address__c.State_Picklist__c.getDescribe();
        List<PicklistEntry> stateAvailableValues = describeState.getPicklistValues();
        Address__c testBusinessLocationObj = TestDataUtility.createBusinessLocation(new List<Account>{new Account(Id = testAccount.Id)}, 'BL1', 'City1', 
                                                                                stateAvailableValues[0].getValue(), '1111111', '88888888888', 
                                                                                'US', 1)[0];
        
      
        
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
        Contact testContact = TestDataUtility.createContact(recordTypeId,testAccount.Id,new List<Address__c>{testBusinessLocationObj},'testFirst','testLast','test@samp.com',null,False,null,1)[0];
        
        
        Booking__c testBooking = TestDataUtility.enterData_BookingInformation(testAccount.Id);
        CustomerDocument__c testDocument = TestDataUtility.enterData_DocumentInformation(testBooking.Id);

        Profile p = [SELECT Id FROM Profile WHERE Name = 'Customer Community Plus User' LIMIT 1];
        
        User newuser = new User(UserName='test@samp.com',Email='test@samp.com',Alias='tsamp',ProfileId= p.Id,isActive=true,ContactId=testContact.Id,
                                LastName='Testing',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles',EmailEncodingKey='ISO-8859-1');
        insert newuser;
    }
    static testMethod void saveChunk(){
    
        Test.startTest();
        Booking__c bookingDetail = [Select Name from Booking__c where Name = 'Test Booking'];    
        CustomerDocument__c customerDocumentDetail = [Select Document_Type__c from CustomerDocument__c where Name = 'TestDocument'];        
        CustomerCommunity_FileController.saveChunk(bookingDetail.Name,customerDocumentDetail.Document_Type__c,'test','vnjfn','fbhbh','123'); 
        Test.stopTest();
    }
    static testMethod void getAllBookings(){
        Test.startTest();
        User userDetail = [Select id from User where UserName = 'test@samp.com'];
        System.runAs(userDetail){
            CustomerCommunity_FileController.getAllBookings();    
        }  
        System.runAs(new user(ID = UserInfo.getUserID())){   
            CustomerCommunity_FileController.getAllBookings(); 
        }
        Test.stopTest();
    }
    static testMethod void readFileContent(){
        Test.startTest();
        
        Account testAccount = TestDataUtility.createAccount('Nagarro123',null,null,False,1)[0];
        
        Booking__c testBooking = TestDataUtility.enterData_BookingInformation(testAccount.Id);
        CustomerDocument__c testDocument = TestDataUtility.enterData_DocumentInformation(testBooking.Id);
        ContentVersion testVersion = TestDataUtility.createContentVersion();
        
        ContentDocumentLink testDocumentLink = TestDataUtility.createContentDocumentLink(testVersion, testDocument);
   
        CustomerCommunity_FileController.readFileContent(testDocument.Id);
        Test.stopTest();
    }
    static testMethod void readFileContentException(){
        Test.startTest();
        CustomerDocument__c testDocument = [Select id from CustomerDocument__c where Name = 'TestDocument']; 
        CustomerCommunity_FileController.readFileContent(testDocument.Id);
        Test.stopTest();
    }
    static testMethod void deleteDocumentRecordException(){
        Test.startTest();
        CustomerDocument__c testDocument = [Select id from CustomerDocument__c where Name = 'TestDocument'];
        CustomerCommunity_FileController.deleteDocumentRecord(testDocument.Id);
        Test.stopTest();
    }
        static testMethod void deleteDocumentRecord(){
        Test.startTest();
        
		Account testAccount = TestDataUtility.createAccount('Nagarro123',null,null,False,1)[0];
        
        Booking__c testBooking = TestDataUtility.enterData_BookingInformation(testAccount.Id);
        CustomerDocument__c testDocument = TestDataUtility.enterData_DocumentInformation(testBooking.Id);
        ContentVersion testVersion = TestDataUtility.createContentVersion();
        
        ContentDocumentLink testDocumentLink = TestDataUtility.createContentDocumentLink(testVersion, testDocument);
        CustomerCommunity_FileController.deleteDocumentRecord(testDocument.Id);
        Test.stopTest();
    }
}