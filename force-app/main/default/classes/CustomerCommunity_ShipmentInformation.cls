public class CustomerCommunity_ShipmentInformation {
    @AuraEnabled public String currentStatus{get; set;}
    @AuraEnabled public CustomerCommunity_ShipmentDischargeInfo discharge {get; set;}
    @AuraEnabled public String estSailDate{get; set;}
    @AuraEnabled public String shipmentID{get; set;}
    @AuraEnabled public String shipmentType{get; set;}
    @AuraEnabled public String statusDate{get; set;}
    @AuraEnabled public CustomerCommunity_ShipmentVoyageInfo voyage {get; set;}
    @AuraEnabled public String equipmentBooked{get; set;}
    @AuraEnabled public String equipmentReceived{get; set;}
    @AuraEnabled public Integer sequence{get; set;}
}