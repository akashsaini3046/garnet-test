/*
* Name: XMLParser
* Purpose: This class is used to parse the xml.
* Author: Nagarro
* Created Date: 27-June-2019
* Modification History
*  Modification #   Story/Defect#      Modified By     Date   Description
*/
public class XMLParser {
    //This holds parent id with which XML parsed object will merge to.
    private String strParentIdVal;
    //This holds API name of merged record 
    private String strParentAPIVal;
    //This holds list of strings which can be considered as true.
    private List<String> lstBooleanTrue = new List<String>();
    //This holds list of objects to be created where list contains in-memory unique identification of the object 
    //records. Example 2_1(i.e record numner 1 of object type 2 where 2 is the tag key). This list maintains the
    //order of records to be inserted to maintain parent child relationship.
    private List<String> lstObjectsToBeCreated = new List<String>();
    //This holds object wise parent in-memory id.
    private Map<String, String> mapObjectKeyWithParent = new Map<String, String>();
    //This holds object wise in-memory record unique identification.
    private Map<Integer, List<String>> mapObjectKeyWithINMUniqueRecordId = new Map<Integer, List<String>>();
    //This holds object wise fields details(field key and field value).
    private Map<String, Map<Integer, String>> mapObjectKeyWithChildField = new Map<String, Map<Integer, String>>();
    //This holds xml meta data by tage name. Here tag name is in the format of TagName_ParentTagName
    private Map<String, XML_Parser_Metadata__c> mapXMLMetaData = new Map<String, XML_Parser_Metadata__c>();
    //This holds xml meta data by tag key.
    private Map<Integer, XML_Parser_Metadata__c> mapXMLMetaDataByKey = new Map<Integer, XML_Parser_Metadata__c>();
    //This holds xml meta data by api name of tag.
    private Map<String, XML_Parser_Metadata__c> mapXMLMetaDataByObjectAPI = new Map<String, XML_Parser_Metadata__c>();
    
    /*
    * Method Name: loadXmlbdfkkgtgf
    * Input Parameters: 
    * String strXML: This holds XML String.
    * Return value: 
    * Purpose: This method loades xml for parsing.
    */
    public void loadXml(String strXML, String strParentId, String strParentAPI) {
        XML_Parser_Constant__mdt objXMLParserConstant;
        
        //Check if theare is any thing present in the strXML. If no data then return.
        if(String.isBlank(strXML)) {
            return;
        }
        strParentIdVal = strParentId;
        strParentAPIVal = strParentAPI;
        //Read boolean yes meta data.
        objXMLParserConstant = [SELECT Boolean_True_Values__c FROM XML_Parser_Constant__mdt WHERE DeveloperName = :Label.XMLP_BOOLEAN_METADATA_NAME LIMIT 1];
        List<String> lstBooleanTrue = new List<String>();
        for(String strBooleanTrue : objXMLParserConstant.Boolean_True_Values__c.split(ConstantClass.STRING_SPLITTER_XML_PARSER_BOOLEAN_YES)) {
            lstBooleanTrue.add(strBooleanTrue);
        }
        
        //Read XML meta data and create maps.
        for(XML_Parser_Metadata__c objXMLMeta : [SELECT Id, Name, API__c, Is_Data_Element__c, Is_Object__c, Is_Root_Element__c, Is_Storable__c, Key__c, Parent_Element__c, Parent_sObject_API__c, Data_Type__c, Parent_Field_API__c FROM XML_Parser_Metadata__c]) {
            mapXMLMetaData.put(objXMLMeta.Name, objXMLMeta);
            mapXMLMetaDataByKey.put(Integer.valueOf(objXMLMeta.Key__c), objXMLMeta);
            if(objXMLMeta.Is_Object__c && objXMLMeta.Is_Storable__c) {
                mapXMLMetaDataByObjectAPI.put(objXMLMeta.API__c, objXMLMeta);
            }
        } 
        
        try {
            DOM.Document objDoc = new DOM.Document();
            objDoc.load(strXML);
            DOM.XmlNode rootNode = objDoc.getRootElement();
            //Send root node for parsing.
            parseXML(rootNode);
            //Store parsed xml meta data.
            storeNodes();
        } catch(Exception objEx) {
            System.debug('XMLParser > loadXml ::' + objEx.getMessage());
            new ExceptionHandler().logBOLXMLPraserException(objEx, strParentIdVal);
        }
    }
    
    /*
    * Method Name: parseXML
    * Input Parameters: 
    * DOM.XMLNode node: This holds XML node.
    * Return value: 
    * Purpose: This method iterates over xml elements and submits node for processing.
    */
    private void parseXML(DOM.XMLNode node) {
        //Check if the node is of type element and has parent. If yes then send it for processing.
        System.debug('nodeInfo@@@@'+ node.getNodeType( ) + node.getParent() + (node.getNodeType() == DOM.XMLNodeType.ELEMENT));
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT && node.getParent() != null) {
            processNode(node); 
        }
        
        //Iterate over the clild node and parse recursively.
        for (Dom.XMLNode child: node.getChildElements()) {
            parseXML(child);
        }
    }
    
    /*
    * Method Name: processNode
    * Input Parameters: 
    * DOM.XmlNode node: This holds XML node.
    * Return value: 
    * Purpose: This method process xml elements to decide whether it is object or storable element and establishes parent child logical relationship.
    */
    private void processNode(DOM.XmlNode node) {
        String strParentNode;
        String strObjectRecordUniqueKey;
        String strParentRecordKey;
        List<String> lstObjectINMUniqueRecordId;
        List<String> lstObjectTobeCreatedforType;
        XML_Parser_Metadata__c objParentXMLMetaData;
        XML_Parser_Metadata__c objXMLMetaData;
        
        //Check if node is not null. If null then return.
        if(node.getParent() == null) {
            return;
        }
        
        //Get the parent node name and fetch the xml meta data by creating the tag key. TagName_ParentTag.
        strParentNode = node.getParent() != null ? node.getParent().getName() : '';
        if(node != null && String.isNotBlank(node.getName()) && mapXMLMetaData.containsKey(node.getName() + ConstantClass.UNDER_SCORE_THREE + strParentNode)) {
            objXMLMetaData = mapXMLMetaData.get(node.getName() + ConstantClass.UNDER_SCORE_THREE + strParentNode);
        } else {
            return;
        }
        
        //Check if the node is not the root node. 
        if(!objXMLMetaData.Is_Root_Element__c) {
            //Check if the node is of type object and can be stored in SF.
            if(objXMLMetaData.Is_Object__c && objXMLMetaData.Is_Storable__c) {
                //check if there is already an entry for current type of object in mapObjectKeyWithINMUniqueRecordId. 
                if(mapObjectKeyWithINMUniqueRecordId.containsKey(Integer.valueOf(objXMLMetaData.Key__c))) {
                    lstObjectTobeCreatedforType = mapObjectKeyWithINMUniqueRecordId.get(Integer.valueOf(objXMLMetaData.Key__c));
                    //Create record unique key by combining tag key and number of existing records of same type + 1.
                    strObjectRecordUniqueKey = String.valueOf(objXMLMetaData.Key__c) + ConstantClass.UNDER_SCORE_THREE  + (mapObjectKeyWithINMUniqueRecordId.get(Integer.valueOf(objXMLMetaData.Key__c)).size() + 1);
                    lstObjectTobeCreatedforType.add(strObjectRecordUniqueKey);
                    //Store record unique key by object type.
                    mapObjectKeyWithINMUniqueRecordId.put(Integer.valueOf(objXMLMetaData.Key__c), lstObjectTobeCreatedforType);
                } else {
                    strObjectRecordUniqueKey = String.valueOf(objXMLMetaData.Key__c) + ConstantClass.UNDER_SCORE_THREE  + 1;
                    mapObjectKeyWithINMUniqueRecordId.put(Integer.valueOf(objXMLMetaData.Key__c), new List<String>{strObjectRecordUniqueKey});
                }
                //Check if current object has parent. If so then identify the parent and create child to parent map(mapObjectKeyWithParent).
                if(String.isNotBlank(objXMLMetaData.Parent_sObject_API__c)) {
                    objParentXMLMetaData = mapXMLMetaDataByObjectAPI.get(objXMLMetaData.Parent_sObject_API__c);
                    lstObjectINMUniqueRecordId = mapObjectKeyWithINMUniqueRecordId.get(Integer.valueOf(objParentXMLMetaData.Key__c));
                    strParentRecordKey = lstObjectINMUniqueRecordId.get(lstObjectINMUniqueRecordId.size() - 1);
                    mapObjectKeyWithParent.put(strObjectRecordUniqueKey, strParentRecordKey);
                }
            }
            
            //Check if the tag is of type data element and is storable in salesforce.
            if(objXMLMetaData.Is_Data_Element__c && objXMLMetaData.Is_Storable__c) {
                if(mapXMLMetaDataByKey.get(Integer.valueOf(objXMLMetaData.Parent_Element__c)).Is_Storable__c) {//Check if the current tag has storable parent then get the parent metadata according to that.
                    objParentXMLMetaData = mapXMLMetaDataByKey.get(Integer.valueOf(objXMLMetaData.Parent_Element__c));  
                } else if(!mapXMLMetaDataByKey.get(Integer.valueOf(objXMLMetaData.Parent_Element__c)).Is_Storable__c) { //Check if the current tag dont has storable parent then get the parent metadata according to that.
                    objParentXMLMetaData = mapXMLMetaDataByKey.get(Integer.valueOf(objXMLMetaData.Parent_Element__c));
                    objParentXMLMetaData = mapXMLMetaDataByObjectAPI.get(objParentXMLMetaData.API__c);  
                }
                
                //Get the list of object of current type to be created. 
                lstObjectINMUniqueRecordId = mapObjectKeyWithINMUniqueRecordId.get(Integer.valueOf(objParentXMLMetaData.Key__c));
                //Get the recrd with with field will be associate to.
                strParentRecordKey = lstObjectINMUniqueRecordId.get(lstObjectINMUniqueRecordId.size() - 1);
                //Store object with fiels data in-memory.
                System.debug('----> strParentRecordKey-->' + strParentRecordKey);
                if(mapObjectKeyWithChildField.containsKey(strParentRecordKey)) {
                    mapObjectKeyWithChildField.get(strParentRecordKey).put(Integer.valueOf(objXMLMetaData.Key__c), node.getText());
                } else {
                    mapObjectKeyWithChildField.put(strParentRecordKey, new Map<Integer, String>{Integer.valueOf(objXMLMetaData.Key__c) => node.getText()});
                }
                System.debug('----> mapObjectKeyWithChildField-->' + mapObjectKeyWithChildField);
            } 
        }
    }
    
    /*
    * Method Name: storeNodes
    * Input Parameters: 
    * Return value: 
    * Purpose: This method stores processed xml nodes into salesforce.
    */
    private void storeNodes() {
        String strObjectTagKey;
        String strDateString;
        String strDateTimeString;
        String strError;
        Integer intDay;
        Integer intMonth;
        Integer intYear;
        Integer intHour;
        Integer intMin;
        Integer intIndex;
        sObject sObj;
        sObject sobjCurrent;
        List<Database.SaveResult> lstSaveResult;
        List<Database.SaveResult> lstUpdateResult;
        Map<String, String> mapInmemoryIdToSFId = new Map<String, String>();
        Map<Integer, String> mapFieldKeyWithValue = new Map<Integer, String>();
        List<sObject> lstsObjectToInsert = new List<sObject>();
        List<sObject> lstsObjectToUpdate = new List<sObject>();
        List<sObject> lstsObjectToUpdateWithParent = new List<sObject>();
        List<sObject> lstUpdatedAndInsertedObject = new List<sObject>();
        List<Database.SaveResult> lstUpdateResultAfterParentAssociation;
        Map<String, Schema.SObjectType> mapSobjectTypeByAPIName = Schema.getGlobalDescribe();
        
        //Create the list of object/records to be created. So that there will be order of creation to associate in-memory record with SF Id.
        for(Integer intKey : mapObjectKeyWithINMUniqueRecordId.keySet()) {
            lstObjectsToBeCreated.addAll(mapObjectKeyWithINMUniqueRecordId.get(intKey));
        }
        
        //Iterate over list of object to be created and make it SF compatible to store.
        for(String objStr : lstObjectsToBeCreated) {
            //Get the object tag name. 
            strObjectTagKey = objStr.mid(0, objStr.lastIndexOf(ConstantClass.UNDER_SCORE_THREE));
            //Instantiate the sobject type.
            sObj = mapSobjectTypeByAPIName.get(mapXMLMetaDataByKey.get(Integer.valueOf(strObjectTagKey)).API__c).newSObject(); 
            System.debug('sObj@@@'+sObj);
            System.debug('objStr--->'+objStr);
            //Get all fields with value which are related with current object.
            mapFieldKeyWithValue = mapObjectKeyWithChildField.get(objStr);
            System.debug('mapObjectKeyWithChildField -->' +mapObjectKeyWithChildField);
            System.debug('mapFieldKeyWithValue@@@'+mapFieldKeyWithValue);
            //Iterate over all fields and associate it with sObject.
            if(mapFieldKeyWithValue!=NULL){ 
            for(Integer intFieldKey : mapFieldKeyWithValue.keySet()) {
                //Cast the string as per data type of the object.
                if(mapXMLMetaDataByKey.get(intFieldKey).Data_Type__c == ConstantClass.DATA_TYPE_STRING) {
                    sObj.put(mapXMLMetaDataByKey.get(intFieldKey).API__c, mapFieldKeyWithValue.get(intFieldKey));
                } else if(mapXMLMetaDataByKey.get(intFieldKey).Data_Type__c == ConstantClass.DATA_TYPE_INTEGER && String.isNotBlank(mapFieldKeyWithValue.get(intFieldKey))) {
                    sObj.put(mapXMLMetaDataByKey.get(intFieldKey).API__c, Integer.valueOf(mapFieldKeyWithValue.get(intFieldKey)));
                } else if(mapXMLMetaDataByKey.get(intFieldKey).Data_Type__c == ConstantClass.DATA_TYPE_DECIMAL && String.isNotBlank(mapFieldKeyWithValue.get(intFieldKey))) {
                    sObj.put(mapXMLMetaDataByKey.get(intFieldKey).API__c, Decimal.valueOf(mapFieldKeyWithValue.get(intFieldKey)));
                } else if(mapXMLMetaDataByKey.get(intFieldKey).Data_Type__c == ConstantClass.DATA_TYPE_DATE && String.isNotBlank(mapFieldKeyWithValue.get(intFieldKey))) {
                    strDateString = mapFieldKeyWithValue.get(intFieldKey).trim();
                    if(strDateString.length() == 8 && strDateString.isNumeric()) {
                        intYear = Integer.valueOf(strDateString.left(4));
                        intMonth = Integer.valueOf(strDateString.mid(4, 2));
                        intDay = Integer.valueOf(strDateString.mid(6, 2));
                    } else if(strDateString.length() == 7 && strDateString.isNumeric()) {
                        intYear = Integer.valueOf(strDateString.left(4));
                        intMonth = Integer.valueOf(strDateString.mid(4, 1));
                        intDay = Integer.valueOf(strDateString.mid(5, 2)); 
                    } else if(strDateString.length() == 6 && strDateString.isNumeric()) {
                        intYear = Integer.valueOf(strDateString.left(4));
                        intMonth = Integer.valueOf(strDateString.mid(4, 1));
                        intDay = Integer.valueOf(strDateString.mid(5, 1)); 
                    }
                    sObj.put(mapXMLMetaDataByKey.get(intFieldKey).API__c, Date.newInstance(intYear, intMonth, intDay));
                    
                } else if(mapXMLMetaDataByKey.get(intFieldKey).Data_Type__c == ConstantClass.DATA_TYPE_DATETIME && String.isNotBlank(mapFieldKeyWithValue.get(intFieldKey))) {
                    strDateTimeString = mapFieldKeyWithValue.get(intFieldKey).trim();
                    if(strDateTimeString.length() == 12 && strDateTimeString.isNumeric()) {
                        intYear = Integer.valueOf(strDateTimeString.left(4));
                        intMonth = Integer.valueOf(strDateTimeString.mid(4, 2));
                        intDay = Integer.valueOf(strDateTimeString.mid(6, 2));
                        intHour = Integer.valueOf(strDateTimeString.mid(8, 2));
                        intMin = Integer.valueOf(strDateTimeString.mid(10, 2));
                    }
                    sObj.put(mapXMLMetaDataByKey.get(intFieldKey).API__c, DateTime.newInstance(intYear, intMonth, intDay, intHour, intMin, 00));
                    
                } else if(mapXMLMetaDataByKey.get(intFieldKey).Data_Type__c == ConstantClass.DATA_TYPE_BOOLEAN && String.isNotBlank(mapFieldKeyWithValue.get(intFieldKey))) {
                    sObj.put(mapXMLMetaDataByKey.get(intFieldKey).API__c, lstBooleanTrue.contains(mapFieldKeyWithValue.get(intFieldKey).toLowerCase()));
                }
             }
            }
            //If current object has to be merge with existing record in salesforce then put the Id with sObject to update.
            if(String.isNotBlank(strParentIdVal) && String.isNotBlank(strParentAPIVal) && mapXMLMetaDataByKey.get(Integer.valueOf(strObjectTagKey)).API__c == strParentAPIVal) {
                sObj.put('Id', strParentIdVal);
                lstsObjectToUpdate.add(sObj);
            } else {
                lstsObjectToInsert.add(sObj);   
            }
        }
        
        //Create a save point
        Savepoint spThisTransaction = Database.setSavepoint();
        
        //Perform DML
        try {
            lstUpdateResult = Database.update(lstsObjectToUpdate, true);
            lstSaveResult = Database.insert(lstsObjectToInsert, true);
        } catch(Exception objEx) {
            Database.rollback(spThisTransaction);
            throw new XMLParserException(objEx.getMessage() + ConstantClass.SPACE + objEx.getStackTraceString());
        }
        //Add all the updated and created records in single list.
        lstUpdatedAndInsertedObject.addAll(lstsObjectToUpdate);
        lstUpdatedAndInsertedObject.addAll(lstsObjectToInsert);
        
        intIndex = 0;
        //Iterate over update result and add into in-memory id to SF id map.
        for(Database.SaveResult objUPR : lstUpdateResult) {
            if(objUPR.isSuccess()) {
                mapInmemoryIdToSFId.put(lstObjectsToBeCreated.get(intIndex), objUPR.getId());
            } else {
                for(Database.Error objErr : objUPR.getErrors()) {
                    strError = strError + ConstantClass.SPACE + objErr.getMessage() + ConstantClass.SPACE + objErr.getFields();
                }
                Database.rollback(spThisTransaction);
                throw new XMLParserException(strError);
            }
            intIndex++;
        }
        
        //Iterate over save result and add into in-memory id to SF id map.
        for(Database.SaveResult objSVR : lstSaveResult) {
            if(objSVR.isSuccess()) {
                mapInmemoryIdToSFId.put(lstObjectsToBeCreated.get(intIndex), objSVR.getId());
            } else {
                for(Database.Error objErr : objSVR.getErrors()) {
                    strError = strError + ConstantClass.SPACE + objErr.getMessage() + ConstantClass.SPACE + objErr.getFields();
                }
                Database.rollback(spThisTransaction);
                throw new XMLParserException(strError);
            }
            intIndex++;
        }
        
        intIndex = 0;
        //Iterate over object to be created list and associate with the parent.
        for(String objStr : lstObjectsToBeCreated) {
            sobjCurrent = lstUpdatedAndInsertedObject.get(intIndex);
            strObjectTagKey = objStr.mid(0, objStr.lastIndexOf(ConstantClass.UNDER_SCORE_THREE));
            if(mapObjectKeyWithParent.containsKey(objStr)) {
                sobjCurrent.put(mapXMLMetaDataByKey.get(Integer.valueOf(strObjectTagKey)).Parent_Field_API__c, mapInmemoryIdToSFId.get(mapObjectKeyWithParent.get(objStr)));
                lstsObjectToUpdateWithParent.add(sobjCurrent);
            }
            intIndex++;
        }
        
        //Update objects after parent association.
        try {
            lstUpdateResultAfterParentAssociation = Database.update(lstsObjectToUpdateWithParent, true);
        } catch(Exception objEx) {
            Database.rollback(spThisTransaction);
            throw new XMLParserException(objEx.getMessage() + ConstantClass.SPACE + objEx.getStackTraceString());
        }
        
        //Iterate over update result to shee if any error
        for(Database.SaveResult objUPR : lstUpdateResultAfterParentAssociation) {
            if(objUPR.isSuccess() == false) {
                for(Database.Error objErr : objUPR.getErrors()) {
                    strError = strError + ConstantClass.SPACE + objErr.getMessage() + ConstantClass.SPACE + objErr.getFields();
                }
                Database.rollback(spThisTransaction);
                throw new XMLParserException(strError);
            }
        }
    }
}