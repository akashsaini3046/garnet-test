@isTest
public class CustomerCommunity_BookingsChatbot_Test {
    
    @testSetup static void testShareUser(){
       
        Account testAccount = TestDataUtility.createAccount('Nagarro',null,null,False,1)[0];
        
        DescribeFieldResult describeState = Address__c.State_Picklist__c.getDescribe();
        List<PicklistEntry> stateAvailableValues = describeState.getPicklistValues();
        Address__c businessLocationObj = TestDataUtility.createBusinessLocation(new List<Account>{new Account(Id = testAccount.Id)}, 'BL1', 'City1', 
                                                                                stateAvailableValues[0].getValue(), '1111111', '88888888888', 
                                                                                'US', 1)[0];
          
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
        Contact contactDetail = TestDataUtility.createContact(recordTypeId,testAccount.Id,new List<Address__c>{businessLocationObj},'testFirst','testLast','test@samp.com',null,False,null,1)[0];
        Booking__c testBooking = TestDataUtility.enterData_BookingInformation(testAccount.Id);
    }
    
    static testMethod void FindARoute(){

        test.startTest();
        CustomerCommunity_BookingsChatbot.InputFields testInputFields = new CustomerCommunity_BookingsChatbot.InputFields();
        Booking__c testBookingDetail = [Select Name,Origin__c,Destination__c from Booking__c where Name = 'Test Booking'];
        testInputFields.sOrigin=testBookingDetail.Origin__c;
        testInputFields.sDestination=testBookingDetail.Destination__c;
        testInputFields.sContactId = [Select id from Contact where LastName='testLast'].Id;
        List<CustomerCommunity_BookingsChatbot.InputFields> listInput = new List<CustomerCommunity_BookingsChatbot.InputFields>();
        listInput.add(testInputFields);
        CustomerCommunity_BookingsChatbot.FindARoute(listInput);
        test.stopTest();
    }
    static testMethod void FindARouteException(){
        
        test.startTest();
        CustomerCommunity_BookingsChatbot.InputFields testInputExcep = new CustomerCommunity_BookingsChatbot.InputFields();
        List<CustomerCommunity_BookingsChatbot.InputFields> listInput = new List<CustomerCommunity_BookingsChatbot.InputFields>();
        listInput.add(testInputExcep);
        CustomerCommunity_BookingsChatbot.FindARoute(listInput);
        test.stopTest();
    }
}