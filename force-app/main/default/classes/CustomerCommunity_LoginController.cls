global without sharing class CustomerCommunity_LoginController {
    @AuraEnabled
    public static Boolean checkPortal(String username, String password, String currentURL) {
        try {
            if (currentURL.contains('header')) {
                currentURL = currentURL.replaceAll('%2F', '/');
                currentURL = currentURL.substring(currentURL.indexOf('startURL=') + 9);
                if (currentURL.contains('&')) {
                    currentURL = currentURL.substringBefore('&');
                }
            } else {
                currentURL = CustomerCommunity_Constants.COMMUNITY_URL;
            }
            System.PageReference lgn = Site.login(username, password, currentURL);
            if (lgn != null) {
                aura.redirect(lgn);
                CustomerCommunity_BadgesController.checkBadges(username);
                return true;
            } else
                return false;
            
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return false;
        }
    }
}