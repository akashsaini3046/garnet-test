public class CustomerCommunity_VerticalMenuController {
    @AuraEnabled
    public static List<Vertical_Menu_List__mdt> getMenuItems(){
        String communityName = getCommunityName();
        Id userId = UserInfo.getUserId();
        User userDetail = new User();
        Contact conDetail = new Contact();
        List<Vertical_Menu_List__mdt> list_MenuOptions = new List<Vertical_Menu_List__mdt>();
        Integer newSequence = 1; Integer visibilityLevel = 1;
        try{
            userDetail = [select Id, contactId from User where Id = : userId];
            if(userDetail.contactId != Null) {
                conDetail = [SELECT Id, Name, Account_Type_Level__c FROM Contact WHERE Id = :userDetail.contactId];
                visibilityLevel = Integer.valueOf(conDetail.Account_Type_Level__c);
            }
            if(communityName=='Crowley_CustomerCommunity'){
                list_MenuOptions = [SELECT Id, MasterLabel, Is_Active__c, Sequence__c, URL__c, Style_Classes__c, Lightning_Component_Name__c, Visibility_Level__c ,Target__c
                                    FROM Vertical_Menu_List__mdt WHERE (Is_Active__c = True AND Visibility_Level__c <= :visibilityLevel AND Community_Name__c='CrowleyCustomerCommunity') ORDER By Sequence__c ASC];
            }
            
            else if(communityName=='CustomerCommunity'){
                list_MenuOptions = [SELECT Id, MasterLabel, Is_Active__c, Sequence__c, URL__c, Style_Classes__c, Lightning_Component_Name__c, Visibility_Level__c ,Target__c
                                    FROM Vertical_Menu_List__mdt WHERE (Is_Active__c = True AND Visibility_Level__c <= :visibilityLevel AND Community_Name__c='CustomerCommunity') ORDER By Sequence__c ASC];
            }   
            
            //list_MenuOptions = [SELECT Id, MasterLabel, Is_Active__c, Sequence__c, URL__c, Style_Classes__c, Lightning_Component_Name__c, Visibility_Level__c ,Target__c
            //                        FROM Vertical_Menu_List__mdt WHERE (Is_Active__c = True AND Visibility_Level__c <= :visibilityLevel) ORDER By Sequence__c ASC]; 
            //list_MenuOptions = [SELECT Id, MasterLabel, Is_Active__c, Sequence__c, URL__c, Style_Classes__c, Lightning_Component_Name__c, Visibility_Level__c ,Target__c
                                 //   FROM Vertical_Menu_List__mdt WHERE (Is_Active__c = True AND Visibility_Level__c <= :visibilityLevel AND Community_Name__c='CrowleyCustomerComunity') ORDER By Sequence__c ASC];                           
            for(Vertical_Menu_List__mdt menu : list_MenuOptions) {
                menu.Sequence__c = newSequence++;
            }
            System.debug('list_MenuOptions'+list_MenuOptions);
            return list_MenuOptions;
                
            
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }       
        
    }
    
    @AuraEnabled		 
    public static String getCommunityName(){
        String nwid = Network.getNetworkId();
        if (!String.isBlank(nwid)) {
            String communityName = [SELECT name from Network where id =: nwid][0].name ;
            System.debug('COMMUNITY NAME '+ communityName);
            return communityName;
    	}
        else{
            return null;
        }
}
}