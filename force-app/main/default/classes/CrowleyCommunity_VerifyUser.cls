global without sharing class CrowleyCommunity_VerifyUser {
    @AuraEnabled
    public static String checkforDuplicate(String userName){
        List<User> listUser = new List<User>();
        try{
            listUser = [Select id from User where userName = :userName];
            if(listUser.isEmpty()){
                return CrowleyCommunity_Constants.FALSE_MESSAGE;
            }
            else{
               return CrowleyCommunity_Constants.TRUE_MESSAGE; 
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return CrowleyCommunity_Constants.ERROR_MESSAGE;
        }
    }
    @AuraEnabled
    public static Contact getContactDetails(String conEmail){
        Contact contactRecord = new Contact();
        try{
            contactRecord = [Select firstName,LastName,account.Name,Email,Phone from Contact where Email = :conEmail LIMIT 1];
            if(contactRecord != NULL){
                return contactRecord;
            }
            else{
                return NULL;
            }
        }
        catch(Exception ex){
           ExceptionHandler.logApexCalloutError(ex);
           return NULL; 
        }
    }
}