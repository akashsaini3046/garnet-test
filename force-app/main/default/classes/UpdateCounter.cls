global class UpdateCounter {
    @future
    public static void updatecounteronAccount(Id accId){
        System.debug('infuture-->');
        Account acc=[Select id,Counter__c from Account where id=:accId];
        acc.Counter__c=acc.Counter__c+1;
        update acc;
}
}