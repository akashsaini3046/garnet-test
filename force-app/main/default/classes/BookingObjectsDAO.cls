/*
 * 24-06-2020 – Drupad Preenja – Created this class for handling Booking and Related Object Queries.
 *
 */

/*
 * @company     : Nagarro Inc.
 * @date        : 24-06-2020
 * @author      : Nagarro
 * @description : DAO for Booking and Related Objects
 * @history     : Version 1.0
 * @test class  : BookingObjectsDAOTest
 */
public inherited sharing class BookingObjectsDAO implements IQueryDAO {
    
    /*
     * @company     : Nagarro Inc.
     * @date        : 24-06-2020
     * @author      : Nagarro
     * @description : Interface for the DAO - Contains methods which must be implemented
     * @history     : Version 1.0
     */
    public interface IQueryDAO {
        Booking__c getBookingData(String bookingId);
        List < Stop__c > getStopsList(Set < Id > transportIds);
        List < FreightDetail__c > getFreightDetailList(Set < Id > shipmentIds);
        List < Voyage__c > getVoyageList(Set < Id > shipmentIds);
        List < Equipment__c > getEquipmentsList(Set < Id > requirementIds);
        List < Ports_Information__c > getPorts(String location);
    }

    /*
     * @purpose     : Method to get the Booking Record and related child records corresponding to the Booking Id provided.
     * @parameter   : bookingId - Id of the booking record to be fetched
     * @return      : Booking__c - Booking Record with Children
     */
    public static Booking__c getBookingData(String bookingId) {
        Booking__c booking;
        booking = [SELECT Name, Booking_Number__c, Status__c, Booked_Date__c, Contract_Number__c, Is_Hazardous__c, Payment_Terms__c, Inbond_Indicator__c, description__c,
            Pickup_Location__c, Insurance_Required__c, Customer_Origin_Reference_Number__c, Delivery_Location__c, Customer_Destination_Reference_Number__c,
            (SELECT Id FROM Transports__r),
            (SELECT Id, Is_Sed_Indicator__c, Origin_City__c, Origin_Country__c, Destination_City__c, Destination_Country__c FROM Shipments__r),
            (SELECT Id, Name, Type__c, REF_Number__c FROM Parties__r)
            FROM Booking__c WHERE Id =: bookingId WITH SECURITY_ENFORCED LIMIT 1
        ];
        return booking;
    }

    /*
     * @purpose     : Method to get the List of Stops Record corresponding to the provided Set of Transport Ids.
     * @parameter   : transportIds - Set of Transport Ids
     * @return      : List of Stop__c - List of Stop records corresponding to the Transport ids provided.
     */
    public static List < Stop__c > getStopsList(Set < Id > transportIds) {
        List < Stop__c > stopsList = new List < Stop__c > ();
        stopsList = [SELECT Id, Contact_Name__c, Address__c, Address_2__c, City__c, Country__c, State__c, Zip_Code_Postal_Code__c, Transport__r.Origin_Destination_Code__c, Transport__c
            FROM Stop__c
            WHERE Transport__c IN: transportIds WITH SECURITY_ENFORCED
            ORDER BY StopId__c
        ];
        return stopsList;
    }

    /*
     * @purpose     : Method to get the List of Freight Detail Record and children corresponding to the provided Set of Shipment Ids.
     * @parameter   : shipmentIds - Set of Shipment Ids
     * @return      : List of FreightDetail__c - List of Freight Detail records corresponding to the Shipment ids provided.
     */
    public static List < FreightDetail__c > getFreightDetailList(Set < Id > shipmentIds) {
        List < FreightDetail__c > freightDetailList = new List < FreightDetail__c > ();
        freightDetailList = [SELECT Id, Declared_Value__c, Freight_Reference_Number__c, Shipment__c, Cargo_Type__c,
            Freight_Quantity__c, Freights_Unit_Of_Measure__c,
            Width_Major__c, Length_Major__c, Height_Major__c, Linear_Unit_Of_Measure__c,
            Width_Minor__c, Length_Minor__c, Height_Minor__c,
            (SELECT Id, Name, Weights_Unit_of_Measure__c, Weight__c, Freight__c, Quantity__c, Length__c, High_Cube__c,
                Tarp__c, Special_Equipment_Code__c, Running_Reefer__c, Temperature__c, Category__c, Temperatures_Unit_of_Measure__c,
                Freight__r.Freight_Quantity__c, Freight__r.Freights_Unit_Of_Measure__c, Freight__r.Cargo_Type__c,
                Freight__r.Width_Major__c, Freight__r.Length_Major__c, Freight__r.Height_Major__c, Freight__r.Linear_Unit_Of_Measure__c,
                Freight__r.Width_Minor__c, Freight__r.Length_Minor__c, Freight__r.Height_Minor__c FROM Requirements__r),
            (SELECT Id, Prefix__c, Number__c, Reported_Quantity__c, Quantity_Unit_of_Measure__c, Weight_value__c, Weight_Unit_of_Measure__c, Primary_Class__c,
                Secondary_Class__c, Package_Group__c, Freight__c, Dot_Name__c, Quantity_value__c, Sub_Risk_Class__c, Commodity__c FROM Commodities__r)
            FROM FreightDetail__c WHERE Shipment__c =: shipmentIds WITH SECURITY_ENFORCED
        ];
        return freightDetailList;
    }

    /*
     * @purpose     : Method to get the List of Voyage Record and children corresponding to the provided Set of Shipment Ids.
     * @parameter   : shipmentIds - Set of Shipment Ids
     * @return      : List of Voyage__c - List of Voyage records corresponding to the Shipment ids provided.
     */
    public static List < Voyage__c > getVoyageList(Set < Id > shipmentIds) {
        List < Voyage__c > voyageList = new List < Voyage__c > ();
        voyageList = [SELECT Id, Voyage_Number__c, Vessel_Name__c, Estimate_Sail_Date__c, Estimate_Arrival_Date__c, Loading_Port__c, Loading_Country__c,
            Discharge_Port__c, Discharge_Country__c, Shipment__c
            FROM Voyage__c WHERE Shipment__c =: shipmentIds WITH SECURITY_ENFORCED
        ];
        return voyageList;
    }

    /*
     * @purpose     : Method to get the List of Equipment Records corresponding to the provided Set of Requirement Ids.
     * @parameter   : requirementIds - Set of Requirement Ids
     * @return      : List of Equipment__c - List of Equipment records corresponding to the Requirement ids provided.
     */
    public static List < Equipment__c > getEquipmentsList(Set < Id > requirementIds) {

        List < Equipment__c > equipmentsList = new List < Equipment__c > ();
        equipmentsList = [SELECT Id, Name, Requirement__c, Requirement__r.Quantity__c, Requirement__r.Name,
            Requirement__r.Category__c, Requirement__r.Length__c, Requirement__r.High_Cube__c,
            Requirement__r.Tarp__c, Requirement__r.Special_Equipment_Code__c, Requirement__r.Running_Reefer__c,
            Requirement__r.Temperatures_Unit_of_Measure__c, Requirement__r.Temperature__c,
            Requirement__r.Freight__r.Freight_Quantity__c, Requirement__r.Freight__r.Cargo_Type__c,
            Requirement__r.Freight__r.Length_Major__c, Requirement__r.Freight__r.Width_Major__c,
            Requirement__r.Freight__r.Length_Minor__c, Requirement__r.Freight__r.Width_Minor__c,
            Requirement__r.Freight__r.Height_Minor__c, Requirement__r.Freight__r.Height_Major__c,
            Requirement__r.Freight__r.Linear_Unit_Of_Measure__c, Requirement__r.Freight__c,
            Requirement__r.Freight__r.Freights_Unit_Of_Measure__c, Prefix__c
            FROM Equipment__c
            WHERE Requirement__c IN: requirementIds WITH SECURITY_ENFORCED
        ];
        return equipmentsList;
    }

    /*
     * @purpose     : Method to get the List of Ports Information Record corresponding to the provided Port terminal location.
     * @parameter   : location - Port Terminal
     * @return      : List of Ports_Information__c - List of Port Information records corresponding to the port terminal location provided.
     */
    public static List < Ports_Information__c > getPorts(String location) {
        List < Ports_Information__c > ports = new List < Ports_Information__c > ();
        ports = [Select Id, name, Place_Name__c, Street__c, Street2__c, City__c, State__c, Country_Code__c, Zip_Code__c, Phone__c,
            PortTerminal__c from Ports_Information__c where PortTerminal__c != null AND PortTerminal__c =: location WITH SECURITY_ENFORCED LIMIT 1
        ];
        return ports;
    }
}