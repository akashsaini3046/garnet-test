public class CustomerCommunity_MatchingPortsChatbot {
    @InvocableMethod(label='Get Matching Ports')
    public static List<List<String>> getMatchingPorts(List<String> listPorts) {
        List<String> listMatchingPorts = new List<String>();
        List<List<String>> listMatchingResponse = new List<List<String>>();
        List<portDistances> listPortDistances = new List<portDistances>();
        try{
            Schema.DescribeFieldResult portFieldResult = Find_Route__c.Origin_Port__c.getDescribe();
            List<Schema.PicklistEntry> portList = portFieldResult.getPicklistValues();
            for (Schema.PicklistEntry port: portList) {
                if(port.getLabel() != CustomerCommunity_Constants.PICKLISTVAL_SELECT){
                    portDistances currentPortDistance = new portDistances();
                    currentPortDistance.portName = port.getLabel().substring(4);
                    currentPortDistance.portLevenshteinDistance = listPorts[0].toUpperCase().getLevenshteinDistance(port.getLabel().substring(4).toUpperCase());
                    listPortDistances.add(currentPortDistance);
                }
            }
            if(!listPortDistances.isEmpty()){
                listPortDistances.sort();
                if(listPortDistances[0] != Null && listPortDistances[0].portName != CustomerCommunity_Constants.EMPTY_STRING)
                    listMatchingPorts.add(listPortDistances[0].portName);
                if(listPortDistances[1] != Null && listPortDistances[1].portName != CustomerCommunity_Constants.EMPTY_STRING)
                    listMatchingPorts.add(listPortDistances[1].portName);
                if(listPortDistances[2] != Null && listPortDistances[2].portName != CustomerCommunity_Constants.EMPTY_STRING)
                    listMatchingPorts.add(listPortDistances[2].portName);
                listMatchingResponse.add(listMatchingPorts);
            }
            return listMatchingResponse;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return listMatchingResponse;
        }
    }
    public class portDistances implements Comparable {
        public String portName;
        public Integer portLevenshteinDistance;
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(portLevenshteinDistance - ((portDistances)objToCompare).portLevenshteinDistance);
        }
    }
}