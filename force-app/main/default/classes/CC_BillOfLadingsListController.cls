/*
* @company     : Nagarro Inc.
* @date        : 10-08-2020
* @author      : Nagarro
* @description : DAO for Bill Of Lading. and Related Objects
* @history     : Version 1.0
* @test class  : CC_BillOfLadingsListCtrlTest
*/
public with sharing class CC_BillOfLadingsListController {
    
    /*
    * @purpose     : Method to get Bill Of Lading Records.
    * @parameter   : intLimit - Number of records to be fetched
    * @parameter   : intOffset - Starting position of the records to be fetched
    * @parameter   : fieldName - Fields name for sorting basis
    * @parameter   : order - Sorting Direction
    * @parameter   : status - Bill Of Lading Status for query condition
    * @return      : List<Bill_Of_Lading__c> - List of BillsOfLading records
	*/
    @AuraEnabled
    public static List<Bill_Of_Lading__c> fetchBillOfLadingData(Integer recordLimit, Integer recordOffset, String fieldName, String order){
        Integer intLimit = Integer.valueof(recordLimit);
        Integer intOffset = Integer.valueof(recordOffset);
        System.debug('intLimit '+intLimit +' intOffset '+intOffset);
        String status='RR';
        List<Bill_Of_Lading__c> billsOfLading = BillOfLadingDAO.getBolRecords(intLimit,intOffset,fieldName,order,status);
        return billsOfLading;
    }
    
    /*
    * @purpose     : Method to get total number of Bill Of Lading Records with status 'RR'.
    * @return      : Integer - Total number of records.
    */
    @AuraEnabled
    public static Integer getTotalNumberOfBillOfLadings(){
        Integer totalRecords = BillOfLadingDAO.getTotalBillOfLadingsRecords();
        return totalRecords;
    }
    
    /*
    * @purpose     : Method to get urlPathPrefix for the currently logged user in community.
    * @return      : String - CommunityUrlPathPrefix.
    */
    @AuraEnabled
    public static String getCommunityUrlPathPrefix(){
        return CommonUtility.getCommunityUrlPathPrefix();
    }
    
}