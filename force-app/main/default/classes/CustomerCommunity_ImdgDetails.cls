public class CustomerCommunity_ImdgDetails {
    @AuraEnabled public String containerNumber{get; set;}
    @AuraEnabled public String unCode{get; set;}
    @AuraEnabled public String imdgClass{get; set;}
    @AuraEnabled public String imdgClassTechDescription{get; set;}
    @AuraEnabled public String quantity{get; set;}
}