public class CustomerCommunity_InfoPanelController {
    @AuraEnabled
    public static Contact getUserData() {
        Id userId = UserInfo.getUserId();
        User userDetail = new User();
        Contact conDetail = new Contact();
        Address__c addDetail = new Address__c();
       	
        try{
            userDetail = [select Id, contactId from User where Id = : userId];
            if(userDetail.contactId != Null) {
                conDetail = [SELECT Id, Name, Account.Name, Title, MailingCountry, Address__c, Account_Type_Level__c FROM Contact 
                             WHERE Id = :userDetail.contactId];
                if((conDetail.MailingCountry == Null || conDetail.MailingCountry == CustomerCommunity_Constants.EMPTY_STRING) && conDetail.Address__c != Null) {
                    addDetail = [SELECT Id, toLabel(Country__c) FROM Address__c WHERE Id= : conDetail.Address__c];
                    if(addDetail.Country__c != Null)
                        conDetail.MailingCountry = addDetail.Country__c;
                }
                return conDetail;
            }
            return Null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        } 
    }
}