@IsTest
public class CustomerCommunity_FindARouteCtrl_Test{
    
   
    static testMethod void getRoutesTestMethod(){
        CustomerCommunity_MockResponses.FindARouteMockResponse mock=new CustomerCommunity_MockResponses.FindARouteMockResponse();
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        
        CustomerCommunity_FindARouteController.getRoutes('DO, CAUCEDO', 'US, CHICAGO-CFS', 'FCL', '1 Week'); 
        
        Test.StopTest(); 
    }
    
    static testMethod void getOriginPortsTestMethod(){
        
        Test.startTest();
        List<String> ports= new list<string>();
        ports = CustomerCommunity_FindARouteController.getOriginPorts();
        Test.StopTest(); 
        
        System.assertEquals(ports.get(0),'--Select--');
    }
    
    static testMethod void getDestinationPortsTestMethod(){
        Test.startTest();
        List<String> ports= new list<string>();
        ports=CustomerCommunity_FindARouteController.getDestinationPorts();
        Test.StopTest(); 
        
        System.assertEquals(ports.get(0),'--Select--');
    }
    
    static testMethod void getSailingWeeksTestMethod(){
        Test.startTest();
        List<Sailing_Weeks__mdt> sw = new List<Sailing_Weeks__mdt>();
        sw=CustomerCommunity_FindARouteController.getSailingWeeks();
        Test.StopTest(); 
        
        System.assertNotEquals(0, sw.size());
    }
    
    static testMethod void routeInformationTestMethod(){
        Test.startTest();
        TestDataUtility.enterData_CustomerCommunity_RouteInformation();
        Test.StopTest(); 
    }
    
    static testMethod void getFCLPortsTestMethod(){
        Test.startTest();
        List<FCL_Ports__mdt> FCLOptions = new List<FCL_Ports__mdt>();
        FCLOptions=CustomerCommunity_FindARouteController.getFCLPorts();
        Test.stopTest();
        
        System.assertNotEquals(0, FCLOptions.size());
    }
    
    static testMethod void getLCLPortsTestMethod(){
        Test.startTest();
        List<LCL_Ports__mdt> LCLOptions = new List<LCL_Ports__mdt>();
        LCLOptions=CustomerCommunity_FindARouteController.getLCLPorts();
        Test.stopTest();     
        
        System.assertNotEquals(0,LCLOptions.size());
    }
    
}