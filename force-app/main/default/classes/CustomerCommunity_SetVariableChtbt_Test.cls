@isTest
public class CustomerCommunity_SetVariableChtbt_Test {
 
    static testmethod void setVariable_test(){
        
        CustomerCommunity_SetVariableChatbot.InputVariable testinput1 = new CustomerCommunity_SetVariableChatbot.InputVariable();
        CustomerCommunity_SetVariableChatbot.InputVariable testinput2 = new CustomerCommunity_SetVariableChatbot.InputVariable();
        testinput1.sinputVariable = 'ABCD';
        testinput2.sinputVariable = null;
        List<CustomerCommunity_SetVariableChatbot.InputVariable> list_testinput1 = new List<CustomerCommunity_SetVariableChatbot.InputVariable>();
  
        list_testinput1.add(testinput1);
        list_testinput1.add(testinput2);
        
        Test.startTest();
        
        CustomerCommunity_SetVariableChatbot.setVariable(list_testinput1);
 
        Test.stopTest();
    }
    
    static testmethod void setVariable_catchtest(){
        
         List<CustomerCommunity_SetVariableChatbot.InputVariable> list_testinput2 = new List<CustomerCommunity_SetVariableChatbot.InputVariable>();
        
          CustomerCommunity_SetVariableChatbot.setVariable(list_testinput2);
        
    }
}