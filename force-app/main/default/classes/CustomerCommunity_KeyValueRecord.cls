public class CustomerCommunity_KeyValueRecord {
    @AuraEnabled public String key{get; set;}
    @AuraEnabled public String value{get; set;}
}