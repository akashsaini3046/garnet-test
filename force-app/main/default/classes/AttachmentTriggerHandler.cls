/*
* Name: AttachmentTriggerHandler
* Purpose: Handler class for Attachment trigger
* Author: Nagarro
* Created Date: 05-July-2019
* Modification History
*  Modification #   Story/Defect#      Modified By     Date   Description
*/
public with sharing class AttachmentTriggerHandler extends TriggerHandler{
    
    public static Boolean isRecursion = FALSE;
    List<Attachment> newAttachmentList;
    
    //Constructor
    public AttachmentTriggerHandler(){
        this.newAttachmentList = (List<Attachment>) Trigger.new;
    }
    
    //Override the after insert
    public override void afterInsert(){
       /*validateAttachmentParentAndParse(newAttachmentList, 1);
        List<Attachment> listAttachment = new List<Attachment>();
        for(Attachment objattachment:newAttachmentList){
           System.debug('attach-->'+objattachment+'id--->'+objattachment.Id);
            listAttachment.add(objattachment);   
       }
        System.debug('listAttachment---->'+listAttachment);
        
        CustomerCommunity_ShipmentController.updateHeaderStatus(listAttachment);*/
    }
    
    /*
    * Method Name: validateAttachmentParentAndParse
    * Input Parameters: 
    * List<Attachment> lstAttachments: This holds attachments list.
    * Integer intNumberOfRecordToProcess: This holds how many attachment to process from the list.
    * Return value: 
    * Purpose: This method checks if OBL object is attachment's parent, if yes then send attachment body for parsing.
    */
    private void validateAttachmentParentAndParse(List<Attachment> lstAttachments, Integer intNumberOfRecordToProcess) {
        Attachment objAttachmentToProcess;
        String strAttachmentBody;
        if(intNumberOfRecordToProcess == 1 && lstAttachments != null && lstAttachments.size() > 0) {
            objAttachmentToProcess = lstAttachments[0];
            if(objAttachmentToProcess.ParentId != null && objAttachmentToProcess.ParentId.getSObjectType().getDescribe().getName() == Label.BOL_ATTACHMENT_PARENT_OBJ_API) {
                strAttachmentBody =  objAttachmentToProcess.Body.toString();
                if(String.isNotBlank(strAttachmentBody)) {
                    new XMLParser().loadXml(strAttachmentBody, objAttachmentToProcess.ParentId, Label.BOL_ATTACHMENT_PARENT_OBJ_API);    
                }
            }
        }
    }
}