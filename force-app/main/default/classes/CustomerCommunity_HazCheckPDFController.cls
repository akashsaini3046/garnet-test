public class CustomerCommunity_HazCheckPDFController {
    
    public CustomerCommunity_BookingValidationRes bookingResponse {get;set;} 
    
    public void attachPDF(Id IdBooking){
       
       
        ContentVersion objContentVersion = new ContentVersion();
        ContentDocumentLink objContentDocumentLink = new ContentDocumentLink(); 
        PageReference objpageReference = new PageReference('/apex/CustomerCommunity_HazCheckResponsePDF');  
        try{
            objContentVersion.PathOnClient = 'file.pdf';
            objContentVersion.Title = 'Title for this contentVersion';
            objContentVersion.VersionData = objpageReference.getContentAsPDF();
            objContentVersion.Origin = 'H';
            insert objContentVersion;      
            objContentDocumentLink.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = : objContentVersion.Id].ContentDocumentId;
            if(objContentDocumentLink.ContentDocumentId != NULL){
                objContentDocumentLink.LinkedEntityId = IdBooking;
                objContentDocumentLink.ShareType = 'I';
                insert objContentDocumentLink;
            }
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
        }
    }
}