public class HazcheckResponsePDFController {
    public Transient  CustomerCommunity_BookingValidtnResPDF bookingResponse{get;set;}
    public Transient Map<String, Set<String>> mapCTUtoPackageMap{get;set;}
    public Transient Map<String, String> mapCTUPackagetoItemMap{get;set;}
    public Transient Integer intNumberOfCTU{get;set;}
    Attachment objAttachment;
    public Transient Map<String, List<String>> mapItemClash{get;set;}
    public Transient Map<String, List<String>> mapPackageClash{get;set;}
    public  Transient String strItemClashIn{get;set;}
    public  Transient String strPackageClashIn{get;set;}
    public Transient Map<String, List<String>> mapCTUDGBookingItemStatusImage{get;set;}
    public Transient String strContainLabelImage{get;set;}
    public Transient String strCTUClassString{get;set;}
    public Transient Map<String, List<String>> mapCTUWiseItemStatus{get;set;}
    public Transient String strItemStatusKey{get;set;}
    public Transient Map<String, List<String>> mapCTUWisePackageStatus{get;set;}
    public Transient String strPackageStatusKey{get;set;}
    public Transient Map<String, String> mapCTUWiseRequest{get;set;}
    public Transient String strCTUWiseRequestKey{get;set;}
    
    public HazcheckResponsePDFController() {
        mapItemClash = new Map<String, List<String>>();
        mapPackageClash = new Map<String, List<String>>();
        mapCTUtoPackageMap = new Map<String, Set<String>>();
        mapCTUPackagetoItemMap = new Map<String, String>();
        mapCTUDGBookingItemStatusImage = new Map<String, List<String>>();
        mapCTUWiseItemStatus = new Map<String, List<String>>();
        mapCTUWiseRequest = new Map<String, String>();
        String strHazcheckResponse = apexpages.currentpage().getparameters().get('hazardousResponse');
        String strCTUWiseRequestJson = apexpages.currentpage().getparameters().get('CTUWiseRequestJson');
        if(String.isNotBlank(strCTUWiseRequestJson)) {
            try {
                mapCTUWiseRequest = (Map<String, String>)JSON.deserialize(strCTUWiseRequestJson, Map<String, String>.class);      
            } catch(Exception objEx) {
                System.debug('Error occured in HazcheckResponseController > HazcheckResponseController ' + objEx.getMessage());
            }
        } 
        objAttachment = [SELECT Id, Body FROM Attachment WHERE Id = :strHazcheckResponse]; 
        strCTUWiseRequestKey = String.join(new List<String>(mapCTUWiseRequest.keySet()), ', ');
        
        try {
            parseUntyped();    
        } catch(Exception objEx) {
            System.debug('Error occured in HazcheckResponseController > HazcheckResponseController ' + objEx.getMessage());
        }
        
        bookingResponse = (CustomerCommunity_BookingValidtnResPDF) JSON.deserialize(objAttachment.Body.toString(), CustomerCommunity_BookingValidtnResPDF.class);
        intNumberOfCTU = 0;
        for(CustomerCommunity_BookingValidtnResPDF.DGCTUStatus objDGCTUStatus : bookingResponse.Body.ValidateBookingResponse.ValidateBookingResult.BookingStatus.CTUStatus.DGCTUStatus) {
            intNumberOfCTU ++;
            for(CustomerCommunity_BookingValidtnResPDF.DGBookingItemStatus objDGBookingItemStatus : objDGCTUStatus.ItemsStatus.DGBookingItemStatus) {
                if(String.isNotBlank(objDGBookingItemStatus.RefID) && objDGBookingItemStatus.RefID.length() >= 13) {
                    String strPackage = objDGBookingItemStatus.RefID.substring(0, 8);
                    if(mapCTUtoPackageMap.containsKey(objDGCTUStatus.RefID)) {
                        mapCTUtoPackageMap.get(objDGCTUStatus.RefID).add(strPackage);  
                    } else {
                        mapCTUtoPackageMap.put(objDGCTUStatus.RefID, new Set<String>{strPackage}); 
                    }
                    
                    String strItem = objDGBookingItemStatus.RefID.substring(8, objDGBookingItemStatus.RefID.length());
                    String strItemDetails='';
                    if(mapCTUWiseRequest.containsKey(objDGCTUStatus.RefID + strPackage + strItem + 'UNCode')) {
                        strItemDetails += 'UN - ' + mapCTUWiseRequest.get(objDGCTUStatus.RefID + strPackage + strItem + 'UNCode') + ', ';	    
                    }
                    if(mapCTUWiseRequest.containsKey(objDGCTUStatus.RefID + strPackage + strItem + 'IMDGClass')) {
                        strItemDetails += 'Class - ' + mapCTUWiseRequest.get(objDGCTUStatus.RefID + strPackage + strItem + 'IMDGClass') + ', ';	    
                    }
                    if(mapCTUWiseRequest.containsKey(objDGCTUStatus.RefID + strPackage + strItem + 'PackageGroup')) {
                        strItemDetails += 'PG - ' + mapCTUWiseRequest.get(objDGCTUStatus.RefID + strPackage + strItem + 'PackageGroup');	    
                    }
                    if(String.isNotBlank(strItemDetails)) {
                        strItemDetails = '(' + strItemDetails + ')';    
                    }
                    
                    if(mapCTUPackagetoItemMap.containsKey(objDGCTUStatus.RefID + strPackage)) {
                        mapCTUPackagetoItemMap.put(objDGCTUStatus.RefID + strPackage, mapCTUPackagetoItemMap.get(objDGCTUStatus.RefID + strPackage) + ', ' + strItem + strItemDetails); 
                    } else {
                        mapCTUPackagetoItemMap.put(objDGCTUStatus.RefID + strPackage, strItem + strItemDetails); 
                    }
                }
            }
            
        }
        
    }
    
    private void parseUntyped() {
        Map<String, Object> bookingResponseUntyped = (Map<String, Object>) JSON.deserializeUntyped(objAttachment.Body.toString());
        
        Map<String, Object> mapValidateBookingBody  = (Map<String, Object>)bookingResponseUntyped.get('Body');
        Map<String, Object> mapValidateBookingResponse = (Map<String, Object>) mapValidateBookingBody.get('ValidateBookingResponse');
        Map<String, Object> mapValidateBookingResult = (Map<String, Object>) mapValidateBookingResponse.get('ValidateBookingResult');
        Map<String, Object> mapBookingStatus = (Map<String, Object>) mapValidateBookingResult.get('BookingStatus');
        Map<String, Object> mapCTUStatus = (Map<String, Object>) mapBookingStatus.get('CTUStatus');
        List<Object> lstDGCTUStatus = (List<Object>)mapCTUStatus.get('DGCTUStatus');
        
        //Handle CTU clash
        try {
            if(mapBookingStatus != null) {
                Map<String, Object> mapClashBetweenCTUs = (Map<String, Object>) mapBookingStatus.get('ClashBetweenCTUsList');
                String strPack = '', strSubstance = '', strNote = '', strDegree = '', strCTU = '';
                if(mapClashBetweenCTUs == null) {
                    strCTUClassString = 'No Clash found';
                }
                if(mapClashBetweenCTUs != null) {
                    Map<String, Object> mapClashDetails;
                    List<Map<String, Object>> lstCTUClashes = new List<Map<String, Object>>(); 
                    try {
                        mapClashDetails = (Map<String, Object>)mapClashBetweenCTUs.get('ClashDetails');
                        lstCTUClashes.add(mapClashDetails);
                    } catch(Exception objEx) {
                        List<Object> lstClashDetails = (List<Object>)mapClashBetweenCTUs.get('ClashDetails');
                        if(lstClashDetails != null) {
                            for(Object objClashDetails : lstClashDetails) {
                                mapClashDetails = (Map<String, Object>)objClashDetails;
                                lstCTUClashes.add(mapClashDetails);
                            } 
                        }
                    }
                    if(mapClashDetails != null && lstCTUClashes.size() > 0) {
                        strCTUClassString = '';
                        for(Map<String, Object> objClashMap : lstCTUClashes) {
                            mapClashDetails =  objClashMap;   
                            strPack = ''; strSubstance = ''; strNote = ''; strDegree = ''; strCTU = '';
                            for(String strClashDetailsKey : mapClashDetails.keySet()) {
                                
                                if(strClashDetailsKey.startsWith('CTU')) {
                                    strCTU += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                                }
                                
                                if(strClashDetailsKey.startsWith('Degree')) {
                                    strDegree += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                                }
                                
                                if(strClashDetailsKey.startsWith('PACK')) {
                                    strPack += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                                }
                                
                                if(strClashDetailsKey.startsWith('Substance')) {
                                    strSubstance += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                                }
                                
                                if(strClashDetailsKey == 'Note') {
                                    try {
                                        Map<String, Object> mapClashNote = (Map<String, Object>)mapClashDetails.get(strClashDetailsKey);
                                        for(String strNoteKey : mapClashNote.keySet()) {
                                            if(strNoteKey == '@nil' && mapClashNote.get(strNoteKey).toString() == 'true') {
                                                strNote += strClashDetailsKey + ' : ' + 'None' + '<br/>';      
                                            }
                                            
                                        }                                                                              
                                    } catch(Exception objEx) {
                                        strNote += strClashDetailsKey + ' : ' + (String)mapClashDetails.get(strClashDetailsKey) + '<br/>';
                                    }
                                }
                                
                            }
                            if(String.isNotBlank(strCTU)) strCTUClassString += strCTU;
                            if(String.isNotBlank(strDegree)) strCTUClassString += strDegree;
                            if(String.isNotBlank(strNote)) strCTUClassString += strNote; 
                            if(String.isNotBlank(strPack)) strCTUClassString += strPack;
                            if(String.isNotBlank(strSubstance)) strCTUClassString += strSubstance + '<br/>';
                        }
                    }
                }
                if(String.isBlank(strCTUClassString)) {
                    strCTUClassString = 'No Clash found';    
                }
            }
        } catch(Exception objEx) {
            System.debug('Error occured in HazcheckResponsePDFController > parseUntyped ' + objEx.getMessage());
        }
        
        for(Object objDGCTUStatus : lstDGCTUStatus) {
            Map<String, Object> mapDGCTUStatus = (Map<String, Object>) objDGCTUStatus;
            Map<String, Object> mapItemsClashList = (Map<String, Object>) mapDGCTUStatus.get('ItemsClashList');	
            List<Object> lstClashDetails = new List<Object>(); 
            if(mapItemsClashList != null) {
                try {
                    lstClashDetails = (List<Object>)mapItemsClashList.get('ClashDetails'); 
                } catch(Exception objEx) {
                    lstClashDetails.add(mapItemsClashList.get('ClashDetails'));
                }
            }
            
            //Handle item clash
            try {
                if(lstClashDetails != null) {
                    for(Object objClashDetails : lstClashDetails) {
                        String strPack = '', strSubstance = '', strNote = '', strDegree = '';
                        Map<String, Object> mapClashDetails = (Map<String, Object>) objClashDetails;
                        
                        for(String strClashDetailsKey : mapClashDetails.keySet()) {
                            if(strClashDetailsKey.startsWith('Degree')) {
                                strDegree += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                            }
                            
                            if(strClashDetailsKey.startsWith('PACK')) {
                                strPack += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                            }
                            
                            if(strClashDetailsKey.startsWith('Substance')) {
                                strSubstance += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                            }
                            
                            if(strClashDetailsKey == 'Note') {
                                try {
                                    Map<String, Object> mapClashNote = (Map<String, Object>)mapClashDetails.get(strClashDetailsKey);
                                    for(String strNoteKey : mapClashNote.keySet()) {
                                        if(strNoteKey == '@nil' && mapClashNote.get(strNoteKey).toString() == 'true') {
                                            strNote += strClashDetailsKey + ' : ' + 'None' + '<br/>';      
                                        }
                                        
                                    }                                                                              
                                } catch(Exception objEx) {
                                    strNote += strClashDetailsKey + ' : ' + (String)mapClashDetails.get(strClashDetailsKey) + '<br/>';
                                }
                            }
                            
                        }	
                        
                        String strItemClassString = '';
                        if(String.isNotBlank(strDegree)) strItemClassString += strDegree;
                        if(String.isNotBlank(strNote)) strItemClassString += strNote; 
                        if(String.isNotBlank(strPack)) strItemClassString += strPack;
                        if(String.isNotBlank(strSubstance)) strItemClassString += strSubstance + '<br/>';
                        
                        if(String.isNotBlank(strItemClassString) && mapItemClash.get(mapDGCTUStatus.get('RefID').toString()) != null) {
                            mapItemClash.get(mapDGCTUStatus.get('RefID').toString()).add(strItemClassString);    
                        } else if(String.isNotBlank(strItemClassString)) {
                            strItemClashIn += mapDGCTUStatus.get('RefID').toString() + ',';
                            mapItemClash.put(mapDGCTUStatus.get('RefID').toString(), new List<String>{strItemClassString});
                        }
                    }
                    
                }
            } catch(Exception objEx) {
                System.debug('Error occured in HazcheckResponsePDFController > parseUntyped ' + objEx.getMessage());
            }
            
            //Handle Package clash
            try {
                Map<String, Object> mapPackagesClashList = (Map<String, Object>) mapDGCTUStatus.get('PackagesClashList');
                Map<String, Object> mapPackagesClashDetails;
                if(mapPackagesClashList != null) {
                    mapPackagesClashDetails = (Map<String, Object>) mapPackagesClashList.get('ClashDetails');
                }
                if(mapPackagesClashDetails != null) {
                    String strPack = '', strSubstance = '', strNote = '', strDegree = '', strCTU = '';
                    Map<String, Object> mapClashDetails = (Map<String, Object>) mapPackagesClashDetails;
                    
                    for(String strClashDetailsKey : mapClashDetails.keySet()) {
                        if(strClashDetailsKey.startsWith('Degree')) {
                            strDegree += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                        }
                        
                        if(strClashDetailsKey.startsWith('PACK')) {
                            strPack += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                        }
                        
                        if(strClashDetailsKey.startsWith('Substance')) {
                            strSubstance += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                        }
                        
                        if(strClashDetailsKey == 'Note') {
                            try {
                                Map<String, Object> mapClashNote = (Map<String, Object>)mapClashDetails.get(strClashDetailsKey);
                                for(String strNoteKey : mapClashNote.keySet()) {
                                    if(strNoteKey == '@nil' && mapClashNote.get(strNoteKey).toString() == 'true') {
                                        strNote += strClashDetailsKey + ' : ' + 'None' + '<br/>';      
                                    }
                                    
                                }                                                                              
                            } catch(Exception objEx) {
                                strNote += strClashDetailsKey + ' : ' + (String)mapClashDetails.get(strClashDetailsKey) + '<br/>';
                            }
                        }
                        
                        if(strClashDetailsKey.startsWith('CTU')) {
                            //strCTU += strClashDetailsKey + ' : ' + mapClashDetails.get(strClashDetailsKey).toString() + '<br/>';
                        }
                        
                    }	
                    
                    String strPackageClassString = '';
                    if(String.isNotBlank(strCTU)) strPackageClassString += strCTU;
                    if(String.isNotBlank(strDegree)) strPackageClassString += strDegree;
                    if(String.isNotBlank(strNote)) strPackageClassString += strNote; 
                    if(String.isNotBlank(strPack)) strPackageClassString += strPack;
                    if(String.isNotBlank(strSubstance)) strPackageClassString += strSubstance ;
                    
                    if(String.isNotBlank(strPackageClassString) && mapPackageClash.get(mapDGCTUStatus.get('RefID').toString()) != null) {
                        mapPackageClash.get(mapDGCTUStatus.get('RefID').toString()).add(strPackageClassString);    
                    } else if(String.isNotBlank(strPackageClassString)) {
                        strPackageClashIn += mapDGCTUStatus.get('RefID').toString() + ',';
                        mapPackageClash.put(mapDGCTUStatus.get('RefID').toString(), new List<String>{strPackageClassString});
                    }
                }
            } catch(Exception objEx) {
                System.debug('Error occured in HazcheckResponsePDFController > parseUntyped ' + objEx.getMessage());
            }	
            
            
            //Handle lable and image
            try {
                Map<String, Object> mapItemsStatus = (Map<String, Object>) mapDGCTUStatus.get('ItemsStatus');
                List<Object> lstDGBookingItemStatus;
                if(mapItemsStatus != null) {
                    lstDGBookingItemStatus = (List<Object>) mapItemsStatus.get('DGBookingItemStatus');
                }
                if(lstDGBookingItemStatus != null) {
                    for(object objDGBookingItemStatus : lstDGBookingItemStatus) {
                        Map<String, Object> mapDGBookingItemStatus = (Map<String, Object>) objDGBookingItemStatus;
                        Map<String, Object> mapLabelsAndMarks = (Map<String, Object>)mapDGBookingItemStatus.get('LabelsAndMarks');
                        try {
                            List<Object> lstPlacardsMarksLabels = (List<Object>)mapLabelsAndMarks.get('PlacardsMarksLabels');
                            if(lstPlacardsMarksLabels != null) {
                                for(object objPlacardsMarksLabels : lstPlacardsMarksLabels) {
                                    Map<String, Object> mapLabelImage = (Map<String, Object>)objPlacardsMarksLabels;
                                    if(mapLabelImage != null) {
                                        if(mapCTUDGBookingItemStatusImage.containsKey(mapDGCTUStatus.get('RefID').toString() + mapDGBookingItemStatus.get('RefID').toString()) == false) {
                                            strContainLabelImage += mapDGCTUStatus.get('RefID').toString() + mapDGBookingItemStatus.get('RefID').toString() + ',';
                                            mapCTUDGBookingItemStatusImage.put((mapDGCTUStatus.get('RefID').toString() + mapDGBookingItemStatus.get('RefID').toString()), new List<String>{mapLabelImage.get('ImageName').toString()});
                                        } else {
                                            mapCTUDGBookingItemStatusImage.get((mapDGCTUStatus.get('RefID').toString() + mapDGBookingItemStatus.get('RefID').toString())).add(mapLabelImage.get('ImageName').toString());
                                        }
                                    }
                                }
                            }
                        } catch(Exception objEx) {
                            Map<String, Object> mapPlacardsMarksLabels = (Map<String, Object>)mapLabelsAndMarks.get('PlacardsMarksLabels');
                            if(mapPlacardsMarksLabels != null) {
                                if(mapCTUDGBookingItemStatusImage.containsKey(mapDGCTUStatus.get('RefID').toString() + mapDGBookingItemStatus.get('RefID').toString()) == false) {
                                    strContainLabelImage += mapDGCTUStatus.get('RefID').toString() + mapDGBookingItemStatus.get('RefID').toString() + ',';
                                    mapCTUDGBookingItemStatusImage.put((mapDGCTUStatus.get('RefID').toString() + mapDGBookingItemStatus.get('RefID').toString()), new List<String>{mapPlacardsMarksLabels.get('ImageName').toString()});
                                } else {
                                    mapCTUDGBookingItemStatusImage.get((mapDGCTUStatus.get('RefID').toString() + mapDGBookingItemStatus.get('RefID').toString())).add(mapPlacardsMarksLabels.get('ImageName').toString());
                                }
                            }
                        }
                    }   
                }
            } catch(Exception objEx) {
                System.debug('Error occured in HazcheckResponsePDFController > parseUntyped ' + objEx.getMessage());
            }
            
            //Handle Items Status 
            try {
                Map<String, Object> mapItemsStatus1 = (Map<String, Object>) mapDGCTUStatus.get('ItemsStatus');
                List<Object> lstDGBookingItemStatus1;
                
                if(mapItemsStatus1 != null) {
                    lstDGBookingItemStatus1 = (List<Object>) mapItemsStatus1.get('DGBookingItemStatus');
                }
                
                if(lstDGBookingItemStatus1 != null) {
                    for(Object objDGBookingItemStatus : lstDGBookingItemStatus1) {
                        Map<String, Object> mapDGBookingItemStatus = (Map<String, Object>) objDGBookingItemStatus;
                        Map<String, Object> mapResults = (Map<String, Object>)mapDGBookingItemStatus.get('Results');
                        if(mapResults != null) {
                            try {
                                List<Object> lstDGResult = (List<Object>) mapResults.get('DGResult');
                                for(Object objDGResult : lstDGResult) {
                                    Map<String, Object> mapDGResult = (Map<String, Object>)objDGResult;
                                    
                                    if(String.isNotBlank(mapDGResult.get('Description').toString()) && mapCTUWiseItemStatus.containsKey(mapDGCTUStatus.get('RefID').toString())) {
                                        mapCTUWiseItemStatus.get(mapDGCTUStatus.get('RefID').toString()).add(mapDGBookingItemStatus.get('RefID').toString() + ' : ' + mapDGResult.get('Description').toString());   
                                    } else if(String.isNotBlank(mapDGResult.get('Description').toString())) {
                                        strItemStatusKey +=  mapDGCTUStatus.get('RefID').toString() + ',';
                                        mapCTUWiseItemStatus.put(mapDGCTUStatus.get('RefID').toString(), new List<String>{mapDGBookingItemStatus.get('RefID').toString() + ' : ' + mapDGResult.get('Description').toString()}); 
                                    } 
                                }
                            } catch(Exception objEx) {
                                Map<String, Object> mapDGResult = (Map<String, Object>)mapResults.get('DGResult');
                                
                                if(mapDGResult != null && String.isNotBlank(mapDGResult.get('Description').toString())) {
                                    if(mapCTUWiseItemStatus.containsKey(mapDGCTUStatus.get('RefID').toString())) {
                                        mapCTUWiseItemStatus.get(mapDGCTUStatus.get('RefID').toString()).add(mapDGBookingItemStatus.get('RefID').toString() + ' : ' + mapDGResult.get('Description').toString());   
                                    } else {
                                        strItemStatusKey +=  mapDGCTUStatus.get('RefID').toString() + ',';
                                        mapCTUWiseItemStatus.put(mapDGCTUStatus.get('RefID').toString(), new List<String>{mapDGBookingItemStatus.get('RefID').toString() + ' : ' + mapDGResult.get('Description').toString()}); 
                                    } 
                                }
                            }
                        }
                    }
                }
            } catch(Exception objEx) {
                System.debug('Error occured in HazcheckResponsePDFController > parseUntyped ' + objEx.getMessage());
            }
            
            
            //Handle Package Status 
            try {
                Map<String, Object> mapPackageStatus = (Map<String, Object>) mapDGCTUStatus.get('PackagesStatus');
                List<Object> lstDGBookingPackageStatus;
                
                if(mapPackageStatus != null) {
                    lstDGBookingPackageStatus = (List<Object>) mapPackageStatus.get('DGBookingPackageStatus');
                }
                
                if(lstDGBookingPackageStatus != null) {
                    for(Object objDGBookingPackageStatus : lstDGBookingPackageStatus) {
                        Map<String, Object> mapDGBookingPackageStatus = (Map<String, Object>) objDGBookingPackageStatus;
                        Map<String, Object> mapResults = (Map<String, Object>)mapDGBookingPackageStatus.get('Results');
                        if(mapResults != null) {
                            try {
                                List<Object> lstDGResult = (List<Object>) mapResults.get('DGResult');
                                for(Object objDGResult : lstDGResult) {
                                    Map<String, Object> mapDGResult = (Map<String, Object>)objDGResult;
                                    
                                    if(String.isNotBlank(mapDGResult.get('Description').toString()) && mapCTUWisePackageStatus.containsKey(mapDGCTUStatus.get('RefID').toString())) {
                                        mapCTUWisePackageStatus.get(mapDGCTUStatus.get('RefID').toString()).add(mapDGResult.get('Description').toString());   
                                    } else if(String.isNotBlank(mapDGResult.get('Description').toString())) {
                                        strPackageStatusKey +=  mapDGCTUStatus.get('RefID').toString() + ',';
                                        mapCTUWisePackageStatus.put(mapDGCTUStatus.get('RefID').toString(), new List<String>{mapDGResult.get('Description').toString()}); 
                                    } 
                                }
                            } catch(Exception objEx) {
                                Map<String, Object> mapDGResult = (Map<String, Object>)mapResults.get('DGResult');
                                
                                if(mapDGResult != null && String.isNotBlank(mapDGResult.get('Description').toString())) {
                                    if(mapCTUWisePackageStatus.containsKey(mapDGCTUStatus.get('RefID').toString())) {
                                        mapCTUWisePackageStatus.get(mapDGCTUStatus.get('RefID').toString()).add(mapDGResult.get('Description').toString());   
                                    } else {
                                        strPackageStatusKey +=  mapDGCTUStatus.get('RefID').toString() + ',';
                                        mapCTUWisePackageStatus.put(mapDGCTUStatus.get('RefID').toString(), new List<String>{mapDGResult.get('Description').toString()}); 
                                    } 
                                }
                            }
                        }
                    }
                }
            } catch(Exception objEx) {
                System.debug('Error occured in HazcheckResponsePDFController > parseUntyped ' + objEx.getMessage());
            }
        }
    }
}