public with sharing class OpportunityTriggerHandler extends TriggerHandler{
    List<Opportunity> newOpportunityList;
    List<Opportunity> oldOpportunityList;
    public static Boolean isRecursion = FALSE;
    Map<Id, Opportunity> oldOpportunityMap = new Map<Id, Opportunity>();
    Map<Id, Opportunity> newOpportunityMap = new Map<Id, Opportunity>();
    
    //Constructor
    public OpportunityTriggerHandler(){
        this.newOpportunityList = (List<Opportunity>) Trigger.new;
        this.oldOpportunityList = (List<Opportunity>) Trigger.old;
        this.oldOpportunityMap  = (Map<Id, Opportunity>) Trigger.oldMap;
        this.newOpportunityMap  = (Map<Id, Opportunity>) Trigger.newMap;
    }
    //Override the before insert method
    public override void beforeInsert(){
      //  updateOppNameOnLeadConversion(newOpportunityList);
       destinatonPortValidation(newOpportunityList);
        setOppOwnerAsAccOwner(newOpportunityList);
        
    }
    
    //Override the before update method
    public override void beforeUpdate(){
       destinatonPortValidation(newOpportunityList);
        opportunityCannotBeClosed(newOpportunityList);
    }
    
    //Override the before delete method
    public override void beforeDelete(){
        preventOppFromDeletion(oldOpportunityList);
        sendEmailToAccountTeamMembers(newOpportunityMap, oldOpportunityMap);
    }
    
    
    public override void afterInsert(){
        createTask(newOpportunityList);
        createTaskForHotStage(newOpportunityList, oldOpportunityMap);
        if(isRecursion == FALSE){
            createOppLineItems(newOpportunityMap, oldOpportunityMap);
            sendEmailToAccountTeamMembers(newOpportunityMap, oldOpportunityMap);
        }
        uniqueServicesOnAccount(newOpportunityList);
        CreateOpportunityContactRole(newOpportunityList);
        
    }
    
    public override void afterUpdate(){
        if(isRecursion == FALSE){
            createOppLineItems(newOpportunityMap, oldOpportunityMap);
            sendEmailToAccountTeamMembers(newOpportunityMap, oldOpportunityMap);
        }
        createContractRecord(newOpportunityMap, oldOpportunityMap);
        uniqueServicesOnAccount(newOpportunityList);
        sendEmailtoPricingTeam(newOpportunityList, oldOpportunityMap);
        createTaskForHotStage(newOpportunityList, oldOpportunityMap);
        
    }
    
    
     private static void createTask(List<Opportunity> opportunityList)
    {
        List<Task> tasksToCreateList = new List<Task>();
        if(opportunityList != NULL && !opportunityList.isEmpty())
        {
            for(Opportunity opp: opportunityList)
            {
                if(opp.StageName == ConstantClass.PROSPECTING_OPP_STAGE && opp.Rating__c != ConstantClass.OPPORTUNITY_RATING_HOT && opp.Create_Follow_Up_Task__c)
                {
                    Task t = new Task();
                    t.Subject = 'Follow up on Opportunity';
                    t.Priority = 'Normal';
                    t.Status = 'Open';
                    t.TaskSubtype = 'Task';
                    t.WhatId = opp.Id;
                    t.ActivityDate = Date.today() +2;
                    t.OwnerId = opp.OwnerId;
                    t.Description = 'Automated task to follow up on new prospecting opportunity.';
                    tasksToCreateList.add(t);
                } 
            }
        }
        if(tasksToCreateList != NULL && !tasksToCreateList.isEmpty()){
            insert tasksToCreateList;
        }
    }
    
    
    private static void createTaskForHotStage(List<Opportunity> opportunityList, Map<Id, Opportunity> mapOldOpportunity)
    {
        List<Task> tasksToCreateList = new List<Task>();
        if(opportunityList != NULL && !opportunityList.isEmpty())
        {
            for(Opportunity opp: opportunityList)
            {
                if(opp.Rating__c == ConstantClass.OPPORTUNITY_RATING_HOT && (mapOldOpportunity == null || (mapOldOpportunity != null && mapOldOpportunity.get(opp.Id).Rating__c != opp.Rating__c)) && opp.Create_Follow_Up_Task__c )
                {
                    Task t = new Task();
                    t.Subject = 'Follow up on Opportunity';
                    t.Priority = 'Normal';
                    t.Status = 'Open';
                    t.TaskSubtype = 'Task';
                    t.WhatId = opp.Id;
                    t.ActivityDate = Date.today();
                    t.OwnerId = opp.OwnerId;
                    t.Description = 'Automated task to follow up on new prospecting opportunity.';
                    tasksToCreateList.add(t);
                } 
            }
        }
        if(tasksToCreateList != NULL && !tasksToCreateList.isEmpty()){
            insert tasksToCreateList;
        }
    }
    
    //Prevent opportunity from deletion if 
    // -> opportunity stage is Closed Won
    // -> user profile is not Sales Manager or Sales Leader
    
    private static void preventOppFromDeletion(List<Opportunity> opportunityList){
        
        Id profileId=UserInfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        
        Map<String, ProfileNames__c> profileNames = ProfileNames__c.getall();
        
        if(opportunityList !=NULL && !opportunityList.isEmpty())
        {
            for(Opportunity opp: opportunityList)
            {
                if(!Label.SYSTEM_ADMINISTRATOR.contains(profileName ))
                {
                    if(opp.IsClosed )
                        opp.addError('You cannot delete a closed opportunity.');
                    else
                    {
                        if(!profileNames.containsKey(profileName))
                            //           if(profileName != ConstantClass.PROFILE_SALES_MANAGER && profileName != ConstantClass.PROFILE_SALES_LEADER)
                            opp.addError('Deletion of opportunity is not allowed, please contact your manager.');
                    }
                }
            }
        }
    }
    
    //Assign Opportunity owner as Account owner on which opportunity is created.
    private static void setOppOwnerAsAccOwner(List<Opportunity> opportunityList)
    {
        Set<Id> accountIdSet = new Set<Id>();
        if(opportunityList != NULL && !opportunityList.isEmpty())
        {
            for(Opportunity opp: opportunityList)
            {
                if(opp.AccountId !=NULL)
                    accountIdSet.add(opp.AccountId);
            }
        }
        
        List<Account> accList = new List<Account>();
        if(accountIdSet != NULL && !accountIdSet.isEmpty())
            accList = [Select Id, OwnerId from Account where Id in: accountIdSet];
        
        Map<Id, String> map_AccId_AccOwner = new Map<Id, String>();
        if(accList != NULL && !accList.isEmpty())
        {
            for(Account acc: accList)
            {
                map_AccId_AccOwner.put(acc.Id, acc.OwnerId);
            }
        }
        
        if(opportunityList != NULL && !opportunityList.isEmpty())
        {
            for(Opportunity opp: opportunityList)
            {
                if(map_AccId_AccOwner.containsKey(opp.AccountId))
                {
                    String accOwner=map_AccId_AccOwner.get(opp.AccountId);
                    opp.OwnerId = accOwner;
                }
            }
        }
    }
    
    
    //Create new opportunity line items from values as selected in ‘Service Type’ field on opportunity and 
    //Delete existing opportunity line items that are not selected in 'Service Type' field after update
    private static void createOppLineItems(Map<Id, Opportunity> newOppMap, Map<Id, Opportunity> oldOppMap)
    {
        isRecursion = TRUE;
        //Create map of old opportunity id and services
        Set<String> allNewOldServices = new Set<String>();
        Map<Id, Set<String>> oldmap_OppId_servicesSet = new Map<Id, Set<String>>();
        if(oldOppMap != NULL && !oldOppMap.isEmpty())
        {
            for(Opportunity oldOpp: oldOppMap.values())
            {
                if(newOppMap.get(oldOpp.Id).Service_Type__c != oldOppMap.get(oldOpp.Id).Service_Type__c)
                {
                    if(oldOpp.Service_Type__c!=null)
                    {
                        allNewOldServices.addAll(oldOpp.Service_Type__c.split(';'));
                        Set<String> oldServicesSet = new Set<String>();
                        oldServicesSet.addAll(oldOpp.Service_Type__c.split(';'));
                        oldmap_OppId_servicesSet.put(oldOpp.Id, oldServicesSet);
                    }
                }
            }
        }
        
        //Create map of new opportunity id and services
        Map<Id, Set<String>> newmap_OppId_servicesSet = new Map<Id, Set<String>>();
        if(newOppMap.values() != NULL && !newOppMap.values().isEmpty())
        {
            for(Opportunity newOpp: newOppMap.values())
            {
                if(newOpp.Service_Type__c!=null)
                {
                    allNewOldServices.addAll(newOpp.Service_Type__c.split(';'));
                    Set<String> newServicesSet = new Set<String>();
                    newServicesSet.addAll(newOpp.Service_Type__c.split(';'));
                    newmap_OppId_servicesSet.put(newOpp.Id, newServicesSet);
                }
            }
        }
        System.debug('newmap_OppId_servicesSet@@ '+newmap_OppId_servicesSet);
        //Fetching only those services that needs to be created
        Map<Id, Set<String>> map_oppId_ServicesToCreate = new Map<Id, Set<String>>(); 
        if(newOppMap.values() != NULL && !newOppMap.values().isEmpty())
        {
            for(Opportunity opp: newOppMap.values())
            {
                Set<String> newServiceSet = newmap_OppId_servicesSet.get(opp.Id);
                Set<String> oldServiceSet = oldmap_OppId_servicesSet.get(opp.Id);
                
                // In case of UPDATE
                if(oldServiceSet != NULL && !oldServiceSet.isEmpty())
                {
                    if(newServiceSet != NULL && !newServiceSet.isEmpty())
                    {
                        for(String newService: newServiceSet)
                        {
                            if(!oldServiceSet.contains(newService))
                            {
                                Set<String> servicesToCreateSet = map_oppId_ServicesToCreate.get(opp.Id);
                                if(servicesToCreateSet == NULL)
                                    servicesToCreateSet = new Set<String>();
                                servicesToCreateSet.add(newService);
                                map_oppId_ServicesToCreate.put(opp.Id, servicesToCreateSet);  
                            }
                        }
                    }
                }
                
                // In case of INSERT
                if(oldServiceSet == NULL || oldServiceSet.isEmpty())
                {
                    if(newServiceSet != NULL && !newServiceSet.isEmpty())
                    {
                        for(String newService: newServiceSet)
                        {
                            Set<String> servicesToCreateSet = map_oppId_ServicesToCreate.get(opp.Id);
                            if(servicesToCreateSet == NULL)
                                servicesToCreateSet = new Set<String>();
                            servicesToCreateSet.add(newService);
                            map_oppId_ServicesToCreate.put(opp.Id, servicesToCreateSet);  
                        }
                    }
                }
            }
        }
        
        //Opportunity Line Item will be fetched in case of Opportunity Update only
        List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
        if(oldOppMap != NULL)
        {
            if(oldOppMap.keySet() != NULL && newOppMap.keySet() != NULL)
            {
                oppLineItemList = [Select Id, Name, Product2.Name,OpportunityId from OpportunityLineItem where OpportunityId in: newOppMap.keySet() OR opportunityId in: oldOppMap.keySet()];
            }
        }
        
        //Fetch the PriceBookEntry records corresponding to the services selected 
        List<PriceBookEntry> priceBookList = new List<PriceBookEntry>();
        if(allNewOldServices != NULL && !allNewOldServices.isEmpty())
        {
            priceBookList = [SELECT Id, Product2Id, Product2.Id, Product2.Name FROM PriceBookEntry WHERE Product2.Name in: allNewOldServices];
        }
        
        //Created map of product name and the PriceBookEntry id. 
        //Used to fetch the PriceBookEntry Id corresponding to the service while creating Opportunity Line Items.
        //Useful in case where user inserts or updates services with wrong spelling
        Map<String, Id> map_productName_productId = new Map<String, Id>();
        if(priceBookList != NULL && !priceBookList.isEmpty())
        {
            for(PriceBookEntry p: priceBookList)
            {
                map_productName_productId.put(p.Product2.Name, p.Id);
            }
        }
        
        //Created map of existing opportunity line items' product and opportunity line item.
        //Used to check whether the service selected already has an opportunity line item record associated with the opportunity.
        Map<String, OpportunityLineItem> map_prodName_oppLineItemId = new Map<String, OpportunityLineItem>();
        if(oppLineItemList!=NULL && !oppLineItemList.isEmpty())
        {
            for(OpportunityLineItem oppLineItem: oppLineItemList)
            {
                map_prodName_oppLineItemId.put(oppLineItem.OpportunityId + oppLineItem.Product2.Name,oppLineItem);
            }
        }
        
        //Fetch opportunity line items that needs to be deleted
        Map<Id, List<OpportunityLineItem>> map_oppId_ServicesToDelete = new Map<Id, List<OpportunityLineItem>>();
        if(newOppMap.values() != NULL && !newOppMap.values().isEmpty())
        {
            for(Opportunity opp: newOppMap.values())
            {
                Set<String> newServiceSet = newmap_OppId_servicesSet.get(opp.Id);
                Set<String> oldServiceSet = oldmap_OppId_servicesSet.get(opp.Id);
                
                if(oldServiceSet != NULL && !oldServiceSet.isEmpty())
                {
                    for(String oldService: oldServiceSet)
                    {
                        if(!newServiceSet.contains(oldService))
                        {
                            List<OpportunityLineItem> servicesToDeleteList = map_oppId_ServicesToDelete.get(opp.Id);
                            if(servicesToDeleteList == NULL)
                                servicesToDeleteList = new List<OpportunityLineItem>();
                            
                            servicesToDeleteList.add(map_prodName_oppLineItemId.get(opp.Id + oldService));
                            map_oppId_ServicesToDelete.put(opp.Id, servicesToDeleteList);
                        }
                    }
                }
            }
        }
        
        List<OpportunityLineItem> oppLineItemListToDelete = new List<OpportunityLineItem>();
        if(map_oppId_ServicesToDelete.values() != NULL && !map_oppId_ServicesToDelete.values().isEmpty())
        {
            for(List<OpportunityLineItem> oppLineItem: map_oppId_ServicesToDelete.values())
            {
                oppLineItemListToDelete.addAll(oppLineItem);
            }
        }
        
        //Fetch list of existing opportunity line items related to opportunity
        Map<Id, Set<OpportunityLineItem>> map_oppId_oppLineItemSet1 = new Map<Id, Set<OpportunityLineItem>>();
        Map<Id, Set<String>> map_oppId_oppLineItemSet = new Map<Id, Set<String>>();
        if(oppLineItemList != NULL && !oppLineItemList.isEmpty())
        {
            for(OpportunityLineItem oppLineItem: oppLineItemList)
            {
                Set<String> oppLineItemSet = map_oppId_oppLineItemSet.get(oppLineItem.OpportunityId);
                if(oppLineItemSet == NULL)
                    oppLineItemSet = new Set<String>();
                oppLineItemSet.add(oppLineItem.Product2.Name);
                map_oppId_oppLineItemSet.put(oppLineItem.OpportunityId, oppLineItemSet);
            }
        }
        System.debug('map_oppId_ServicesToCreate-- @@'+map_oppId_ServicesToCreate);
        //Create new opportunity line item
        List<OpportunityLineItem> oppLineItemToInsert = new List<OpportunityLineItem>();
        if(map_oppId_ServicesToCreate.keySet() != NULL && ! map_oppId_ServicesToCreate.keySet().isEmpty())
        {
            for(Id oppId: map_oppId_ServicesToCreate.keySet())
            {
                Set<String> servicesSet = new Set<String>();
                servicesSet = map_oppId_ServicesToCreate.get(oppId);
                Set<String> existingOppLineItemSet = map_oppId_oppLineItemSet.get(oppId);
                if(servicesSet != NULL && !servicesSet.isEmpty())
                {
                    for(String service: servicesSet)
                    {
                        // In case of UPDATE
                        if(existingOppLineItemSet != NULL && !existingOppLineItemSet.isEmpty())
                        {
                            if(!existingOppLineItemSet.contains(service))
                            {
                                if(map_productName_productId.containsKey(service))
                                {
                                    OpportunityLineItem oppLineItem= new OpportunityLineItem();
                                    oppLineItem.UnitPrice = 0;
                                    oppLineItem.Quantity = 1;
                                    oppLineItem.ServiceDate = newoppMap.get(OppId).Est_Business_Start_Date__c;
                                    oppLineItem.OpportunityId = oppId;
                                    oppLineItem.PricebookEntryId = map_productName_productId.get(service);
                                    oppLineItemToInsert.add(oppLineItem);
                                }
                            }
                        }
                        
                        // In case of INSERT
                        if(existingOppLineItemSet == NULL || existingOppLineItemSet.isEmpty())
                        {
                            if(map_productName_productId.containsKey(service))
                            {
                                OpportunityLineItem oppLineItem= new OpportunityLineItem();
                                oppLineItem.UnitPrice = 0;
                                oppLineItem.Quantity = 1;
                                oppLineItem.ServiceDate = newoppMap.get(OppId).Est_Business_Start_Date__c;
                                oppLineItem.OpportunityId = oppId;
                                oppLineItem.PricebookEntryId = map_productName_productId.get(service);
                                oppLineItemToInsert.add(oppLineItem);
                            }
                        }
                    }
                }
            }
        }
        System.debug('oppLineItemToInsert----- @@'+oppLineItemToInsert);
        if(oppLineItemToInsert != NULL && !oppLineItemToInsert.isEmpty())
        {
            try
            {
               // insert oppLineItemToInsert;
               WithoutSharingUtility.insertServiceRecords(oppLineItemToInsert);
            }
            catch(Exception e)
            {
                System.debug(logginglevel.ERROR, 'MESSAGE:'+ e.getMessage() +'STACK TRACE:'+e.getStackTraceString());
            }
        }
        
        if(oppLineItemListToDelete != NULL && !oppLineItemListToDelete.isempty())
        {
            try
            {
                delete oppLineItemListToDelete;
            }
            catch(Exception e)
            {
                System.debug(logginglevel.ERROR, 'MESSAGE:'+ e.getMessage() +'STACK TRACE:'+e.getStackTraceString());
            }
        }
    }
    
    //Count number of Unique Services associated with account
    //This field is used in Integrated Sales Report    
    private void uniqueServicesOnAccount(List<Opportunity> newOpportunityList)
    {
        Set<Id> accountIdSet = new Set<Id>();
        if(newOpportunityList != NULL && !newOpportunityList.isEmpty())
        {
            for(Opportunity opp: newOpportunityList)
            {
                if(opp.AccountId !=NULL)
                    accountIdSet.add(opp.AccountId);
            }
        }
        
        List<Account> accountList = new List<Account>();
        if(accountIdSet != NULL && !accountIdSet.isEmpty())
        {
            accountList = [Select Id, UniqueServices__c from Account where Id in: accountIdSet];
        }
        
        Map<Id, Set<String>> map_AccId_ServicesSet = new Map<Id, Set<String>>();
        if(newOpportunityList != NULL && !newOpportunityList.isEmpty())
        {
            for(Opportunity opp: newOpportunityList)
            {
                Set<String> servicesSet = map_AccId_ServicesSet.get(opp.AccountId);
                if(servicesSet == NULL)
                    servicesSet = new Set<String>();
                if(opp.Service_Type__c != NULL)
                    servicesSet.addAll(opp.Service_Type__c.split(';'));
                map_AccId_ServicesSet.put(opp.AccountId, servicesSet);  
            }
        }
        
        List<Account> accListToUpdate = new List<Account>();
        if(accountList != NULL && !accountList.isEmpty())
        {
            for(Account acc: accountList)
            {
                integer uniqueServicesCount = 0;
                if(map_AccId_ServicesSet.containsKey(acc.Id))
                {
                    uniqueServicesCount = map_AccId_ServicesSet.get(acc.Id).size();
                }
                acc.UniqueServices__c = uniqueServicesCount;
                accListToUpdate.add(acc);
            }
        }
        
        if(accListToUpdate != NULL && !accListToUpdate.isEmpty())
            update accListToUpdate;
    }
    
    private void destinatonPortValidation(List<Opportunity> newOpportunityList){
        String accId = '';
        String conId = '';
        for(Opportunity opp: newOpportunityList){
            
            if(opp.Opportunity_Created_From_Lead__c){
                ConstantClass.OPPLEADCONVERSION = true;
                opp.Opportunity_Created_From_Lead__c = false;
                accId = opp.AccountId;
                break;
            }       
        }
        if(accId!=''){
            for(Contact c :[Select Id from Contact where Contact_Created_From_Lead__c = true AND AccountId = :accId Limit 1]){
                conId = c.Id;   
            }
        }
        
        if(null != conId && !String.IsBlank(conId)){
            for(Opportunity opp: newOpportunityList){
                opp.Contact__c = conId;
            }
        }
        
        if(!ConstantClass.OPPLEADCONVERSION){
            for(Opportunity opp: newOpportunityList){
                if(!String.IsBlank(opp.Service_Type__c) && opp.Service_Type__c.containsIgnoreCase(ConstantClass.CUSTOMCLEARANCE) && String.ISBlank(opp.Destination_port__c)){
                //    opp.addError('\"Destination Port\" is mandatory for Opportunity with customs service.');    
                }
            }   
        }
    }
    
    private void createContractRecord(Map<Id, Opportunity> newOppMap, Map<Id, Opportunity> oldOppMap){
        
        List<Contract> contractList = new List<Contract>();
        Set<Id> accIdList = new Set<Id>();
        
        Map<String, List<String>> oppToExistingContractMap = new Map<String, List<String>>();
        
        for(Contract contractObj: [Select Id, Opportunity__c from Contract where Opportunity__c IN :newOppMap.keySet()]){
            if(!oppToExistingContractMap.containsKey(contractObj.Opportunity__c)){
                oppToExistingContractMap.put(contractObj.Opportunity__c, new List<String>());
            }
            oppToExistingContractMap.get(contractObj.Opportunity__c).add(contractObj.Id);
        }
        
        
        
        Id priceBookId = [Select Id from Pricebook2 where Name = 'Standard Price Book' LIMIT 1].Id;
        
        if(newOppMap != NULL && !newOppMap.isEmpty())
        {
            for(Opportunity opp: newOppMap.values())
            {
                if(opp.AccountId != NULL)
                    accIdList.add(opp.AccountId);
            }
        }
        
        List<Account> accList = new List<Account>();
        if(accIdList != NULL && !accIdList.isEmpty())
            accList = [Select Id, BillingCity, BillingCountry, BillingPostalCode, BillingState, BillingStreet from Account where id in: accIdList];
        
        Map<Id, Account> mapAccIdAccount = new Map<Id, Account>();
        if(accList != NULL && !accList.isEmpty()){
            for(Account acc: accList)
            {
                mapAccIdAccount.put(acc.Id, acc);
            }
        }
        
        if(newOppMap != NULL && !newOppMap.isEmpty()) // Added on 17th Dec
        {
            for(Opportunity oppObj: newOppMap.values()){
                if(OldOppMap.get(oppObj.Id).StageName!= oppObj.StageName && oppObj.StageName=='Contracting' && oppToExistingContractMap.size()==0){
                    
                    Account acc;
                    if(mapAccIdAccount.containsKey(oppObj.AccountId))
                        acc = mapAccIdAccount.get(oppObj.AccountId);
                    
                    Contract contractObj = new Contract();
                    contractObj.AccountId = OppObj.AccountId;
                    contractObj.Opportunity__c = OppObj.Id;
                    contractObj.StartDate = OppObj.Est_Business_Start_Date__c;
                    contractObj.ContractTerm = 12;
                    
                    if(!String.isBlank(OppObj.Service_Type__c))
                    {
                        contractObj.Services__c = OppObj.Service_Type__c;
                    }
                    
                    if(priceBookId != NULL)
                        contractObj.Pricebook2Id = priceBookId;
                    
                    if(acc !=NULL){
                        contractObj.BillingCity = acc.BillingCity;
                        contractObj.BillingCountry = acc.BillingCountry;
                        contractObj.BillingPostalCode = acc.BillingPostalCode;
                        contractObj.BillingState = acc.BillingState;
                        contractObj.BillingStreet = acc.BillingStreet;
                    }
                    
                    contractList.add(contractObj);
                }
            }
        }
        if(!contractList.IsEmpty()){
            insert contractList;
        }
    }
    
    public void opportunityCannotBeClosed(List<Opportunity> opportunityList) {
        
        // One cannot close an opportunity until all associated contracts have been activated
        
        Set<Id> oppIdSet = new Set<Id>();
        if(opportunityList != NULL && !opportunityList.isEmpty())
        {
            for(Opportunity opp: opportunityList)
            {
                oppIdSet.add(opp.Id);
            }
        }
        
        List<Contract> contractList = new List<Contract>();
        if(oppIdSet != NULL && !oppIdSet.isEmpty())
            contractList = [Select Id, Status, Opportunity__c from Contract where Opportunity__c in: oppIdSet];
        
        // Map to count non activated contracts associated with an opportunity
        Map<Id, Integer> mapOppIdNonActivatedContractCount = new Map<Id, Integer>(); 
        if(contractList != NULL && !contractList.isEmpty())
        {
            for(Contract contractObj: contractList)
            {
                Integer nonActivatedContractCount = mapOppIdNonActivatedContractCount.get(contractObj.Opportunity__c);
                if(nonActivatedContractCount == NULL)
                    nonActivatedContractCount = 0;
                if(contractObj.Status != 'Activated')
                    nonActivatedContractCount++;
                mapOppIdNonActivatedContractCount.put(contractObj.Opportunity__c, nonActivatedContractCount);
            }
        }
        
        
        if(opportunityList !=NULL && !opportunityList.isEmpty())
        {
            for(Opportunity opp: opportunityList)
            { 
                if(mapOppIdNonActivatedContractCount.containsKey(opp.Id))
                    if(opp.IsClosed && mapOppIdNonActivatedContractCount.get(opp.Id)>0)
                    opp.addError('You cannot close an opportunity until all associated contracts have been activated.');
            }
        }
        
    }
    
    public void CreateOpportunityContactRole(List<Opportunity> listOpportunity) {
        Set<Id> contactId = new Set<Id>();
        if(listOpportunity != NULL && !listOpportunity.isEmpty()) // Added on 17th Dec
        {
            for(Opportunity opp : listOpportunity) {
                if(opp.Contact__c != Null)
                    contactId.add(opp.Contact__c);
            }
        }
        System.Debug('@@@ contactId - ' + contactId);
    }
    
    //Update Opportunity name to Accountname-ContactName-Region if opportunity name is not modified by the user and isConverted
    public void updateOppNameOnLeadConversion(List<Opportunity> newOppList) // Added on 17th Dec
    {
        if(newOppList != NULL && !newOppList.isEmpty())
        {
            for(Opportunity newOpp: newOppList)
            { 
                if(newOpp.Opportunity_Created_from_Lead__c == TRUE)
                {
                    if(newOpp.OpportunityNameonConversion__c != '' && newOpp.Name == (newOpp.AccountNameForOpportunityMapping__c+'-'))
                    {
                        newOpp.Name = newOpp.OpportunityNameonConversion__c;
                    }
                }
            }
        }
    }
    
    //Send email to Account Team Members, when a opportunity is created, updated(stage change) or deleted.
    public void sendEmailToAccountTeamMembers(Map<Id, Opportunity> newOppMap, Map<Id, Opportunity> oldOppMap) {
        
        System.debug('sendEmailToAccountTeamMembers @@@@@');
        isRecursion = TRUE;
        Set<String> accountIdSet = new Set<String>();
        Map<Id, Set<String>> oldmap_OppId_servicesSet = new Map<Id, Set<String>>();
        
        EmailTemplate emailTemplate;
        
        //In case of UPDATE
        if(oldOppMap != NULL && !oldOppMap.isEmpty())
        {
            emailTemplate=[Select id,Name from EmailTemplate where DeveloperName = 'Notification_to_all_account_team_member_on_opportunity_stage_change' limit 1];
            for(Opportunity oldOpp: oldOppMap.values())
            {
                if(newOppMap != NULL && !newOppMap.isEmpty())
                {
                    if(newOppMap.get(oldOpp.Id).StageName != oldOppMap.get(oldOpp.Id).StageName)
                    {
                        accountIdSet.add(oldOpp.AccountId);
                    }
                }
                else
                {
                    emailTemplate=[Select id,Name from EmailTemplate where DeveloperName = 'Notification_to_all_account_team_member_on_opportunity_deletion' limit 1];
                    accountIdSet.add(oldOpp.AccountId);
                }
            }
        }
        
        //In case of INSERT
        if(newOppMap != NULL && !newOppMap.isEmpty() && (oldOppMap == NULL || oldOppMap.isEmpty()))
        {
            emailTemplate=[Select id, Name from EmailTemplate where DeveloperName = 'Notification_to_all_account_team_member_on_opportunity_creation' limit 1];
            
            for(Opportunity newOpp: newOppMap.values())
            { 
                if(ConstantClass.OPPLEADCONVERSION == FALSE) // To prevent sending email when opportunity is created from lead conversion
                    accountIdSet.add(newOpp.AccountId);
            }
        }
        
        //Fetching account team members related to opportunities' accounts
        List<AccountTeamMember> accountTeamMemberList = new List<AccountTeamMember>();
        if(accountIdSet != NULL && !accountIdSet.isEmpty())
            accountTeamMemberList = [Select Id, UserId, AccountId from AccountTeamMember where AccountId in: accountIdSet];
        
        
        //Used to create map of user id and user's email id. This map is further used to create map of account id and email id of the account team members
        List<Id> userIdList = new List<Id>();
        if(accountTeamMemberList != NULL && !accountTeamMemberList.isEmpty())
        {
            for(AccountTeamMember accTeamMem: accountTeamMemberList)
            {
                userIdList.add(accTeamMem.UserId);
            }
        }
        
        List<User> userList = new List<User>();
        if(userIdList != NULL && !userIdList.isEmpty())
            userList = [Select Id, Email from User where Id in: userIdList];
        
        Map<Id, String> mapUserIdEmail = new Map<Id, String>();
        if(userList != NULL && !userList.isEmpty())
        {
            for(User u: userList)
            {
                mapUserIdEmail.put(u.Id, u.Email);
            }
        }        
        
        //Map of account id and email id of the account team members
        Map<Id, List<String>> mapAccIdUserEmailList = new Map<Id, List<String>>();        
        if(accountTeamMemberList != NULL && !accountTeamMemberList.isEmpty())
        {
            for(AccountTeamMember accTeamMem: accountTeamMemberList)
            {
                List<String> uEmailList = mapAccIdUserEmailList.get(accTeamMem.AccountId);
                if(uEmailList == NULL)
                    uEmailList = new List<String>();
                uEmailList.add(mapUserIdEmail.get(accTeamMem.UserId));
                mapAccIdUserEmailList.put(accTeamMem.AccountId, uEmailList);
            }
        }
        
        List<Messaging.SingleEmailMessage> list_singleEmails = new List<Messaging.SingleEmailMessage>();
        Set<Messaging.SingleEmailMessage> set_singleEmails = new Set<Messaging.SingleEmailMessage>();
        
        //Fetching random contact's email for setTargetObjectId.
        List<Contact> contactList = new List<Contact>();
        contactList = [select id, Email from Contact where email <> null limit 1];
        
        List<String> sendToAddresses;
        
        //For sending email when opportunity is INSERTED or UPDATED
        if(newOppMap!= NULL && !newOppMap.isEmpty())
        {
            for(Opportunity opp: newOppMap.values())
            {
                if(mapAccIdUserEmailList.containsKey(opp.AccountId))
                    sendToAddresses = mapAccIdUserEmailList.get(opp.AccountId);
                System.debug('sendToAddresses------------'+sendToAddresses);
                
                if(sendToAddresses != NULL && !sendToAddresses.isEmpty() && contactList != NULL && !contactList.isEmpty())
                {
                    Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();
                    singleEmail.setWhatId(opp.Id);
                    singleEmail.setTemplateID(emailTemplate.Id);
                    singleEmail.setToAddresses(sendToAddresses);
                    singleEmail.setTargetObjectId(contactList[0].Id);
                    singleEmail.saveAsActivity = False;
                    singleEmail.setTreatTargetObjectAsRecipient(FALSE);
                    set_singleEmails.add(singleEmail);
                }
            }
        }
        
        //For sending email when opportunity is DELETED
        if((newOppMap == NULL || newOppMap.isEmpty()) && oldOppMap!= NULL && !oldOppMap.isEmpty())
        {
            for(Opportunity opp: oldOppMap.values())
            {
                if(mapAccIdUserEmailList.containsKey(opp.AccountId))
                    sendToAddresses = mapAccIdUserEmailList.get(opp.AccountId);
                System.debug('sendToAddresses------------'+sendToAddresses);
                
                if(sendToAddresses != NULL && !sendToAddresses.isEmpty())
                {
                    Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();
                    singleEmail.setWhatId(opp.Id);
                    singleEmail.setTemplateID(emailTemplate.Id);
                    singleEmail.setToAddresses(sendToAddresses);
                    singleEmail.setTargetObjectId(contactList[0].Id);
                    singleEmail.saveAsActivity = False;
                    singleEmail.setTreatTargetObjectAsRecipient(FALSE);
                    set_singleEmails.add(singleEmail);
                }
            }
        }
        
        list_singleEmails.addAll(set_singleEmails);
        System.debug('list_singleEmails--> '+list_singleEmails.size() + '@@@@@@@ '+list_singleEmails);
        
        if(!list_singleEmails.isEmpty())
        {
            Messaging.sendEmail(list_singleEmails);
        }
    }
    
    private static void sendEmailtoPricingTeam(List<Opportunity> newOppList, Map<Id, Opportunity> oldOppMap){
        System.debug('------------newOppList'+newOppList);
        Map<String, List<String>> oppToPricingEmailMap = new Map<String, List<String>>();
        Set<Opportunity> oppSet = new Set<Opportunity>();
        Set<Messaging.SingleEmailMessage> set_singleEmails = new Set<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> list_singleEmails = new list<Messaging.SingleEmailMessage>();
        
        for(Opportunity oppObj: NewOppList){
            if(oppObj.Request_Quote_From_Pricing_Team__c && !oldOppMap.get(oppObj.Id).Request_Quote_From_Pricing_Team__c
                && !String.IsBlank(oppObj.PricingTeamEmailPicklist__c)){
                    oppSet.add(oppObj);
                    for(String emailAddress: oppObj.PricingTeamEmailPicklist__c.split(';')){
                    if(!oppToPricingEmailMap.containsKey(oppObj.id)){
                        oppToPricingEmailMap.put(oppObj.Id, new List<String>());
                    }
                    oppToPricingEmailMap.get(oppObj.Id).add(emailAddress);                    
                }        
            }    
        }
        
        EmailTemplate emailTemplate = [Select id, Name from EmailTemplate where DeveloperName = 'Request_For_Quote_Generation' limit 1];
                
        for(Opportunity opp: oppSet){
            
            Messaging.SingleEmailMessage singleEmail = new Messaging.SingleEmailMessage();
            singleEmail.setWhatId(opp.Id);
            singleEmail.setTemplateID(emailTemplate.Id);
            singleEmail.setToAddresses(oppToPricingEmailMap.get(opp.Id));
            singleEmail.setTargetObjectId(opp.Contact__c);
            singleEmail.saveAsActivity = False;
            singleEmail.setTreatTargetObjectAsRecipient(FALSE);
            set_singleEmails.add(singleEmail);
            
        }
        list_singleEmails.addAll(set_singleEmails);
        
        if(!list_singleEmails.isEmpty())
        {
            Messaging.sendEmail(list_singleEmails);
        }
    }
    
}