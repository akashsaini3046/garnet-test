public class CrowleyCommunity_LoginController {
    @AuraEnabled
    public static String checkPortal(String username, String password, String currentURL){
    try { 
        System.PageReference lgn = Site.login(username, password, currentURL);
        if (lgn != null) {
            aura.redirect(lgn);
            CustomerCommunity_BadgesController.checkBadges(username);
            return CrowleyCommunity_Constants.TRUE_MESSAGE;
        } else
            return CrowleyCommunity_Constants.FALSE_MESSAGE;
        
    } catch (Exception ex) {
        ExceptionHandler.logApexCalloutError(ex);
        return CrowleyCommunity_Constants.FALSE_MESSAGE;
    }
 }   
}