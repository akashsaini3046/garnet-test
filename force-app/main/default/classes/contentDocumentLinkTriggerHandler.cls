public without sharing class contentDocumentLinkTriggerHandler {
    public static void createContentDistributionRecord(List<ContentDocumentLink> listContentDocumentLinkRecords){
        List<ContentVersion> listContentVersionRecords = new List<ContentVersion>();
        Set<ContentDistribution> setContentDistribution = new Set<ContentDistribution>();
        List<ContentDistribution> listContentDistribution = new List<ContentDistribution>();
        Set<Id> setContentDocumentId = new Set<Id>();
        
        for(ContentDocumentLink cdl : listContentDocumentLinkRecords){
            String linkedEntity = String.valueOf(cdl.LinkedEntityId);
            if(linkedEntity.startsWithIgnoreCase('a09') || linkedEntity.startsWithIgnoreCase('a08') || linkedEntity.startsWithIgnoreCase('a1o'))
                setContentDocumentId.add(cdl.ContentDocumentId);
        }
        if(!setContentDocumentId.isEmpty()){
        	listContentVersionRecords = [SELECT Id, ContentDocumentId, FileType FROM ContentVersion WHERE ContentDocumentId IN :setContentDocumentId];
            if(!listContentVersionRecords.isEmpty()){
                for(ContentVersion contentVersionRecord : listContentVersionRecords){
                    if(contentVersionRecord.FileType == 'PDF'){
                        ContentDistribution newContentDistribution = new ContentDistribution();
                        newContentDistribution.Name = 'Distribution Record';
                        newContentDistribution.ContentVersionId = contentVersionRecord.Id;
                        newContentDistribution.PreferencesAllowViewInBrowser= true;
                        newContentDistribution.PreferencesLinkLatestVersion=true;
                        newContentDistribution.PreferencesNotifyOnVisit=false;
                        newContentDistribution.PreferencesPasswordRequired=false;
                        newContentDistribution.PreferencesAllowOriginalDownload= false;
                        setContentDistribution.add(newContentDistribution);
                    }
                }
                if(!setContentDistribution.isEmpty()){
                    listContentDistribution = new List<ContentDistribution>(setContentDistribution);
                    insert listContentDistribution;
                }
            }
        }
    } 
    
    public static void uploadToEsker(List<ContentDocumentLink> listContentDocumentLinkRecords){
        System.debug('uploadToEsker--->');
        List<ContentVersion> listContentVersionRecords = new List<ContentVersion>();
        Map<Id, Header__c> mapHeaders;
        Map<Id, Header__c> mapShippingInstruction;
        Map<Id, Header__c> mapIMO;
        Map<Id, ContentVersion> mapContentDocumentVersion = new Map<Id, ContentVersion>();
        Map<Id, Id> mapDocumentVsHeaderIds = new Map<Id, Id>();
        Map<Id, Id> mapDocumentIMOIds = new Map<Id, Id>();
        Set<Id> setShippingInstrutionId = new Set<Id>();
        Set<Id> setContentDocumentId = new Set<Id>();
        Set<Id> setContentDocumentLinkId = new Set<Id>();
		Set<Id> setIMOId = new Set<Id>();
        Set<Id> headerIds = new Set<Id>();
		        
        for(ContentDocumentLink cdl : listContentDocumentLinkRecords){
            String linkedEntity = String.valueOf(cdl.LinkedEntityId);
            if(linkedEntity.startsWithIgnoreCase('a09')){
                headerIds.add(cdl.LinkedEntityId);
                setContentDocumentId.add(cdl.ContentDocumentId);
                mapDocumentVsHeaderIds.put(cdl.ContentDocumentId, cdl.LinkedEntityId);
            }
        }
        if(headerIds != null && !headerIds.isEmpty()){
            mapHeaders = new Map<Id, Header__c>([SELECT Id, Name, Booking__c, Booking__r.Name, RecordTypeId FROM Header__c WHERE Id IN :headerIds]);
        }
        
        if(!setContentDocumentId.isEmpty()){
        	listContentVersionRecords = [SELECT Id, ContentDocumentId, Title, FileType, VersionData,PathOnClient FROM ContentVersion WHERE ContentDocumentId IN :setContentDocumentId AND FileType = 'PDF'];
            if(!listContentVersionRecords.isEmpty()){
                for(ContentVersion contentVersionRecord : listContentVersionRecords){
                 	mapContentDocumentVersion.put(contentVersionRecord.ContentDocumentId, contentVersionRecord);   
                }
            }
        }
         System.debug('inside mapContentDocumentVersion------'+mapContentDocumentVersion);
        //Upload File to Esker
        if(!listContentVersionRecords.isEmpty() && mapHeaders != Null && !mapHeaders.isEmpty() && !mapContentDocumentVersion.isEmpty()){
               System.debug('inside ifffffffff------');
            for(ContentVersion contentVersionRecord : listContentVersionRecords){
                if(mapDocumentVsHeaderIds.containsKey(contentVersionRecord.ContentDocumentId) && mapHeaders.containsKey(mapDocumentVsHeaderIds.get(contentVersionRecord.ContentDocumentId))){
                    CustomerCommunity_FileController.UploadFileInEsker(EncodingUtil.base64Encode(contentVersionRecord.VersionData), contentVersionRecord.PathOnClient, 
                                                                       mapHeaders.get(mapDocumentVsHeaderIds.get(contentVersionRecord.ContentDocumentId)).Id, 
                                                                       mapHeaders.get(mapDocumentVsHeaderIds.get(contentVersionRecord.ContentDocumentId)).Booking__r.Name, 
                                                                       mapHeaders.get(mapDocumentVsHeaderIds.get(contentVersionRecord.ContentDocumentId)).RecordTypeId);
                }
            }
        }
    }
}