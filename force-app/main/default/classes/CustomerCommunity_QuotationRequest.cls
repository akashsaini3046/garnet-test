public class CustomerCommunity_QuotationRequest{
 
    @AuraEnabled public String receiptTermVal{get; set;}
    @AuraEnabled public String deliveryTermVal{get; set;}
    @AuraEnabled public String bookingRequestNumber{get; set;}
    @AuraEnabled public String customerId{get; set;}
    @AuraEnabled public String customerContactId{get; set;}
    @AuraEnabled public String bookingType{get; set;}
    @AuraEnabled public Contact customerContactRecord{get; set;}
    @AuraEnabled public Address__c customerAddressRecord{get; set;}
    @AuraEnabled public String shipperId{get; set;}
    @AuraEnabled public String shipperContactId{get; set;}
    @AuraEnabled public Contact shipperContactRecord{get; set;}
    @AuraEnabled public Address__c shipperAddressRecord{get; set;}
    @AuraEnabled public String consigneeId{get; set;}
    @AuraEnabled public String consigneeContactId{get; set;}
    @AuraEnabled public Contact consigneeContactRecord{get; set;}
    @AuraEnabled public Address__c consigneeAddressRecord{get; set;}
    @AuraEnabled public String originCode{get; set;}
    @AuraEnabled public String destinationCode{get; set;}
    @AuraEnabled public Date estSailingDate{get; set;}
    @AuraEnabled public List<Requirement__c> requirementRecords{get; set;}
    @AuraEnabled public List<Commodity__c> commodityRecords{get; set;}
    @AuraEnabled public Contact contactRecord{get; set;}
    @AuraEnabled public String ContainerMode{get; set;}
    @AuraEnabled public Double totalWeight{get; set;}
    @AuraEnabled public Double totalVolume{get; set;}
    @AuraEnabled public String totalWeightUnit{get; set;}
    @AuraEnabled public String totalVolumeUnit{get; set;}
    @AuraEnabled public Quote__c quoteRecord{get; set;}
    @AuraEnabled public String contractNRA{get; set;}
    @AuraEnabled public List < CustomerCommunity_CargoDetailsWrapper > cargodetailWrapperList{get; set;}
    @AuraEnabled public String cargoType{get; set;}
    //@AuraEnabled public List<ContainerCargoDetail> listCargoDetails{get; set;}

    @AuraEnabled public Body Body{get;set;}
    @AuraEnabled public String Header{get;set;}
    public class PcsDims{
        @AuraEnabled public list<Item> Item{get;set;}
    }
	public class Item{
		@AuraEnabled public String Volume{get;set;}
		@AuraEnabled public String Height{get;set;}
		@AuraEnabled public String Width{get;set;}
		@AuraEnabled public String ItemClass{get;set;}
		@AuraEnabled public String Length{get;set;}
		@AuraEnabled public String Commodity{get;set;}
		@AuraEnabled public String Weight{get;set;}
		@AuraEnabled public Boolean Hazmat{get;set;}
		@AuraEnabled public String DimType{get;set;}
		@AuraEnabled public list<String> UNNmbr{get;set;}
		@AuraEnabled public String Qty{get;set;}
		@AuraEnabled public list<String> NMFC{get;set;}
		@AuraEnabled public list<String> StackAmount{get;set;}
		@AuraEnabled public String Stack{get;set;}
	}
	public class GetRatesRequest{
		@AuraEnabled public String DestinationCountry{get;set;}
		@AuraEnabled public String DestinationState{get;set;}
		@AuraEnabled public String DestinationCity{get;set;}
		@AuraEnabled public PcsDims PcsDims{get;set;}
		@AuraEnabled public String DestinationZipCode{get;set;}
		@AuraEnabled public String WeightEach{get;set;}
		@AuraEnabled public String OriginCountry{get;set;}
		@AuraEnabled public String UOM{get;set;}
		@AuraEnabled public String OriginState{get;set;}
		@AuraEnabled public String OriginCity{get;set;}
		@AuraEnabled public Accessorials Accessorials{get;set;}
		@AuraEnabled public String OriginZipCode{get;set;}
		@AuraEnabled public String Carrier{get;set;}
	}
	public class Body{
		@AuraEnabled public GetRatesRequest GetRatesRequest{get;set;}
	}
	public class Accessorials{
		@AuraEnabled public list<String> Item{get;set;}
	} 
}