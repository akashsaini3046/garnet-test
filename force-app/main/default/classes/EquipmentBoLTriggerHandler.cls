public class EquipmentBoLTriggerHandler {
    public static void populateFieldData(List<Equipment_BOL__c> listEquipmentRecords){
        if(!listEquipmentRecords.isEmpty()){
            for(Equipment_BOL__c equipRecord : listEquipmentRecords){
                if(equipRecord.Id__c != Null && equipRecord.Id__c != ''){
                    String containerId = equipRecord.Id__c;
                    if(containerId.contains(' '))
                        containerId = containerId.replaceAll(' ','');
                    if(containerId.contains('-'))
                        containerId = containerId.substringBeforeLast('-');
                    if((equipRecord.Name == Null || equipRecord.Name == '') && containerId.length() > 4)
                        equipRecord.Name = containerId.substring(4);
                    if((equipRecord.Prefix__c == Null || equipRecord.Prefix__c == '') && containerId.length() >= 4)
                        equipRecord.Prefix__c = containerId.substring(0,4);
                }
            }
        }
    }
}