public without sharing class CustomerCommunity_ContainerController {
    public CustomerCommunity_ContainerController() {

    }

    @AuraEnabled 
    public static List<Container__c> fetchContainerList(){
        List<Container__c> listContainers= new  List<Container__c>();
        try {
            listContainers = [Select Id, Name, CICS_ISO_Code__c, Description__c, EBR__c, Genset_Type__c, Genset__c, Group__c, Handle_As__c, Height__c, Hide__c, Reefer__c, Size__c, Softship_ISO_Code__c, Tare__c, Type__c, Width__c, Used_For__c from Container__c ORDER BY NAME ASC];
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
        return listContainers;
    }

    @AuraEnabled
    public static LIST<Substance__c> getSubstancesRecord(String searchKey){
        searchKey = '%'+ searchKey + '%';
		LIST<Substance__c> substances = new LIST<Substance__c>([SELECT Id, Suffix__c,Prefix__c ,Name, Primary_Class__c, Substance_Name__c, Variation__c, 
                                              	Packing_Group__c, Secondary_Class__c, Tertiary_Class__c, Marine_Pollutant__c,FlashPoint__c
                                              FROM Substance__c where RecordType.DeveloperName='Substance' AND Name LIKE: searchKey]);
        return substances;
    }
    @AuraEnabled
    public static LIST<Substance__c> getVehicles(){ 
        
        //insert new et4ae5__abTest__c(Name='Roopal Test',et4ae5__mid__c='test');
		LIST<Substance__c> substances = new LIST<Substance__c>([Select Id, Name, Model__c, Type__c, Model_Year__c, Length__c, Width__c, Weight__c, Model_Name__c, Height__c,RecordType.DeveloperName from Substance__c where RecordType.DeveloperName='Vehicle']);
        return substances;
    }

    @AuraEnabled
    public static CustomerCommunity_RatingAPIResponse fetchRatesForMultiContainer(BookingWrapper bookingWrapper){
        CustomerCommunity_RatingAPIResponse responseWrapper = new CustomerCommunity_RatingAPIResponse();
        //responseWrapper = BookingFCLRatesService.fetchUpdateBookingWrapper(bookingWrapper);
        bookingWrapper.shipment = bookingWrapper.shipmentMap.get('CONTAINER');
        String containerRequest = BookingFCLRatesService.softshipRatingRequest(bookingWrapper);
        bookingWrapper.shipment = bookingWrapper.shipmentMap.get('RORO');
        String roroRequest = BookingFCLRatesService.softshipRatingRequest(bookingWrapper);
        bookingWrapper.shipment = bookingWrapper.shipmentMap.get('BREAKBULK');
        String breakBulkRequest = BookingFCLRatesService.softshipRatingRequest(bookingWrapper);

        system.debug('containerRequest-------->>>>>'+containerRequest);
        system.debug('roroRequest-------->>>>>'+roroRequest);
        system.debug('breakBulkRequest-------->>>>>'+breakBulkRequest);
        return responseWrapper;
    }

    /*@AuraEnabled(continuation=true cacheable=true)
    public static object createSoftshipRateAPIWRequest(BookingWrapper bookingWrapper) 
    {        
        Continuation continuationObject = new Continuation(120);
        continuationObject.continuationMethod='processResponse';
        Map<String,String> mapString = new Map<String,String>();
        String keys='';
        bookingWrapper.shipment = bookingWrapper.shipmentMap.get('CONTAINER');
        String containerRequest = BookingFCLRatesService.softshipRatingRequest(bookingWrapper);
        HttpRequest ratesRequestContainer = CustomerCommunity_APICallouts.getAPIRequest('RatingAPINew', containerRequest);            
        keys = continuationObject.addHttpRequest(ratesRequestContainer);
        mapString.put(keys,'CONTAINER');

        bookingWrapper.shipment = bookingWrapper.shipmentMap.get('RORO');
        String roroRequest = BookingFCLRatesService.softshipRatingRequest(bookingWrapper);
        HttpRequest ratesRequestRoRo = CustomerCommunity_APICallouts.getAPIRequest('RatingAPINew', roroRequest);            
        keys = continuationObject.addHttpRequest(ratesRequestRoRo);
        mapString.put(keys,'RORO');

        bookingWrapper.shipment = bookingWrapper.shipmentMap.get('BREAKBULK');
        String breakBulkRequest = BookingFCLRatesService.softshipRatingRequest(bookingWrapper);
        HttpRequest ratesRequestBreakbulk = CustomerCommunity_APICallouts.getAPIRequest('RatingAPINew', breakBulkRequest);            
        keys = continuationObject.addHttpRequest(ratesRequestBreakbulk);
        mapString.put(keys,'BREAKBULK');

        continuationObject.state=mapString;
        return continuationObject;
    }
    
    @AuraEnabled(cacheable=true)
    public static Map<String,CustomerCommunity_RatingAPIResponse> processResponse(List<String> labels, Object state) {
        CustomerCommunity_RatingAPIResponse responseWrapper = new CustomerCommunity_RatingAPIResponse();
        Map<String,CustomerCommunity_RatingAPIResponse> mapResponseBody= new Map<String,CustomerCommunity_RatingAPIResponse>();
        Map<String, String> mapString= (Map<String,String>) state;
        HttpResponse response = Continuation.getResponse(labels[0]);
        if(response.getBody() != null && response.getStatusCode() == 200){
            responseWrapper =(CustomerCommunity_RatingAPIResponse) JSON.deserialize(response.getBody(), CustomerCommunity_RatingAPIResponse.class);
            mapResponseBody.put(mapString.get(labels[0]),responseWrapper);
        }
                
        HttpResponse response1 = Continuation.getResponse(labels[1]);
        if(response.getBody() != null && response.getStatusCode() == 200){
            responseWrapper =(CustomerCommunity_RatingAPIResponse) JSON.deserialize(response.getBody(), CustomerCommunity_RatingAPIResponse.class);
            mapResponseBody.put(mapString.get(labels[1]),responseWrapper);
        }

        HttpResponse response2 = Continuation.getResponse(labels[2]);
        if(response.getBody() != null && response.getStatusCode() == 200){
            responseWrapper =(CustomerCommunity_RatingAPIResponse) JSON.deserialize(response.getBody(), CustomerCommunity_RatingAPIResponse.class);
            mapResponseBody.put(mapString.get(labels[2]),responseWrapper);
        }
       
        return mapResponseBody;
    }*/
    

}