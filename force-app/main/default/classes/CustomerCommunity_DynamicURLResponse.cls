public class CustomerCommunity_DynamicURLResponse {
		 @AuraEnabled public HeaderTexts HeaderTexts{get;set;}
		 @AuraEnabled public list<Voyages> Voyages{get;set;}
	public class Voyages{
		@AuraEnabled public String StatusDescription{get;set;}
		@AuraEnabled public String StatusCode{get;set;}
		@AuraEnabled public String CentralPortCode{get;set;}
		@AuraEnabled public String TypeDescription{get;set;}
		@AuraEnabled public String CentralPortDescription{get;set;}
		@AuraEnabled public String TypeCode{get;set;}
		@AuraEnabled public String PortOfLoadingCode{get;set;}
		@AuraEnabled public String VesselName{get;set;}
		@AuraEnabled public String PortOfLoadingDescription{get;set;}
		@AuraEnabled public String VesselCode{get;set;}
		@AuraEnabled public String PortOfDischargeCode{get;set;}
		@AuraEnabled public String VoyageNumber{get;set;}
		@AuraEnabled public String PortOfDischargeDescription{get;set;}
		@AuraEnabled public String VoyageMachineNumber{get;set;}
		@AuraEnabled public String StartDate{get;set;}
		@AuraEnabled public String ServiceCode{get;set;}
		@AuraEnabled public String HasTransshipment{get;set;}
		@AuraEnabled public String ServiceName{get;set;}
		@AuraEnabled public String DurationHours{get;set;}
		@AuraEnabled public String Route{get;set;}
		@AuraEnabled public String Duration{get;set;}
		@AuraEnabled public list<Ports> Ports{get;set;}
		@AuraEnabled public String EndDate{get;set;}
	}
	public class Terminals{
		@AuraEnabled public String Code{get;set;}
		@AuraEnabled public String ArrivalTime{get;set;}
		@AuraEnabled public String DepartureDate{get;set;}
		@AuraEnabled public String ArrivalDate{get;set;}
		@AuraEnabled public String DepartureTime{get;set;}
		@AuraEnabled public String Name{get;set;}
	}
	public class Ports{
		@AuraEnabled public String SailingDate{get;set;}
		@AuraEnabled public String ArrivalTime{get;set;}
		@AuraEnabled public String SailingTime{get;set;}
		@AuraEnabled public String ArrivalDate{get;set;}
		@AuraEnabled public String TypeDescription{get;set;}
		@AuraEnabled public String PortCallId{get;set;}
		@AuraEnabled public list<Terminals> Terminals{get;set;}
		@AuraEnabled public String PortName{get;set;}
		@AuraEnabled public String PortStatus{get;set;}
		@AuraEnabled public String PortCode{get;set;}
	}
	public class HeaderTexts{
		@AuraEnabled public String CentralPortCode{get;set;}
		@AuraEnabled public String PortOfDischargeDescription{get;set;}
		@AuraEnabled public String PortOfDischargeCallId{get;set;}
		@AuraEnabled public String PortCallId{get;set;}
		@AuraEnabled public String PortOfDischargeCode{get;set;}
		@AuraEnabled public String CentralPortDescription{get;set;}
		@AuraEnabled public String PortOfLoadingDescription{get;set;}
		@AuraEnabled public String StartDate{get;set;}
		@AuraEnabled public String PortOfLoadingCallId{get;set;}
		@AuraEnabled public String EndDate{get;set;}
		@AuraEnabled public String PortOfLoadingCode{get;set;}
		@AuraEnabled public String Duration{get;set;}
		@AuraEnabled public String StatusDescription{get;set;}
		@AuraEnabled public String HasTransshipment{get;set;}
		@AuraEnabled public String StatusCode{get;set;}
		@AuraEnabled public String ServiceName{get;set;}
		@AuraEnabled public String TypeDescription{get;set;}
		@AuraEnabled public String PortCode{get;set;}
		@AuraEnabled public String TypeCode{get;set;}
		@AuraEnabled public String PortName{get;set;}
		@AuraEnabled public String VesselName{get;set;}
		@AuraEnabled public String ArrivalDate{get;set;}
		@AuraEnabled public String VesselCode{get;set;}
		@AuraEnabled public String SailingDate{get;set;}
		@AuraEnabled public String VoyageNumber{get;set;}
	}
}