public class CustomerCommunity_VoyagesInfo {

		 @AuraEnabled public String VoyageMachineNumber{get; set;}	//1964
		 @AuraEnabled public String VoyageNumber{get; set;}	//NZV9053
		 @AuraEnabled public String VesselCode{get; set;}	//KSM
		 @AuraEnabled public String VesselName{get; set;}	//K Storm
		 @AuraEnabled public String TypeCode{get; set;}	//export
		 @AuraEnabled public String TypeDescription{get; set;}	//
		 @AuraEnabled public String StatusCode{get; set;}	//Active
		 @AuraEnabled public String StatusDescription{get; set;}	//
		 @AuraEnabled public String CentralPortCode{get; set;}	//USILM
		 @AuraEnabled public String CentralPortDescription{get; set;}	//Wilmington, NC
		 @AuraEnabled public String PortOfLoadingCode{get; set;}	//USJAX
		 @AuraEnabled public String PortOfLoadingDescription{get; set;}	//Jacksonville, FL
		 @AuraEnabled public String PortOfDischargeCode{get; set;}	//HNPCR
		 @AuraEnabled public String PortOfDischargeDescription{get; set;}	//Puerto Cortes, Honduras
		 @AuraEnabled public String StartDate{get; set;}	//03-May-2019
		 @AuraEnabled public String EndDate{get; set;}	//08-May-2019
		 @AuraEnabled public String Duration{get; set;}	//4
		 @AuraEnabled public String DurationHours{get; set;}	//21
		 @AuraEnabled public String HasTransshipment{get; set;}	//False
		 @AuraEnabled public String ServiceCode{get; set;}
		 @AuraEnabled public String ServiceName{get; set;}
	
}