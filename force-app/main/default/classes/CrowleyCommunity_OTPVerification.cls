public without sharing class CrowleyCommunity_OTPVerification {
    @AuraEnabled
    public static String OTPVerification(String Identifier,String verifyCode){
        try{ 
            System.debug('identifier@@'+identifier);
            Auth.VerificationResult res = System.UserManagement.verifySelfRegistration(Auth.VerificationMethod.EMAIL, identifier, verifyCode, null);
            //String  res =   System.UserManagement.verifyRegisterVerificationMethod(verifyCode, Auth.VerificationMethod.EMAIL);
            System.debug('res --->'+ res);
            if(String.valueOf(res.message) == 'Token not valid' || String.valueOf(res.message).contains('Invalid') || String.valueOf(res.message).contains('expired'))  
                return CrowleyCommunity_Constants.FALSE_MESSAGE;             
            else
                return CrowleyCommunity_Constants.TRUE_MESSAGE;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return CrowleyCommunity_Constants.ERROR_MESSAGE;    
        }   
    }
    @AuraEnabled
    public static String OTPResend(String userEmail){
        try{
            String result = CrowleyCommunity_ValidateUser.validateUserDetail(userEmail);
            if(result == 'False'){
               return CrowleyCommunity_Constants.FALSE_MESSAGE;   
            }
            else {
               return CrowleyCommunity_Constants.TRUE_MESSAGE; 
            }    
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return CrowleyCommunity_Constants.ERROR_MESSAGE;  
        }
    }
}