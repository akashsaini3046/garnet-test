/*
* Name: ExceptionHandler
* Purpose: This class is use to insert exception log.
* Author: Nagarro
* Created Date: 04-July-2019
* Modification History
*  Modification #   Story/Defect#      Modified By     Date   Description
*/
public class ExceptionHandler
{
    /*
	* Method Name: logApexCalloutError
	* Input Parameters: 
	* Exception ex: This holds exception.
	* Return value: 
	* Purpose: This method logs call out error.
	*/
    public static void logApexCalloutError(Exception ex){
        Exception__c exceptionRecord = new Exception__c();
        exceptionRecord.Cause__c = String.valueOf(ex.getCause());
        exceptionRecord.LineNumber__c =ex.getLineNumber();
        exceptionRecord.Message__c = ex.getMessage();
        exceptionRecord.Stack_Trace__c=ex.getStackTraceString();
        exceptionRecord.Type__c = ex.getTypeName();
        exceptionRecord.User__c = UserInfo.getUserId();
        insert exceptionRecord; 
    }
    
    public static void logDMLError( List<Database.Error> listOfError){
        /*Exception__c exceptionRecord = new Exception__c();
        exceptionRecord.Cause__c = String.valueOf(ex.getCause());
        exceptionRecord.LineNumber__c =ex.getLineNumber();
        exceptionRecord.Message__c = ex.getMessage();
        exceptionRecord.Stack_Trace__c=ex.getStackTraceString();
        exceptionRecord.Type__c = ex.getTypeName();
        exceptionRecord.User__c = UserInfo.getUserId();
        insert exceptionRecord;*/ 
    }
    
   
    
    /*
	* Method Name: logBOLXMLPraserException
	* Input Parameters: 
	* Exception objEx: This holds exception.
	* Return value: 
	* Purpose: This method logs BOL xml parser error.
	*/
    public void logBOLXMLPraserException(Exception objEx, String strParentBOLId) {
        Exception__c objException = new Exception__c();
        objException.Cause__c = String.valueOf(objEx.getCause());
        objException.LineNumber__c =objEx.getLineNumber();
        objException.Message__c = objEx.getMessage();
        objException.User__c = UserInfo.getUserId();
        objException.Stack_Trace__c = objEx.getMessage();
        objException.Parent_BOL__c = strParentBOLId;
        insert objException;
    }
}