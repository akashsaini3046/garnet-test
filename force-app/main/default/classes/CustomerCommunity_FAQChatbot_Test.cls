@isTest
public class CustomerCommunity_FAQChatbot_Test {
 
     static testmethod void searchFAQ_test(){
               User thisUser = [ select Id from User where username = 'ayush.mishra@nagarro.com.crowley.comdev'];
               system.runAs(thisuser){
                 Knowledge__kav test_article = new Knowledge__kav(Title = 'test apex',
                   Summary = 'test from apex',
                   URLName = 'test',
                   Language = 'en_US',
                   Description__c = 'test test',
                   IsVisibleInCsp = True);
                 insert test_article; 
                 
                 test_article = [SELECT KnowledgeArticleId FROM Knowledge__kav WHERE Id = :test_article.Id];
                 KbManagement.PublishingService.publishArticle(test_article.KnowledgeArticleId, true);
                 
            CustomerCommunity_FAQChatbot.FAQSearchInput input1 = new CustomerCommunity_FAQChatbot.FAQSearchInput();
            input1 .sKeyword = 'test\'';
            
                
            List<CustomerCommunity_FAQChatbot.FAQSearchInput> listinput1 = new List<CustomerCommunity_FAQChatbot.FAQSearchInput>();
 
            listinput1.add(input1);
            
            
            Id [] fixedSearchResults= new Id[1];
            fixedSearchResults[0] = test_article.Id;
            Test.setFixedSearchResults(fixedSearchResults);

               
               test.startTest();
              
               CustomerCommunity_FAQChatbot.searchFAQ(listInput1);
                          
               test.stopTest();
           }     
    }
    }