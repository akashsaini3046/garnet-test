public class FileUploadController {
    
    @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
        // check if fileId id ''(Always blank in first chunk), then call the saveTheFile method,
        //  which is save the check data and return the attachemnt Id after insert, 
        //  next time (in else) we are call the appentTOFile() method
        //   for update the attachment with reamins chunks   
        if (fileId == '') {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            appendToFile(fileId, base64Data);
        }
 
        return Id.valueOf(fileId);
    }
 
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
        try{
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            List<Idea> ideas = [Select Id, AttachmentBody, AttachmentName, AttachmentContentType From Idea Where Id=:parentId];
            System.debug('parentId : '+parentId);
            System.debug('ideas: '+ideas);
            if(!ideas.isEmpty()){
                ideas[0].AttachmentBody = EncodingUtil.base64Decode(base64Data);
                ideas[0].AttachmentName = fileName;
                ideas[0].AttachmentContentType = String.isBlank(contentType)? 'text/plain': contentType;
                update ideas[0];
            }
            return parentId;
        }catch(Exception ex){
            System.debug('message -> '+ex.getMessage() + ' stack trace -> '+ex.getStackTraceString());
            throw new CC_IdeaCustomException(ex.getMessage());
        }
        
	}
 
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
 
        List<Idea> ideas = [Select Id, AttachmentBody, AttachmentName, AttachmentContentType From Idea Where Id=:fileId];
        if(!ideas.isEmpty()){
            String existingBody = EncodingUtil.base64Encode(ideas[0].AttachmentBody);
            
            ideas[0].AttachmentBody = EncodingUtil.base64Decode(existingBody + base64Data);
            
            update ideas[0];
        }
    }
}