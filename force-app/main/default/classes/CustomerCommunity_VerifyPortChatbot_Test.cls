@isTest
public class CustomerCommunity_VerifyPortChatbot_Test {   
    testMethod static void verifyPort(){
        Test.startTest();
        CustomerCommunity_VerifyPortChatbot.PortData testPort = new CustomerCommunity_VerifyPortChatbot.PortData();
        testPort.portName = 'BONAIRE';
        List<CustomerCommunity_VerifyPortChatbot.PortData> listPort = new List<CustomerCommunity_VerifyPortChatbot.PortData>();
        listPort.add(testPort);
        CustomerCommunity_VerifyPortChatbot.verifyPort(listPort);
        Test.stopTest();
    }
    testMethod static void verifyPortExcep(){
       Test.startTest();
       List<CustomerCommunity_VerifyPortChatbot.PortData> listPortExcep = new List<CustomerCommunity_VerifyPortChatbot.PortData>(); 
       CustomerCommunity_VerifyPortChatbot.verifyPort(listPortExcep);
       Test.stopTest(); 
    }
}