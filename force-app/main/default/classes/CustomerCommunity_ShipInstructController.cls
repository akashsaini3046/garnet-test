public class CustomerCommunity_ShipInstructController {
	@future(callout=true)
    public static void sendNotification () {
        String tokenService = CustomerCommunity_Constants.SMS_SERVICE_GET_TOKEN;
        JSONGenerator tokenGen = JSON.createGenerator(true);   
        tokenGen.writeStartObject();     
        tokenGen.writeStringField('clientId', '9g3sxfanfyreo4h0kx2p1cvf');
        tokenGen.writeStringField('clientSecret','upXN3JRNSTK1qUhwrdn7O08O');
        tokenGen.writeEndObject();   
        String tokenRequestBody = tokenGen.getAsString();
        String tokenResponse = CustomerCommunity_APICallouts.getAPIResponse(tokenService, tokenRequestBody, Null);
        CustomerCommunity_SMSTokenInformation smsTokenInformation = (CustomerCommunity_SMSTokenInformation)JSON.deserialize(tokenResponse, CustomerCommunity_SMSTokenInformation.class);        
        if(smsTokenInformation.accessToken!=null){
			List<SMS_Notifications__mdt> listOfMobileNumbers = new List<SMS_Notifications__mdt>();
            listOfMobileNumbers = [SELECT Id, Phone_Number__c  FROM SMS_Notifications__mdt];
		    String smsService = CustomerCommunity_Constants.SMS_SERVICE_SEND_SMS;
			JSONGenerator smsGen = JSON.createGenerator(true);   
			smsGen.writeStartObject();
			User currentUser=[Select id,PhoneNumber_SMS__c From User Where id = :UserInfo.getUserId() ];
			if(currentUser.PhoneNumber_SMS__c!=null){
				List<String> mobileList = currentUser.PhoneNumber_SMS__c.split(',');	
				smsGen.writeFieldName('mobileNumbers');
				smsGen.writeObject(mobileList);
				smsGen.writeStringField('Subscribe', 'true');
				smsGen.writeStringField('Resubscribe','true');
				smsGen.writeStringField('Override','true');
				smsGen.writeStringField('Keyword','CROWLYSTART');
				smsGen.writeStringField('messageText','A new shipping instruction document has been uploaded');
				smsGen.writeEndObject();   
				String messageRequestBody = smsGen.getAsString();
                Http http = new Http();
        		HttpRequest httpReq = new  HttpRequest();
        		httpReq.setEndpoint('https://mch9qhzz9dq0k470rgxj3zfmbfy8.rest.marketingcloudapis.com/sms/v1/messageContact/NTM6Nzg6MA/send');
        		httpReq.setMethod('POST');
				httpReq.setHeader('Authorization','Bearer ' + smsTokenInformation.accessToken );  
        		httpReq.setHeader('Content-Type','application/json');
        		httpReq.setBody( messageRequestBody );
        		HttpResponse httpRes = http.send( httpReq );
			}
		}
    }
}