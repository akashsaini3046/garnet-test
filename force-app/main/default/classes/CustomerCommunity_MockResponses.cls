public class CustomerCommunity_MockResponses  {
	public class FindARouteMockResponse implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String fullJson = '[{"originPort":"DO, CAUCEDO", "destinationPort":"US, CHICAGO-CFS", "shipmentType":"FCL", "sailingWeeks":"1 Week", "transitTime":"2015-07-27 00:00:00.000", "details":[{"voyageNumber":"12", "vesselName":"vName", "startDate":"10-11-2019", "arrivalDate":"11-11-2019", "transitTime":"2015-07-27 00:00:00.000"}]}]';
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    public class FindARouteMockResponseStatic implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            //String fullJson = '[{"originPort":"DO, CAUCEDO", "destinationPort":"US, CHICAGO-CFS", "shipmentType":"FCL", "sailingWeeks":"1 Week", "transitTime":"2015-07-27 00:00:00.000", "details":[{"voyageNumber":"12", "vesselName":"vName", "startDate":"10-11-2019", "arrivalDate":"11-11-2019", "transitTime":"2015-07-27 00:00:00.000"}]}]';
            StaticResource staticresRecord = [SELECT Id, Body FROM StaticResource WHERE Name = 'findARoute' LIMIT 1]; 
            System.debug('staticresRecordBody'+staticresRecord.Body);
            String fullJson = staticresRecord.Body.toString();
            System.debug('fullJson->>'+fullJson);
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fullJson);
            res.setStatusCode(200);   
            //System.assertEquals(body, '{"key1":"value1","key2":"value2"}');
            return res;
        }
    }
    public class ShipmentControllergetShipmentInfoMockResponse implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            String fullJson ='"[{\"currentStatus\":\"Not booked\",\"discharge\":{\"date\":\"\",\"flag\":\"\",\"location\":\"\"},\"estSailDate\":\"\",\"shipmentID\":\"CMCU4554958         \",\"shipmentType\":\"Equipment\",\"statusDate\":\"\",\"voyage\":{\"number\":\"\",\"vessel\":\"\"}},{\"currentStatus\":\"Not Found !!\",\"discharge\":{\"date\":\"\",\"flag\":\"\",\"location\":\"\"},\"estSailDate\":\"\",\"shipmentID\":\"CMCU4554959         \",\"shipmentType\":\"Equipment\",\"statusDate\":\"\",\"voyage\":{\"number\":\"\",\"vessel\":\"\"}},{\"currentStatus\":\"Not Found !!\",\"discharge\":{\"date\":\"\",\"flag\":\"\",\"location\":\"\"},\"estSailDate\":\"\",\"shipmentID\":\"CMCU4554951         \",\"shipmentType\":\"Equipment\",\"statusDate\":\"\",\"voyage\":{\"number\":\"\",\"vessel\":\"\"}}]"';
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    public class ShipmentControllergetCWShipmentInfoMockResponse implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            String fullxml = '<DataSourceCollection><Type>ForwardingConsol</Type><Key>CUCA0077423</Key><MilestoneCollection><Milestone><Description>Departure from First Load Port</Description><EventCode>DEP</EventCode><Sequence>1</Sequence><ActualDate>2019-06-24T00:00:00</ActualDate><ConditionReference></ConditionReference><ConditionType></ConditionType><EstimatedDate>2019-06-22T09:00:00</EstimatedDate></Milestone><Milestone><Description>Arrived at Port</Description><EventCode>ARV</EventCode><Sequence>2</Sequence><ActualDate>2019-06-27T07:30:00</ActualDate><ConditionReference></ConditionReference><ConditionType></ConditionType><EstimatedDate>2019-06-27T06:30:00</EstimatedDate></Milestone><Milestone><Description>Arrived at Warehouse</Description><EventCode>Z20</EventCode><Sequence>3</Sequence><ActualDate></ActualDate><ConditionReference></ConditionReference><ConditionType></ConditionType><EstimatedDate></EstimatedDate></Milestone><Milestone><Description>Cargo Available</Description><EventCode>CAV</EventCode><Sequence>4</Sequence><ActualDate></ActualDate><ConditionReference></ConditionReference><ConditionType></ConditionType><EstimatedDate></EstimatedDate></Milestone></MilestoneCollection><DataSource><Type>ForwardingShipment</Type><Key>SUCA278342</Key><MilestoneCollection><Milestone><Description>Pickup Cartage Complete/Finalised</Description><EventCode>PCF</EventCode><Sequence>2</Sequence><ActualDate></ActualDate><ConditionReference></ConditionReference><ConditionType></ConditionType><EstimatedDate></EstimatedDate></Milestone><Milestone><Description>Origin Receival from Wharf / Depot</Description><EventCode>GIN</EventCode><Sequence>3</Sequence><ActualDate></ActualDate><ConditionReference></ConditionReference><ConditionType>REF</ConditionType><EstimatedDate></EstimatedDate></Milestone><Milestone><Description>All Export Documents Received</Description><EventCode>AED</EventCode><Sequence>4</Sequence><ActualDate></ActualDate><ConditionReference></ConditionReference><ConditionType></ConditionType><EstimatedDate></EstimatedDate></Milestone><Milestone><EventCode>FLO</EventCode><Sequence>6</Sequence>                <ActualDate>2019-06-24T09:00:25</ActualDate>                <ConditionReference></ConditionReference>                <ConditionType></ConditionType> <EstimatedDate></EstimatedDate> </Milestone></MilestoneCollection> </DataSource></DataSourceCollection>';
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'XML');
            res.setBody(fullxml);
            res.setStatusCode(200);
            return res;
        }
    }
    Public class ShipmentControllerupdateShortenURLMockResponse implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req){
            String urljson = '[{"status_code":200,"status_txt":"OK","data":{"url":"https://sforce.co/2oNgW2Z","hash":"2oNgW2Z","global_hash":"2oNgWjv","long_url":"https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/header/a090t000009AH6TAAW","new_hash":0}}]';
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setBody(urljson);
            res.setStatusCode(200);
            return res;
        }
    }
    Public class DescribeLayoutMockResponse implements HttpCalloutMock{
        
        public boolean isError;
        
        public DescribeLayoutMockResponse(boolean isError){
            this.isError = isError;
        }
        
        public HTTPResponse respond(HTTPRequest req){
            String jsonData = '{"detailLayoutSections":[{"layoutRows":[{"layoutItems":[{"editableForNew":true,"editableForUpdate":true,"label":"Benefits","layoutComponents":[{"details":{"picklistValues":[{"active":true,"defaultValue":false,"label":"Others","validFor":null,"value":"Others"},{"active":true,"defaultValue":false,"label":"Business Requirement","validFor":null,"value":"Business Requirement"},{"active":true,"defaultValue":false,"label":"Nice to Have","validFor":null,"value":"Nice to Have"}]},"displayLines":1,"tabOrder":1,"type":"Field","value":"Benefits__c"}]}]},{"layoutItems":[{"editableForNew":true,"editableForUpdate":true,"label":"Status","layoutComponents":[{"details":{"picklistValues":[{"active":true,"defaultValue":false,"label":"Open","validFor":null,"value":"Open"},{"active":true,"defaultValue":false,"label":"Accepted","validFor":null,"value":"Accepted"},{"active":true,"defaultValue":false,"label":"Being Implemented","validFor":null,"value":"Being Implemented"},{"active":true,"defaultValue":false,"label":"Implemented and Closed","validFor":null,"value":"Implemented and Closed"},{"active":true,"defaultValue":false,"label":"On Hold","validFor":null,"value":"On Hold"}]},"displayLines":1,"tabOrder":2,"type":"Field","value":"Status"}]}]},{"layoutItems":[{"editableForNew":true,"editableForUpdate":true,"label":"Categories","layoutComponents":[{"details":{"picklistValues":[{"active":true,"defaultValue":false,"label":"Sales","validFor":null,"value":"Sales"},{"active":true,"defaultValue":false,"label":"Service","validFor":null,"value":"Service"},{"active":true,"defaultValue":false,"label":"Community","validFor":null,"value":"Community"},{"active":true,"defaultValue":false,"label":"Marketing","validFor":null,"value":"Marketing"}]},"displayLines":7,"tabOrder":3,"type":"Field","value":"Categories"}]}]}]}]}';            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            if(isError){               
                  res.setBody('{"error" : true}');
                res.setStatusCode(500);

               
            }else{
                res.setBody(jsonData);
                res.setStatusCode(200);
                                             
            }
            return res;
        }
    }
}