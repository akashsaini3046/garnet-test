public with sharing class CustomerCommunity_BookingsChatbot {
    public class InputFields {
        @InvocableVariable(required=true)
        public String sOrigin;
        @InvocableVariable(required=true)
        public String sDestination;
        @InvocableVariable(required=true)
        public String sContactId;
    }  
    public class BookingName {
        @InvocableVariable(required=true)
        public String sBookingName;
    }
    @InvocableMethod(label='Create Bookings')
    public static List<BookingName> FindARoute(List<InputFields> inputParam) {
        List<BookingName> listResponses = new List<BookingName>();
        Booking__c book = new Booking__c();
        List<Booking__c> listBooking = new List<Booking__c>();
        Integer bookingRecordsCount = 0;
        List<Contact> customerContact = new List<Contact>();
        BookingName bookName = new BookingName();
        bookName.sBookingName = CustomerCommunity_Constants.EMPTY_STRING;
        try{
            Schema.DescribeFieldResult portFieldResult = Booking__c.Origin__c.getDescribe();
            List<Schema.PicklistEntry> portList = portFieldResult.getPicklistValues();
            for (Schema.PicklistEntry p: portList) {
                if(p.getLabel().substring(4).equalsIgnoreCase(inputParam[0].sOrigin)) {
                    inputParam[0].sOrigin = p.getValue();
                }
                if(p.getLabel().substring(4).equalsIgnoreCase(inputParam[0].sDestination)) {
                    inputParam[0].sDestination = p.getValue();
                }
            }
            if(inputParam[0].sContactId != Null){
                customerContact = [SELECT Id, AccountId FROM Contact WHERE Id = :inputParam[0].sContactId LIMIT 1];
            }
            //book.Origin__c = inputParam[0].sOrigin;
            //book.Destination__c = inputParam[0].sDestination;
            if(!customerContact.isEmpty())
                book.Contact__c = customerContact[0].Id;
            if(!customerContact.isEmpty() && customerContact[0].AccountId != Null)
                book.Account__c = customerContact[0].AccountId;
            //bookingRecordsCount = [SELECT count() FROM Booking__c]+1;
            //book.Name = 'CAT'+'0000'+String.valueOf(bookingRecordsCount);
            insert book;
            listBooking = [SELECT Name FROM Booking__c WHERE Id = :book.Id LIMIT 1];
            bookName.sBookingName = bookName.sBookingName + Label.CustomerCommunity_BookingSuccessMsg + listBooking[0].Name + Label.CustomerCommunity_BookingURLMessage + listBooking[0].Id;
            listResponses.add(bookName);
            return listResponses;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            bookName.sBookingName = bookName.sBookingName + Label.CustomerCommunity_BookingErrorMsg;
            listResponses.add(bookName);
            return listResponses;
        }
        
    }
}