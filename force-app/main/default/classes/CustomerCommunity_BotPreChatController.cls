public without sharing class CustomerCommunity_BotPreChatController {
    @AuraEnabled
    public static String getCurrentUser(){
        Map<String,Object> output = new Map<String,Object>();
        try{
            User u = [SELECT UserName, FirstName, LastName, Email, ContactId, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
            if(u.ContactId != null){
                output.put(CustomerCommunity_Constants.JSONOBJECT_USERID, u.UserName);
                output.put(CustomerCommunity_Constants.JSONOBJECT_FIRSTNAME, u.FirstName);
                output.put(CustomerCommunity_Constants.JSONOBJECT_LASTNAME, u.LastName);
                output.put(CustomerCommunity_Constants.JSONOBJECT_EMAIL, u.Email);
                output.put('language', u.LanguageLocaleKey);
            }else{
                output.put(CustomerCommunity_Constants.JSONOBJECT_USERID, CustomerCommunity_Constants.EMPTY_STRING);
            }
            System.debug('@@@@ '+output);
            return JSON.serialize(output);
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            output.put(CustomerCommunity_Constants.JSONOBJECT_USERID, CustomerCommunity_Constants.EMPTY_STRING);
            return JSON.serialize(output);
        }
    }
}