public class CrowleyBookingRecordDetailCtrl {
    
    @AuraEnabled
    public static void sendEmailPDF(Id bookingId, String emailAddress){
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
		Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
		PageReference pref = page.BookingRecordPDF;
        pref.getParameters().put('Id',bookingId);
		Blob b = pref.getContent();
		attach.setFileName('BookDetails.pdf');
		attach.setBody(b);
		semail.setSubject('Booking Details');
        List<String> emailsAddrs = emailAddress.split(';');
        String[] emailAddressArray = new String[emailsAddrs.size()];
        for(Integer i = 0 ; i < emailsAddrs.size(); i++){
            emailAddressArray[i] = emailsAddrs.get(i);
        }
		String[] sendTo = emailAddressArray;
		semail.setToAddresses(sendTo);
		semail.setPlainTextBody('Please find the attached booking document');
		semail.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
		Messaging.sendEmail(new Messaging.SingleEmailMessage[]{semail});
    }
    
    @AuraEnabled
    public static Map<String, List<String>> getBookingDetail(Id bookingId){   
        try {
            Map<String, List<String>> relatedObjectVsIdsList = new Map<String, List<String>>();
            
            Booking__c bookingRecord = [SELECT Id,Is_Hazardous__c,Name,
                                        (SELECT Id, Name FROM Transports__r),
                                        (SELECT Id, Name FROM Booking_Remarks__r),
                                        (SELECT Id, Name FROM Shipments__r),
                                        (SELECT Id, Type__c FROM Parties__r)
                                        FROM Booking__c
                                        WHERE Id = :bookingId];
            relatedObjectVsIdsList.put('Is_Hazardous__c', new List<String>{String.valueOf(bookingRecord.Is_Hazardous__c)});
            if(Network.getNetworkId() != null){
                relatedObjectVsIdsList.put('bookingUrl', new List<String>{'/CrowleyCustomerCommunity/apex/BookingPDFMain?Id='+bookingId+'&bookingName='+String.valueOf(bookingRecord.Name)});
            }else{
                relatedObjectVsIdsList.put('bookingUrl', new List<String>{URL.getSalesforceBaseUrl().toExternalForm()+'/apex/BookingPDFMain?Id='+bookingId+'&bookingName='+String.valueOf(bookingRecord.Name)});
            }
            List<String> parties = new List<String>();
            for(Party__c party : bookingRecord.Parties__r){
                parties.add(party.Id +'___'+party.Type__c);
            }
            if(parties != null && !parties.isEmpty()){
                relatedObjectVsIdsList.put('parties', parties);
            }
            
            List<String> bookingRemarks = new List<String>();
            for(Booking_Remark__c bookingRemark : bookingRecord.Booking_Remarks__r){
                bookingRemarks.add(bookingRemark.Id +'___'+bookingRemark.Name);
            }
            if(bookingRemarks != null && !bookingRemarks.isEmpty()){
                relatedObjectVsIdsList.put('bookingRemarks', bookingRemarks);
            }
            
            List<String> transports = new List<String>();
            Set<Id> transportsIds = new Set<Id>();
            for(Transport__c transport : bookingRecord.Transports__r){
                transports.add(transport.Id +'___'+transport.Name);
                transportsIds.add(transport.Id);
            }
            if(transports != null && !transports.isEmpty()){
                List<Stop__c> stopsList = new List<Stop__c>();
                List<Transport__c> transportsList = [SELECT Id, 
                                                     (SELECT Id, Name FROM Stops__r)
                                                     FROM Transport__c
                                                     WHERE Id IN :transportsIds];
                for(Transport__c trans : transportsList){
                    if(trans.Stops__r != null && !trans.Stops__r.isEmpty()){
                        stopsList.addAll(trans.Stops__r);
                    }
                }
                if(stopsList != null && !stopsList.isEmpty()){
                    List<String> stops = new List<String>();
                    for(Stop__c stop : stopsList) {
                        stops.add(stop.Id + '___'+ stop.Name);
                    }
                    relatedObjectVsIdsList.put('stops', stops);
                }
                relatedObjectVsIdsList.put('transports', transports);
            }
            
            // Shipments
            List<String> shipments = new List<String>();
            Set<Id> shipmentsIds = new Set<Id>();
            for(Shipment__c shipment : bookingRecord.Shipments__r){
                shipments.add(shipment.Id +'___'+shipment.Name);
                shipmentsIds.add(shipment.Id);
            }
            if(shipmentsIds != null && !shipmentsIds.isEmpty()){
                List<Voyage__c> voyagesList = new List<Voyage__c>();
                List<FreightDetail__c> freightDetailsList = new List<FreightDetail__c>();
                List<Dock_Receipt__c> dockReceiptsList = new List<Dock_Receipt__c>();
                List<Customer_Notification__c> customerNotificationsList = new List<Customer_Notification__c>();
                List<Shipment__c> shipmentsList = [SELECT Id, 
                                                   (SELECT Id, Name FROM Voyages__r),
                                                   (SELECT Id, Name FROM Freight_Details__r),
                                                   (SELECT Id, Name FROM Dock_Receipts__r),
                                                   (SELECT Id, Name FROM Customer_Notifications__r)
                                                   FROM Shipment__c
                                                   WHERE Id IN :shipmentsIds];
                for(Shipment__c shipment : shipmentsList){
                    if(shipment.Voyages__r != null && !shipment.Voyages__r.isEmpty()){
                        voyagesList.addAll(shipment.Voyages__r);
                    }
                    if(shipment.Freight_Details__r != null && !shipment.Freight_Details__r.isEmpty()){
                        freightDetailsList.addAll(shipment.Freight_Details__r);
                    }
                    if(shipment.Dock_Receipts__r != null && !shipment.Dock_Receipts__r.isEmpty()){
                        dockReceiptsList.addAll(shipment.Dock_Receipts__r);
                    }
                    if(shipment.Customer_Notifications__r != null && !shipment.Customer_Notifications__r.isEmpty()){
                        customerNotificationsList.addAll(shipment.Customer_Notifications__r);
                    }
                }
                if(voyagesList != null && !voyagesList.isEmpty()){
                    List<String> voyages = new List<String>();
                    for(Voyage__c voyage : voyagesList) {
                        voyages.add(voyage.Id + '___'+ voyage.Name);
                    }
                    relatedObjectVsIdsList.put('voyages', voyages);
                }
                if(dockReceiptsList != null && !dockReceiptsList.isEmpty()){
                    List<String> dockReceipts = new List<String>();
                    for(Dock_Receipt__c dockReceipt : dockReceiptsList) {
                        dockReceipts.add(dockReceipt.Id + '___'+ dockReceipt.Name);
                    }
                    relatedObjectVsIdsList.put('dockReceipts', dockReceipts);
                }
                if(customerNotificationsList != null && !customerNotificationsList.isEmpty()){
                    List<String> customerNotifications = new List<String>();
                    for(Customer_Notification__c customerNotification : customerNotificationsList) {
                        customerNotifications.add(customerNotification.Id + '___'+ customerNotification.Name);
                    }
                    relatedObjectVsIdsList.put('customerNotifications', customerNotifications);
                }
                
                Set<Id> freightDetailIds = new Set<Id>();
                if(freightDetailsList != null && !freightDetailsList.isEmpty()){
                    List<String> freightDetails = new List<String>();
                    for(FreightDetail__c freightDetail : freightDetailsList) {
                        freightDetails.add(freightDetail.Id + '___'+ freightDetail.Name);
                        freightDetailIds.add(freightDetail.Id);
                    }
                    if(freightDetailIds != null && !freightDetailIds.isEmpty()){
                        List<Commodity__c> commoditiesList = new List<Commodity__c>();
                        List<Requirement__c> requirementsList = new List<Requirement__c>();
                        List<FreightDetail__c> freightDetailList = [SELECT Id, 
                                                                    (SELECT Id, Name FROM Commodities__r),
                                                                    (SELECT Id, Name FROM Requirements__r)
                                                                    FROM FreightDetail__c
                                                                    WHERE Id IN :freightDetailIds];
                        for(FreightDetail__c freightDetail : freightDetailList){
                            if(freightDetail.Commodities__r != null && !freightDetail.Commodities__r.isEmpty()){
                                commoditiesList.addAll(freightDetail.Commodities__r);
                            }
                            if(freightDetail.Requirements__r != null && !freightDetail.Requirements__r.isEmpty()){
                                requirementsList.addAll(freightDetail.Requirements__r);
                            }
                        }
                        if(commoditiesList != null && !commoditiesList.isEmpty()){
                            List<String> commodities = new List<String>();
                            for(Commodity__c commodity : commoditiesList) {
                                commodities.add(commodity.Id + '___'+ commodity.Name);
                            }
                            relatedObjectVsIdsList.put('commodities', commodities);
                        }
                        if(requirementsList != null && !requirementsList.isEmpty()){
                            List<String> requirements = new List<String>();
                            Set<Id> requirementIds = new Set<Id>();
                            for(Requirement__c requirement : requirementsList) {
                                requirements.add(requirement.Id + '___'+ requirement.Name);
                                requirementIds.add(requirement.Id);
                            }
                            if(requirementIds != null && !requirementIds.isEmpty()){
                                List<Equipment__c> equipmentsList = new List<Equipment__c>();
                                List<Requirement__c> requirementList = [SELECT Id, 
                                                                        (SELECT Id, Name FROM Equipments__r)
                                                                        FROM Requirement__c
                                                                        WHERE Id IN :requirementIds];
                                for(Requirement__c requirement : requirementList){
                                    if(requirement.Equipments__r != null && !requirement.Equipments__r.isEmpty()){
                                        equipmentsList.addAll(requirement.Equipments__r);
                                    }
                                }
                                if(equipmentsList != null && !equipmentsList.isEmpty()){
                                    List<String> equipments = new List<String>();
                                    for(Equipment__c equipment : equipmentsList) {
                                        equipments.add(equipment.Id + '___'+ equipment.Name);
                                    }
                                    relatedObjectVsIdsList.put('equipments', equipments);
                                }
                                relatedObjectVsIdsList.put('requirements', requirements);
                            }
                        }
                        relatedObjectVsIdsList.put('freightDetails', freightDetails);
                    }
                }
                relatedObjectVsIdsList.put('shipments', shipments);
            }
            
            return relatedObjectVsIdsList;
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    @AuraEnabled
    public static Map<String, List<String>> getFieldsDefinition(){   
        try {
            Map<String, List<String>> sectionNameVsFieldsList = new Map<String, List<String>>();
            
            List<String> bookingFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.Booking_Detail_Internal.getFields()){
                bookingFields.add(fieldMember.getFieldPath()); 
            }
            if(bookingFields != null && !bookingFields.isEmpty()){
                sectionNameVsFieldsList.put('bookingFields', bookingFields);
            }
            
            List<String> bookingFieldsServiceType = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.Service_Type_Detail_Internal.getFields()){
                bookingFieldsServiceType.add(fieldMember.getFieldPath()); 
            }
            if(bookingFieldsServiceType != null && !bookingFieldsServiceType.isEmpty()){
                sectionNameVsFieldsList.put('bookingFieldsServiceType', bookingFieldsServiceType);
            }
            
            List<String> bookingFieldsIMType = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.IM_Type_Detail_Internal.getFields()){
                bookingFieldsIMType.add(fieldMember.getFieldPath()); 
            }
            if(bookingFieldsIMType != null && !bookingFieldsIMType.isEmpty()){
                sectionNameVsFieldsList.put('bookingFieldsIMType', bookingFieldsIMType);
            }
            
            List<String> bookingFieldsTMSType = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.TMS_Type_Detail_Internal.getFields()){
                bookingFieldsTMSType.add(fieldMember.getFieldPath()); 
            }
            if(bookingFieldsTMSType != null && !bookingFieldsTMSType.isEmpty()){
                sectionNameVsFieldsList.put('bookingFieldsTMSType', bookingFieldsTMSType);
            }
            
            List<String> bookingFieldsCustomerOrigin = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.Customer_Origin_Detail_Internal.getFields()){
                bookingFieldsCustomerOrigin.add(fieldMember.getFieldPath()); 
            }
            if(bookingFieldsCustomerOrigin != null && !bookingFieldsCustomerOrigin.isEmpty()){
                sectionNameVsFieldsList.put('bookingFieldsCustomerOrigin', bookingFieldsCustomerOrigin);
            }
            
            List<String> bookingFieldsCustomerDestination = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.Customer_Destination_Detail_Internal.getFields()){
                bookingFieldsCustomerDestination.add(fieldMember.getFieldPath()); 
            }
            if(bookingFieldsCustomerDestination != null && !bookingFieldsCustomerDestination.isEmpty()){
                sectionNameVsFieldsList.put('bookingFieldsCustomerDestination', bookingFieldsCustomerDestination);
            }
            
            List<String> bookingFieldsConnectingCarrier = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.Connecting_Carrier_Detail_Internal.getFields()){
                bookingFieldsConnectingCarrier.add(fieldMember.getFieldPath()); 
            }
            if(bookingFieldsConnectingCarrier != null && !bookingFieldsConnectingCarrier.isEmpty()){
                sectionNameVsFieldsList.put('bookingFieldsConnectingCarrier', bookingFieldsConnectingCarrier);
            }
            
            List<String> bookingFieldsConnectAtLoc = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.Connect_At_Loc_Detail_Internal.getFields()){
                bookingFieldsConnectAtLoc.add(fieldMember.getFieldPath()); 
            }
            if(bookingFieldsConnectAtLoc != null && !bookingFieldsConnectAtLoc.isEmpty()){
                sectionNameVsFieldsList.put('bookingFieldsConnectAtLoc', bookingFieldsConnectAtLoc);
            }
            
            List<String> bookingFieldsConnectToLoc = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking__c.FieldSets.Connect_To_Loc_Detail_internal.getFields()){
                bookingFieldsConnectToLoc.add(fieldMember.getFieldPath()); 
            }
            if(bookingFieldsConnectToLoc != null && !bookingFieldsConnectToLoc.isEmpty()){
                sectionNameVsFieldsList.put('bookingFieldsConnectToLoc', bookingFieldsConnectToLoc);
            }
            
            List<String> partyFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Party__c.FieldSets.Party_Detail_Internal.getFields()){
                partyFields.add(fieldMember.getFieldPath()); 
            }
            if(partyFields != null && !partyFields.isEmpty()){
                sectionNameVsFieldsList.put('partyFields', partyFields);
            }
            
            List<String> bookingRemarkFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Booking_Remark__c.FieldSets.Booking_Remarks_Detail_Internal.getFields()){
                bookingRemarkFields.add(fieldMember.getFieldPath()); 
            }
            if(bookingRemarkFields != null && !bookingRemarkFields.isEmpty()){
                sectionNameVsFieldsList.put('bookingRemarkFields', bookingRemarkFields);
            }
            
            List<String> transportFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Transport__c.FieldSets.Transport_Detail_Internal.getFields()){
                transportFields.add(fieldMember.getFieldPath()); 
            }
            if(transportFields != null && !transportFields.isEmpty()){
                sectionNameVsFieldsList.put('transportFields', transportFields);
            }
            
            List<String> stopFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Stop__c.FieldSets.Stop_Detail_Internal.getFields()){
                stopFields.add(fieldMember.getFieldPath()); 
            }
            if(stopFields != null && !stopFields.isEmpty()){
                sectionNameVsFieldsList.put('stopFields', stopFields);
            }
            
            List<String> shipmentFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Shipment__c.FieldSets.Shipment_Detail_Internal.getFields()){
                shipmentFields.add(fieldMember.getFieldPath()); 
            }
            if(shipmentFields != null && !shipmentFields.isEmpty()){
                sectionNameVsFieldsList.put('shipmentFields', shipmentFields);
            }
            
            List<String> voyageFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Voyage__c.FieldSets.Voyage_Detail_Internal.getFields()){
                voyageFields.add(fieldMember.getFieldPath()); 
            }
            if(voyageFields != null && !voyageFields.isEmpty()){
                sectionNameVsFieldsList.put('voyageFields', voyageFields);
            }
            
            List<String> dockReceiptFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Dock_Receipt__c.FieldSets.Dock_Receipt_Detail_Internal.getFields()){
                dockReceiptFields.add(fieldMember.getFieldPath()); 
            }
            if(dockReceiptFields != null && !dockReceiptFields.isEmpty()){
                sectionNameVsFieldsList.put('dockReceiptFields', dockReceiptFields);
            }
            
            List<String> freightDetailFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.FreightDetail__c.FieldSets.Freight_Detail_Internal.getFields()){
                freightDetailFields.add(fieldMember.getFieldPath()); 
            }
            if(freightDetailFields != null && !freightDetailFields.isEmpty()){
                sectionNameVsFieldsList.put('freightDetailFields', freightDetailFields);
            }
            
            List<String> commodityFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Commodity__c.FieldSets.Commodity_Detail_Internal.getFields()){
                commodityFields.add(fieldMember.getFieldPath()); 
            }
            if(commodityFields != null && !commodityFields.isEmpty()){
                sectionNameVsFieldsList.put('commodityFields', commodityFields);
            }
            
            List<String> requirementFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Requirement__c.FieldSets.Requirement_Detail_Internal.getFields()){
                requirementFields.add(fieldMember.getFieldPath()); 
            }
            if(requirementFields != null && !requirementFields.isEmpty()){
                sectionNameVsFieldsList.put('requirementFields', requirementFields);
            }
            
            List<String> equipmentFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Equipment__c.FieldSets.Equipment_Detail_Internal.getFields()){
                equipmentFields.add(fieldMember.getFieldPath()); 
            }
            if(equipmentFields != null && !equipmentFields.isEmpty()){
                sectionNameVsFieldsList.put('equipmentFields', equipmentFields);
            }
            
            List<String> customerNotificationFields = new List<String>();
            for(Schema.FieldSetMember fieldMember : SObjectType.Customer_Notification__c.FieldSets.Customer_Notification_Detail_Internal.getFields()){
                customerNotificationFields.add(fieldMember.getFieldPath()); 
            }
            if(customerNotificationFields != null && !customerNotificationFields.isEmpty()){
                sectionNameVsFieldsList.put('customerNotificationFields', customerNotificationFields);
            }
            
            return sectionNameVsFieldsList;
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    @AuraEnabled
    public static String generatePDF(Id bookingId){
        return null;
    }
    
    public static Map<String,String> fetchReceiptDeliveryTermsPickListValues(String fieldName){
        Map<String,String> pickLabelValueMap = new Map<String,String>();
        
        Map<String,List<Schema.PicklistEntry>> picklistSchema= new Map<String,List<Schema.PicklistEntry>>();
        Schema.sObjectType objType = New_Booking_Fields__mdt.getSObjectType();
        //Schema.sObjectType objType = objectName.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        //List<String> fieldsToGetValues=new List<String>();
        //String fieldName ='Receipt_Delivery_Terms__c' ;
        
        
        //fieldsToGetValues.add('Receipt_Delivery_Terms__c');
        
        
        
        if(fieldMap.containsKey(fieldName)){
            list<Schema.PicklistEntry> values =
                fieldMap.get(fieldName).getDescribe().getPickListValues();
            for(Schema.PicklistEntry p : values){
                
                pickLabelValueMap.put(p.getLabel(),p.getValue());
            }
            
        }
        System.debug(fieldName+' values :');
        System.debug(pickLabelValueMap);
        return pickLabelValueMap; 
    }
    
    @AuraEnabled
    @future(callout=True)
    public static void getRates(Id IdBooking){
        
        System.debug('Entered inside GetRates method');
        System.debug('Booking id is : '+IdBooking);
        Booking__c bookingRec = new Booking__c();
        Shipment__c shipmentRec = new Shipment__c();
        Commodity__c commodityRec = new Commodity__c();
        
        FreightDetail__c freightDetailRec = new FreightDetail__c();
        Requirement__c requirementRec = new Requirement__c();
        Party__c partyRec = new Party__c();
        Voyage__c voyageRec = new Voyage__c();
        List<Voyage__c> voyageRecList = new List<Voyage__c>();
        Map<String, String> mapCommodityNameCategory = new Map<String, String>();
        // List<New_Booking_Commodities__mdt> listCommoditiesRecords = new List<New_Booking_Commodities__mdt>();
        
        
        
        bookingRec = [Select Name,Destination_Type__c,Origin_Type__c,Customer_Destination_Code__c,Date_of_Discharge__c,Total_Weight__c,	Total_Volume__c,
                      Total_Volume_Unit__c,Total_Weight_Unit__c,Contact__c,Customer_Origin_Code__c,Container_Mode__c from Booking__c 
                      where id = :IdBooking];
        
        shipmentRec = [Select Origin_Code__c,Destination_Code__c from Shipment__c where Booking__c = :bookingRec.Id];
        
        voyageRec = [Select Estimate_Sail_Date__c from Voyage__c where Shipment__c = :shipmentRec.Id Limit 1];
        voyageRecList=[Select Estimate_Sail_Date__c,Loading_Sequence__c from Voyage__c where Shipment__c = :shipmentRec.Id order By Loading_Sequence__c];
        
        freightDetailRec = [Select id from FreightDetail__c where Shipment__c  = :shipmentRec.Id];
        
        System.debug('Before commodity');
        commodityRec = [Select Name from Commodity__c where Freight__c = :freightDetailRec.Id Limit 1];
        System.debug('After commodity');
        
        requirementRec = [Select Container_Type__c,Category__c from Requirement__c where Freight__c = :freightDetailRec.Id];
        
        partyRec = [Select CVIF__c,Party_Id__c,Type__c from Party__c where Booking__c = :bookingRec.Id and Type__c='CUST'];
        
        System.debug('Party Data :');
        System.debug(partyRec);
        
        
        
        
        //requirementRec.Category__c = 'Cargo, NOS';
        /* listCommoditiesRecords = [SELECT MasterLabel, Commodity_Name__c, Category__c FROM New_Booking_Commodities__mdt WHERE Category__c != Null AND Category__c != ''];
if(!listCommoditiesRecords.isEmpty()){
for(New_Booking_Commodities__mdt currentCommodity : listCommoditiesRecords){
if(currentCommodity.Commodity_Name__c != Null && currentCommodity.Commodity_Name__c != '' && currentCommodity.Category__c != NULL && currentCommodity.Category__c != '')
mapCommodityNameCategory.put(currentCommodity.Category__c, currentCommodity.Commodity_Name__c); 
}
}    */
        
        System.debug('@@@');
        
        List<Commodity__c> listCommodity = new List<Commodity__c>();
        List<CustomerCommunity_BookingRecord.ContainerCargoDetail> listCargoDetail = new List<CustomerCommunity_BookingRecord.ContainerCargoDetail>();
        CustomerCommunity_BookingRecord bookingRecordValues = new CustomerCommunity_BookingRecord();
        
        bookingRecordValues.customerId = partyRec.CVIF__c;
        bookingRecordValues.bookingRequestNumber = bookingRec.Name;
        
        //bookingRecordValues.bookingType = 'FCL';
        bookingRecordValues.bookingType = bookingRec.Container_Mode__c;
        
        Map<String,String> mapValueNameReceiptDeliveryTerms= fetchReceiptDeliveryTermsPickListValues('Receipt_Delivery_Terms__c');
        //List<New_Booking_Fields__mdt> listReceiptDeliveryTerms = new List<New_Booking_Fields__mdt>();
        
        // bookingRecordValues.deliveryTermVal = 'P';
        //bookingRecordValues.receiptTermVal = 'D';
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Origin_Type__c))
            bookingRecordValues.receiptTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Origin_Type__c);
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Destination_Type__c))
            bookingRecordValues.deliveryTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Destination_Type__c); 
        
        System.debug('receiptTermVal  : '+bookingRecordValues.receiptTermVal+' deliveryTermVal : '+bookingRecordValues.deliveryTermVal);
        bookingRecordValues.estSailingDate = voyageRec.Estimate_Sail_Date__c;
        bookingRecordValues.originCode = shipmentRec.Origin_Code__c;
        bookingRecordValues.destinationCode = shipmentRec.Destination_Code__c;
        bookingRecordValues.totalVolume = bookingRec.Total_Volume__c;
        bookingRecordValues.totalWeight = bookingRec.Total_Weight__c;
        bookingRecordValues.totalVolumeUnit = bookingRec.Total_Volume_Unit__c;
        bookingRecordValues.totalWeightUnit = bookingRec.Total_Weight_Unit__c;
        CustomerCommunity_BookingRecord.ContainerCargoDetail containerCargoRecordValues = new CustomerCommunity_BookingRecord.ContainerCargoDetail();
        containerCargoRecordValues.sequenceId = 1;
        containerCargoRecordValues.UniqueId = 'commodity1';
        containerCargoRecordValues.isHazardous = false;
        containerCargoRecordValues.freightRecord = freightDetailRec;
        containerCargoRecordValues.requirementRecord = requirementRec;
        listCommodity.add(commodityRec);
        containerCargoRecordValues.listCommodityRecords = listCommodity; 
        listCargoDetail.add(containerCargoRecordValues);             
        
        bookingRecordValues.listCargoDetails = listCargoDetail;
        String bookingRecordString = JSON.serialize(bookingRecordValues);
        System.debug('bookingRecordString --->'+ bookingRecordString);
        String jsonResponseData = CustomerCommunity_NewBookingsController.getSoftshipRates(bookingRecordString,'ChargeLineItem1');
        System.debug('Response value is-----> '+jsonResponseData);
        
        
    }
    
    
    @AuraEnabled
    public static String validateIMDG(Id IdBooking){
        System.debug('Entered inside validateIMDG()');
        
        CustomerCommunity_BookingRecord bookingRecordValues=new CustomerCommunity_BookingRecord();
        Booking__c bookingRec = new Booking__c();
        Shipment__c shipmentRec = new Shipment__c();
        FreightDetail__c freightDetailRec = new FreightDetail__c();
        Requirement__c requirementRec = new Requirement__c();
        Voyage__c voyageRec = new Voyage__c();
        List<Party__c> partyList = new List<Party__c>();
        List<Commodity__c> commodityList = new List<Commodity__c>();
        List<Contact> contactList = new List<Contact>();
        Set<String> partyCodes = new Set<String>();
        Set<Id> setAddressId = new Set<Id>();
        Map<Id, Address__c> mapAddressRecords;
        Map<String,Id> mapCVIFAddressId = new Map<String,Id>();
        Map<String,Contact> mapCVIFContactRecords = new Map<String,Contact>();
        
        bookingRec = [Select Name,Destination_Type__c,Status__c, Origin_Type__c,Customer_Destination_Code__c,Date_of_Discharge__c,Total_Weight__c,	Total_Volume__c,
                      Total_Volume_Unit__c,Total_Weight_Unit__c,Contact__c,Customer_Origin_Code__c,Container_Mode__c from Booking__c 
                      where id = :IdBooking];
        partyList = [Select id ,CVIF__c,Party_Id__c,Type__c from Party__c where Booking__c = :bookingRec.Id];
        shipmentRec = [Select Origin_Code__c,Destination_Code__c from Shipment__c where Booking__c = :bookingRec.Id];
        freightDetailRec = [Select id from FreightDetail__c where Shipment__c  = :shipmentRec.Id];
        requirementRec = [Select Container_Type__c,Category__c from Requirement__c where Freight__c = :freightDetailRec.Id];
        commodityList = new List<Commodity__c>([Select Name,Emergency_Contact_Name__c,Emergency_Contact_Number__c,Number__c,Package_Group__c,Technical_Name__c,Suffix__c,Primary_Class__c,IMO_Class__c,Phone_Number__c,Name__c from Commodity__c where Is_Hazardous__c=true AND Freight__c = :freightDetailRec.Id]);
        voyageRec = [Select Estimate_Sail_Date__c from Voyage__c where Shipment__c = :shipmentRec.Id Limit 1];
        
        
        for(Party__c partyRecs : partyList)
            partyCodes.add(partyRecs.CVIF__c);
        
        contactList = [SELECT Id, Name, Phone, MobilePhone, Fax, Address__c, Account.CVIF__c FROM Contact WHERE Account.CVIF__c IN :partyCodes];
        for(Contact con: contactList){
            mapCVIFContactRecords.put(con.Account.CVIF__c,con);
            mapCVIFAddressId.put(con.Account.CVIF__c,con.Address__c);
            setAddressId.add(con.Address__c);
        }
        mapAddressRecords= new Map<Id, Address__c>([SELECT Id, Name, Address_Line_2__c, Address_Line_3__c, City__c, State_Picklist__c, Postal_Code__c, Country__c, 
                                                    CVIF_Location_Id__c FROM Address__c WHERE Id IN :setAddressId]);
        
        for(Party__c partyRecord : partyList){
            if(partyRecord.Type__c=='CUST'){
                bookingRecordValues.customerContactId=partyRecord.CVIF__c;
                bookingRecordValues.customerAddressRecord= mapAddressRecords.get(mapCVIFAddressId.get(partyRecord.CVIF__c));
                bookingRecordValues.customerContactRecord= mapCVIFContactRecords.get(partyRecord.CVIF__c);
                bookingRecordValues.customerId= partyRecord.CVIF__c;
            }
            else if(partyRecord.Type__c=='SHP'){
                bookingRecordValues.shipperContactId=partyRecord.CVIF__c;
                bookingRecordValues.shipperAddressRecord=mapAddressRecords.get(mapCVIFAddressId.get(partyRecord.CVIF__c));
                bookingRecordValues.shipperContactRecord= mapCVIFContactRecords.get(partyRecord.CVIF__c);
                bookingRecordValues.shipperId= partyRecord.CVIF__c;
            }
            else if(partyRecord.Type__c=='CON'){
                bookingRecordValues.consigneeContactId=partyRecord.CVIF__c;
                bookingRecordValues.shipperAddressRecord=mapAddressRecords.get(mapCVIFAddressId.get(partyRecord.CVIF__c));
                bookingRecordValues.consigneeContactRecord= mapCVIFContactRecords.get(partyRecord.CVIF__c);
                bookingRecordValues.consigneeId= partyRecord.CVIF__c;
            }
        }
        
        
        Map<String,String> mapValueNameReceiptDeliveryTerms= fetchReceiptDeliveryTermsPickListValues('Receipt_Delivery_Terms__c');
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Origin_Type__c))
            bookingRecordValues.receiptTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Origin_Type__c);
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Destination_Type__c))
            bookingRecordValues.deliveryTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Destination_Type__c);
        
        bookingRecordValues.estSailingDate = voyageRec.Estimate_Sail_Date__c;
        bookingRecordValues.originCode = shipmentRec.Origin_Code__c;
        bookingRecordValues.destinationCode = shipmentRec.Destination_Code__c;
        bookingRecordValues.totalVolume = bookingRec.Total_Volume__c;
        bookingRecordValues.totalWeight = bookingRec.Total_Weight__c;
        bookingRecordValues.totalVolumeUnit = bookingRec.Total_Volume_Unit__c;
        bookingRecordValues.bookingRequestNumber = bookingRec.Name;
        bookingRecordValues.bookingType = bookingRec.Container_Mode__c;
        
        
        //List<Commodity__c> listCommodity = new List<Commodity__c>();
        //Either comment the below line or listCommodity.add(commodityRec);
        // listCommodity=[Select Name from Commodity__c where Freight__c = :freightDetailRec.Id];
        List<CustomerCommunity_BookingRecord.ContainerCargoDetail> listCargoDetail = new List<CustomerCommunity_BookingRecord.ContainerCargoDetail>();
        
        CustomerCommunity_BookingRecord.ContainerCargoDetail containerCargoRecordValues = new CustomerCommunity_BookingRecord.ContainerCargoDetail();
        
        containerCargoRecordValues.sequenceId = 1;
        containerCargoRecordValues.UniqueId = 'commodity1';
        containerCargoRecordValues.isHazardous = true;
        containerCargoRecordValues.freightRecord = freightDetailRec;
        containerCargoRecordValues.requirementRecord = requirementRec;
        //listCommodity.add(commodityRec);
        containerCargoRecordValues.listCommodityRecords = commodityList; 
        
        listCargoDetail.add(containerCargoRecordValues); 
        
        bookingRecordValues.listCargoDetails = listCargoDetail;
        
        String bookingRecordString = JSON.serialize(bookingRecordValues);
        System.debug('bookingRecordString --->'+ bookingRecordString);
        String jsonResponseData = CustomerCommunity_NewBookingsController.validateHazardousBooking(IdBooking,bookingRecordString);
        System.debug('Response value is-----> '+jsonResponseData);
        return jsonResponseData;
    }
    
    @AuraEnabled
    public static List<Attachment> getAllAttachments(String parentId){
        return AttachmentDAOCls.getAllAttachments(parentId);
    }
    
    @AuraEnabled
    public static String getStringQuery(Map<String,List<String>> mapFieldSet,String type){
        String strQuery='';
        for(String fields:mapFieldSet.get(type) ){
            strQuery+=fields+',' ;
        }
        strQuery=strQuery.substring(0,strQuery.length()-1);
        return strQuery;
    }
    
}