@isTest
private class CrowleyCalloutServiceTest {
	@isTest static void testGetAndPostRequests(){
		String theUrl = 'test';
		String theBody = 'test';
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

		Test.startTest();
		String getResponse = CrowleyCalloutService.sendGet(theUrl);
		String postResponse = CrowleyCalloutService.sendPost(theUrl, theBody);
		STring putResponse = CrowleyCalloutService.sendPut(theUrl, new Map<String, String> {'Test' => 'Test'}, theBody);
		Test.stopTest();

		System.assertEquals(true, String.isBlank(getResponse));
		System.assertEquals(true, String.isBlank(postResponse));
		System.assertEquals(true, String.isBlank(putResponse));
	}
	public class MockHttpResponseGenerator implements HttpCalloutMock{
		public HTTPResponse respond(HTTPRequest req){
			HttpResponse res = new HttpResponse();
			res.setHeader('Content-Type', 'application/json');
			res.setStatusCode(200);
			return res;
		}
	}
}