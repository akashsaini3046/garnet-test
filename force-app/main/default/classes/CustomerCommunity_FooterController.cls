public class CustomerCommunity_FooterController {
   @AuraEnabled(cacheable=true)
    public static List<Footer_Column_One__mdt> queryColumnOneValues(){
        List<Footer_Column_One__mdt> footerListItems = [SELECT Label,SortOrder__c FROM Footer_Column_One__mdt ORDER by SortOrder__c];
        return footerListItems;
    }
    @AuraEnabled(cacheable=true)
    public static List<Footer_Column_Two__mdt> queryColumnTwoValues(){
        List<Footer_Column_Two__mdt> footerListItems = [SELECT Label,SortOrder__c FROM Footer_Column_Two__mdt ORDER by SortOrder__c];
        return footerListItems;
    }
    @AuraEnabled(cacheable=true)
    public static List<Footer_Column_Three__mdt> queryColumnThreeValues(){
        List<Footer_Column_Three__mdt> footerListItems = [SELECT Label,SortOrder__c FROM Footer_Column_Three__mdt ORDER BY SortOrder__c];
        return footerListItems;
    }
}