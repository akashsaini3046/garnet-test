public without sharing class CustomerCommunity_BillOfLadingFormCtrl {
    @AuraEnabled
   public static Bill_Of_Lading__c fetchBillOfLadingRecord(id billOfLadingId){
      /* Bill_Of_Lading__c bol = [SELECT id, name, Bill_Of_Lading_Number__c,Booking_Number__r.Booking_Number__c,Booking_Number__r.id,Booking_Reference_Number__c,Consignee_Reference_Number__c,Customer_Billing_Reference_Number__c,Numeric_Reference_Number__c,
                                Export_Identification_Number__c,Export_Reference_Number__c,Forwarder_Reference_Number__c,Inbound_Reference_Number__c,Option_4_Reference_Number__c,
                                Special_Reference_Number__c,Shipper_Reference_Number__c,Supplier_Reference_Number__c,Tax_Reference_Number__c,Value_Reference_Number__c,Mode_Type__c,Move_Type__c,
                                Initiate_Date__c,Issue_Date__c,SCAC_Code__c,Load_Port_Description__c,Control_Load_Port_Description__c,Discharge_Port_Description__c,Control_Discharge_Port_Description__c,
                                Point_Of_Origin_Description__c,Final_Destination_Description__c,Relay_Point_Description__c,Final_Destination__c,Final_Destination_State__c,Final_Destination_Country__c,
                                Origin_Voyage__c,Hazardous__c,Hazardous_Emergency_Contact__c,Hazardous_Emergency_Phone_Number_1__c,Hazardous_Emergency_Phone_Number_2__c,Prepaid_Collect__c,
                                Route__c,Move_Type_Description__c,BOL_Release_Description__c,Free_Text_Lines_DSC__c,
                                (Select Vessel_Name__c,Vessel_Country_Code__c from Voyages__r)
                                from Bill_Of_Lading__c where id=:billOfLadingId][0]; */
       
       Bill_Of_Lading__c bol = BillOfLadingDAO.getBolRecordById(billOfLadingId);
       return bol;
   }
   @AuraEnabled
   public static List<Party__c> fetchPartyRecords(id billOfLadingId){
       try{
           //List<Party__c> partyList= [Select id,Name,Address_Line1__c,Address_Line2__c,Address_Line3__c,Address_Line4__c,CVIF__c,CVIF_Location_Code__c,Type__c,CHB_Number__c,City__c,Country__c,State__c From Party__c where Bill_Of_Lading_Number__c=:billOfLadingId];
           List<Party__c> partyList = BillOfLadingDAO.getBOLPartyReords(billOfLadingId);
           if(partyList.size()>0)
               return partyList;
           else
               return null;
       }	
       catch(Exception e){
           return null;
       }
   }
   @AuraEnabled 
   public static List<Bill_Item__c> fetchBillItemRecords(id billOfLadingId){
       try{
          /* List<Bill_Item__c> billItemList=[Select id,Name,Loading_Port__c,Discharge_Port__c,Commodity_Type__c,Rate_Authority_Type__c,Authority_Number__c,Unit_Quantity__c,Unit_of_measure__c,Weight__c,Weight_Unit_of_measure__c,Cube__c,Cube_Feet__c,Cube_Meters__c,
                                           Schedule_B_Number__c,Shipper_Declare_value__c,Equipment_container__c,Sequence_Number__c,Items_Text_DSC__c
                                           From Bill_Item__c where Bill_Of_Lading_Number__c=:billOfLadingId]; */
           List<Bill_Item__c> billItemList = BillOfLadingDAO.getBillItemRecords(billOfLadingId);
           System.debug('billItemList '+billItemList);
           if(billItemList.size()>0)
               return billItemList;
           else
               return null;
       }
        catch(Exception e){
           return null;
       }
   }
   
   @AuraEnabled
   public static List<Charge_Line__c> fetchChargeLineItems(id billOfLadingId){
       try{
          // List<Charge_Line__c> chargeLineItems=[Select id,ChargeDescription__c,Quantity__c,Rate__c,Prepaid_Collect__c,Currency__c,Basis__c,Amount__c From Charge_Line__c where Bill_Of_Lading_Number__c=:billOfLadingId];
           List<Charge_Line__c> chargeLineItems = BillOfLadingDAO.getChargeLineItems(billOfLadingId);
           if(chargeLineItems.size()>0)
               return chargeLineItems;
           else
               return null;
       }
       catch(Exception e){
           return null;
       }
   }
   
   @AuraEnabled
   public static List<Equipment__c> fetchEquipmentRecords(id billOfLadingId){
       try{
           //List<Equipment__c> equipList=[Select id,Prefix__c,Number__c,Seal_Numbers__c From Equipment__c where Bill_Of_Lading_Number__c=:billOfLadingId];
           List<Equipment__c> equipList= BillOfLadingDAO.getEquipmentRecords(billOfLadingId);
           if(equipList.size()>0)
               return equipList;
           else
               return null;
       }
       catch(Exception e){
           return null;
       }
   }
    
    @AuraEnabled
    public static String getCommunityUrlPathPrefix(){
        return CommonUtility.getCommunityUrlPathPrefix();
    }
   
   @AuraEnabled
   public static String handleGetRates(id bookingId){
       try{
           System.debug('bookingId '+bookingId);
           
           Booking__c bookingRec = new Booking__c();
           Shipment__c shipmentRec = new Shipment__c();
           Ports_Information__c portInfoDest = new Ports_Information__c();
           List<FreightDetail__c> freightDetailRec = new List<FreightDetail__c>();
           Map<String,Container__c> mapContainer = new Map<String,Container__c>();
          
           
                       
           bookingRec = [SELECT Customer_Origin_Country__c,Customer_Origin_City__c,Customer_Origin_Zip__c,Customer_Origin_Code__c,
                         Customer_Destination_Country__c,Customer_Destination_City__c,Customer_Destination_Zip__c,Customer_Destination_Code__c,
                         Description__c,Contract_Number__c,Origin_Type__c,Destination_Type__c,Account__c,Ready_Date__c FROM Booking__c WHERE id =:bookingId];
           
           shipmentRec = [Select Origin_Code__c,Destination_Code__c,Origin_Country__c,Origin_Port__c,Destination_Port__c,Destination_Country__c from Shipment__c where Booking__c = :bookingRec.Id Limit 1];            
           freightDetailRec = [Select Freight_Quantity__c,Cargo_Type__c, Length_Major__c, Width_Major__c, Height_Major__c, Length__c, Width__c,Height__c,Declared_Weight_Value__c,Privately_Owned_Dealer_Owned_Vehicle__c, (Select Container_Type__c,Category__c,Is_Empty__c,Quantity__c from Requirements__r),(Select Name from Commodities__r) from FreightDetail__c where Shipment__c  = :shipmentRec.Id];
           
           for( Container__c container : [Select Id, Name, CICS_ISO_Code__c, Description__c from Container__c]){
               mapContainer.put(container.CICS_ISO_Code__c,container);
           }
           
           
           BookingWrapper bookingWrapper =  BookingWrapper.getBookingWrapper();
           System.debug('bookingWrapper '+bookingWrapper); 
           
           /************ Fetching Leg Information START*****************/
           bookingWrapper.booking = bookingRec;
           bookingWrapper.booking.Origin_Type__c = bookingRec.Description__c.substring(0,1);
           bookingWrapper.booking.Destination_Type__c = bookingRec.Description__c.substring(1);
          if(bookingWrapper.booking.Origin_Type__c=='D'){
              if(bookingRec.Customer_Origin_Zip__c!=null && bookingRec.Customer_Origin_Zip__c!=''){
                   bookingWrapper.booking.Customer_Origin_Code__c =bookingRec.Customer_Origin_Zip__c;
               }
               else{
                   portInfoDest = [SELECT Name FROM Ports_Information__c WHERE Zip_Code__c!=null AND city__c =: bookingRec.Customer_Origin_City__c AND Country__c =: bookingRec.Customer_Origin_Country__c 
                       AND recordType.DeveloperName='ZipCode' LIMIT 1];
                    bookingWrapper.booking.Customer_Origin_Code__c =portInfoDest.Name;
               }
          }else{
              bookingWrapper.booking.Customer_Origin_Code__c = shipmentRec.Origin_Country__c+shipmentRec.Origin_Port__c;
          }
          if(bookingWrapper.booking.Destination_Type__c=='D'){
              if(bookingRec.Customer_Destination_Zip__c!=null && bookingRec.Customer_Destination_Zip__c!=''){
                   bookingWrapper.booking.Customer_Destination_Code__c =bookingRec.Customer_Destination_Zip__c;
               }
               else{
                    portInfoDest = [SELECT Name FROM Ports_Information__c WHERE Zip_Code__c!=null AND city__c =:bookingRec.Customer_Destination_City__c AND Country__c =: bookingRec.Customer_Destination_Country__c AND recordType.DeveloperName='ZipCode' LIMIT 1];
                    bookingWrapper.booking.Customer_Destination_Code__c =portInfoDest.Name;
               }
               
          }else{
              bookingWrapper.booking.Customer_Destination_Code__c = shipmentRec.Destination_Country__c+shipmentRec.Destination_Port__c;
          }
          
          /**************** Fetching Leg Information END********************/
          
          
          /**************** Fetching Cargo Information START****************/
          
          //bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.clear();
          if(freightDetailRec.size()>0){
              for(FreightDetail__c freightDetail : freightDetailRec){	
					BookingWrapper.FreightDetailWrapper freightWrapper= new BookingWrapper.FreightDetailWrapper();
					if(freightDetail.Cargo_Type__c.equalsIgnoreCase('EQUIP')){
						bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper.clear();
						bookingWrapper.shipment.listCargo[0].cargoType = 'Container';
						for(Requirement__c requirement: freightDetail.Requirements__r){					   
						  BookingWrapper.RequirementWrapper reqWrapper= new BookingWrapper.RequirementWrapper();
						  reqWrapper.commodityDesc = 'Cargo, NOS';
						  reqWrapper.containerDesc = (mapContainer.get(requirement.Container_Type__c)!=null) ? mapContainer.get(requirement.Container_Type__c).Description__c : '';
						  reqWrapper.containerType = (mapContainer.get(requirement.Container_Type__c)!=null) ? mapContainer.get(requirement.Container_Type__c).Name : '';
						  reqWrapper.requirement = requirement;
						  bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper.add(reqWrapper);
						}
					}
                    if(freightDetail.Cargo_Type__c.equalsIgnoreCase('AUTO')){				   
						bookingWrapper.shipment.listCargo[0].cargoType = 'RoRo';
						
						freightWrapper.commodityDesc ='8700000000';
						if(freightDetail.Privately_Owned_Dealer_Owned_Vehicle__c=='P' || freightDetail.Privately_Owned_Dealer_Owned_Vehicle__c== 'PVEH'){
						   freightWrapper.typeOfPackage ='PVEH';
						}else{
						   freightWrapper.typeOfPackage ='CVEH';
						}
						freightWrapper.freightDetail = freightDetail;
						if(bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.size()==1){
						   bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0] = freightWrapper;
						}else{
						   bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.add(freightWrapper);
						}
					}
					if(freightDetail.Cargo_Type__c.equalsIgnoreCase('BBULK')){
						bookingWrapper.shipment.listCargo[0].cargoType = 'BreakBulk';
						freightWrapper.commodityDesc ='8700000000';
						freightWrapper.typeOfPackage ='NIT';
						if(bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.size()==1){
						   bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0] = freightWrapper;
						}else{
						   bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.add(freightWrapper);
						}
					  
					}
              }
          }
          
           
          
          /**************** Fetching Cargo Information END******************/
          
          System.debug('bookingWrapper--->>>'+json.serialize(bookingWrapper)); 
          CustomerCommunity_RatingAPIResponse responseWrapper = new CustomerCommunity_RatingAPIResponse();
          //responseWrapper = BookingFCLRatesService.fetchUpdateBookingWrapper(bookingWrapper);
          //System.debug('responseWrapper---->>>'+json.serialize(responseWrapper)); 
     
       /*  
           
          
           String startLocation = portInfoOrigin.Softship_Zipcodes__c ;
           String endLocation = portInfoDest.Softship_Zipcodes__c;  */
           
       
           return '';  
       }
       catch(Exception e){
           throw e;
       }
   }
    
     @AuraEnabled
   public static CustomerCommunity_RatingAPIResponse handleGetRatesTest(id bookingId){
       String bookingNumber = [SELECT Booking_Number__c FROM Booking__c WHERE id =: bookingId].Booking_Number__c;
       String requestString = BookingManager.handleGetRates(bookingNumber);
       CustomerCommunity_RatingAPIRequest requestWrapper = (CustomerCommunity_RatingAPIRequest)JSON.deserialize(requestString,CustomerCommunity_RatingAPIRequest.class);
       CustomerCommunity_RatingAPIResponse response = BookingFCLRatesService.sendRatingRequest(requestWrapper);
       return response; 
   }
}