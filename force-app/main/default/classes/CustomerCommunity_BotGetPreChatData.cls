public without sharing class CustomerCommunity_BotGetPreChatData {
    public class PrechatOutput{
        @InvocableVariable
        public String sFirstName;
        @InvocableVariable
        public String sLastName;
        @InvocableVariable
        public String sEmail;
        @InvocableVariable
        public String sContactID;
        @InvocableVariable
        public String sLoggedUser;
    }
    public class PrechatInput{
        @InvocableVariable
        public String sChatKey;
    }
    @InvocableMethod(label='Get SnapIns Prechat Data')
    public static List<PrechatOutput> getSnapInsPrechatData(List<PrechatInput> inputParameters){
        String sChatKey = inputParameters[0].sChatKey;
        String sContactId = Null;
        List<prechatoutput> listOutputParameters = new List<prechatoutput>();
        PrechatOutput outputParameter = new PrechatOutput();
        outputParameter.sLoggedUser = CustomerCommunity_Constants.EMPTY_STRING;
        outputParameter.sFirstName = CustomerCommunity_Constants.EMPTY_STRING;
        outputParameter.sLastName = CustomerCommunity_Constants.EMPTY_STRING;
        outputParameter.sEmail = CustomerCommunity_Constants.EMPTY_STRING;
        outputParameter.sContactId = CustomerCommunity_Constants.EMPTY_STRING;
        System.debug(inputParameters);
        try{
            if (sChatKey != null && sChatKey != CustomerCommunity_Constants.EMPTY_STRING){
                List<LiveChatTranscript> transcripts = [SELECT Id, CaseId, ContactId, Username__c FROM LiveChatTranscript WHERE ChatKey = :sChatKey];
                
                if (transcripts.size()>0) {
                    sContactId = transcripts[0].ContactId;
                    outputParameter.sLoggedUser = outputParameter.sLoggedUser + transcripts[0].Username__c;
                }
            }
            
            if (sContactId != Null && sContactId != CustomerCommunity_Constants.EMPTY_STRING){
                List<Contact> contacts = [SELECT Id, FirstName, LastName, Email  FROM Contact WHERE Id = :sContactId];
                if (contacts.size()>0){
                    outputParameter.sFirstName = outputParameter.sFirstName + contacts[0].FirstName;
                    outputParameter.sLastName = outputParameter.sLastName + contacts[0].LastName;
                    outputParameter.sEmail = outputParameter.sEmail + contacts[0].Email;
                    outputParameter.sContactId = outputParameter.sContactId + contacts[0].Id;
                }
            }
            System.debug(listOutputParameters);
            listOutputParameters.add(outputParameter);
            return listOutputParameters;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            listOutputParameters.add(outputParameter);
            return listOutputParameters;
        }
    }
}