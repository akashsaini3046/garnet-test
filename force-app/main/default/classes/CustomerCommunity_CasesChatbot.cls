public class CustomerCommunity_CasesChatbot {
    public class CaseParameters {
        @InvocableVariable(required=true)
        public String sCaseNumber;
    }
    public class ResponseData {
        @InvocableVariable(required=true)
        public String sResponseStatus;
    }
    @InvocableMethod(label='Find Case Status')
    public static List<ResponseData> GetCaseStatus(List<CaseParameters> listCaseParams) {
        List<ResponseData> listResponses = new List<ResponseData>();
        ResponseData caseResponse = new ResponseData();
        caseResponse.sResponseStatus = CustomerCommunity_Constants.EMPTY_STRING;
        List<Case> listCases = new List<Case>();
        try{
            listCases = [SELECT CaseNumber, Status, Owner.Name, isClosed FROM Case WHERE CaseNumber = :listCaseParams[0].sCaseNumber LIMIT 1];
            if(!listCases.isEmpty()){
                if(listCases[0].isClosed){
                    caseResponse.sResponseStatus = CustomerCommunity_Constants.CASE_STRING + listCases[0].CaseNumber + CustomerCommunity_Constants.CASECLOSED_MSG + listCases[0].Status + CustomerCommunity_Constants.SINGLEQUOTE_CHARACTER;
                }
                else{
                    caseResponse.sResponseStatus = CustomerCommunity_Constants.CASE_STRING + listCases[0].CaseNumber + CustomerCommunity_Constants.CASEOPEN_MSG + listCases[0].Status + CustomerCommunity_Constants.SINGLEQUOTE_CHARACTER;
                }
            }
            else{
                caseResponse.sResponseStatus = CustomerCommunity_Constants.CASENOTFOUND_MSG + listCaseParams[0].sCaseNumber;
            }
            listResponses.add(caseResponse);
            return listResponses;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            caseResponse.sResponseStatus = Label.CustomerCommunity_CaseErrorMessage;
            listResponses.add(caseResponse);
            return listResponses;
        }
    }
}