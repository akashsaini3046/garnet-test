public class CustomerCommunity_ShipmentController {
    @AuraEnabled
    public static List<CustomerCommunity_ShipmentInformation> getShipmentInfo(List<String> listTrackingIDs) {
        List<CustomerCommunity_ShipmentInformation> listTrackingInfo = new List<CustomerCommunity_ShipmentInformation>();
        String response, URL = CustomerCommunity_Constants.EMPTY_STRING, ServiceName = CustomerCommunity_Constants.ST_SERVICE_NAME, diffChar = CustomerCommunity_Constants.DIFF_CHAR;
        for(Integer i=0; i<listTrackingIDs.size()-1; i++) {
            URL = URL + listTrackingIDs[i] + diffChar;
        }
        URL = URL + listTrackingIDs[listTrackingIDs.size()-1];
        System.debug('URL-->'+URL);
        try {
            response = CustomerCommunity_APICallouts.getAPIResponse(ServiceName, Null, URL);
            System.debug('response 1-> '+response);
            if(response != Null && response != CustomerCommunity_Constants.EMPTY_STRING) {
                response = response.substring(1, response.length()-1).replaceAll(CustomerCommunity_Constants.BACKSLASH_STRING,CustomerCommunity_Constants.EMPTY_STRING).replaceAll(CustomerCommunity_Constants.DATE_STRING,CustomerCommunity_Constants.DATEINFO_STRING).replaceAll(CustomerCommunity_Constants.NUMBER_STRING,CustomerCommunity_Constants.NUMBERINFO_STRING);
            }
            System.debug('response 2-> '+response);
            listTrackingInfo = (List<CustomerCommunity_ShipmentInformation>)JSON.deserialize(response, List<CustomerCommunity_ShipmentInformation>.class);
            System.debug('listTrackingInfo--> '+listTrackingInfo);
            if(listTrackingInfo != null){
                Integer count = 1;
                for(CustomerCommunity_ShipmentInformation track : listTrackingInfo) {
                    track.equipmentBooked = CustomerCommunity_Constants.ZERO_STRING;
                    track.equipmentReceived = CustomerCommunity_Constants.ZERO_STRING;
                    track.sequence = count++;
                    if(track.statusDate == CustomerCommunity_Constants.EMPTY_STRING)
                        track.statusDate = CustomerCommunity_Constants.HYPHEN_STRING;
                    if(track.voyage.vessel == CustomerCommunity_Constants.EMPTY_STRING && track.voyage.numberInfo == CustomerCommunity_Constants.EMPTY_STRING)
                        track.voyage.vessel = CustomerCommunity_Constants.HYPHEN_STRING;
                    if(track.discharge.dateInfo == CustomerCommunity_Constants.EMPTY_STRING)
                        track.discharge.dateInfo = CustomerCommunity_Constants.HYPHEN_STRING;
                    if(track.discharge.location == CustomerCommunity_Constants.EMPTY_STRING)
                        track.discharge.location = CustomerCommunity_Constants.HYPHEN_STRING;
                }
                return listTrackingInfo;
            }
            else {
                return null;
            }
        }
        catch(Exception ex){
            System.debug('Exception-> '+ex.getMessage() + ' ** '+ex.getCause());
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        } 
    }
    
    // Updates the status field of Header to "Available for Review"
    public static void updateHeaderStatus (List<Attachment> listAttachment) {
        try {
            List<Header__c> listHeader = new List<Header__c>();
            Set<Id> setOfParentId = new Set<Id>();
            for(Attachment attach:listAttachment){ 
                if(attach.name.contains('.xml')&& String.valueof(attach.ParentId).startsWith('a09')){
                    setOfParentId.add(attach.ParentId);
                }   
            }	
            for(Header__c header:[Select Status__c from Header__c where id in :setOfParentId]){	
                header.Status__c = 'Available for Review';
                listHeader.add(header);
            }
            if(!listHeader.isEmpty() && listHeader!=NULL){
                System.debug('header list updated ---->' +listHeader);
                update listHeader;
            }
        }
        catch(Exception ex){
            System.debug('Exception-> '+ex.getMessage() + ' ** '+ex.getCause());
            ExceptionHandler.logApexCalloutError(ex);
        }
    }
    
    @AuraEnabled
    public static List<CustomerCommunity_CargoShipmentWrapper.CustomerCommunity_MilestonesInfo> getCWShipmentInfo(String shipmentType, String trackingId) {   
        CustomerCommunity_CargoShipmentWrapper listOfShipmentResponse = new CustomerCommunity_CargoShipmentWrapper();
        List<CustomerCommunity_CargoShipmentWrapper.CustomerCommunity_MilestonesInfo> listOfMileStones = new List<CustomerCommunity_CargoShipmentWrapper.CustomerCommunity_MilestonesInfo>();
        Set<String> setOfShipmentTypes=new Set<String>{'ForwardingShipment','WarehouseReceive','WarehouseOrder'};
            String URL = 'https://dev-cargowise-sailingschedule.us-e1.cloudhub.io/v1/track?moduleKey='+TrackingId.trim()+'&moduleType='+ShipmentType.trim();
        system.debug('request URL--->'+URL);
        HttpRequest request = new HttpRequest();
        request.setEndpoint(URL);
        request.setMethod('GET');
        request.setHeader('client_id','f1bd172b39184ab2bbb92c89d6e78d17');
        request.setHeader('client_secret','7a09ca2CE3cC473BAaB07d90ec29D902');
        
        Http service = new Http();
        HttpResponse response = service.send(request);
        String xmlBody = response.getBody();
        System.debug('xmlBody-->'+xmlBody);
        xmlBody = xmlBody.substringBefore('<NoteCollection Content="Partial">')+xmlBody.substringAfter('</NoteCollection>');
        String jsonContent = CustomerCommunity_ShipmentXMLParser.xmlToJson(xmlBody);
        System.debug('jsonContent-->'+jsonContent);
        listOfShipmentResponse = (CustomerCommunity_CargoShipmentWrapper) JSON.deserialize(jsonContent, CustomerCommunity_CargoShipmentWrapper.class);
        System.debug('listOfShipmentResponse-->'+listOfShipmentResponse);
        CustomerCommunity_CargoShipmentWrapper.DataSourceCollection dataSourceColl=listOfShipmentResponse.dataSourceCollection;
        for(CustomerCommunity_CargoShipmentWrapper.MilestoneCollection mileSton:listOfShipmentResponse.dataSourceCollection.MilestoneCollection){
            CustomerCommunity_CargoShipmentWrapper.CustomerCommunity_MilestonesInfo milestoneInfo = new CustomerCommunity_CargoShipmentWrapper.CustomerCommunity_MilestonesInfo();    
            milestoneInfo.ParentJob=dataSourceColl.Key;
            milestoneInfo.Description=mileSton.Description;
            if(mileSton.ActualDate!=null){
                milestoneInfo.Shipmentdate=mileSton.ActualDate;
                milestoneInfo.Status='Completed';
            }
            else{
                milestoneInfo.Shipmentdate=null;
                milestoneInfo.Status='';
            }
            if(mileSton.ActualDate!=null && mileSton.EstimatedDate!=null && mileSton.ActualDate>mileSton.EstimatedDate){
                milestoneInfo.Status='Completed Late';
            }
            if(mileSton.EstimatedDate!=null && mileSton.ActualDate==null){
                milestoneInfo.Status='Pending';
            }
            listOfMileStones.add(milestoneInfo);
        }
        if(listOfShipmentResponse.dataSourceCollection.dataSource!=null){
            CustomerCommunity_CargoShipmentWrapper.DataSource dataSourceVar=listOfShipmentResponse.dataSourceCollection.dataSource;
            for(CustomerCommunity_CargoShipmentWrapper.MilestoneCollection dataContext:listOfShipmentResponse.dataSourceCollection.dataSource.MilestoneCollection){
                System.debug('dataContext-->'+dataContext);
                CustomerCommunity_CargoShipmentWrapper.CustomerCommunity_MilestonesInfo subMilestoneInfo = new CustomerCommunity_CargoShipmentWrapper.CustomerCommunity_MilestonesInfo();
                subMilestoneInfo.ParentJob=dataSourceVar.key;
                subMilestoneInfo.Description=dataContext.Description;
                if(dataContext.ActualDate!=null){
                    subMilestoneInfo.Shipmentdate=dataContext.ActualDate;
                    subMilestoneInfo.Status='Completed';
                }
                else{
                    subMilestoneInfo.Shipmentdate=null;
                    subMilestoneInfo.Status='';
                }
                if(dataContext.ActualDate!=null && dataContext.EstimatedDate!=null && dataContext.ActualDate>dataContext.EstimatedDate){
                    subMilestoneInfo.Status='Completed Late';
                }
                if(dataContext.EstimatedDate!=null && dataContext.ActualDate==null){
                    subMilestoneInfo.Status='Pending';
                }
                listOfMileStones.add(subMilestoneInfo);	
            }
        }
        
        return listOfMileStones;
    }
    
    // Updates Shorten_URL__c field on Header Object 
    @future(callout=true)
    public static void updateShortenURL(Set<Id> setHeaderId) {
        try {
            List<Header__c> listHeader = new List<Header__c>();
            for(Header__c header : [Select ID,Shorten_URL__c from Header__c WHERE id  =:setHeaderId]){
                String currentURL=CustomerCommunity_Constants.COMMUNITY_URL+'header/'+header.id;
                Http http1 = new Http();
                HttpRequest httpReq1 = new  HttpRequest();
                String authorizationToken = '70ff8aaa322f6c85861255c1b8851bd4a3170ea7';
                httpReq1.setEndpoint('https://api-ssl.bitly.com/v3/shorten?access_token='+authorizationToken+'&longUrl='+
                                     EncodingUtil.urlEncode('https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/header/'+header.id,'UTF-8'));
                httpReq1.setMethod('GET');
                httpReq1.setHeader('Authorization',authorizationToken);  
                httpReq1.setHeader('Content-Type','application/json');
                HttpResponse httpRes1 = http1.send( httpReq1 );				
                System.debug('httpResponse ---->'+httpRes1.getBody());     
                CustomerCommunity_BitlyResponse bitlyResponse = (CustomerCommunity_BitlyResponse)JSON.deserialize(httpRes1.getBody(), CustomerCommunity_BitlyResponse.class); 
                System.debug('bitly created url --->'+bitlyResponse.data.url);   
                header.Shorten_URL__c = bitlyResponse.data.url;
                listHeader.add(header);                
            }
            if(!listHeader.isEmpty() && listHeader != NULL){
            	update listHeader;
           }
        }
        catch(Exception ex){
            System.debug('Exception-> '+ex.getMessage() + ' ** '+ex.getCause()); 
            ExceptionHandler.logApexCalloutError(ex); 
        }
    }
    // Updates the shippingStatus__c and Shorten_URL__c field on Booking according to it's child object(Header).
    public static void updateBookingFields(List<Header__c> listHeader){
        try {   
            List<Booking__c> listBooking = new List<Booking__c>();
            Map<ID,Header__c> mapHeader = new Map<ID,Header__c>();
            Set<Id> headerIds= new Set<Id>();
            system.debug('listheader-->'+listHeader);
            for(Header__c objHeader : listHeader){
                mapHeader.put(objHeader.Booking__c,objHeader);
                headerIds.add(objHeader.Id);
            }
            //getChargeLineItems(headerIds);
            for(Booking__c objBooking :[Select id,Shorten_URL__c,shippingStatus__c from Booking__c where id in :mapHeader.keyset()]){
                objBooking.Shorten_URL__c = mapHeader.get(objBooking.Id).Shorten_URL__c;
                objBooking.shippingStatus__c = mapHeader.get(objBooking.Id).Status__c;
                listBooking.add(objBooking);
                //getChargeLineItems(objBooking.Id,mapHeader.get(objBooking.Id).Id);
            } 
            if(!listBooking.isEmpty() && listBooking != NULL){
                update listBooking; 
            }
            
        }   
        catch(Exception ex){
            System.debug('Exception-> '+ex.getMessage() + ' ** '+ex.getCause()); 
            ExceptionHandler.logApexCalloutError(ex); 
      } 
   }
    @future(callout=True)
    public static void getChargeLineItems(Id IdBooking,Id headerId){
        
        Booking__c bookingRec = new Booking__c();
        Shipment__c shipmentRec = new Shipment__c();
        Commodity__c commodityRec = new Commodity__c();
        FreightDetail__c freightDetailRec = new FreightDetail__c();
        Requirement__c requirementRec = new Requirement__c();
        Voyage__c voyageRec = new Voyage__c();
        Map<String, String> mapCommodityNameCategory = new Map<String, String>();
        List<New_Booking_Commodities__mdt> listCommoditiesRecords = new List<New_Booking_Commodities__mdt>();
        

        
        bookingRec = [Select Name,Destination_Type__c,Origin_Type__c,Customer_Destination_Code__c,/*Date_of_Discharge__c,*/
                                 Total_Volume_Unit__c,Total_Weight_Unit__c,Customer_Origin_Code__c from Booking__c 
                                 where id = :IdBooking];
        shipmentRec = [Select Origin_Code__c,Destination_Code__c from Shipment__c where Booking__c = :bookingRec.Id];
        voyageRec = [Select Estimate_Sail_Date__c from Voyage__c where Shipment__c = :shipmentRec.Id];
        freightDetailRec = [Select id from FreightDetail__c where Shipment__c  = :shipmentRec.Id];
        commodityRec = [Select Name from Commodity__c where Freight__c = :freightDetailRec.Id];
        requirementRec = [Select Container_Type__c,Category__c from Requirement__c where Freight__c = :freightDetailRec.Id];
        List<TLIEquipType__c> tLEquipType = [Select Id,Name from TLIEquipType__c where Header__c=:headerId Limit 1];
        requirementRec.Category__c = 'Cargo, NOS';
        listCommoditiesRecords = [SELECT MasterLabel, Commodity_Name__c, Category__c FROM New_Booking_Commodities__mdt WHERE Category__c != Null AND Category__c != ''];
        if(!listCommoditiesRecords.isEmpty()){
            for(New_Booking_Commodities__mdt currentCommodity : listCommoditiesRecords){
                if(currentCommodity.Commodity_Name__c != Null && currentCommodity.Commodity_Name__c != '' && currentCommodity.Category__c != NULL && currentCommodity.Category__c != '')
                   mapCommodityNameCategory.put(currentCommodity.Category__c, currentCommodity.Commodity_Name__c); 
            }
        }
        
        

        List<Commodity__c> listCommodity = new List<Commodity__c>();
        List<CustomerCommunity_BookingRecord.ContainerCargoDetail> listCargoDetail = new List<CustomerCommunity_BookingRecord.ContainerCargoDetail>();
        CustomerCommunity_BookingRecord bookingRecordValues = new CustomerCommunity_BookingRecord();
        bookingRecordValues.bookingRequestNumber = bookingRec.Name;
        bookingRecordValues.bookingType = 'FCL';
        bookingRecordValues.deliveryTermVal = 'P';
        bookingRecordValues.receiptTermVal = 'D';
        bookingRecordValues.estSailingDate = voyageRec.Estimate_Sail_Date__c;
        bookingRecordValues.originCode = shipmentRec.Origin_Code__c;
        bookingRecordValues.destinationCode = shipmentRec.Destination_Code__c;
        bookingRecordValues.totalVolumeUnit = '100';
        bookingRecordValues.totalWeightUnit = '100';        
        
        CustomerCommunity_BookingRecord.ContainerCargoDetail containerCargoRecordValues = new CustomerCommunity_BookingRecord.ContainerCargoDetail();
        containerCargoRecordValues.sequenceId = 1;
        containerCargoRecordValues.UniqueId = 'commodity1';
        containerCargoRecordValues.isHazardous = false;
        containerCargoRecordValues.freightRecord = freightDetailRec;
        containerCargoRecordValues.requirementRecord = requirementRec;
        listCommodity.add(commodityRec);
		containerCargoRecordValues.listCommodityRecords = listCommodity; 
        listCargoDetail.add(containerCargoRecordValues);             

        bookingRecordValues.listCargoDetails = listCargoDetail;
        String bookingRecordString = JSON.serialize(bookingRecordValues);
        System.debug('bookingRecordString --->'+ bookingRecordString);
        String jsonChargeLineItem = CustomerCommunity_NewBookingsController.getSoftshipRates(bookingRecordString,'ChargeLineItem1');
        
        if(tLEquipType.size()==0){
            tLEquipType.add(new TLIEquipType__c(Header__c=headerId));
            //insert tLEquipType;
        }
        List<Charge_Line__c> listChargeLineItem = (List<Charge_Line__c>) JSON.deserialize (jsonChargeLineItem,List<Charge_Line__c>.class);
        for(Charge_Line__c chargeLine : listChargeLineItem){
            chargeLine.TLI_Equip_Type__c = tLEquipType[0].Id;
        }
        //insert listChargeLineItem;
    }
    
    @future(callout=True)
    public static void getChargeLineItems(Set<Id> headerIds){
		List<Charge_Line__c> chargeLineItems = new List<Charge_Line__c>();
        Map<String,Ports_Information__c> mapPlaceNamePortLocation = new Map<String,Ports_Information__c>();
        Map<String,Ports_Information__c> mapNamePortLocation= new Map<String,Ports_Information__c>();
        Set<String> setEquipmentNumber= new Set<String>();
        List<TLIEquipType__c> listTLIEquipment= new List<TLIEquipType__c>([Select Header__r.Id,Header__r.Name,Header__r.Booking_Number_Text__c,Header__r.Created_By_Portal_User__c, Header__r.Booking__r.Account__c,
                                                                           Header__r.Discharge__c,Header__r.Final_Destination__c,Header__r.Origin__c, Header__r.Load_Port__c,Header__r.Voyage_Number__c,Header__r.Customer_Sent_Date__c, (Select Id,Id__c, Name,Prefix__c, Quantity__c From Equipments__r)
                                                                           From TLIEquipType__c where Header__c=:headerIds]);
        
        for(TLIEquipType__c tliEquipType: listTLIEquipment){
            if(!String.isEmpty(tliEquipType.Header__r.Origin__c)){
                mapPlaceNamePortLocation.put(tliEquipType.Header__r.Origin__c.split(',')[0].toLowerCase(), new Ports_Information__c(Softship_Zipcodes__c=''));
            }else{
                mapNamePortLocation.put(tliEquipType.Header__r.Load_Port__c.split(',')[0].toLowerCase(), new Ports_Information__c(Location_Code__c=''));
            }
            
            if(!String.isEmpty(tliEquipType.Header__r.Final_Destination__c)){
                mapPlaceNamePortLocation.put(tliEquipType.Header__r.Final_Destination__c.split(',')[0].toLowerCase(), new Ports_Information__c(Softship_Zipcodes__c=''));
            }else{
                mapNamePortLocation.put(tliEquipType.Header__r.Discharge__c.split(',')[0].toLowerCase(), new Ports_Information__c(Location_Code__c=''));
            }
            for(Equipment_BOL__c equipment: tliEquipType.Equipments__r){
                if(!String.isEmpty(equipment.Id__c)){
                    setEquipmentNumber.add(equipment.Id__c);
                }
            }
        }
        Map<String, Softship_Container_Detail__c> mapSoftshipContainer= new Map<String, Softship_Container_Detail__c>();
        Set<String> sizeTypesSet = new Set<String>();
        for(Softship_Container_Detail__c container: [SELECT Id, Name, Size_Type__c, Company__c from Softship_Container_Detail__c where Name=:setEquipmentNumber]){
            mapSoftshipContainer.put(container.Name,container);
            sizeTypesSet.add(container.Size_Type__c);
        }
        for(Ports_Information__c portLocation : [select Id,Place_Name__c, Softship_Zipcodes__c from Ports_Information__c where  Place_Name__c =:mapPlaceNamePortLocation.keyset() And Softship_Zipcodes__c !='']){
            mapPlaceNamePortLocation.get(portLocation.Place_Name__c.toLowerCase()).Softship_Zipcodes__c = portLocation.Softship_Zipcodes__c;
        }
        
        for(Ports_Information__c portLocation :[select Id,Name,Place_Name__c, Location_Code__c from Ports_Information__c where Name=:mapNamePortLocation.keyset() AND Location_Code__c!='']){
            //mapNamePortLocation.get(portLocation.Name).Location_Code__c = portLocation.Location_Code__c;
            if(mapNamePortLocation.containsKey(portLocation.Name.toLowerCase())){
                mapNamePortLocation.get(portLocation.Name.toLowerCase()).Location_Code__c = portLocation.Location_Code__c;
            }
        }
        
        Map<Id,TLIEquipType__c> mapHeaderTLIEquipement= new Map<Id,TLIEquipType__c>();
        List<Charge_Line__c> listChargeLineItem = new List<Charge_Line__c>();
        for(TLIEquipType__c tliEquipType: listTLIEquipment){
            
            BookingWrapper bookingWrapper = BookingWrapper.getBookingWrapper();
            bookingWrapper.booking.Account__c = tliEquipType.Header__r.Booking__r.Account__c;
            bookingWrapper.booking.Contract_Number__c = '';
            bookingWrapper.booking.Ready_Date__c = Date.valueOf(tliEquipType.Header__r.Customer_Sent_Date__c == null ? System.now() : tliEquipType.Header__r.Customer_Sent_Date__c);
            
            if(!String.isEmpty(tliEquipType.Header__r.Origin__c)){
                System.debug(mapPlaceNamePortLocation.get(tliEquipType.Header__r.Origin__c.split(',')[0].toLowerCase()).Softship_Zipcodes__c);
                bookingWrapper.booking.Origin_Type__c = 'D';
                bookingWrapper.booking.Customer_Origin_Code__c = mapPlaceNamePortLocation.get(tliEquipType.Header__r.Origin__c.split(',')[0].toLowerCase()).Softship_Zipcodes__c;
            }else{
                System.debug(mapNamePortLocation.get(tliEquipType.Header__r.Load_Port__c.split(',')[0].toLowerCase()).Location_Code__c);
                bookingWrapper.booking.Origin_Type__c = 'P';
                bookingWrapper.booking.Customer_Origin_Code__c = mapNamePortLocation.get(tliEquipType.Header__r.Load_Port__c.split(',')[0].toLowerCase()).Location_Code__c; 
            }
            if(!String.isEmpty(tliEquipType.Header__r.Final_Destination__c)){
                System.debug(mapPlaceNamePortLocation.get(tliEquipType.Header__r.Final_Destination__c.split(',')[0].toLowerCase()).Softship_Zipcodes__c);
                bookingWrapper.booking.Destination_Type__c = 'D';
                bookingWrapper.booking.Customer_Destination_Code__c = mapPlaceNamePortLocation.get(tliEquipType.Header__r.Final_Destination__c.split(',')[0].toLowerCase()).Softship_Zipcodes__c;
            }else{
                System.debug(mapNamePortLocation.get(tliEquipType.Header__r.Discharge__c.split(',')[0].toLowerCase()).Location_Code__c);
                bookingWrapper.booking.Destination_Type__c = 'P'; 
                bookingWrapper.booking.Customer_Destination_Code__c = mapNamePortLocation.get(tliEquipType.Header__r.Discharge__c.split(',')[0].toLowerCase()).Location_Code__c;
            }
            if(tliEquipType.Equipments__r.size()>0){
                List<BookingWrapper.RequirementWrapper> requirementWrapperList = new List<BookingWrapper.RequirementWrapper>();

                for(Equipment_BOL__c equipment: tliEquipType.Equipments__r){
                    BookingWrapper.RequirementWrapper requirementWrapper = new BookingWrapper.RequirementWrapper();
                    String key='';
                    if(!String.isEmpty(equipment.Id__c)){
                        key = equipment.Id__c;
                        // If Container Type contains "RF" then Category "Refrigerated Cargo, Chilled or Frozen, NOS"
                        if(mapSoftshipContainer.containsKey(key) && String.isNotEmpty(mapSoftshipContainer.get(key).Size_Type__c)){
                            String containerType = mapSoftshipContainer.get(key).Size_Type__c;
                            if(containerType.length()>1 && containerType.substring(containerType.length()-1, containerType.length()).containsIgnoreCase('RF')){
                                requirementWrapper.commodityDesc = 'Refrigerated Cargo, Chilled or Frozen, NOS';
                                requirementWrapper.requirement = new Requirement__c(Category__c = 'Refrigerated Cargo, Chilled or Frozen, NOS',
                                                                                    Container_Type__c= containerType,
                                                                                    Quantity__c = equipment.Quantity__c);
                            }else{
                                requirementWrapper.commodityDesc = 'Cargo, NOS';
                                requirementWrapper.requirement = new Requirement__c(Category__c = 'Cargo, NOS',
                                                                                    Container_Type__c= containerType,
                                                                                    Quantity__c = equipment.Quantity__c);
                            }
                        }else{
                            requirementWrapper.commodityDesc = 'Cargo, NOS';
                            requirementWrapper.requirement = new Requirement__c(Category__c = 'Cargo, NOS',Container_Type__c='40DS',
                                                                                Quantity__c = 1);
                        }
                    }
                    requirementWrapperList.add(requirementWrapper);
                }
                bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper[0].listRequirementWrapper = requirementWrapperList;
            }
            bookingWrapper.shipment.listCargo[0].cargoType = 'Container';
            BookingFCLRatesService.callingSource = 'ShippingInstruction';
            CustomerCommunity_RatingAPIResponse responseWrapper = BookingFCLRatesService.fetchUpdateBookingWrapper(bookingWrapper);
            
            if(responseWrapper != null && responseWrapper.success){
                CustomerCommunity_RatingAPIResponse.result resultSelected = new CustomerCommunity_RatingAPIResponse.result();
                for(CustomerCommunity_RatingAPIResponse.result result : responseWrapper.result){
                    if(result.RouteId == 1){
                        resultSelected = result;
                    }
                }
                for(CustomerCommunity_RatingAPIResponse.DocValuesData docValues : resultSelected.CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].ValuesGroup[0].DocValuesData){
                    Charge_Line__c chargeLine = new Charge_Line__c();
                    chargeLine.Basis__c = String.valueof(docValues.Basis);
                    chargeLine.Charge_Code__c = docValues.ChargeCode;
                    chargeLine.Currency__c = docValues.CurrencyCode;
                    chargeLine.ChargeDescription__c = docValues.Description;
                    chargeLine.Per__c = docValues.Per;
                    chargeLine.Rate__c = docValues.Rate;
                    chargeLine.Line_Total__c = docValues.AmountTarget;
                    chargeLine.TLI_Equip_Type__c = tliEquipType.Id;
                    chargeLineItems.add(chargeLine);
                }
            }
        }
        if(chargeLineItems != null && !chargeLineItems.isEmpty()){
            insert chargeLineItems;
        }		
	}
}