public with sharing class EmailMessageTriggerHandler extends TriggerHandler{
    
    List<EmailMessage> newEmailMessageList;
    
    public EmailMessageTriggerHandler(){
        this.newEmailMessageList = (List<EmailMessage>) Trigger.new; 
    }
    public override void afterInsert(){
        updateOpportunityStage(newEmailMessageList);
    }
    
    //Update opportunity’s stage to ‘Negotiation/Review’ when ‘Send Quote to Customer From Opportunity’ email template is sent in ‘Price Proposal/Quote’ stage
    private void updateOpportunityStage (List<EmailMessage> newEmailMessageList)
    {
        Set<Id> oppIdSet = new Set<Id>();
        if(newEmailMessageList != NULL && !newEmailMessageList.isEmpty())
        {
            for(EmailMessage email: newEmailMessageList)
            {
                if(email.RelatedToId != NULL)
                    oppIdSet.add(email.RelatedToId);
            }
        }
        List<Opportunity> oppList = new List<Opportunity>();
        if(oppIdSet != NULL && !oppIdSet.isEmpty())
            oppList = [Select Id, StageName,Proposal_Submission_Date__c  from Opportunity where Id in: oppIdSet];
        Map<Id, Opportunity> map_Id_Opportunity = new Map<Id, Opportunity>();
        if(oppList != NULL && !oppList.isEmpty())
        {
            for(Opportunity opp: oppList)
            {
                if(opp.StageName == ConstantClass.PROPOSAL_PRICE_QUOTE_OPP_STAGE)
                    map_Id_Opportunity.put(opp.Id, opp);
            }
        }
        
        List<Opportunity> oppToUpdateList = new List<Opportunity>();
        if(newEmailMessageList != NULL && !newEmailMessageList.isEmpty())
        {
            for(EmailMessage email: newEmailMessageList)
            {
                String oppId ='';
                if(email.RelatedToId != NULL)
                    oppId = email.RelatedToId;
                if(String.isNotBlank(oppId))
                {
                    if(email.TextBody.contains('Send Quote '+oppId.substring(0, 15)))
                    {
                        if(map_Id_Opportunity.containsKey(email.RelatedToId))
                        {
                            Opportunity opp = map_Id_Opportunity.get(email.RelatedToId);
                            //opp.StageName = ConstantClass.NEGOTIATION_REVIEW_OPP_STAGE;
                            opp.Proposal_Submission_Date__c = System.TODAY();
                            oppToUpdateList.add(opp);
                        }
                    }
                }
            }
        }
        if(oppToUpdateList != NULL && !oppToUpdateList.isEmpty())
            update oppToUpdateList;
    }
}