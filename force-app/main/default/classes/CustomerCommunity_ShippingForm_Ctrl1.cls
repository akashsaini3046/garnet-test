public without sharing class CustomerCommunity_ShippingForm_Ctrl1 {
    @AuraEnabled
    public static List <Addresses__c> getAddresses(String idHeader) {
        try {
            List<Addresses__c> listAddressFinal = new List<Addresses__c>();
            Header__c objheader = [Select id from Header__c where id = : idHeader];
            List <Addresses__c> listAddress = [Select id ,Name, Contact_Name__c,Address__c, ID_Code__c, Type__c,ownerId, FMC_Tax_Id__c, Phone__c  from Addresses__c where Header__c = : objheader.Id];
            if (listAddress != NULL) {
                for(Addresses__c address : listAddress){
                    if(address.Name.equals(address.id)){
                        address.Name='';
                    }
                    listAddressFinal.add(address);
                }
                return listAddressFinal;  
                //return listAddress;
            } else {
                return NULL;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;
        }
    }
    @AuraEnabled
    public static List<Hazardous_Line__c> getHazardousLines(String idHeader) {
        try {
            Header__c objHeader = [Select Id,(SELECT Id, Booking_Number__c, Container_Number__c, Emergency_Contact_Name__c, Emergency__c, Flash_Point_Temperature__c, 
                                              Flash_Point_Temperature_Unit__c, IMO_Hazard_Class_Division__c, Limited_Quantity__c, Marine_Pollutant__c, Packaging_Group__c,
                                              Quantity__c, Quantity_Unit_of_Measure__c, Request_Number__c, Shipping_Name__c, Spil_Quantity__c, Technical_Name__c,
                                              UN_Number__c, UN_Prefix__c, Weight__c, Weight_Unit_of_Measure__c FROM Hazardous_Lines__r) from Header__c where Id = : idHeader LIMIT 1];
            if (objHeader != NULL) {
                if(objHeader.Hazardous_Lines__r != null && !objHeader.Hazardous_Lines__r.isEmpty()){
                    return objHeader.Hazardous_Lines__r;
                }
                return null;
            } else {
                return NULL;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;
        }
    }
    
    @AuraEnabled
    public static Header__c getHeader(String idHeader) {
        try {
            Header__c objHeader = [Select Name,Status__c,Customer_Sent_Date__c,Origin__c,Booking_Number_Text__c,Voyage_Number__c, Load_Port__c, Booking__r.Name,Booking__c, 
                                   Final_Destination__c, Discharge__c, Esker_Reference__c,ownerId, BL_Invoice_Number__c, Flag__c, INCO_Term__c, Loading_Pier__c,Booking_Number__c,Emergency_Contact_Name__c,
                                   Emergency_Contact_Number__c,Freight_Forwarder__c,Inland_Freight__c,Ocean_Freight__c,Container_Number__c,Order_Number__c,Proforma_Number__c,
                                   Originals_to_be_released_at__c, Origin_of_goods__c, Relay_Point__c, SCAC_Code__c, RecordTypeId from Header__c where id = : idHeader];
            if (objHeader != NULL) {
                return objHeader;
            } else {
                return NULL;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;
        }
    }
    @AuraEnabled
    public static String getHeaderRecordType(String idHeader) {
        try {
            Header__c objHeader = [Select RecordTypeId from Header__c where Id = : idHeader];
            RecordType aRecordType = [SELECT Id, SobjectType, DeveloperName FROM RecordType WHERE isActive = true AND Id = :objHeader.RecordTypeId LIMIT 1];
            if (aRecordType != NULL) {
                return aRecordType.DeveloperName;
            } else {
                return NULL;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;
        }
    }
    @AuraEnabled
    public static List <Equipment_BOL__c> getEquipments(String idHeader) {
        try {
            Header__c objHeader = [Select id from Header__c  where id = : idHeader];
            TLIEquipType__c objEquipType = [Select id from TLIEquipType__c  where Header__c = : objHeader.Id];
            List <Equipment_BOL__c> listEquipment = [Select Prefix__c,IsDeleted,CreatedDate,Cargo_Description__c,Name,Id__c, Seal__c,Weight__c,VGMWeight__c, Volume__c,ownerId,
                                                     Quantity__c, Temperature__c, TemperatureUOM__c, VGMWeightUOM__c, WeightUOM__c, VolumeUOM__c from Equipment_BOL__c where TLIEquipType__c = : objEquipType.Id];
            if (listEquipment != NULL) {
                return listEquipment;
            } else {
                return NULL;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;
        }
    }
    
    @AuraEnabled
    public static List <Charge_Line__c> getChargeLineItems(String idHeader) {
        try {
            TLIEquipType__c objEquipType = [Select id from TLIEquipType__c  where Header__c = : idHeader];
            List <Charge_Line__c> listChargeLineItem = [Select Id, Name, Basis__c, Per__c, Rate__c,Line_Total__c,ChargeDescription__c,Currency__c from Charge_Line__c where TLI_Equip_Type__c = : objEquipType.Id];
            if (listChargeLineItem != NULL) {
                return listChargeLineItem;
            } else {
                return NULL;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return NULL;
        }
    }
    @AuraEnabled
    public static String updateStatusToApprove(Header__c objHeader){
        try{
            Header__c header = [SELECT Id, Status__c FROM Header__c WHERE Id =: objHeader.Id LIMIT 1]; 
            header.Status__c = 'Approved By Customer';
            update header;
            return 'True';
        }catch(Exception ex){
            return 'False';
        }
    }
    
    @AuraEnabled
    public static String saveBolRecords(List <Addresses__c> listAddress, Header__c objHeader,List<Equipment_BOL__c> listEquipment,Boolean isStatusApprove, List<Hazardous_Line__c> listHazardousLine) {
        boolean result = true;
        System.debug('list of address --->' + listAddress);
        System.debug('size of address -->'+listAddress.size());
        List<Addresses__c> listAddressInsert = new List<Addresses__c>();
        List<Addresses__c> listAddressUpdate = new List<Addresses__c>();
        List<Addresses__c> listAddressInsertLink = new List<Addresses__c>();
        List<Addresses__c> listAddressDelete = new List<Addresses__c>();
        
        if(!listAddress.isEmpty()){
            for(Addresses__c objAddress : listAddress){ 
                if(objAddress.Id != null && objAddress.Address__c !=null && objAddress.Address__c.length() == 0 && objAddress.Name !=null && objAddress.Name.length()==0){
                    listAddressDelete.add(objAddress);
                }
                else if(objAddress.Id != null){
                    listAddressUpdate.add(objAddress);
                }
                else if(objAddress.Id == null && objAddress.Address__c!=null  && objAddress.Address__c.length() !=0){
                    listAddressInsert.add(objAddress); 
                }
            } 
        }
        System.debug('list delete records -->'+listAddressDelete);
        
        System.debug('listAddressInsert --->'+listAddressInsert);
        //Adding the new records entered by user
        if(!listAddressInsert.isEmpty()){
            for(Addresses__c objAddress : listAddressInsert){
                Addresses__c newAddress = new Addresses__c();
                newAddress.Name = objAddress.Name;
                newAddress.Address__c = objAddress.Address__c;
                newAddress.Type__c = objAddress.Type__c;
                newAddress.Header__c = objHeader.Id;
                listAddressInsertLink.add(newAddress);
            } 
        }
        
        System.debug('listAddressUpdate --->'+listAddressUpdate); 
        
        // Records to be updated
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        List <sObject> listSobject = new List <sObject> ();
        Map <ID, Sobject> mapIdWithSobject = getWrapperRecords(objHeader.Id);
        System.debug('mapIdWithSobject--->'+mapIdWithSobject);
        if(!listAddressUpdate.isEmpty()){
            for (Addresses__c objAddress: listAddressUpdate) {
                //using map to store record Id and the corresponding record
                Sobject sObj = mapIdWithSobject.get(objAddress.Id);
                System.debug('addressObj --->' +sObj);
                Schema.SObjectType SObjectTypeObj = schemaMap.get('Addresses__c');
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
                Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('AddressFields');
                List < Schema.FieldSetMember > listSchemaField = fieldSetObj.getFields();   
                for (Schema.FieldSetMember objFieldSetMember: listSchemaField) {
                    String fieldname = objFieldSetMember.getFieldPath();
                    System.debug('fieldNames address ->'+fieldname);
                    if (sObj.get(fieldname) != objAddress.get(fieldname)) {
                        listSobject.add(objAddress);
                        break;
                    }
                }
            }
        }
        System.debug('listSobject adresssss---->'+listSobject);
        System.debug('frontend--->'+objHeader);
        Sobject sObj = mapIdWithSobject.get(objHeader.Id);
        System.debug('HeaderObj --->' +sObj);
        Schema.SObjectType SObjectTypeObj = schemaMap.get('Header__c');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get('HeaderFields');
        List < Schema.FieldSetMember > listSchemaField = fieldSetObj.getFields();
        if(isStatusApprove == true){ 
            objHeader.Status__c = 'Approved By Customer';
            listSobject.add(objHeader);
        }
        else{
            if(listSchemaField != NULL && !listSchemaField.isEmpty()){
                for (Schema.FieldSetMember objFieldSetMember: listSchemaField) {
                    String fieldname = objFieldSetMember.getFieldPath();
                    if (sObj.get(fieldname) != objHeader.get(fieldname)) {
                        listSobject.add(objHeader);
                        break;
                    }
                }
            }
        }
        if(listEquipment != NULL && !listEquipment.isEmpty()){
            for(Equipment_BOL__c objEquipment: listEquipment){ 
                Sobject sObj1 = mapIdWithSobject.get(objEquipment.Id);
                Map <String, Schema.SObjectField> fieldMap1 = schemaMap.get('Equipment_BOL__c').getDescribe().fields.getMap();
                for(Schema.SObjectField sfield : fieldMap1.Values()){
                    String fieldname =  sfield.getDescribe().getname();
                    if(sObj1.get(fieldname) != objEquipment.get(fieldname)){
                        listSobject.add(objEquipment);
                        break;   
                    }
                } 
            } 
        }
        if(listHazardousLine != NULL && !listHazardousLine.isEmpty()){
            for(Hazardous_Line__c objHazardousLine: listHazardousLine){ 
                Sobject sObj1 = mapIdWithSobject.get(objHazardousLine.Id);
                Map <String, Schema.SObjectField> fieldMap1 = schemaMap.get('Hazardous_Line__c').getDescribe().fields.getMap();
                for(Schema.SObjectField sfield : fieldMap1.Values()){
                    String fieldname =  sfield.getDescribe().getname();
                    if(sObj1.get(fieldname) != objHazardousLine.get(fieldname)){
                        listSobject.add(objHazardousLine);
                        break;   
                    }
                } 
            } 
        }
        // Insert the Address and Link with the header
        Database.SaveResult[] listInsertLinkSr = Database.insert(listAddressInsertLink, false);
        for (Database.SaveResult sr: listInsertLinkSr) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted Sobject : ' + sr.getId());
                
            } else {
                for (Database.Error err: sr.getErrors()) {
                    
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Sobjects fields that affected this error: ' + err.getFields());       
                }
                result = false;
            }
        }
        
        // Delete the Address
        Database.DeleteResult[] listDeleteSr = Database.delete(listAddressDelete,false);
        for (Database.DeleteResult sr: listDeleteSr) {
            if (sr.isSuccess()) {
                System.debug('Successfully Deleted Sobject : ' + sr.getId());
                
            } else {
                for (Database.Error err: sr.getErrors()) {
                    
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Sobjects fields that affected this error: ' + err.getFields());       
                }
                result = false;
            }
        }
        // update the Shipping Instructions , Addresses and Equipment records
        Database.SaveResult[] srList = Database.update(listSobject, false);
        for (Database.SaveResult sr: srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully updated Sobject : ' + sr.getId());
                
            } else {
                for (Database.Error err: sr.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Sobjects fields that affected this error: ' + err.getFields());
                    
                }
                result = false;
            }
        }
        if(result){
            return 'TRUE';
        }
        else{
            return 'FALSE';
        }
    }
    
    @AuraEnabled
    public static CustomerCommunity_BookingRecord getBookingRecordValues(Header__c objHeader){
        
        CustomerCommunity_BookingRecord bookingRecordValues = new CustomerCommunity_BookingRecord();
        Booking__c bookingRec = new Booking__c();
        Shipment__c shipmentRec = new Shipment__c();
        FreightDetail__c freightDetailRec = new FreightDetail__c();
        Requirement__c requirementRec = new Requirement__c();
        Voyage__c voyageRec = new Voyage__c();
        List<Party__c> partyList = new List<Party__c>();
        List<Commodity__c> commodityList = new List<Commodity__c>();
        List<Contact> contactList = new List<Contact>();
        Set<String> partyCodes = new Set<String>();
        Set<Id> setAddressId = new Set<Id>();
        Map<Id, Address__c> mapAddressRecords;
        Map<String, Id> mapCVIFAddressId = new Map<String, Id>();
        Map<String, Contact> mapCVIFContactRecords = new Map<String, Contact>();
        
        bookingRec = [Select Name, Destination_Type__c, Status__c, Origin_Type__c, Customer_Destination_Code__c, Date_of_Discharge__c, Total_Weight__c,	Total_Volume__c,
                      Total_Volume_Unit__c, Total_Weight_Unit__c, Contact__c, Customer_Origin_Code__c, Container_Mode__c from Booking__c 
                      where id = :objHeader.Booking__c];
        partyList = [Select id ,CVIF__c,Party_Id__c,Type__c from Party__c where Booking__c = :bookingRec.Id];
        shipmentRec = [Select Origin_Code__c,Destination_Code__c from Shipment__c where Booking__c = :bookingRec.Id];
        freightDetailRec = [Select id from FreightDetail__c where Shipment__c  = :shipmentRec.Id];
        requirementRec = [Select Container_Type__c,Category__c from Requirement__c where Freight__c = :freightDetailRec.Id];
        commodityList = new List<Commodity__c>([Select Name,Emergency_Contact_Name__c,Emergency_Contact_Number__c,Number__c,Package_Group__c,Technical_Name__c,
                                                Suffix__c, Primary_Class__c, IMO_Class__c, Phone_Number__c, Name__c 
                                                from Commodity__c where Is_Hazardous__c = true AND Freight__c = :freightDetailRec.Id]);
        voyageRec = [Select Estimate_Sail_Date__c from Voyage__c where Shipment__c = :shipmentRec.Id Limit 1];
        if(commodityList != null && !commodityList.isEmpty()){
			delete commodityList;          
        }
        // Map Hazardous Line to Commodity
        Header__c header = [Select Id, (Select Id, Name, Booking_Number__c, Container_Number__c, Emergency_Contact_Name__c, Emergency__c, Flash_Point_Temperature__c,
                             Flash_Point_Temperature_Unit__c, IMO_Hazard_Class_Division__c, Limited_Quantity__c, Marine_Pollutant__c, Packaging_Group__c, Quantity__c,
                            Quantity_Unit_of_Measure__c, Request_Number__c, Shipping_Name__c, Spil_Quantity__c, Technical_Name__c, UN_Number__c, UN_Prefix__c,
                            Weight__c, Weight_Unit_of_Measure__c FROM Hazardous_Lines__r) FROM Header__c WHERE Id = :objHeader.Id];
        Id substanceRecordTypeId = getRecordTypeId('Substance__c:Substance');
        LIST<Substance__c> substances = new LIST<Substance__c>([SELECT Id, Suffix__c,Name, Primary_Class__c, Substance_Name__c, Variation__c, Prefix__c,
                                              	Packing_Group__c, Secondary_Class__c, Tertiary_Class__c
                                              FROM Substance__c WHERE RecordTypeId = :substanceRecordTypeId  ]);
        Map<String, String> mapUNPackGrpClassVsSuffix = new Map<String, String>();
        for(Substance__c sub : substances){
            String combination = sub.Prefix__c + sub.Name +';'+sub.Packing_Group__c+';'+sub.Primary_Class__c ;
			mapUNPackGrpClassVsSuffix.put(combination.toUpperCase() , sub.Suffix__c);            
        }
        List<Commodity__c> commodities = new List<Commodity__c>();
        if(freightDetailRec != null){
            for(Hazardous_Line__c hazardousLine : header.Hazardous_Lines__r){
                Commodity__c commodity = new Commodity__c();
                commodity.Is_Hazardous_Line_Commodity__c = true;
                commodity.Name = hazardousLine.Name;
                commodity.Freight__c = freightDetailRec.Id;
                commodity.Emergency_Contact_Name__c = hazardousLine.Emergency_Contact_Name__c;
                commodity.Emergency_Contact_Number__c = hazardousLine.Emergency__c;
                commodity.Package_Group__c = hazardousLine.Packaging_Group__c;
                commodity.Technical_Name__c = hazardousLine.Technical_Name__c;
                //commodity.Number__c = '0004';
                commodity.Number__c = hazardousLine.UN_Number__c;
                commodity.Primary_Class__c = hazardousLine.IMO_Hazard_Class_Division__c;
                commodity.IMO_Class__c = hazardousLine.IMO_Hazard_Class_Division__c;
                commodity.Is_Hazardous__c = true;
                //commodity.Suffix__c = '1';
                String combination = hazardousLine.UN_Prefix__c + hazardousLine.UN_Number__c + ';'+ hazardousLine.Packaging_Group__c + ';' + hazardousLine.IMO_Hazard_Class_Division__c ;
                commodity.Suffix__c = (mapUNPackGrpClassVsSuffix.get(combination.toUpperCase()) != null ? mapUNPackGrpClassVsSuffix.get(combination.toUpperCase()) : '');
                commodities.add(commodity);
            }
        }
        if(commodities != null && !commodities.isEmpty()){
            insert commodities;
        }
        commodityList = new List<Commodity__c>([Select Name,Emergency_Contact_Name__c,Emergency_Contact_Number__c,Number__c,Package_Group__c,Technical_Name__c,
                                                Suffix__c, Primary_Class__c, IMO_Class__c, Phone_Number__c, Name__c 
                                                from Commodity__c where Is_Hazardous__c = true AND Is_Hazardous_Line_Commodity__c = true AND Freight__c = :freightDetailRec.Id]);
        for(Party__c partyRecs : partyList)
            partyCodes.add(partyRecs.CVIF__c);
        
        contactList = [SELECT Id, Name, Phone, MobilePhone, Fax, Address__c, Account.CVIF__c FROM Contact WHERE Account.CVIF__c IN :partyCodes];
        for(Contact con: contactList){
            mapCVIFContactRecords.put(con.Account.CVIF__c,con);
            mapCVIFAddressId.put(con.Account.CVIF__c,con.Address__c);
            setAddressId.add(con.Address__c);
        }
        mapAddressRecords= new Map<Id, Address__c>([SELECT Id, Name, Address_Line_2__c, Address_Line_3__c, City__c, State_Picklist__c, Postal_Code__c, Country__c, 
                                                    CVIF_Location_Id__c FROM Address__c WHERE Id IN :setAddressId]);
        
        for(Party__c partyRecord : partyList){
            if(partyRecord.Type__c=='CUST'){
                bookingRecordValues.customerContactId=partyRecord.CVIF__c;
                bookingRecordValues.customerAddressRecord= mapAddressRecords.get(mapCVIFAddressId.get(partyRecord.CVIF__c));
                bookingRecordValues.customerContactRecord= mapCVIFContactRecords.get(partyRecord.CVIF__c);
                bookingRecordValues.customerId= partyRecord.CVIF__c;
            }
            else if(partyRecord.Type__c=='SHP'){
                bookingRecordValues.shipperContactId=partyRecord.CVIF__c;
                bookingRecordValues.shipperAddressRecord=mapAddressRecords.get(mapCVIFAddressId.get(partyRecord.CVIF__c));
                bookingRecordValues.shipperContactRecord= mapCVIFContactRecords.get(partyRecord.CVIF__c);
                bookingRecordValues.shipperId= partyRecord.CVIF__c;
            }
            else if(partyRecord.Type__c=='CON'){
                bookingRecordValues.consigneeContactId=partyRecord.CVIF__c;
                bookingRecordValues.shipperAddressRecord=mapAddressRecords.get(mapCVIFAddressId.get(partyRecord.CVIF__c));
                bookingRecordValues.consigneeContactRecord= mapCVIFContactRecords.get(partyRecord.CVIF__c);
                bookingRecordValues.consigneeId= partyRecord.CVIF__c;
            }
        }
        
        
        Map<String,String> mapValueNameReceiptDeliveryTerms= fetchReceiptDeliveryTermsPickListValues('Receipt_Delivery_Terms__c');
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Origin_Type__c))
            bookingRecordValues.receiptTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Origin_Type__c);
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Destination_Type__c))
            bookingRecordValues.deliveryTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Destination_Type__c);
        
        bookingRecordValues.estSailingDate = voyageRec.Estimate_Sail_Date__c;
        bookingRecordValues.originCode = shipmentRec.Origin_Code__c;
        bookingRecordValues.destinationCode = shipmentRec.Destination_Code__c;
        bookingRecordValues.totalVolume = bookingRec.Total_Volume__c;
        bookingRecordValues.totalWeight = bookingRec.Total_Weight__c;
        bookingRecordValues.totalVolumeUnit = bookingRec.Total_Volume_Unit__c;
        bookingRecordValues.bookingRequestNumber = bookingRec.Name;
        bookingRecordValues.bookingType = bookingRec.Container_Mode__c;
        
        List<CustomerCommunity_BookingRecord.ContainerCargoDetail> listCargoDetail = new List<CustomerCommunity_BookingRecord.ContainerCargoDetail>();
        
        CustomerCommunity_BookingRecord.ContainerCargoDetail containerCargoRecordValues = new CustomerCommunity_BookingRecord.ContainerCargoDetail();
        
        containerCargoRecordValues.sequenceId = 1;
        containerCargoRecordValues.UniqueId = 'commodity1';
        containerCargoRecordValues.isHazardous = true;
        containerCargoRecordValues.freightRecord = freightDetailRec;
        containerCargoRecordValues.requirementRecord = requirementRec;
        containerCargoRecordValues.listCommodityRecords = commodityList; 
        
        listCargoDetail.add(containerCargoRecordValues); 
        
        bookingRecordValues.listCargoDetails = listCargoDetail;
        
        return bookingRecordValues;
    }
    
    @AuraEnabled
    public static String validateHazchecker(Header__c objHeader,CustomerCommunity_BookingRecord bookingRecordValues){
        String bookingRecordString = JSON.serialize(bookingRecordValues);
        System.debug('bookingRecordString --->'+ bookingRecordString);
        String jsonResponseData = CustomerCommunity_NewBookingsController.validateHazardousBooking(objHeader.Booking__c, bookingRecordString);
        System.debug('Response value is-----> '+jsonResponseData);
        return jsonResponseData;
    }
    
    @AuraEnabled
    public static Id getBookingId(String bookingNumber){ 
        Booking__c bookingRec = [SELECT ID FROM Booking__c WHERE Name =: bookingNumber LIMIT 1];
        return bookingRec.Id;
    }
    
    @AuraEnabled
    public static Booking__c getCICSBookingNumber(String bookingNumber){
        Booking__c bookingRec = [SELECT ID FROM Booking__c WHERE Name =: bookingNumber LIMIT 1];
        Booking__c bookingRecord;
        try{
            bookingRecord = CustomerCommunity_CreateCICSBooking.CreateCICSBooking(bookingRec.Id, true);
            return bookingRecord;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
    
    @AuraEnabled
    public static String getSoftshipRates(String bookingRecValuesString, String sourceType){
        return CustomerCommunity_NewBookingsController.getSoftshipRates(bookingRecValuesString, sourceType);
    }
    
    public static Map<String,String> fetchReceiptDeliveryTermsPickListValues(String fieldName){
        Map<String,String> pickLabelValueMap = new Map<String,String>();
        Map<String,List<Schema.PicklistEntry>> picklistSchema= new Map<String,List<Schema.PicklistEntry>>();
        Schema.sObjectType objType = New_Booking_Fields__mdt.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        if(fieldMap.containsKey(fieldName)){
            list<Schema.PicklistEntry> values =
                fieldMap.get(fieldName).getDescribe().getPickListValues();
            for(Schema.PicklistEntry p : values){
                
                pickLabelValueMap.put(p.getLabel(),p.getValue());
            }
            
        }
        return pickLabelValueMap; 
    }
    
    public static Map <ID,Sobject> getWrapperRecords(String shippingInstructionId) {
        integer count =1;
        Map <String,String> mapObjectRecordId = new Map < String, String > ();
        Map <ID,Sobject> mapIdWithSobject = new Map < ID, Sobject > ();
        List <BOLFormField__mdt> BolFormFields = [Select BolFieldSet__c, Label, customLabel__c, Parent__c, SequenceNumber__c FROM BOLFormField__mdt ORDER BY SequenceNumber__c ASC];
        System.debug('BolFormFields --->' + BolFormFields);
        try {
            if (BolFormFields != null) {
                for (BOLFormField__mdt objbolmdt: BolFormFields) {
                    
                    List <String> listFields = fetchAllFields(objbolmdt);
                    String fieldNamesConcat = '';
                    for (String objFieldSetColl: listFields) {
                        fieldNamesConcat += objFieldSetColl + ', ';
                    }
                    fieldNamesConcat = fieldNamesConcat.removeEnd(', ');
                    List <Sobject> listrecords = getRecordData(fieldNamesConcat, objbolmdt.Label, objbolmdt.SequenceNumber__c, objbolmdt.Parent__c, mapObjectRecordId, shippingInstructionId);
                    
                    if(listrecords != Null && !listrecords.isEmpty()){ 
                        String recIds = '(\'';
                        for (sObject record: listrecords) {
                            recIds = recIds + record.Id + '\', ';
                        }
                        recIds = recIds.removeEnd(', ') + ')';     
                        mapObjectRecordId.put(objbolmdt.Label, recIds);
                        System.debug('mapObjectRecordId --->' + mapObjectRecordId);
                        
                        for (sObject record: listrecords) {
                            mapIdWithSobject.put(record.Id, record);
                        }
                        System.debug('@@@mapIdWithSobject   --> '+mapIdWithSobject);
                        System.debug('Count : '+count);
                        count++;
                    }
                    
                }
                
                System.debug('@@@@mapIdWithSobject  ----> '+mapIdWithSobject);
                
            }
            if (mapIdWithSobject != NULL) {
                return mapIdWithSobject;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
    public static List <Sobject> getRecordData(String fieldNames, String objectApiName, Decimal sequenceNumber, String parentName, Map < String, String > mapObjectRecordId, String shippingInstructionId) {
        String query = '';
        List <Sobject> records = new List < SObject > ();
        System.debug('fieldNames --->' + fieldNames);
        query = 'select ' + fieldNames + ' from ' + objectApiName;
        System.debug('intial query -->' + query);
        if (sequenceNumber == 1) {
            query = query + ' WHERE Id = \'' + shippingInstructionId + '\'';
            System.debug('query --->' + query);
            records = Database.query(query);
        } else if (!mapObjectRecordId.isEmpty() && mapObjectRecordId.containsKey(parentName)) {
            query = query + ' WHERE ' + parentName + ' IN ' + mapObjectRecordId.get(parentName);
            System.debug('query --->' + query);
            records = Database.query(query);
        }
        System.debug('records --->' + records.size() + records);
        if (records.size() > 0) return records;
        else return NULL;
    }
    public static List <String> fetchAllFields(BOLFormField__mdt objApiName) {
        try {
            List<String> listFieldNames = new List<String>();
            Map <String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objApiName.Label);
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(objApiName.BolFieldSet__c);
            List <Schema.FieldSetMember> listSchemaField = fieldSetObj.getFields();
            System.debug('listSchemaField -->' + listSchemaField);
            for (Schema.FieldSetMember objFieldSetMember: listSchemaField) {
                listFieldNames.add(objFieldSetMember.getFieldPath());
            }
            if (listFieldNames != null) {
                return listFieldNames;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
    
    public static Id getRecordTypeId(String recTypeName){
        if (recordTypesMap!= null && recordTypesMap.containsKey(recTypeName)){
            return recordTypesMap.get(recTypeName);
        }
        return null;
    }
    
    private static Map<String, ID> recordTypesMap{
        get{
            if (recordTypesMap == null ){
                recordTypesMap = new Map<String, Id>();
                for (RecordType aRecordType : [SELECT SobjectType, DeveloperName FROM RecordType WHERE isActive = true]){
                    recordTypesMap.put(aRecordType.SobjectType + ':' + aRecordType.DeveloperName, aRecordType.Id);
                }
            }
            return recordTypesMap;
        }
        set;
    }
}