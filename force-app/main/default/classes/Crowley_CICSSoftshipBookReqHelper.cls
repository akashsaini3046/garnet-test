public with sharing class Crowley_CICSSoftshipBookReqHelper {
    public static HttpResponse createWrapper(BookingWrapper bookingWrapper, Booking__c bookingRecord){
        Crowley_CICSSoftshipBookingRequestCls wrapper = getWrapper(bookingWrapper, bookingRecord);
        System.debug(JSON.serializePretty(wrapper, true));
        String requestString = JSON.serialize(wrapper, true);
        requestString = requestString.replaceAll('number_replace', 'number');
        System.debug(requestString);
        //requestString = '{"transports":[],"shipments":[{"voyages":[{"voyageNumber":"SAL0052S","vesselName":"PARADERO","vesselLloydsCode":"9368998","vesselAbbreviation":"","shipmentNumber":"001","roldReason":"","load":{"sequence":1,"port":"JAX","location":{"state":"FL","country":"US","code":"USJAX","city":"JACKSONVILLE"}},"estimateSailDate":"2020-06-30","estimateArrivalDate":"2020-07-03","discharge":{"sequence":1,"port":"SJU","location":{"state":"","country":"PR","code":"PRSJU","city":"SAN JUAN"}}}],"status":"N","origin":{"port":"JAX","location":{"state":"FL","locCode":"USJAX","country":"US","city":"JACKSONVILLE"}},"number":"001","freightDetails":[{"width":{"minor":0.00,"major":0.00},"volume":{"value":0.00,"unitofMeasure":""},"roroInd":"N","requirements":[{"width":0.00,"weight":{"value":"","unitofMeasure":""},"ventilated":"","type":"4500","termsCd":"PP","temperature":{"value":"","unitofMeasure":""},"tarpInd":"","specEquipCd":"","rrInd":"N","reqId":"001","quantity":1,"length":40,"highCubeInd":"","category":"DRY"}],"povDealerCd":"","overDimInd":"N","moveType":{"moveTypeDesc":"PP"},"lumberQty":{"value":0.00,"unitofMeasure":"","linearUom":""},"length":{"minor":0.00,"major":0.00},"height":{"minor":0.00,"major":0.00},"freightQty":{"value":"","unitofMeasure":""},"freightId":"001","fleetUsedCd":"","declaredWeight":{"value":0.00,"unitofMeasure":""},"declaredValue":0.00,"commodities":[{"weight":{"value":"2500099","unitofMeasure":"L"},"volume":{"value":"100888","unitofMeasure":"Cubic Feet"},"technicalName":"","tariffNbr":"","subRiskClass":"","secondaryClass":"","requiredQtyInd":"","reportedQty":0.00,"quantity":{"value":"955","unitofMeasure":"SKD"},"primaryClass":"","prefix":"","permitNumber":"","packageGroup":"","number":"","isMarinePollutant":"","isLimitedQuantity":"","isHazardous":false,"imoClass":"","imdgPage":"","hazIMOLock":"","flashTemperature":{"value":"","unitofMeasure":""},"emergencyContact":{"phoneNumber":"","name":""},"dotName":"","description":"GNFR, INCLUDING RETURNS","contractNbr":"002423","commodityId":"001"}],"cargoType":"EQUIP"}],"destination":{"port":"SJU","location":{"state":"","locCode":"PRSJU","country":"PR","city":"SAN JUAN"}}}],"serviceType":{"description":"PP"},"parties":[{"type":"OTH","refNumber":"","phoneNumber":"4792777615","partyName":"SAMS CLUB","notifyInd":"N","location":{"zip":"00659","state":"PR","country":"US","city":"HATILLO","addressLine2":"PR 2 KM 84.2","addressLine1":"STORE 6270"},"id":"001","faxNumber":"","emailAddr":"paul.decarr@samsclub.com","cvifLocationCode":"38","cvif":"0762040","contactName":"MORENO PEDRO","bookbyInd":"N","billToInd":"N"},{"type":"SHP","refNumber":"","phoneNumber":"4792777615","partyName":"SAMS CLUB","notifyInd":"N","location":{"zip":"00659","state":"PR","country":"US","city":"HATILLO","addressLine2":"PR 2 KM 84.2","addressLine1":"STORE 6270"},"id":"002","faxNumber":"","emailAddr":"paul.decarr@samsclub.com","cvifLocationCode":"38","cvif":"0762040","contactName":"MORENO PEDRO","bookbyInd":"N","billToInd":"N"},{"type":"CON","refNumber":"","phoneNumber":"4792777615","partyName":"SAMS CLUB","notifyInd":"N","location":{"zip":"00659","state":"PR","country":"US","city":"HATILLO","addressLine2":"PR 2 KM 84.2","addressLine1":"STORE 6270"},"id":"003","faxNumber":"","emailAddr":"paul.decarr@samsclub.com","cvifLocationCode":"38","cvif":"0762040","contactName":"MORENO PEDRO","bookbyInd":"N","billToInd":"N"}],"number":"BRN-006126","headers":{"status":"N","paymentTerms":"PP","label":"VELOZPORTAL BKGREQ 20200625 16:04 ID:000000332","cmcPickupLoc":"JAXT1","cmcDeliverLoc":"SJUT1","bookDate":"2020-06-26"},"customerOrigin":{},"customerDestination":{}}';
        HttpRequest req = new HttpRequest();
        //req.setEndpoint('http://salesforce-cics-booking.us-e1.cloudhub.io/createBooking');
        req.setEndpoint('https://dev.api.crowley.com/v1/bookings');
        req.setHeader('content-type', 'application/json');
        req.setHeader('client_id','9567932436aa4c55b872eeb92960a799');
        req.setHeader('client_secret', 'e21F083673dA45e0814Bc57e16C97Bb0');
        req.setTimeout(60000);
        req.setBody(requestString);
        req.setMethod('POST');
        HttpResponse response = new Http().send(req);
        System.debug(response);
        System.debug(response.getbody());
        return response;
        //String responseString = CustomerCommunity_APICallouts.getAPIResponse('CICSAPI', requestString);
    }

    public static Crowley_CICSSoftshipBookingRequestCls getWrapper(BookingWrapper bookingWrapper, Booking__c bookingRecord){
        Crowley_CICSSoftshipBookingRequestCls wrapper = new Crowley_CICSSoftshipBookingRequestCls();
        wrapper.number_replace = bookingRecord.Name;
        wrapper.headers = getHeaders(bookingWrapper, bookingRecord);
        wrapper.serviceType = getServiceType(bookingWrapper);
        wrapper.customerOrigin = getCustomerOrigin(bookingWrapper);
        wrapper.customerDestination = getCustomerDestination(bookingWrapper);
        wrapper.parties = getParties(bookingWrapper);
        wrapper.transports = getTransports(bookingWrapper);
        wrapper.shipments = getShipments(bookingWrapper);
        return wrapper;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.HeadersWrap getHeaders(BookingWrapper bookingWrapper, Booking__c bookingRecord){
        Crowley_CICSSoftshipBookingRequestCls.HeadersWrap headersWrap = new Crowley_CICSSoftshipBookingRequestCls.HeadersWrap();
        String paymentTerm = '';
        if(checkNull(bookingWrapper.booking.Payment_Terms__c)=='Collect'){
            paymentTerm = 'CO';
        }
        if(checkNull(bookingWrapper.booking.Payment_Terms__c)=='Prepaid'){
            paymentTerm = 'PP';
        }
        
        String dateToday = String.valueOf(System.today()).remove('-');
        String timeNow = System.now().format('HH:mm');
        headersWrap.label = 'VELOZPORTAL BKGREQ '+dateToday+' '+timeNow+' ID:'+bookingRecord.Request_Number__c;
		headersWrap.paymentTerms = paymentTerm;
		headersWrap.status = 'N';
        //headersWrap.bookDate = checkNull(bookingWrapper.booking.Booked_Date__c);
        headersWrap.bookDate = String.valueOf(Date.valueOf(bookingWrapper.booking.Booked_Date__c));
		headersWrap.cmcPickupLoc = checkNull(bookingWrapper.booking.Pickup_Location__c);
		headersWrap.cmcDeliverLoc = checkNull(bookingWrapper.booking.Delivery_Location__c);
        return headersWrap;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.ServiceTypeWrap getServiceType(BookingWrapper bookingWrapper){
        Crowley_CICSSoftshipBookingRequestCls.ServiceTypeWrap serviceTypeWrap = new Crowley_CICSSoftshipBookingRequestCls.ServiceTypeWrap();
        serviceTypeWrap.description = checkNull(bookingWrapper.booking.Origin_Type__c) + checkNull(bookingWrapper.booking.Destination_Type__c);
        return serviceTypeWrap;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.CustomerLocationWrap getCustomerOrigin(BookingWrapper bookingWrapper){
        Crowley_CICSSoftshipBookingRequestCls.CustomerLocationWrap customerOrigin = new Crowley_CICSSoftshipBookingRequestCls.CustomerLocationWrap();
        if(checkNull(bookingWrapper.booking.Origin_Type__c).equalsIgnoreCase('D')){
            customerOrigin.transId = '001';
            
            customerOrigin.stopId = '0'+bookingWrapper.transportOrigin.listStop.size()+'0';
            customerOrigin.city = checkNull(bookingWrapper.booking.Customer_Origin_City__c);
            customerOrigin.code = checkNull(bookingWrapper.booking.Customer_Origin_Code__c);
            customerOrigin.country = checkNull(bookingWrapper.booking.Customer_Origin_Country__c);
            customerOrigin.referenceNumber = checkNull(bookingWrapper.booking.Customer_Origin_Reference_Number__c);
            customerOrigin.state = checkNull(bookingWrapper.booking.Customer_Origin_State__c);
            customerOrigin.zip = checkNull(bookingWrapper.booking.Customer_Origin_Zip__c);
        }
        return customerOrigin;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.CustomerLocationWrap getCustomerDestination(BookingWrapper bookingWrapper){
        Crowley_CICSSoftshipBookingRequestCls.CustomerLocationWrap customerDestination = new Crowley_CICSSoftshipBookingRequestCls.CustomerLocationWrap();
        if(checkNull(bookingWrapper.booking.Destination_Type__c).equalsIgnoreCase('D')){
            customerDestination.transId = '001';
            customerDestination.stopId = '0'+bookingWrapper.transportDestination.listStop.size()+'0';
            customerDestination.city = checkNull(bookingWrapper.booking.Customer_Destination_City__c);
            customerDestination.code = checkNull(bookingWrapper.booking.Customer_Destination_Code__c);
            customerDestination.country = checkNull(bookingWrapper.booking.Customer_Destination_Country__c);
            customerDestination.referenceNumber = checkNull(bookingWrapper.booking.Customer_Destination_Reference_Number__c);
            customerDestination.state = checkNull(bookingWrapper.booking.Customer_Destination_State__c);
            customerDestination.zip = checkNull(bookingWrapper.booking.Customer_Destination_Zip__c);
        }
        return customerDestination;
    }

    private static List<Crowley_CICSSoftshipBookingRequestCls.PartyWrapperCls> getParties(BookingWrapper bookingWrapper){
        List<Crowley_CICSSoftshipBookingRequestCls.PartyWrapperCls> partyWrapperList = new List<Crowley_CICSSoftshipBookingRequestCls.PartyWrapperCls>();
        if(bookingWrapper.mapParty != null && !bookingWrapper.mapParty.isEmpty()){
            List<Party__c> allParties = new List<Party__c>();
            for(String partyType : bookingWrapper.mapParty.keySet()){
                allParties.addAll(bookingWrapper.mapParty.get(partyType));
            }
            if(allParties != null && !allParties.isEmpty()){
                partyWrapperList = getPartyWrapper(allParties);
            }
        }
        return partyWrapperList;
    }

    private static List<Crowley_CICSSoftshipBookingRequestCls.PartyWrapperCls> getPartyWrapper(List<Party__c> parties){
        List<Crowley_CICSSoftshipBookingRequestCls.PartyWrapperCls> partyWrapperList = new List<Crowley_CICSSoftshipBookingRequestCls.PartyWrapperCls>();
        for(Party__c party : parties){
            Crowley_CICSSoftshipBookingRequestCls.PartyWrapperCls partyWrapper = new Crowley_CICSSoftshipBookingRequestCls.PartyWrapperCls();
            partyWrapper.id = checkNull(party.party_Id__c);
            partyWrapper.type = getPartyType(checkNull(party.Type__c));
            partyWrapper.cvif = checkNull(party.CVIF__c);
            partyWrapper.cvifLocationCode = checkNull(party.CVIF_Location_Code__c);
            partyWrapper.refNumber = checkNull(party.REF_Number__c);
            partyWrapper.partyName = checkNull(party.Name);
            partyWrapper.billToInd = 'N'; // HC
            partyWrapper.bookbyInd = 'N'; // HC
            partyWrapper.notifyInd = 'N';
            if(partyWrapper.type.equalsIgnoreCase('NOT') ){
                partyWrapper.notifyInd = 'Y';
            }
            partyWrapper.contactName = checkNull(party.Contact_Name__c);
            partyWrapper.phoneNumber = checkNull(party.Phone_Number__c);
            partyWrapper.emailAddr = checkNull(party.Email_Address__c);
            partyWrapper.faxNumber = checkNull(party.Fax_Number__c);
            partyWrapper.location = getPartyLocationWrap(party);
            partyWrapperList.add(partyWrapper);
        }
        return partyWrapperList;
    }

    private static String getPartyType(String partyType){
        if(partyType.equalsIgnoreCase('CUST')){
            return  'OTH';
        } else if(partyType.equalsIgnoreCase('SHP')){
            return  'SHP';
        } else if(partyType.equalsIgnoreCase('CON')){
            return  'CON';
        } else if (partyType.equalsIgnoreCase('NOT')){
            return  'NOT';
        } else if (partyType.equalsIgnoreCase('FOR')){
            return 'FOR';
        } else {
            return partyType;
        }
    }

    private static List<Crowley_CICSSoftshipBookingRequestCls.TransportWrapperCls> getTransports(BookingWrapper bookingWrapper){
        List<Crowley_CICSSoftshipBookingRequestCls.TransportWrapperCls> transportWrapperList = new List<Crowley_CICSSoftshipBookingRequestCls.TransportWrapperCls>();

        if(bookingWrapper.transportOrigin != null && checkNull(bookingWrapper.booking.Origin_Type__c).equalsIgnoreCase('D')){
            Crowley_CICSSoftshipBookingRequestCls.TransportWrapperCls transportWrapperOrigin = new Crowley_CICSSoftshipBookingRequestCls.TransportWrapperCls();
            transportWrapperOrigin.id = '001';
            if(bookingWrapper.transportOrigin.transport != null){
                transportWrapperOrigin.originDestCd = checkNull(bookingWrapper.transportOrigin.transport.Origin_Destination_Code__c);
                transportWrapperOrigin.ctlLocAbbr = checkNull(bookingWrapper.transportOrigin.transport.Control_Location__c);
            }
            if(bookingWrapper.transportOrigin.listStop != null && !bookingWrapper.transportOrigin.listStop.isEmpty()){
                transportWrapperOrigin.stops = getStopWrap(bookingWrapper.transportOrigin.listStop);
            }
            transportWrapperList.add(transportWrapperOrigin);
        }

        if(bookingWrapper.transportDestination != null && checkNull(bookingWrapper.booking.Destination_Type__c).equalsIgnoreCase('D') ){
            Crowley_CICSSoftshipBookingRequestCls.TransportWrapperCls transportWrapperDest = new Crowley_CICSSoftshipBookingRequestCls.TransportWrapperCls();
            transportWrapperDest.id = '002';
            if(bookingWrapper.transportDestination.transport != null){
                transportWrapperDest.originDestCd = checkNull(bookingWrapper.transportDestination.transport.Origin_Destination_Code__c);
                transportWrapperDest.ctlLocAbbr = checkNull(bookingWrapper.transportDestination.transport.Control_Location__c);
            }
            if(bookingWrapper.transportDestination.listStop != null && !bookingWrapper.transportDestination.listStop.isEmpty()){
                transportWrapperDest.stops = getStopWrap(bookingWrapper.transportDestination.listStop);
            }
            transportWrapperList.add(transportWrapperDest);
        }

        return transportWrapperList;
    }

    private static List<Crowley_CICSSoftshipBookingRequestCls.ShipmentWrapperCls> getShipments(BookingWrapper bookingWrapper){
        List<Crowley_CICSSoftshipBookingRequestCls.ShipmentWrapperCls> shipmentWrapperList = new List<Crowley_CICSSoftshipBookingRequestCls.ShipmentWrapperCls>();
        if(bookingWrapper.shipment != null && bookingWrapper.shipment.shipment != null){
            Crowley_CICSSoftshipBookingRequestCls.ShipmentWrapperCls shipmentWrapper = new Crowley_CICSSoftshipBookingRequestCls.ShipmentWrapperCls();
            shipmentWrapper.number_replace = bookingWrapper.shipment.shipment.Shipment_Number__c;
            shipmentWrapper.origin = getOrigin(bookingWrapper.shipment.shipment);
            shipmentWrapper.destination = getDestination(bookingWrapper.shipment.shipment);
            shipmentWrapper.status = 'N'; // HC
            shipmentWrapper.voyages = getVoyages(bookingWrapper); // HC
            if(bookingWrapper.shipment.listCargo != null && !bookingWrapper.shipment.listCargo.isEmpty()){
                shipmentWrapper.freightDetails = getFreightDetails(bookingWrapper.shipment.listCargo.get(0), bookingWrapper);
            }
            shipmentWrapperList.add(shipmentWrapper);
        }
        return shipmentWrapperList;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.ParyLocationWrap getPartyLocationWrap(Party__c party){
        Crowley_CICSSoftshipBookingRequestCls.ParyLocationWrap partyLocationWrap = new Crowley_CICSSoftshipBookingRequestCls.ParyLocationWrap();
        partyLocationWrap.addressLine1 = checkNull(party.Address_Line1__c);
        partyLocationWrap.addressLine2 = checkNull(party.Address_Line2__c);
        partyLocationWrap.city = checkNull(party.City__c);
        partyLocationWrap.state = checkNull(party.State__c);
        partyLocationWrap.country = checkNull(party.Country__c);
        partyLocationWrap.zip = checkNull(party.Zip__c);
        return partyLocationWrap;
    }

    private static List<Crowley_CICSSoftshipBookingRequestCls.StopWrap> getStopWrap(List<Stop__c> stops){
        List<Crowley_CICSSoftshipBookingRequestCls.StopWrap> stopWrapList = new List<Crowley_CICSSoftshipBookingRequestCls.StopWrap>();
        
        for(Stop__c stop : stops){
            String dateToday = String.valueOf(checkNull(stop.Pick_up_Date__c)).remove('-');
            dateToday = dateToday.split(' ')[0];
            Crowley_CICSSoftshipBookingRequestCls.StopWrap stopWrap = new Crowley_CICSSoftshipBookingRequestCls.StopWrap();
            stopWrap.stopId = checkNull(stop.StopId__c);
            stopWrap.cvif = checkNull(stop.CVIF__c);
            stopWrap.cvifLocationCode = checkNull(stop.CVIF_Location_Code__c);
            stopWrap.stopName = checkNull(stop.stopName__c);
            stopWrap.name = checkNull(stop.Contact_Name__c);
            stopWrap.phoneNumber = checkNull(stop.Phone_Number__c);
            stopWrap.pickupDate = dateToday;
            stopWrap.address1 = checkNull(stop.Address__c);
            stopWrap.address2 = checkNull(stop.Address_2__c);
            stopWrap.city = checkNull(stop.City__c);
            stopWrap.state = checkNull(stop.State__c);
            stopWrap.zip = checkNull(stop.Zip_Code_Postal_Code__c);
            stopWrap.country = checkNull(stop.Country__c);
            stopWrapList.add(stopWrap);
        }
        return stopWrapList;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.OriginWrap getOrigin(Shipment__c shipment){
        Crowley_CICSSoftshipBookingRequestCls.OriginWrap originWrap = new Crowley_CICSSoftshipBookingRequestCls.OriginWrap();
        originWrap.port = checkNull(shipment.Origin_Port__c);
        originWrap.location = getLocationWrap(
            checkNull(shipment.Origin_Code__c),
            null,
            checkNull(shipment.Origin_City__c),
            checkNull(shipment.Origin_State__c),
            checkNull(shipment.Origin_Country__c)
        );
        return originWrap;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.DestinationWrap getDestination(Shipment__c shipment){
        Crowley_CICSSoftshipBookingRequestCls.DestinationWrap destinationWrap = new Crowley_CICSSoftshipBookingRequestCls.DestinationWrap();
        destinationWrap.port = checkNull(shipment.Destination_Port__c);
        destinationWrap.location = getLocationWrap(
            checkNull(shipment.Destination_Code__c),
            null,
            checkNull(shipment.Destination_City__c),
            checkNull(shipment.Destination_State__c),
            checkNull(shipment.Destination_Country__c)
        );
        return destinationWrap;
    }

   /*private static List<Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls> getVoyages(BookingWrapper bookingWrapper){
        List<Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls> voyageWrapperList = new List<Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls>();
        for(Voyage__c voyage : bookingWrapper.shipment.listVogage){
            Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls voyageWrapper = new Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls();
            voyageWrapper.shipmentNumber = '001';

            voyageWrapper.vesselName = (string.isBlank(voyage.Vessel_Name__c) || voyage.Vessel_Name__c =='TBN') ? 'DUMMY' : voyage.Vessel_Name__c;
            voyageWrapper.voyageNumber = (string.isBlank(voyage.Voyage_Number__c) || voyage.Voyage_Number__c =='TBN') ? 'JAX9999S' : voyage.Voyage_Number__c;
            //voyageWrapper.estimateSailDate = String.valueOf(Date.valueOf(voyage.Estimate_Sail_Date__c));
            //voyageWrapper.estimateArrivalDate = String.valueOf(Date.valueOf(voyage.Estimate_Arrival_Date__c));
            voyageWrapper.estimateSailDate = '2018-10-01';
            voyageWrapper.estimateArrivalDate = '2018-12-31';
            voyageWrapper.vesselAbbreviation = '';        
            voyageWrapper.vesselLloydsCode = '';
            voyageWrapper.roldReason = '';
            voyageWrapper.load = getLoadDischargeWrap(
                '',
                1,
                getLocationWrap(null,voyage.Loading_Location_Code__c,'','','')
            );
            voyageWrapper.discharge = getLoadDischargeWrap(
                '',
                1,
                getLocationWrap(null,voyage.Discharge_Location_Code__c,'','','')
            );
            
            voyageWrapperList.add(voyageWrapper);
        }

        return voyageWrapperList;
    }*/

    //  ! HC
    private static List<Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls> getVoyages(BookingWrapper bookingWrapper){
        List<Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls> voyageWrapperList = new List<Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls>();
        
        Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls voyageWrapper = new Crowley_CICSSoftshipBookingRequestCls.VoyagesWrapperCls();
        voyageWrapper.shipmentNumber = '001';
        voyageWrapper.voyageNumber = 'SAL0052S';
        voyageWrapper.estimateSailDate = '2020-06-30';
        voyageWrapper.estimateArrivalDate = '2020-07-03';
        voyageWrapper.vesselAbbreviation = '';
        voyageWrapper.vesselName = 'PARADERO';
        voyageWrapper.vesselLloydsCode = '9368998';
        voyageWrapper.roldReason = '';
        voyageWrapper.load = getLoadDischargeWrap(
            'JAX',
            1,
            getLocationWrap(null,'USJAX','JACKSONVILLE','FL','US')
        );
        voyageWrapper.discharge = getLoadDischargeWrap(
            'SJU',
            1,
            getLocationWrap(null,'PRSJU','SAN JUAN','','PR')
        );
        
        voyageWrapperList.add(voyageWrapper);

        return voyageWrapperList;
    }

    private static List<Crowley_CICSSoftshipBookingRequestCls.FreightDetailsWrapperCls> getFreightDetails(BookingWrapper.CargoWrapper cargoWrapper, BookingWrapper bookingWrapper){
        List<Crowley_CICSSoftshipBookingRequestCls.FreightDetailsWrapperCls> freightDetailWrapperList = new List<Crowley_CICSSoftshipBookingRequestCls.FreightDetailsWrapperCls>();
        
        if(cargoWrapper != null && cargoWrapper.listFreightDetailWrapper != null && !cargoWrapper.listFreightDetailWrapper.isEmpty()){
            for(BookingWrapper.FreightDetailWrapper freightWrapper : cargoWrapper.listFreightDetailWrapper){
                Crowley_CICSSoftshipBookingRequestCls.FreightDetailsWrapperCls freightDetailWrapper = new Crowley_CICSSoftshipBookingRequestCls.FreightDetailsWrapperCls();
                freightDetailWrapper.freightId = checkNull(freightWrapper.freightDetail.FreightId__c);
                String cargoType = '';
                if(cargoWrapper.cargoType =='container'){
                    cargoType = 'EQUIP';
                }else{
                    cargoType=cargoWrapper.cargoType;
                }
                freightDetailWrapper.cargoType = cargoType;
                freightDetailWrapper.declaredWeight = getValueUnitFreightWrap( checkNullDecimal(freightWrapper.freightDetail.Declared_Weight_Value__c), checkNull(freightWrapper.freightDetail.Declared_Weights_Unit_of_Measure__c));
                freightDetailWrapper.volume = getValueUnitFreightWrap(checkNullDecimal(freightWrapper.freightDetail.Volume_Value__c), checkNull(freightWrapper.freightDetail.Volumes_Unit_of_Measure__c));
                freightDetailWrapper.declaredValue = Integer.valueOf(checkNullDecimal(freightWrapper.freightDetail.Declared_Value__c));
                freightDetailWrapper.moveType = getMoveType(bookingWrapper);
                freightDetailWrapper.freightQty = getValueUnitFreightWrap(checkNullDecimal(freightWrapper.freightDetail.Freight_Quantity__c), checkNull(freightWrapper.freightDetail.Freights_Unit_Of_Measure__c));
                freightDetailWrapper.overDimInd = freightWrapper.freightDetail.Over_Dimension__c ? 'Y' : 'N'; 
                freightDetailWrapper.roroInd = 'N';
                freightDetailWrapper.fleetUsedCd = ''; //HC
                freightDetailWrapper.povDealerCd = '';// HC
                freightDetailWrapper.length = getMajorMinorWrap(checkNullDecimal(freightWrapper.freightDetail.Length_Major__c), checkNullDecimal(freightWrapper.freightDetail.Length_Minor__c));
                freightDetailWrapper.width = getMajorMinorWrap(checkNullDecimal(freightWrapper.freightDetail.Width_Major__c), checkNullDecimal(freightWrapper.freightDetail.Width_Minor__c));
                freightDetailWrapper.height = getMajorMinorWrap(checkNullDecimal(freightWrapper.freightDetail.Height_Major__c), checkNullDecimal(freightWrapper.freightDetail.Height_Minor__c));
                freightDetailWrapper.lumberQty = getLumberQty(checkNull(freightWrapper.freightDetail.Linear_Unit_Of_Measure__c), checkNullDecimal(freightWrapper.freightDetail.Lumber_Quantity__c), checkNull(freightWrapper.freightDetail.Lumber_Unit_Of_Measure__c));
                freightDetailWrapper.commodities = getCommodities(freightWrapper.listCommodityWrapper);
                freightDetailWrapper.requirements = getRequirments(freightWrapper.listRequirementWrapper, bookingWrapper);
                freightDetailWrapperList.add(freightDetailWrapper);
            }
        }

        return freightDetailWrapperList;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.LoadDischargeWrap getLoadDischargeWrap(String port, Decimal sequence, Crowley_CICSSoftshipBookingRequestCls.LocationWrap location){
        Crowley_CICSSoftshipBookingRequestCls.LoadDischargeWrap loadDischargeWrap = new Crowley_CICSSoftshipBookingRequestCls.LoadDischargeWrap();
        loadDischargeWrap.port = port;
        loadDischargeWrap.sequence = sequence;
        loadDischargeWrap.location = location;
        return loadDischargeWrap;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.LocationWrap getLocationWrap(String locCode, String code, String city, String state, String country){
        Crowley_CICSSoftshipBookingRequestCls.LocationWrap locationWarp = new Crowley_CICSSoftshipBookingRequestCls.LocationWrap();
        locationWarp.locCode = locCode;
        locationWarp.code = code;
        locationWarp.city = city;
        locationWarp.state = state;
        locationWarp.country = country;
        return locationWarp;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.ValueUnitWrap getValueUnitWrap(Decimal value, String unitofMeasure){
        Crowley_CICSSoftshipBookingRequestCls.ValueUnitWrap valueUnitWrap = new Crowley_CICSSoftshipBookingRequestCls.ValueUnitWrap();
        valueUnitWrap.value = (value>0) ? String.valueOf(value) : '0';
        valueUnitWrap.unitofMeasure = (value>0) ? unitofMeasure : '';
        return valueUnitWrap;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.ValueUnitFreightWrap getValueUnitFreightWrap(Decimal value, String unitofMeasure){
        Crowley_CICSSoftshipBookingRequestCls.ValueUnitFreightWrap valueUnitWrap = new Crowley_CICSSoftshipBookingRequestCls.ValueUnitFreightWrap();
        valueUnitWrap.value = Integer.valueOf(value);
        valueUnitWrap.unitofMeasure = unitofMeasure;
        return valueUnitWrap;
    }

    

    private static Crowley_CICSSoftshipBookingRequestCls.MoveType getMoveType(BookingWrapper bookingWrapper){
        Crowley_CICSSoftshipBookingRequestCls.MoveType moveType = new Crowley_CICSSoftshipBookingRequestCls.MoveType();
        if(checkNull(bookingWrapper.booking.Origin_Type__c) != '' && checkNull(bookingWrapper.booking.Destination_Type__c) != null){
            String origin = bookingWrapper.booking.Origin_Type__c.equalsIgnoreCase('D') ? 'H' : bookingWrapper.booking.Origin_Type__c;
            String destination = bookingWrapper.booking.Destination_Type__c.equalsIgnoreCase('D') ? 'H' : bookingWrapper.booking.Destination_Type__c;
            moveType.moveTypeDesc = origin + destination;
        }else{
            moveType.moveTypeDesc = '';
        }
        return moveType;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.MajorMinorWrap getMajorMinorWrap(Decimal major, Decimal minor){
        Crowley_CICSSoftshipBookingRequestCls.MajorMinorWrap majorMinorWrap = new Crowley_CICSSoftshipBookingRequestCls.MajorMinorWrap();
        majorMinorWrap.major = Integer.valueOf(major);
        majorMinorWrap.minor = Integer.valueOf(minor);
        return majorMinorWrap;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.LumberWrap getLumberQty(String linearUom, Decimal value, String unitofMeasure){
        Crowley_CICSSoftshipBookingRequestCls.LumberWrap lumberWrap = new Crowley_CICSSoftshipBookingRequestCls.LumberWrap();
        lumberWrap.linearUom = linearUom;
        lumberWrap.value = Integer.valueOf(value);
        lumberWrap.unitofMeasure = unitofMeasure;
        return lumberWrap;
    }

    private static List<Crowley_CICSSoftshipBookingRequestCls.CommodityWrapperCls> getCommodities(List<BookingWrapper.CommodityWrapper> listCommodityWrapper){
        List<Crowley_CICSSoftshipBookingRequestCls.CommodityWrapperCls> commodityWrapperList = new List<Crowley_CICSSoftshipBookingRequestCls.CommodityWrapperCls>();
        if(listCommodityWrapper != null && !listCommodityWrapper.isEmpty()){
            for(BookingWrapper.CommodityWrapper commodity : listCommodityWrapper){
                Crowley_CICSSoftshipBookingRequestCls.CommodityWrapperCls commodityWrapper = new Crowley_CICSSoftshipBookingRequestCls.CommodityWrapperCls();
                commodityWrapper.imoClass = checkNull(commodity.commodity.IMO_Class__c);
                commodityWrapper.prefix = checkNull(commodity.commodity.Prefix__c);
                commodityWrapper.description = checkNull(commodity.commodity.Commodity__c);
                commodityWrapper.secondaryClass = checkNull(commodity.commodity.Secondary_Class__c);
                commodityWrapper.imdgPage = checkNull(commodity.commodity.IMDG_Page_Number__c);
                commodityWrapper.number_replace = checkNull(commodity.commodity.Number__c);
                commodityWrapper.subRiskClass = checkNull(commodity.commodity.Sub_Risk_Class__c);
                commodityWrapper.dotName = checkNull(commodity.commodity.Dot_Name__c);
                commodityWrapper.packageGroup = checkNull(commodity.commodity.Package_Group__c);               
                commodityWrapper.primaryClass = checkNull(commodity.commodity.Primary_Class__c);
                commodityWrapper.requiredQtyInd = commodity.commodity.Required_Quantity__c ? 'Y' : '';
                commodityWrapper.isHazardous = commodity.commodity.Is_Hazardous__c;
                commodityWrapper.contractNbr = checkNull(commodity.commodity.Contract_Number__c);
                commodityWrapper.emergencyContact = getEmergencyContact(checkNull(commodity.commodity.Emergency_Contact_Name__c), checkNull(commodity.commodity.Emergency_Contact_Number__c));
                commodityWrapper.hazIMOLock = checkNull(commodity.commodity.Hazmat_IMO_Lock__c);
                commodityWrapper.commodityId = checkNull(commodity.commodity.CommodityId__c);
                commodityWrapper.technicalName = checkNull(commodity.commodity.Technical_Name__c);
                commodityWrapper.tariffNbr = checkNull(commodity.commodity.Tariff_Number__c);
                //commodityWrapper.volume = getValueUnitWrap(checkNullDecimal(commodity.commodity.Volume_value__c), checkNull(commodity.commodity.Volume_Unit_of_Measure__c));
                //commodity.commodity.Volume_value__c = 00000000;
                commodityWrapper.volume = getValueUnitWrap(Integer.valueOf(checkNullDecimal(commodity.commodity.Volume_value__c)), 'Cubic Feet');
                //commodityWrapper.weight = getValueUnitWrap(checkNullDecimal(commodity.commodity.Weight_value__c), checkNull(commodity.commodity.Weight_Unit_of_Measure__c));
                //commodity.commodity.Weight_value__c = 0000000000;
                commodityWrapper.weight = getValueUnitWrap(Integer.valueOf(checkNullDecimal(commodity.commodity.Weight_value__c)), 'L');
               
                //commodityWrapper.quantity = getValueUnitWrap(checkNullDecimal(commodity.commodity.Quantity_value__c), checkNull(commodity.commodity.Quantity_Unit_of_Measure__c));
                //commodity.commodity.Quantity_value__c = 000;
                commodityWrapper.quantity = getValueUnitWrap(Integer.valueOf(checkNullDecimal(commodity.commodity.Quantity_value__c)), 'SKD');
                commodityWrapper.permitNumber = checkNull(commodity.commodity.Permit_Number__c);
                commodityWrapper.flashTemperature = getValueUnitWrap(checkNullDecimal(commodity.commodity.Flash_Temperature_value__c), checkNull(commodity.commodity.Flash_Temperature_Unit_of_Measure__c));
                commodityWrapper.isLimitedQuantity = (commodity.commodity.Is_Limited_Quantity__c) ? 'true' : '';
                commodityWrapper.isMarinePollutant = (commodity.commodity.Is_Marine_Pollutant__c) ? 'true' : '';
                commodityWrapper.reportedQty = String.valueOf(checkNullDecimal(commodity.commodity.Reported_Quantity__c));
                commodityWrapperList.add(commodityWrapper);
            }
        }
        return commodityWrapperList;
    }

    private static Crowley_CICSSoftshipBookingRequestCls.EmergencyContact getEmergencyContact(String name, String phoneNumber){
        Crowley_CICSSoftshipBookingRequestCls.EmergencyContact emergencyContact = new Crowley_CICSSoftshipBookingRequestCls.EmergencyContact();
        emergencyContact.phoneNumber = phoneNumber;
        emergencyContact.name = name;
        return emergencyContact;
    }
    
    private static List<Crowley_CICSSoftshipBookingRequestCls.RequirementWrapperCls> getRequirments(List<BookingWrapper.RequirementWrapper> listRequirementWrapper, BookingWrapper bookingWrapper){
        List<Crowley_CICSSoftshipBookingRequestCls.RequirementWrapperCls> requirementWrapperList = new List<Crowley_CICSSoftshipBookingRequestCls.RequirementWrapperCls>();
        if(listRequirementWrapper != null && !listRequirementWrapper.isEmpty()){
            Set<String> typeSet = new Set<String>();
            for(BookingWrapper.RequirementWrapper requirement : listRequirementWrapper){
                typeSet.add(requirement.requirement.Container_Type__c);
            }
            Map<String,Container__c> mapContainer= new Map<String,Container__c>();
            for(Container__c container : [Select Id, Name, CICS_ISO_Code__c, Softship_ISO_Code__c,MainFrameCode__c from Container__c where CICS_ISO_Code__c=:typeSet ORDER BY NAME ASC]){
                mapContainer.put(container.CICS_ISO_Code__c, container);
            }
            
            for(BookingWrapper.RequirementWrapper requirement : listRequirementWrapper){
                Crowley_CICSSoftshipBookingRequestCls.RequirementWrapperCls requirementWrapper = new Crowley_CICSSoftshipBookingRequestCls.RequirementWrapperCls();
                requirementWrapper.quantity = checkNullDecimal(requirement.requirement.Quantity__c);
                requirementWrapper.specEquipCd = checkNull(requirement.requirement.Special_Equipment_Code__c);
                requirementWrapper.rrInd = requirement.requirement.Running_Reefer__c ? 'Y' : 'N';
                requirementWrapper.tarpInd = requirement.requirement.Tarp__c ? 'Y' : '';
                requirementWrapper.length = checkNullDecimal(requirement.requirement.Length__c);                
                requirementWrapper.weight = getValueUnitWrap(checkNullDecimal(requirement.requirement.Weight__c), checkNull(requirement.requirement.Weights_Unit_of_Measure__c));
                //requirementWrapper.type = checkNull(requirement.requirement.Container_Type__c);
                requirementWrapper.type = checkNull(mapContainer.get(requirement.requirement.Container_Type__c).MainFrameCode__c);
                //requirementWrapper.type = '4500';
                requirementWrapper.termsCd = checkNull(bookingWrapper.booking.Origin_Type__c) + checkNull(bookingWrapper.booking.Destination_Type__c);
                requirementWrapper.reqId = checkNull(requirement.requirement.RequirementId__c);
                requirementWrapper.highCubeInd = requirement.requirement.High_Cube__c ? 'Y' : '';
                requirementWrapper.ventilated = checkNull(requirement.requirement.Ventilated__c);
                requirementWrapper.temperature = getValueUnitWrap(checkNullDecimal(requirement.requirement.Temperature__c), checkNull(requirement.requirement.Temperatures_Unit_of_Measure__c));
                requirementWrapper.width = checkNullDecimal(requirement.requirement.Width__c);
                requirementWrapper.category = checkNull(requirement.requirement.Category__c);
                requirementWrapperList.add(requirementWrapper);
            }
        }
        return requirementWrapperList;
    }

    private static String setVentilated(String ventilated){

        Map<String, String> mapVentilated = new Map<String, String>();
        mapVentilated.put('Vent 25% Open','A');
        mapVentilated.put('Vent 50% Open','B');
        mapVentilated.put('Vent 75% Open','C');
        mapVentilated.put('Vent 100% Open','D');
        mapVentilated.put('Vent Closed','E');
        mapVentilated.put('Vent 10% Open','F');
        mapVentilated.put('Carrier to Set Based on Commodity Type','Z');

        if(mapVentilated.get(ventilated)!=null){
            return mapVentilated.get(ventilated);
        }

        return '';

    }

    private static String checkNull(Object value){
        String valueString = String.valueOf(value);
        return (valueString != null ? valueString : '');
    }

    private static Decimal checkNullDecimal(Decimal value){
        return (value != null ? value : 0.00);
    }
    
}