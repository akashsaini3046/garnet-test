public class CC_CustomLookupController {
	@AuraEnabled
    public static List<Location__c> getZipLocations(String searchKeyWord,String countryName){
        System.debug('searchKeyWord '+searchKeyWord);
        System.debug('countryName '+countryName);
        try{
            String searchString ='%'+searchKeyWord+'%' ;
            List<Location__c> zipLocations = [SELECT Name, LcCode__c, UN_Location_Code__c,Location_Name__c, Country_Name__c FROM Location__c 
                                              WHERE Location_Type__c = 'ZIPCODE'AND (Name LIKE :searchString OR Location_Name__c LIKE :searchString) AND Country_Name__c=:countryName            
                                              LIMIT 10 ];
            System.debug('zipLocations size '+zipLocations.size());
            return zipLocations;
            
            

        }
        catch(Exception ex){
            throw ex;
        }
    }
    
    @AuraEnabled
    public static List<Location__c> getPortLocations(String searchKeyWord,String countryName){
        System.debug('searchKeyWord '+searchKeyWord);
        System.debug('countryName '+countryName);
        try{
            String searchString ='%'+searchKeyWord+'%' ;
            List<Location__c> portLocations = [SELECT Name,Location_Name__c,City__c,Country_Name__c FROM Location__c 
                                               WHERE (Location_Type__c = 'CITY' OR Location_Type__c='PORT') AND (Name LIKE :searchString OR  City__c LIKE :searchString) AND Country_Name__c=:countryName            
                                               LIMIT 10 ];
            System.debug('portLocations size '+portLocations.size());
            return portLocations;
        }
        catch(Exception ex){
            throw ex;
        }
                        
        }
        @AuraEnabled
        public static List<Location__c> getRailLocations(String searchKeyWord,String countryName){
            System.debug('searchKeyWord '+searchKeyWord);
            System.debug('countryName '+countryName);
            try{
                String searchString ='%'+searchKeyWord+'%' ;
                List<Location__c> portLocations = [SELECT Name,Location_Name__c,City__c,Country_Name__c FROM Location__c 
                                                   WHERE (Location_Type__c = 'CITY' OR Location_Type__c='PORT') AND (Name LIKE :searchString OR  City__c LIKE :searchString) AND Country_Name__c=:countryName            
                                                   LIMIT 10 ];
                System.debug('portLocations size '+portLocations.size());
                return portLocations;                                
                
            }
            catch(Exception ex){
                throw ex;
        }
           
        }
    
    @AuraEnabled
    public static List<Location__c> getCountries(String searchKeyWord,String type){
        System.debug('searchKeyWord '+searchKeyWord);
        System.debug('type '+type);
        List<Location__c> countries = new List<Location__c>();
        try{
            String searchString ='%'+searchKeyWord+'%' ;
            if(type=='D'){
                System.debug('hereee');
                countries = [SELECT Name,Country_Name__c FROM Location__c 
                             WHERE RecordType.Name='Country' AND Country_Name__c LIKE :searchString AND (displayCountryIn__c='Door' OR displayCountryIn__c='Both')    
                             LIMIT 10 ];
                System.debug(countries);
            }
            if(type=='P'){
                countries = [SELECT Name,Country_Name__c FROM Location__c 
                             WHERE RecordType.Name='Country' AND Country_Name__c LIKE :searchString AND (displayCountryIn__c='Port' OR displayCountryIn__c='Both')       
                             LIMIT 10 ];
            }
            if(type=='R'){
                countries = [SELECT Name,Country_Name__c FROM Location__c 
                             WHERE RecordType.Name='Country' AND Country_Name__c LIKE :searchString AND Country_Name__c ='United States'      
                             LIMIT 10 ];
            }
            System.debug('countries size '+countries.size());
            return countries;                                
            
        }
        catch(Exception ex){
            throw ex;
        }
        
    }
}