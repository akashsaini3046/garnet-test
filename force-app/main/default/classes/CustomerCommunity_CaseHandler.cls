public class CustomerCommunity_CaseHandler {
    @AuraEnabled
    public static List<Case> fetchCaseRecords() {
        List<Case> listcs = new List<Case>();
        Id userId = UserInfo.getUserId();
        User userDetail = new User();
        Contact conDetail = new Contact();
        
        try{        
            userDetail = [select Id, contactId from User where Id = :userId];
            System.debug('userDetail-> '+userDetail);
            if(userDetail.contactId != Null) {
                conDetail = [SELECT Id, Name, AccountId FROM Contact WHERE Id = :userDetail.contactId];
                
                listcs = [Select Id, CaseNumber, Type, Subject, CreatedDate, Status, Contact.Name FROM Case 
                          WHERE Type != :CustomerCommunity_Constants.EMPTY_STRING AND Contact.AccountId = :conDetail.AccountId LIMIT 5];
                if(!listcs.isEmpty())
                    return listcs;
            }
            return null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
    
    @AuraEnabled
    public static List<CustomerCommunity_QuickLinks> getQuickLinks() {
        List<CustomerCommunity_QuickLinks> listqlinks = new List<CustomerCommunity_QuickLinks>();
        
        CustomerCommunity_QuickLinks ql1 = new CustomerCommunity_QuickLinks();
        ql1.linkTitle = 'Cargo - Report Loss/Damage';
        ql1.linkRef = 'https://www.google.com';
        listqlinks.add(ql1);
        
        CustomerCommunity_QuickLinks ql2 = new CustomerCommunity_QuickLinks();
        ql2.linkTitle = 'Sailing Schedules';
        ql2.linkRef = 'https://www.google.com';
        listqlinks.add(ql2);
        
        CustomerCommunity_QuickLinks ql3 = new CustomerCommunity_QuickLinks();
        ql3.linkTitle = 'Shipment Tracking';
        ql3.linkRef = 'https://www.google.com';
        listqlinks.add(ql3);
        
        CustomerCommunity_QuickLinks ql4 = new CustomerCommunity_QuickLinks();
        ql4.linkTitle = 'Register for eBusiness';
        ql4.linkRef = 'https://www.google.com';
        listqlinks.add(ql4);
        
        CustomerCommunity_QuickLinks ql5 = new CustomerCommunity_QuickLinks();
        ql5.linkTitle = 'Become a customer';
        ql5.linkRef = 'https://www.google.com';
        listqlinks.add(ql5);
        
        return listqlinks;
    }
     public static void updateUserDetailsOnCase(List<Case> newCase){
        Id userId = UserInfo.getUserId();
        User userDetail = new User();
        Contact conDetail = new Contact();
        List<SObject> queueObj = [select Id from Group where Type = 'Queue' and Name = 'Community_Cases'];
        try{
            userDetail = [SELECT contactId FROM User WHERE Id = : userId];
            if(userDetail.contactId != NULL){
                conDetail = [SELECT AccountId FROM Contact WHERE Id = : userDetail.contactId];  
                if(conDetail.AccountId != NULL){
                    for(Case caseObj : newCase){
                        caseObj.contactId = userDetail.contactId;
                        caseObj.accountId = conDetail.AccountId;
                        caseObj.OwnerId =  queueObj[0].Id;
                    }  
                }
            }
            
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex); 
        }
    }
}