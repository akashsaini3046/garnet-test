public class CustomerCommunity_StaticVariables {
    private static boolean recursiveCheck = true;
    public static boolean runOnce() 
    {
        if (recursiveCheck) 
        {
            recursiveCheck = false;
            return true;
        } else 
        {
            return recursiveCheck;
        }
    }
}