public inherited sharing class SoqlUtility {
	public static List<sObject> dynamicSOQL(String fields, String objectSOQL, String whereCondition, String orderBy, String limitSOQL){
        String query = 'SELECT ' + fields + ' FROM ' + objectSOQL;
        if(String.isNotBlank(whereCondition)){
            query += ' WHERE ' + String.escapeSingleQuotes(whereCondition);
        }
        if(String.isNotBlank(orderBy)){
            query += ' ORDER BY ' + orderBy; 
        }
        if(String.isNotBlank(limitSOQL)){
            query += ' Limit ' + limitSOQL;
        }
        system.debug('### query= '+ query);
       
        return Database.query(query);
    }
	
}