global class LocationUpdateBatch implements Database.Batchable <SObject> {
    //Start Method
    global Database.QueryLocator Start(Database.BatchableContext bc) {
        String Query = 'Select Name from Location__c where Location_Type__c = \'ZIPCODE\'' ;
        return Database.getQueryLocator(Query);
    }
    //Execute Method
    global void execute(Database.BatchableContext bc, List<Location__c> Scope) {
        if(Scope != null && !Scope.isEmpty()) {
            List<Location__c> locs = new List<Location__c> ();
            for(Location__c loc : Scope) {
                String zip = loc.Name;
                integer len = zip.length();
                if(len<5){
                    System.debug('@@@@ '+zip);
                    Integer i= 5-len;
                    for(integer j=0;j<i;j++){
                        zip ='0'+zip;
                    }
                    System.debug('#### '+zip);
                    loc.Name = zip; 
                    locs.add(loc) ;
                }
            }
            update locs;
        }
    }
    //Finish Method
    global void finish(Database.BatchableContext bc) {
        Id BatchId = bc.getJobId();
        System.debug('BatchId:: '+ BatchId);
    }
    
}