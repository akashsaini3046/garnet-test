/*
* Name: AccountDashboardTest
* Purpose: Test class for account dashbaord classes
* Author: Nagarro
* Created Date: 23-Jan-2019
* Modification History
*  Modification #   Story/Defect#      Modified By     Date   Description
*/
@isTest(seeAllData=false)
public class ADAccountDashboardTest {
    
    public static testMethod void testGetCurrentYear() {
        System.assert(ADAccountDashboardController.getCurrentYear() == String.valueOf(Date.today().year()));
    }
    
    public static testMethod void testGetOpportunityChartData() {
        createOpportunityLineItem();
        
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedYear = String.valueOf(Date.today().year());
        ADOpportunityPipelineChartDataWrapper objOpportunityPipelineChartDataWrapper = ADOpportunityPipelineChartController.getOpportunityChartData([SELECT Id FROM Account WHERE NAME = 'Test Account'][0].Id, objMasterFilterWrapper);
        System.assert(objOpportunityPipelineChartDataWrapper.lstOpportunityStage.contains('Prospecting'));
        System.assert(objOpportunityPipelineChartDataWrapper.lstOpportunityPipelineAmountByStageActual.contains(2.00));
    }
    
    public static testMethod void testGetOpportunityChartDataSizeFilter() {
        createOpportunityLineItem();
        
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedYear = String.valueOf(Date.today().year());
        
        objMasterFilterWrapper.strSelectedOppSizeFilterOperator = '>=';
        objMasterFilterWrapper.strOppSizeVal = String.valueOf((2.00/(Integer.valueOf(Label.AD_AMOUNT_CONVERTER))));
        ADOpportunityPipelineChartDataWrapper objOpportunityPipelineChartDataWrapper = ADOpportunityPipelineChartController.getOpportunityChartData([SELECT Id FROM Account WHERE NAME = 'Test Account'][0].Id, objMasterFilterWrapper);
        System.assert(objOpportunityPipelineChartDataWrapper.lstOpportunityPipelineAmountByStageActual.contains(2.00));
        
        objMasterFilterWrapper.strSelectedOppSizeFilterOperator = '>';
        objMasterFilterWrapper.strOppSizeVal = String.valueOf((1.00/(Integer.valueOf(Label.AD_AMOUNT_CONVERTER))));
        objOpportunityPipelineChartDataWrapper = ADOpportunityPipelineChartController.getOpportunityChartData([SELECT Id FROM Account WHERE NAME = 'Test Account'][0].Id, objMasterFilterWrapper);
        System.assert(objOpportunityPipelineChartDataWrapper.lstOpportunityPipelineAmountByStageActual.contains(2.00));
        
        objMasterFilterWrapper.strSelectedOppSizeFilterOperator = '<';
        objMasterFilterWrapper.strOppSizeVal = String.valueOf((3.00/Integer.valueOf(Label.AD_AMOUNT_CONVERTER)));
        objOpportunityPipelineChartDataWrapper = ADOpportunityPipelineChartController.getOpportunityChartData([SELECT Id FROM Account WHERE NAME = 'Test Account'][0].Id, objMasterFilterWrapper);
        System.assert(objOpportunityPipelineChartDataWrapper.lstOpportunityPipelineAmountByStageActual.contains(2.00));
        
        objMasterFilterWrapper.strSelectedOppSizeFilterOperator = '<=';
        objMasterFilterWrapper.strOppSizeVal = String.valueOf((2.00/(Integer.valueOf(Label.AD_AMOUNT_CONVERTER))));
        objOpportunityPipelineChartDataWrapper = ADOpportunityPipelineChartController.getOpportunityChartData([SELECT Id FROM Account WHERE NAME = 'Test Account'][0].Id, objMasterFilterWrapper);
        System.assert(objOpportunityPipelineChartDataWrapper.lstOpportunityPipelineAmountByStageActual.contains(2.00));
        
        objMasterFilterWrapper.strSelectedOppSizeFilterOperator = '=';
        objMasterFilterWrapper.strOppSizeVal = String.valueOf((2.00/(Integer.valueOf(Label.AD_AMOUNT_CONVERTER))));
        objOpportunityPipelineChartDataWrapper = ADOpportunityPipelineChartController.getOpportunityChartData([SELECT Id FROM Account WHERE NAME = 'Test Account'][0].Id, objMasterFilterWrapper);
        System.assert(objOpportunityPipelineChartDataWrapper.lstOpportunityPipelineAmountByStageActual.contains(2.00));
        
    }
    
    public static testMethod void testGetOpportunityForAllYear() {
        createOpportunityLineItem();
        
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedYear = Label.AD_PICKLIST_VAL_ALL_YEAR;
        ADOpportunityPipelineChartDataWrapper objOpportunityPipelineChartDataWrapper = ADOpportunityPipelineChartController.getOpportunityChartData([SELECT Id FROM Account WHERE NAME = 'Test Account'][0].Id, objMasterFilterWrapper);
        System.assert(objOpportunityPipelineChartDataWrapper.lstOpportunityPipelineAmountByStageActual.contains(2.00));
    }
    
    public static testMethod void testGetOpportunityForDefaultYear() {
        createOpportunityLineItem();
        
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedYear = null;
        ADOpportunityPipelineChartDataWrapper objOpportunityPipelineChartDataWrapper = ADOpportunityPipelineChartController.getOpportunityChartData([SELECT Id FROM Account WHERE NAME = 'Test Account'][0].Id, objMasterFilterWrapper);
        System.assert(objOpportunityPipelineChartDataWrapper.lstOpportunityPipelineAmountByStageActual.contains(2.00));
    }
    
    public static testMethod void testGetAllChildAccounts() {
        System.assert(ADAccountDashboardUtil.getAllChildAccounts([SELECT Id FROM Account WHERE NAME = 'Test Account'][0].Id).size() == 1);
    }
    
    public static testMethod void testGetAllActiveUsers() {
        System.assert(ADAccountDashboardUtil.getAllActiveUsers() != null);
    }
    
    public static testMethod void testGetPicklistYear() {
        System.assert(ADAccountDashboardUtil.getPicklistYear().contains(String.valueOf(Date.today().year())));
    }
    
    public static testMethod void testAccountFilterAccNameWise() {
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        mapAccount = new Map<Id, Account>([SELECT Id From Account]);
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedTopFilter = Label.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME;
        objMasterFilterWrapper.lstSelectedAccounts = new List<Id>{[SELECT Id FROM Account WHERE NAME = 'Child Test Account'][0].Id};
            List<Id> lstFilteredAccounts = ADAccountDashboardUtil.applyAccountFilter(mapAccount, objMasterFilterWrapper);
        System.assert(new Set<Id>(lstFilteredAccounts).contains([SELECT Id From Account WHERE Name = 'Child Test Account'][0].Id));
    }
    
    public static testMethod void testAccountFilterAccOwnerWise() {
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        mapAccount = new Map<Id, Account>([SELECT Id, OwnerId From Account]);
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedTopFilter = Label.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_OWNER;
        objMasterFilterWrapper.lstSelectedOwners = new List<Id>{[SELECT Id, OwnerId FROM Account WHERE NAME = 'Child Test Account'][0].OwnerId};
            List<Id> lstFilteredAccounts = ADAccountDashboardUtil.applyAccountFilter(mapAccount, objMasterFilterWrapper);
        System.assert(new Set<Id>(lstFilteredAccounts).contains([SELECT Id From Account WHERE Name = 'Child Test Account'][0].Id));
    }
    
    public static testMethod void testGetAllChildAccountsWithParent() {
        Account objAccount = [SELECT Id From Account WHERE Name = 'Test Account'][0];
        List<Account> lstAccounts = ADAccountDashboardUtil.getAllChildAccountsWithParent(objAccount.Id);
        System.assert(lstAccounts[0].Id == objAccount.Id);
    }
    
    
    @testSetup
    public static void testDataSetup() {
        //Create Account
        Account testAccount = new Account();
        testAccount.name = 'Test Account';
        testAccount.Industry = 'Apparel';
        insert testAccount;
        
        //Create Child Account
        Account testChildAccount = new Account();
        testChildAccount.name = 'Child Test Account';
        testChildAccount.Industry = 'Apparel';
        testChildAccount.ParentId = testAccount.Id;
        insert testChildAccount;
        
        DescribeFieldResult describe1 = Address__c.Country__c.getDescribe();
        List<PicklistEntry> availableValuesCountry = describe1.getPicklistValues();
        
   //     DescribeFieldResult describe = Address__c.State_Picklist__c.getDescribe();
   //     List<PicklistEntry> availableValues = describe.getPicklistValues();
        
        //Create business location
        Address__c testAddress = new Address__c();
        testAddress.Account__c = testAccount.Id;
        testAddress.Address_Line_1__c = 'Test Address line 1';
        testAddress.City__c = 'Test City';
       // testAddress.State_Picklist__c = availableValues[0].getValue();
       testAddress.State__c =  'State1';
        testAddress.Postal_Code__c = '123456';
       // testAddress.Country__c = 'US';
        if(availableValuesCountry.size()>0)
            testAddress.Country__c = availableValuesCountry[0].getValue();
        insert testAddress;
        
        //Create Contact
        Contact testContact = new Contact();
        testContact.Address__c = testAddress.Id;
        testContact.LastName = 'Test LastName';
        testContact.Email = 'test@testExample.com';
        insert testContact;
        
        //Create Opportunity
        Id idOpportunityRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity Record').getRecordTypeId();
        Opportunity testOpportunity = new Opportunity();
        testOpportunity.RecordTypeId = idOpportunityRecordTypeId;
        testOpportunity.Contact__c = testContact.Id;
        testOpportunity.AccountId = testAccount.Id;
        testOpportunity.StageName = 'Prospecting';
        testOpportunity.Region_Picklist__c = 'USA - United States';
        testOpportunity.Name = 'Test Opportunity';
        testOpportunity.CloseDate = Date.today();
        testOpportunity.Service_Type__c = 'Air';
        insert testOpportunity;
        System.debug('testOpportunity-> '+testOpportunity);
        
        
        //Create product
        Product2 objProduct = new Product2();
        objProduct.Name = 'Air';
        insert objProduct;
        
        //Instantiate the Pricebook2 record with StandardPricebookId
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Update standardPricebook;
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = objProduct.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        System.debug('=========>' + [Select Id, Name from Pricebook2]);     
    }
    
    private static void createOpportunityLineItem() {
        //Create Opportunity LineItem
        OpportunityLineItem objOpportunityLineItem = new OpportunityLineItem();
        objOpportunityLineItem.OpportunityId = [SELECT Id FROM Opportunity][0].Id;
        objOpportunityLineItem.Product2Id = [SELECT Id FROM Product2][0].Id;
        objOpportunityLineItem.Quantity = 1;
        objOpportunityLineItem.UnitPrice = 2;
        objOpportunityLineItem.PricebookEntryId = [SELECT Id FROM PricebookEntry][0].Id;
        insert objOpportunityLineItem;
    }
    
    //Create Task data for class: ADTasksController
    private static List<Task> createTask() {
        List<Task> taskList = new List<Task>();
        Task t1 = new Task();
        t1.Type = 'Email';
        t1.Description = 'Task description 1';
        t1.Priority = 'High';
        t1.Status = 'Open';
        t1.WhatId = [SELECT Id From Account WHERE Name = 'Test Account'][0].Id;
        t1.Subject = 'Task subject 1';
        t1.ActivityDate = null;
        insert t1;
        taskList.add(t1);
        return taskList;
    }
    
    //Test method 
    //Class: ADTasksController
    //Method: fetchTasks
    public static testMethod void testFetchTask() {
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedTopFilter = Label.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME;
        objMasterFilterWrapper.lstSelectedAccounts = new List<Id>{[SELECT Id FROM Account WHERE Name = 'Test Account'][0].Id};
            objMasterFilterWrapper.strSelectedYear = String.valueOf(Date.today().year());   
        
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        mapAccount = new Map<Id, Account>([SELECT Id From Account]);
        List<Id> lstFilteredAccounts = ADAccountDashboardUtil.applyAccountFilter(mapAccount, objMasterFilterWrapper);
        
        ADTasksController.fetchTasks(lstFilteredAccounts[0], objMasterFilterWrapper);
        System.assert(new Set<Id>(lstFilteredAccounts).contains([SELECT Id From Account WHERE Name = 'Test Account'][0].Id));
    }
    
    //Test method 
    //Class: ADTasksController
    //Method: applyFilter
    public static testMethod void testTaskFilter() {
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedTopFilter = Label.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME;
        objMasterFilterWrapper.lstSelectedAccounts = new List<Id>{[SELECT Id FROM Account WHERE Name = 'Test Account'][0].Id};
            objMasterFilterWrapper.strSelectedYear = String.valueOf(Date.today().year());   
        
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        mapAccount = new Map<Id, Account>([SELECT Id From Account]);
        List<Id> lstFilteredAccounts = ADAccountDashboardUtil.applyAccountFilter(mapAccount, objMasterFilterWrapper);
        
        System.assert(new Set<Id>(lstFilteredAccounts).contains([SELECT Id From Account WHERE Name = 'Test Account'][0].Id));
        
        List<Task> taskList = new List<Task>();
        taskList = createTask();
        
        System.assertEquals(!taskList.isEmpty(), taskList.size()>0);
        
        List<sObject> lstSelectedOwners = new List<sObject>{[SELECT Id, OwnerId FROM Account WHERE NAME = 'Test Account'][0]};
            Date startDate = System.today()-2;
        Date endDate = System.today()+2;
        String dateFilter = ConstantClass.DATE_FILTER_CREATED_DATE;
        ADTasksController.applyFilter(lstFilteredAccounts[0], startDate, endDate, dateFilter, lstSelectedOwners, taskList);
        
        dateFilter = ConstantClass.DATE_FILTER_DUE_DATE;
        ADTasksController.applyFilter(lstFilteredAccounts[0], startDate, endDate, dateFilter, lstSelectedOwners, taskList);
        
        dateFilter ='';
        ADTasksController.applyFilter(lstFilteredAccounts[0], startDate, endDate, dateFilter, lstSelectedOwners, taskList);
        
        dateFilter ='';
        lstSelectedOwners = NULL;
        ADTasksController.applyFilter(lstFilteredAccounts[0], startDate, endDate, dateFilter, lstSelectedOwners, taskList);
    }
    
    //Test method 
    //Class: ADTasksController
    //Method: applySorting
    public static testMethod void testTaskSorting() {
        List<Task> taskList = new List<Task>();
        taskList = createTask();
        System.assertEquals(!taskList.isEmpty(), taskList.size()>0);
        
        String sortingDateOption = ConstantClass.SORTING_DUE_DATE_ASC;
        ADTasksController.applySorting(sortingDateOption, taskList);
        
        sortingDateOption = ConstantClass.SORTING_CREATED_DATE_ASC;
        ADTasksController.applySorting(sortingDateOption, taskList);
        
        sortingDateOption = ConstantClass.SORTING_DUE_DATE_DESC;
        ADTasksController.applySorting(sortingDateOption, taskList);
        
        sortingDateOption = ConstantClass.SORTING_CREATED_DATE_DESC;
        ADTasksController.applySorting(sortingDateOption, taskList);
    }
    
    //Create ContentNote data for class: ADNotesController
    public static ContentNote createContentNote(){
        ContentNote note = new ContentNote();
        note.Title = 'Test';
        note.Content = Blob.valueOf('Hello');
        insert note;
        
        return note;
    }
    
    //Test method 
    //Class: ADNotesController
    //Method: fetchNotes
    public static testMethod void testFetchNotes() {
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedTopFilter = Label.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME;
        objMasterFilterWrapper.lstSelectedAccounts = new List<Id>{[SELECT Id FROM Account WHERE Name = 'Test Account'][0].Id};
            objMasterFilterWrapper.strSelectedYear = String.valueOf(Date.today().year());   
        
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        mapAccount = new Map<Id, Account>([SELECT Id From Account]);
        List<Id> lstFilteredAccounts = ADAccountDashboardUtil.applyAccountFilter(mapAccount, objMasterFilterWrapper);
        System.assert(new Set<Id>(lstFilteredAccounts).contains([SELECT Id From Account WHERE Name = 'Test Account'][0].Id));
        
        ContentNote note = createContentNote();
        
        //Insert ContentDocumentLink
        ContentDocumentLink contentDocumentLinkObj = new ContentDocumentLink();
        contentDocumentLinkObj.contentdocumentid = note.id;
        contentDocumentLinkObj.LinkedEntityId = [SELECT Id From Account WHERE Id =: lstFilteredAccounts[0]][0].Id;
        contentDocumentLinkObj.ShareType= 'V';
        insert contentDocumentLinkObj;
        
        ADNotesController.fetchNotes(lstFilteredAccounts[0], objMasterFilterWrapper);
    }
    
    //Test method 
    //Class: ADNotesController
    //Method: applyFilter
    public static testMethod void testNotesFilter() {
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedTopFilter = Label.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME;
        objMasterFilterWrapper.lstSelectedAccounts = new List<Id>{[SELECT Id FROM Account WHERE Name = 'Test Account'][0].Id};
            objMasterFilterWrapper.strSelectedYear = String.valueOf(Date.today().year());   
        
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        mapAccount = new Map<Id, Account>([SELECT Id From Account]);
        List<Id> lstFilteredAccounts = ADAccountDashboardUtil.applyAccountFilter(mapAccount, objMasterFilterWrapper);
        System.assert(new Set<Id>(lstFilteredAccounts).contains([SELECT Id From Account WHERE Name = 'Test Account'][0].Id));
        
        ContentNote note = createContentNote();
        List<ContentNote> contentNotesList = new List<ContentNote>();
        contentNotesList.add(note);
        System.assertEquals(!contentNotesList.isEmpty(), contentNotesList.size()>0);
        
        List<sObject> lstSelectedOwners = new List<sObject>{[SELECT Id, OwnerId FROM Account WHERE NAME = 'Test Account'][0]};
            Date startDate = System.today()-2;
        Date endDate = System.today()+2;
        String dateFilter = ConstantClass.DATE_FILTER_CREATED_DATE;
        ADNotesController.applyFilter(lstFilteredAccounts[0], startDate, endDate, lstSelectedOwners, contentNotesList);
    }
    
    //Test method 
    //Class: ADNotesController
    //Method: applySorting
    public static testMethod void testNotesSorting() {
        ContentNote note = createContentNote();
        List<ContentNote> contentNotesList = new List<ContentNote>();
        contentNotesList.add(note);
        System.assertEquals(!contentNotesList.isEmpty(), contentNotesList.size()>0);
        
        String sortingDateOption = ConstantClass.SORTING_CREATED_DATE_ASC;
        ADNotesController.applySorting(contentNotesList, sortingDateOption);
        
        sortingDateOption = ConstantClass.SORTING_CREATED_DATE_DESC;
        ADNotesController.applySorting(contentNotesList, sortingDateOption);
    }
    
    //Test method 
    //Class: ADActualVsNormalisedRevenueController
    //Method: getActualAndNormalisedRevenue
    public static testMethod void testActualAndNormalisedRevenue() {
        
        Id accountId = [SELECT Id From Account WHERE Name = 'Test Account'][0].Id;
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedTopFilter = Label.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME;
        objMasterFilterWrapper.lstSelectedAccounts = new List<Id>{[SELECT Id FROM Account WHERE Id=:accountId].Id};
            objMasterFilterWrapper.strSelectedYear = String.valueOf(Date.today().year());   
        
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        mapAccount = new Map<Id, Account>([SELECT Id, OwnerId From Account where Id=:accountId]);
        List<Id> lstFilteredAccounts = ADAccountDashboardUtil.applyAccountFilter(mapAccount, objMasterFilterWrapper);
        System.assert(new Set<Id>(lstFilteredAccounts).contains([SELECT Id From Account WHERE Name = 'Test Account'][0].Id));
        
        ADActualVsNormalisedRevenueController.getActualAndNormalisedRevenue(lstFilteredAccounts[0], objMasterFilterWrapper);
    }
    
    //Test method 
    //Class: ADMasterFilterController
    public static testMethod void testMasterFilterComponent() {
        Id accountId = [SELECT Id From Account WHERE Name = 'Test Account'][0].Id;
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedTopFilter = Label.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME;
        objMasterFilterWrapper.lstSelectedAccounts = new List<Id>{[SELECT Id FROM Account WHERE Id=:accountId].Id};
            objMasterFilterWrapper.strSelectedYear = String.valueOf(Date.today().year());   
        
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        mapAccount = new Map<Id, Account>([SELECT Id, OwnerId From Account where Id=:accountId]);
        List<Id> lstFilteredAccounts = ADAccountDashboardUtil.applyAccountFilter(mapAccount, objMasterFilterWrapper);
        System.assert(new Set<Id>(lstFilteredAccounts).contains([SELECT Id From Account WHERE Name = 'Test Account'][0].Id));
        
        ADMasterFilterController.getAllChildAccount(lstFilteredAccounts[0]);
        ADMasterFilterController.getAllChildAndParent(lstFilteredAccounts[0]);
        ADMasterFilterController.getAllActiveUsers();
        ADMasterFilterController.getAllActiveUsersRelatedToAccount(lstFilteredAccounts[0]);
        ADMasterFilterController.getPicklistYear();
    }
    
    //Test method 
    //Class: ADAccountDashboardUtil
    //Method: fetchLookUpValues
    public static testMethod void testfetchLookUpValues() {
        String searchKeyWord = 'user1';
         List<Id> ownerIdList = new  List<Id>();
        List<Account> accList = [SELECT Id, OwnerId FROM Account WHERE NAME = 'Test Account'];
        for(Account acc: accList)
        {
            ownerIdList.add(acc.ownerId);
        }

        List<sObject> excludeitemsList = new List<sObject>{[SELECT Id, OwnerId FROM Account WHERE NAME = 'Test Account'][0]};
        ADAccountDashboardUtil.fetchLookUpValues(searchKeyWord, excludeitemsList, ownerIdList);
    }
    
    //Test method 
    //Class: AD_OpportunityConversionRateController
    //Method: getOpportunityConversionRate
    public static testMethod void testOpportunityConversionRate() {
         Id accountId = [SELECT Id From Account WHERE Name = 'Test Account'][0].Id;
        ADMasterFilterWrapper objMasterFilterWrapper = new ADMasterFilterWrapper();
        objMasterFilterWrapper.strSelectedTopFilter = Label.AD_PICKLIST_VAL_FILTER_BY_ACCOUNT_NAME;
        objMasterFilterWrapper.lstSelectedAccounts = new List<Id>{[SELECT Id FROM Account WHERE Id=:accountId].Id};
            objMasterFilterWrapper.strSelectedYear = String.valueOf(Date.today().year());   
        
        Map<String, String> oppChartMap = ADOpportunityConversionRateController.getOpportunityConversionRate(accountId, objMasterFilterWrapper);
    }
}