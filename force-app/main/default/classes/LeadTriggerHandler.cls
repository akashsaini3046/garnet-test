/*
* Name: LeadTriggerHandler
* Purpose: Handler class for Lead Trigger
* Author: Nagarro
* Created Date: 11/09/2018
* 
*  Modification History
*  Modification #   Story/Defect#      Modified By     Date   Description
*
*/

public with sharing class LeadTriggerHandler extends TriggerHandler{
    List<Lead> newLeadList;
    List<Lead> oldLeadList;
    public static boolean isRecursion = FALSE;
    Map<Id, Lead> oldLeadMap = new Map<Id, Lead>();
    Map<Id, Lead> newLeadMap = new Map<Id, Lead>();
   
    //Constructor
    public LeadTriggerHandler(){
        this.newLeadList = (List<Lead>) Trigger.new;
        this.oldLeadList = (List<Lead>) Trigger.old;
        this.oldLeadMap  = (Map<Id, Lead>) Trigger.oldMap;
        this.newLeadMap  = (Map<Id, Lead>) Trigger.newMap;
    }
    //Override the before insert method
    public override void beforeInsert(){
        //populateRegions(newLeadList, oldLeadMap);   
    }
    
    //Override the before update method
    public override void beforeUpdate(){
        //populateRegions(newLeadList, oldLeadMap); 
        populateDetailsBeforeConversion(newLeadList, oldLeadMap);
    }
    
    //Override the before delete method
    public override void beforeDelete(){
        restrictLeadDeletion(oldLeadList); 
    }
    
    public override void afterInsert(){
        if(isRecursion == false)
        {
            isRecursion = TRUE;
            List<Id> leadIdList = new List<Id>();
            for(Lead l: newLeadList)
                leadIdList.add(l.Id);
            createTask(leadIdList); 
        }
    }
    
    public override void afterUpdate(){
        if(isRecursion == false)
        {
            isRecursion = TRUE;
            List<Id> leadIdList = new List<Id>();
            for(Lead leadObj: newLeadList)
            {
                if(oldLeadMap.get(leadObj.Id).Followup__c != newLeadMap.get(leadObj.Id).Followup__c || oldLeadMap.get(leadObj.Id).OwnerId != newLeadMap.get(leadObj.Id).OwnerId
                || oldLeadMap.get(leadObj.Id).Rating != newLeadMap.get(leadObj.Id).Rating)
                { 
                    leadIdList.add(leadObj.Id);
                }
            }
            createTask(leadIdList);
        }
    }
    
    
    /*
* Method Name: populateRegions
* Input Parameters: Map<Id, Contact>
* Return value: void
* Purpose: Populates regions based on the country name popuated in the Address on the lead
*/
  /*  
    private void populateRegions(List<Lead> newLeadList, Map<Id, Lead> oldLeadMap){

        List<CountryRegionMapping__c> countryRegionMappingList = CountryRegionMapping__c.getAll().values();
        List<Schema.PicklistEntry> originCountryEntriesList = Schema.SObjectType.Lead.fields.Origin_Country1__c.getPicklistValues();
        Map<String, String> map_OriginCountry_Value_Name = new Map<String, String>();
        if(originCountryEntriesList != NULL && !originCountryEntriesList.isEmpty())
        {
            for(Schema.PicklistEntry pick: originCountryEntriesList)
            {
                map_OriginCountry_Value_Name.put(pick.getValue(), pick.getLabel());
            }
        }
        for(Lead leadObj: newLeadList)
        {    
            if(!String.IsBlank(leadObj.Origin_Country1__c) && ((null != oldLeadMap && leadObj.Origin_Country1__c != oldLeadMap.get(leadObj.Id).Origin_Country1__c) || null == oldLeadMap)){
                for(CountryRegionMapping__c countryRegionMap: countryRegionMappingList)
                {
                    System.debug('countryRegionMap.Country_Name__c--> '+countryRegionMap.Country_Name__c);
                    System.debug('map_OriginCountry_Value_Name.get(leadObj.Origin_Country1__c)--> '+map_OriginCountry_Value_Name.get(leadObj.Origin_Country1__c));
                    System.debug('countryRegionMap.Region__c--> '+countryRegionMap.Region__c);
                    if(map_OriginCountry_Value_Name.containsKey(leadObj.Origin_Country1__c))
                    {
                        if(countryRegionMap.Country_Name__c == map_OriginCountry_Value_Name.get(leadObj.Origin_Country1__c)){
                            leadObj.Region__c = countryRegionMap.Region__c;
                        }    
                    }
                }    
            }
        }        
    }
  */ 
    /*
* Method Name: restrictLeadDeletion
* Input Parameters: List<Lead>
* Return value: void
* Purpose: Restricts users other than the record creator to delete the record.
*/    
    
    public static void restrictLeadDeletion(List<Lead> leadList)
    {
        Id currentUserId = UserInfo.getUserId();
        Id profileId=UserInfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        List<Lead> recordsCannotBeDeletedList = new List<Lead>();
        if(leadList != NULL && !leadList.isEmpty())
        {
            for(Lead leadObj: leadList)
            {
                if(leadObj.CreatedById != currentUserId && !Label.SYSTEM_ADMINISTRATOR.contains(profileName ))
                {
                    try
                    {
                        leadObj.addError(Label.INSUFFICIENT_ACCESS_TO_DELETE_LEAD_RECORD);
                    }
                    catch(Exception e)
                    {
                        System.debug(logginglevel.ERROR, 'MESSAGE:'+ e.getMessage() +'STACK TRACE:'+e.getStackTraceString());
                    }
                }
            }
        }
    }
    
     
    
    
    /*
* Method Name: createTask
* Input Parameters: List<Lead>
* Return value: void
* Purpose: Creates task record attached to the lead record when followup is checked and record owner is a user.
* This is future method because the assignment date is populated using workflow and workflow is called after After Update
*/
    @future
    public static void createTask(List<Id> leadIdList)
    {
        List<Lead> leadList = new List<Lead>();
        leadList = [select id, Followup__c, OwnerId, AssignmentDate__c, Company, Rating from Lead where Id in: leadIdList];
        List<Task> tasksToCreateList = new List<Task>();
        if(leadList != NULL && !leadList.isEmpty())
        {
            for(Lead leadObj: leadList)
            {
                if(leadObj.Followup__c == TRUE && !String.valueOf(leadObj.OwnerId).startsWithIgnoreCase('00G') && leadObj.Company!='')
                {
                    if(leadObj.Rating!='HOT'){
                        Task t = new Task();
                        t.Subject = 'Lead Follow Up - '+ leadObj.Company;
                        t.Priority = 'Normal';
                        t.Status = 'Open';
                        t.TaskSubtype = 'Task';
                        t.WhoId = leadObj.Id;
                      //  t.ActivityDate = leadObj.AssignmentDate__c.date() + 2 ;
                        t.ActivityDate = Date.today() +2;
                        t.OwnerId = leadObj.OwnerId;
                        tasksToCreateList.add(t);
                    }else if(leadObj.Rating=='HOT'){
                        Task t = new Task();
                        t.Subject = 'Lead Follow Up - '+ leadObj.Company;
                        t.Priority = 'Normal';
                        t.Status = 'Open';
                        t.TaskSubtype = 'Task';
                        t.WhoId = leadObj.Id;
                      //  t.ActivityDate = leadObj.AssignmentDate__c.date() + 2 ;
                        t.ActivityDate = Date.today();
                        t.OwnerId = leadObj.OwnerId;
                        tasksToCreateList.add(t);
                    }
                }
            }
        }
        System.debug('tasksToCreateList->'+tasksToCreateList.size());
        if(tasksToCreateList != NULL && !tasksToCreateList.isEmpty())
        {
            insert tasksToCreateList;
        }
    }


    /*
* Method Name: populateDetailsBeforeConversion
* Input Parameters: List<Lead>
* Return value: void
* Purpose: User must complete contact details before converting the lead.
*/    
    public static void populateDetailsBeforeConversion(List<Lead> leadList,Map<Id, Lead> oldLeadMap)
    {
        for(Lead leadObj: leadList)
        {
            if(leadObj.IsConverted)
            {
                String errorMessage = Label.USER_MUST_ENTER_CONTACT_DETAILS;
                boolean isError = false;
                if(String.isBlank(leadObj.FirstName))
                { 
                    isError = true;
                    errorMessage = errorMessage + Label.FIRST_NAME;
                }
                if(String.isBlank(leadObj.Email) && String.isBlank(leadObj.Phone))
                {
                    isError = true;
                    errorMessage = errorMessage +Label.EMAIL_OR_PHONE;
                }
                if(String.isBlank(leadObj.Country) && String.isBlank(leadObj.City) && String.isBlank(leadObj.State) && String.isBlank(leadObj.Street))
                {
                    isError = true;
                    errorMessage = errorMessage + Label.BUSINESS_ADDRESS;
                }
                if(String.isBlank(leadObj.Company))
                {
                    isError = true;
                    errorMessage = errorMessage + Label.ACCOUNT_NAME;
                }

                if(oldLeadMap.get(leadObj.id).LastActivityDate == NULL)
                {
                    isError = true;
                    errorMessage = errorMessage + Label.LOG_ATLEAST_ONE_FOLLOW_UP_ACTIVITY;
                }
                if(isError) 
                {
                    leadObj.addError(errorMessage);
                }
            }
        }
        
    }
    
}