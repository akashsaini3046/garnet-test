@isTest public class CustomerCommunity_RegisterCtrl_Test {
   
    static testMethod void getUserFields(){
        
        CustomerCommunity_RegisterController.getUserFields();
    }
    static testMethod void getCountries(){
        CustomerCommunity_RegisterController.getCountries();
    }
    static testMethod void generateRandomString(){
        CustomerCommunity_RegisterController.generateRandomString(123);
    }
    @testSetup static void TestShareUser(){
        
        Account accountDetail = TestDataUtility.createAccount('Nagarro',null,null,False,1)[0];
 
        DescribeFieldResult describeState = Address__c.State_Picklist__c.getDescribe();
        List<PicklistEntry> stateAvailableValues = describeState.getPicklistValues();
        Address__c businessLocationObj = TestDataUtility.createBusinessLocation(new List<Account>{new Account(Id = accountDetail.Id)}, 'BL1', 'City1', 
                                                                                stateAvailableValues[0].getValue(), '1111111', '88888888888', 
                                                                                'US', 1)[0];
        
      
        
        Id recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Contact_Record_Type').getRecordTypeId();
        Contact contactDetail = TestDataUtility.createContact(recordTypeId,accountDetail.Id,new List<Address__c>{businessLocationObj},'testFirst','testLast','test@samp.com',null,False,null,1)[0];
        
    }
   
    static testMethod void checkIfAccountExistsPositive(){
       
        Account accountDetail = [Select Name from Account where Name = 'Nagarro'];
        Contact contactDetail = [Select FirstName,LastName,Email,Title,MailingCountry from Contact where LastName = 'testLast'];
           
        test.startTest();
        CustomerCommunity_RegisterController.checkIfAccountExists(accountDetail.Name,contactDetail.FirstName,contactDetail.LastName,contactDetail.Email,contactDetail.Title,
                                                                  contactDetail.MailingCountry);
        
        
        CustomerCommunity_RegisterController.checkIfAccountExists(accountDetail.Name, 'testFirst1', 'testLast1', 'testEmail1@samp.com', 'testTitle1', 'testCountry1');
        CustomerCommunity_RegisterController.checkIfAccountExists('test2','testFirst2','testLast2','testEmail2@samp.com','testTitle2','testCountry2');
        test.stopTest();
        
    }
    static testMethod void checkIfAccountExistsNegative(){
         
        Account accountDetail = [Select Name from Account where Name = 'Nagarro'];
        test.startTest();
        CustomerCommunity_RegisterController.checkIfAccountExists(accountDetail.Name, 'testFirst1', 'testLast1', 'testEmail1', 'testTitle1', 'testCountry1');
        CustomerCommunity_RegisterController.checkIfAccountExists('test3','testFirst3','testLast3','testEmail3','testTitle3','testCountry3');
        test.stopTest();
    }
    
    static testMethod void registerUser(){
        Account accountDetail = [Select Name from Account where Name='Nagarro'];
        Contact contactDetail = [Select FirstName,Email,LastName,Title,MailingCountry from Contact where LastName = 'testLast'];
       
       CustomerCommunity_UserRegWrapper newWrapperObj = new CustomerCommunity_UserRegWrapper();
        newWrapperObj.FirstName = contactDetail.FirstName;
        newWrapperObj.Email = contactDetail.Email;
        newWrapperObj.CompanyName = accountDetail.Name;
        newWrapperObj.LastName = contactDetail.LastName;
        newWrapperObj.UserName = 'test';
        newWrapperObj.Title = contactDetail.Title;
        newWrapperObj.PhoneNumber = '9999999999';
        newWrapperObj.Country = contactDetail.MailingCountry;
        CustomerCommunity_RegisterController.registerUser(newWrapperObj, contactDetail.Id);
        
        
        //Exception
        CustomerCommunity_UserRegWrapper newWrapperObjException = new CustomerCommunity_UserRegWrapper();
        String incorrectContactId = 'WrongContactID123';
        CustomerCommunity_RegisterController.registerUser(newWrapperObjException, incorrectContactId);
    }
}