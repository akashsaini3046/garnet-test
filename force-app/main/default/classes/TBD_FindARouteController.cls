public class TBD_FindARouteController {
 @AuraEnabled
    public static List<CustomerCommunity_RouteInformation> getRoutes(String originPort, String destinationPort, String shipmentType, String sailingWeeks){
        System.debug('Entered getRoutes() in CustomerCommunity_FindARouteController apex ');
        System.debug('Parameter values : '+originPort+' '+destinationPort+' '+shipmentType+' '+sailingWeeks);
        String dateText=date.today().format();
        String ServiceName = CustomerCommunity_Constants.FAR_SERVICE_NAME;
        //ServiceName = 'FindARoutePOST';
        try {
            Schema.DescribeFieldResult OriginFieldResult = Find_Route__c.Origin_Port__c.getDescribe();
            List<Schema.PicklistEntry> originList = OriginFieldResult.getPicklistValues();
            for (Schema.PicklistEntry p: originList) {
                if(p.getLabel() == originPort) {
                    originPort = p.getValue();
                    break;
                }
            }
            Schema.DescribeFieldResult destinationFieldResult = Find_Route__c.Destination_Port__c.getDescribe();
            List<Schema.PicklistEntry> destinationList = destinationFieldResult.getPicklistValues();
            for (Schema.PicklistEntry p: destinationList) {
                if(p.getLabel() == destinationPort) {
                    destinationPort = p.getValue();
                    break;
                }
            }
            JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStartObject();      
            gen.writeStringField('originPort', originPort);
            gen.writeStringField('destPort',destinationPort);
            gen.writeDateField('cargoReadyDate',System.Today());
            gen.writeStringField('FclLclData',shipmentType);
            gen.writeStringField('displayRoutes',sailingWeeks);
            gen.writeEndObject();    
            String requestbody = gen.getAsString();
            //String response = APICalloutUtility.callAPIResponseService(ServiceName, requestbody, Null, null);
            String response = CustomerCommunity_APICallouts.getAPIResponse(ServiceName, requestbody, Null);
            if(response != Null) {
                System.debug('Response size : --> '+response.length());
                List<CustomerCommunity_RouteInformation> routeInformationList = (List<CustomerCommunity_RouteInformation>)JSON.deserialize(response, List<CustomerCommunity_RouteInformation>.class);
                Integer count = 1;
                if(routeInformationList.size()>0) {
                    for(CustomerCommunity_RouteInformation ri : routeInformationList){
                        ri.stopCount = String.valueOf(ri.details.size()-1);
                        ri.routeCount = String.valueOf(routeInformationList.size());
                        ri.transitTime = ri.transitTime.substring(0,ri.transitTime.indexOf(CustomerCommunity_Constants.SPACE_STRING));
                        ri.sequenceCount = String.valueOf(count++);
                        for(CustomerCommunity_DetailInfo di : ri.details) {
                            di.transitTime = di.transitTime.substring(0,di.transitTime.indexOf(CustomerCommunity_Constants.SPACE_STRING));
                            Datetime startDt = Datetime.newInstance(Integer.valueOf(di.startDate.substring(6, di.startDate.length())), Integer.valueOf(di.startDate.substring(0, 2)), Integer.valueOf(di.startDate.substring(3, 5)));
                            di.startDay = startDt.format(CustomerCommunity_Constants.DATE_FORMAT);
                            Datetime arrivalDt = Datetime.newInstance(Integer.valueOf(di.arrivalDate.substring(6, di.arrivalDate.length())), Integer.valueOf(di.arrivalDate.substring(0, 2)), Integer.valueOf(di.arrivalDate.substring(3, 5)));
                            di.arrivalDay = arrivalDt.format(CustomerCommunity_Constants.DATE_FORMAT);
                        }
                    }
                    System.debug('Length  --->  '+routeInformationList.size());
                    System.debug('routeInformationList --> '+routeInformationList);
                    return routeInformationList;
                    
                }
            }
            System.debug('response is --> '+response);
            return Null;
        }
        catch(Exception ex){
            LogFactory.error('CustomerCommunity_FindARouteController', 'getRoutes', 'Error in Find a Route', ex.getMessage());
            LogFactory.saveLog();
            //System.debug('exception-> '+ex.getCause() + '**********'+ex.getMessage());
            //ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
}