public class CustomerCommunity_BookingValidtnResPDF {
    @AuraEnabled public Body Body{get;set;}
    public class ValidateBookingResult{
        @AuraEnabled public BookingStatus BookingStatus{get;set;}
        @AuraEnabled public SessionStatus SessionStatus{get;set;}
    }
    public class ValidateBookingResponse{
        @AuraEnabled public ValidateBookingResult ValidateBookingResult{get;set;}
    }
    public class StowageSegregationComments{
        @AuraEnabled public String Comment{get;set;}
        @AuraEnabled public String Code{get;set;}
    }
    public class StowageSegregation{
        @AuraEnabled public list<StowageSegregationComments> StowageSegregationComments{get;set;}
    }
    public class status{
        @AuraEnabled public DGStatus DGStatus{get;set;}
    }
    public class SessionStatus{
        @AuraEnabled public status status{get;set;}
    }
    public class Results{
        @AuraEnabled public DGResult DGResult{get;set;}
    }
    public class RestrictionStatus{
        @AuraEnabled public String CountCTUError{get;set;}
        @AuraEnabled public String CountItemInformation{get;set;}
        @AuraEnabled public String CountItemError{get;set;}
        @AuraEnabled public String CountItemWarning{get;set;}
        @AuraEnabled public String CountCTUWarning{get;set;}
        @AuraEnabled public String CountPackageError{get;set;}
        @AuraEnabled public String CountCTUInformation{get;set;}
        @AuraEnabled public String CountPackageInformation{get;set;}
        @AuraEnabled public String CountWarning{get;set;}
        @AuraEnabled public String CountPackageWarning{get;set;}
        @AuraEnabled public String CountInformation{get;set;}
        @AuraEnabled public String Reference{get;set;}
        @AuraEnabled public String CountError{get;set;}
    }
    public class PlacardsMarksLabels{
        @AuraEnabled public String ImageName{get;set;}
    }
    public class PlacardsAndMarks{
        //@AuraEnabled public list<PlacardsMarksLabels> PlacardsMarksLabels{get;set;}
    }
    public class PackagesStatus{
        @AuraEnabled public list<DGBookingPackageStatus> DGBookingPackageStatus{get;set;}
    }
    public class Message{
        @AuraEnabled public DGResult DGResult{get;set;}
    }
    public class LabelsAndMarks{
        //@AuraEnabled public list<PlacardsMarksLabels> PlacardsMarksLabels{get;set;}
    }
    public class ItemsStatus{
        @AuraEnabled public list<DGBookingItemStatus> DGBookingItemStatus{get;set;}
    }
    public class DGStatus{
        @AuraEnabled public Message Message{get;set;}
        @AuraEnabled public String CountInformation{get;set;}
        @AuraEnabled public String CountWarning{get;set;}
        @AuraEnabled public String CountError{get;set;}
    }
    public class DGResult{
        @AuraEnabled public String Code{get;set;}
        @AuraEnabled public String ResultSource{get;set;}
        @AuraEnabled public String Description{get;set;}
        @AuraEnabled public String Level{get;set;}
    }
    public class DGCTUStatus{
        @AuraEnabled public Results Results{get;set;}
        @AuraEnabled public String LoadDetails{get;set;}
        @AuraEnabled public String HasWarning{get;set;}
        @AuraEnabled public String HasWarningSummary{get;set;}
        @AuraEnabled public String HasErrorSummary{get;set;}
        @AuraEnabled public String RefID{get;set;}
        @AuraEnabled public String HasError{get;set;}
        @AuraEnabled public String DGCtuId{get;set;}
        @AuraEnabled public String CountRestrictionsWarning{get;set;}
        @AuraEnabled public String DGCtuRef{get;set;}
        @AuraEnabled public ItemsClashList ItemsClashList{get;set;}
        @AuraEnabled public PackagesClashList PackagesClashList{get;set;}
        @AuraEnabled public String CountRestrictionsInformation{get;set;}
        @AuraEnabled public ItemsStatus ItemsStatus{get;set;}
        @AuraEnabled public String CountRestrictionsError{get;set;}
        @AuraEnabled public PackagesStatus PackagesStatus{get;set;}
        @AuraEnabled public String CountRegsWarning{get;set;}
        @AuraEnabled public PlacardsAndMarks PlacardsAndMarks{get;set;}
        @AuraEnabled public String CountRegsInformation{get;set;}
        @AuraEnabled public StowageSegregation StowageSegregation{get;set;}
        @AuraEnabled public String CountRegsError{get;set;}
    }
    public class DGBookingPackageStatus{
        @AuraEnabled public String CountRegsError{get;set;}
        @AuraEnabled public String HasError{get;set;}
        @AuraEnabled public String CountRestrictionsWarning{get;set;}
        @AuraEnabled public String HasErrorSummary{get;set;}
        @AuraEnabled public String CountRestrictionsInformation{get;set;}
        @AuraEnabled public String HasWarning{get;set;}
        @AuraEnabled public String CountRestrictionsError{get;set;}
        @AuraEnabled public String HasWarningSummary{get;set;}
        @AuraEnabled public String CountRegsWarning{get;set;}
        @AuraEnabled public String Id{get;set;}
        @AuraEnabled public String CountRegsInformation{get;set;}
    }
    public class DGBookingItemStatus{
        @AuraEnabled public String HasWarning{get;set;}
        @AuraEnabled public String CountRegsWarning{get;set;}
        @AuraEnabled public String CountRestrictionsError{get;set;}
        @AuraEnabled public String CountRegsInformation{get;set;}
        @AuraEnabled public String CountRestrictionsInformation{get;set;}
        @AuraEnabled public String CountRegsError{get;set;}
        @AuraEnabled public String CountRestrictionsWarning{get;set;}
        //@AuraEnabled public Results Results{get;set;}
        @AuraEnabled public String RefID{get;set;}
        @AuraEnabled public String Id{get;set;}
        @AuraEnabled public String HasWarningSummary{get;set;}
        @AuraEnabled public LabelsAndMarks LabelsAndMarks{get;set;}
        @AuraEnabled public String HasErrorSummary{get;set;}
        @AuraEnabled public String HasError{get;set;}
    }
    public class CTUStatus{
        @AuraEnabled public list<DGCTUStatus> DGCTUStatus{get;set;}
    }
    public class BookingStatus{
        @AuraEnabled public String BookingRef{get;set;}
        @AuraEnabled public String HasErrorSummary{get;set;}
        @AuraEnabled public String HasError{get;set;}
        @AuraEnabled public String HasWarning{get;set;}
        @AuraEnabled public String CountRestrictionsWarning{get;set;}
        @AuraEnabled public String HasWarningSummary{get;set;}
        @AuraEnabled public String CountRestrictionsInformation{get;set;}
        @AuraEnabled public Results Results{get;set;}
        @AuraEnabled public String CountRestrictionsError{get;set;}
        @AuraEnabled public CTUStatus CTUStatus{get;set;}
        @AuraEnabled public String CountRegsWarning{get;set;}
        @AuraEnabled public RestrictionStatus RestrictionStatus{get;set;}
        @AuraEnabled public String CountRegsInformation{get;set;}
        @AuraEnabled public String CountRegsError{get;set;}
    }
    public class Body{
        @AuraEnabled public ValidateBookingResponse ValidateBookingResponse{get;set;}
    }
    
    public class ClashDetails {
        @AuraEnabled public String Degree{get;set;}	
    }
    
    public class ItemsClashList {
        //@AuraEnabled public List<ClashDetails> ClashDetails{get;set;}
    }
    
    public class PackagesClashList {
        @AuraEnabled public ClashDetails ClashDetails{get;set;}
    }
    
}