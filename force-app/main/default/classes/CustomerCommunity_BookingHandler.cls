public class CustomerCommunity_BookingHandler {
    @AuraEnabled
    public static List<Booking__c> fetchBookingRecords() {
        List<Booking__c> listBooking = new List<Booking__c>();
        Id userId = UserInfo.getUserId();
        User userDetail = new User();
        Contact conDetail = new Contact();
        try{
            userDetail = [select Id, contactId from User where Id = : userId];
            if(userDetail.contactId != Null) {
                conDetail = [SELECT Id, Name, AccountId FROM Contact WHERE Id = :userDetail.contactId];
                if(conDetail != Null)
                    listBooking = [Select Id, Name, Account__c, /*Date_of_Discharge__c, Date_of_Loading__c, Destination__c, Origin__c,*/ Status__c, Voyage_Vessel__c 
                                   FROM Booking__c WHERE Account__c = : conDetail.AccountId LIMIT 5];
            }
            if(!listBooking.isEmpty())
                return listBooking;
            else
                return null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        } 
    }
}