public class CustomerCommunity_FileController {
    
    @AuraEnabled
    public static String getValidBookingId(String bookingName){
        ID IDBooking;
        try {
            IDBooking = [SELECT ID FROM Booking__c WHERE Name = :bookingName OR Booking_Number__c = :bookingName LIMIT 1].ID;
            if(IDBooking != NULL){
                return String.valueOf(IDBooking);
            }
            return 'FALSE';
        }
        catch(Exception ex){   
            ExceptionHandler.logApexCalloutError(ex);
            return 'FALSE';
        }    
    }
    
    @AuraEnabled
    public static String getValidBooking(String bookingName,String documentType, String headerSource){
        
        ID IDDocument,IDBooking;
        try {
            IDBooking = [SELECT ID FROM Booking__c WHERE Name = :bookingName OR Booking_Number__c = :bookingName LIMIT 1].ID;
            if(IDBooking != NULL){
                if(documentType == 'Shipping Instructions'){
                    IDDocument = createShippingInstructionRecord(IDBooking, bookingName);
                } else if(documentType.equalsIgnoreCase('Hazardous Documentation')) {
                    IDDocument = createHazardousDocumentRecord(IDBooking, bookingName);
                } else {
                    IDDocument = CustomerCommunity_FileController.createCustomerDocumentRecord(IDBooking,documentType, bookingName);
                }
            }
            if(IDDocument != NULL)
                return String.valueOf(IDDocument); 
            else
                return 'FALSE';
        }
        catch(Exception ex){   
            ExceptionHandler.logApexCalloutError(ex);
            return 'FALSE';
        }    
    }
    @AuraEnabled
    public static List<CustomerCommunity_DocumentsWrapper> getAllBookings() {
        try {
            Id userId = UserInfo.getUserId();
            User userDetail = new User();
            Contact conDetail = new Contact();
            Set<ID> setlinkedIdentity = new Set<ID>();
            Set <ID> bookingSet = new Set<ID>();
            Set<ID> setHeader = new Set<ID>();
            Set<ID> setIMO = new Set<ID>();
            Set<ID> setCustomerDocument = new Set<ID>();
            List<ContentDocumentLink> listContentDocumentLink = new List<ContentDocumentLink>();
            List<Header__c> listHeaderFilter = new List<Header__c>();
            List<Header__c> listIMOFilter = new List<Header__c>();
            List<CustomerDocument__c> listCustomerDocumentFilter = new List<CustomerDocument__c>();
            
            userDetail = [SELECT id, contactId FROM User WHERE Id = : userId];
            if (userDetail.contactId != NULL) {
                conDetail = [SELECT Id, Name, AccountId FROM Contact WHERE Id = : userDetail.contactId];
                List <Booking__c> bookingList = [SELECT ID, Name FROM Booking__c WHERE Account__c = :conDetail.AccountId];
                for (Booking__c booking: bookingList) {
                    bookingSet.add(booking.ID);
                }
                System.debug(bookingSet);
                List <CustomerDocument__c> listCustomerDocument = [SELECT Booking__r.Name,Booking__r.Booking_Number__c, Name, Document_Type__c, Created_Name__c, CreatedById, CreatedDate from CustomerDocument__c WHERE Booking__c in : bookingSet AND Created_By_Portal_User__c = TRUE ORDER BY CreatedDate DESC];
                for(CustomerDocument__c objCustomerDocument:listCustomerDocument){
                    setCustomerDocument.add(objCustomerDocument.ID);
                }
                Id shippingInstRecordTypeId = getRecordTypeId('Header__c:Shipping_Instruction');
                List<Header__c> listHeader = [Select Booking__r.Name,Booking__r.Booking_Number__c,Name,CreatedName__c,CreatedById,CreatedDate,Status__c from Header__c WHERE Booking__c in : bookingSet AND Created_By_Portal_User__c = TRUE AND RecordTypeId = :shippingInstRecordTypeId ORDER BY CreatedDate DESC];   
                for(Header__c objHeader : listHeader){
                    setHeader.add(objHeader.ID);
                }
                Id hazardousDocRecordTypeId = getRecordTypeId('Header__c:Hazardous_Document');
                List<Header__c> listIMO = [Select Booking__r.Name,Booking__r.Booking_Number__c,Name,CreatedById,CreatedDate, CreatedName__c,Status__c from Header__c WHERE Booking__c in : bookingSet AND RecordTypeId = :hazardousDocRecordTypeId ORDER BY CreatedDate DESC];   
                for(Header__c objIMO : listIMO){
                    setIMO.add(objIMO.ID);
                }
                listContentDocumentLink = [SELECT ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId in :setHeader OR LinkedEntityId in :setCustomerDocument OR LinkedEntityId in :setIMO];
                for(ContentDocumentLink objContentDocumentLink : listContentDocumentLink){
                    setlinkedIdentity.add(objContentDocumentLink.LinkedEntityId);
                }   
                for(Header__c objHeader : listHeader){
                    if(setlinkedIdentity.contains(objHeader.Id)){
                        listHeaderFilter.add(objHeader); 
                    }
                }
                for(Header__c objIMO : listIMO){
                    if(setlinkedIdentity.contains(objIMO.Id)){
                        listIMOFilter.add(objIMO); 
                    }
                }
                for(CustomerDocument__c objCustomerDocument : listCustomerDocument){
                    if(setlinkedIdentity.contains(objCustomerDocument.Id)){
                        listCustomerDocumentFilter.add(objCustomerDocument);
                    }
                } 
                List<CustomerCommunity_DocumentsWrapper> documentsWrapper = new List<CustomerCommunity_DocumentsWrapper>();
                for(CustomerDocument__c customerDocumentObj : listCustomerDocumentFilter){
                    CustomerCommunity_DocumentsWrapper CustomerCommunity_DocumentsWrapperObj = new CustomerCommunity_DocumentsWrapper
                        (customerDocumentObj.Id,customerDocumentObj.Booking__r.Booking_Number__c,customerDocumentObj.Booking__r.Name,customerDocumentObj.Name,customerDocumentObj.Document_Type__c,customerDocumentObj.Created_Name__c,customerDocumentObj.CreatedById,customerDocumentObj.CreatedDate,' ');
                    documentsWrapper.add(CustomerCommunity_DocumentsWrapperObj);
                }  
                for(Header__c headerObj : listHeaderFilter){
                    CustomerCommunity_DocumentsWrapper CustomerCommunity_DocumentsWrapperObj = new CustomerCommunity_DocumentsWrapper
                        (headerObj.Id,headerObj.Booking__r.Booking_Number__c,headerObj.Booking__r.Name,headerObj.Name,'Shipping Instructions',headerObj.CreatedName__c,headerObj.CreatedById,headerObj.CreatedDate,headerObj.Status__c);
                    documentsWrapper.add(CustomerCommunity_DocumentsWrapperObj);
                }
                for(Header__c imoObj : listIMOFilter){
                    CustomerCommunity_DocumentsWrapper CustomerCommunity_DocumentsWrapperObj = new CustomerCommunity_DocumentsWrapper
                        (imoObj.Id,imoObj.Booking__r.Booking_Number__c,imoObj.Booking__r.Name,imoObj.Name,'Hazardous Documentation',imoObj.CreatedName__c,imoObj.CreatedById,imoObj.CreatedDate,imoObj.Status__c);
                    documentsWrapper.add(CustomerCommunity_DocumentsWrapperObj);
                }
                documentsWrapper.sort();
                System.debug('@@@@'+documentsWrapper);
                return documentsWrapper;
            } else {
                return NULL;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
    @AuraEnabled
    public static String readFileContent(Id recordId) {
        try {
            ContentDocumentLink CDList = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = : recordId LIMIT 1];
            if (CDList.ContentDocumentId != NULL) {
                ContentDocument entries = [SELECT Id FROM ContentDocument WHERE ID = : CDList.ContentDocumentId LIMIT 1];
                return String.valueOf(entries.Id);
            } 
            else{
                return null;
            }
        } catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return null;
        }
    }
    @AuraEnabled
    public static String deleteDocumentRecord(Id recordId,String documentType) {
        
        CustomerDocument__c customerDocument = new CustomerDocument__c();
        Header__c headerDocument = new Header__c(); 
        try {
            if(documentType.equalsIgnoreCase('Shipping Instructions')){
                headerDocument = [SELECT id, Name from Header__c WHERE Id = : recordId LIMIT 1];
                delete headerDocument;
                return CustomerCommunity_Constants.TRUE_MESSAGE;
            }
            else {
                customerDocument = [SELECT id, Name from CustomerDocument__c WHERE Id = : recordId LIMIT 1];
                delete customerDocument; 
                return CustomerCommunity_Constants.TRUE_MESSAGE;
            }       
        }
        catch (Exception ex) {
            ExceptionHandler.logApexCalloutError(ex);
            return CustomerCommunity_Constants.FALSE_MESSAGE;
        }
    }
    @future(callout=true)   
    public static void UploadFileInEsker(String fileVersionData, String fileName, Id IdHeader, String bookingName, Id recordTypeId){
        system.debug('fileVersionData---->>>'+fileVersionData);
        Id shippingInstRecordTypeId = getRecordTypeId('Header__c:Shipping_Instruction');
        Id hazardousDocRecordTypeId = getRecordTypeId('Header__c:Hazardous_Document');
        
        sessionservice2.SessionServiceSoap objSessionServiceSoap = new sessionservice2.SessionServiceSoap();
        sessionservice2.BindingResult objBindingResult = objSessionServiceSoap.GetBindings('crowleywebservicesuser.qa@crowley.com');
        system.debug('objBindingResult---->>>'+objBindingResult);
        objSessionServiceSoap.endpoint_x = objBindingResult.sessionServiceLocation;
        sessionservice2.LoginResult objLoginResult = objSessionServiceSoap.Login('crowleywebservicesuser.qa@crowley.com','guilt-nGHrJ');
        system.debug('objLoginResult---->>>'+objLoginResult);
        
        SubmissionService2.SubmissionServiceSoap objSubmissionService = new SubmissionService2.SubmissionServiceSoap();
        objSubmissionService.endpoint_x = objBindingResult.submissionServiceLocation; 
        objSubmissionService.SessionHeaderValue = new submissionservice2.SessionHeader();
        objSubmissionService.SessionHeaderValue.sessionID = objLoginResult.sessionID;
        
        submissionservice2.WSFile objFile = objSubmissionService.UploadFile(fileVersionData,fileName);
        system.debug('objFile---->>>'+objFile);
        
        submissionservice2.Transport objTransport = new submissionservice2.Transport();
        if(recordTypeId == hazardousDocRecordTypeId){
            System.debug('IMO');
            objTransport.recipientType = 'CD#HUMEDF5DC1';
        }else if(recordTypeId == shippingInstRecordTypeId){
            System.debug('Shipping Instruction');
            objTransport.recipientType = 'CD#VUBYB6C277';
        } 
        objTransport.transportName = 'CustomData';
        objTransport.attachments = new submissionservice2.attachments_element();
        objTransport.attachments.Attachment = new List<submissionservice2.Attachment>();
        submissionservice2.Attachment objAttachment1 = new submissionservice2.Attachment();
        objAttachment1.sourceAttachment = objFile;
        
        objAttachment1.vars = new submissionservice2.vars_element();
        objAttachment1.vars.Var = new List<submissionservice2.Var>();
        submissionservice2.Var AttachProcess = new submissionservice2.Var();
        AttachProcess.attribute = 'AttachToProcess';
        AttachProcess.simpleValue = '1';
        objAttachment1.vars.Var.add(AttachProcess);
        
        objTransport.attachments.Attachment.add(objAttachment1);
        objTransport.vars = new submissionservice2.vars_element();
        objTransport.vars.Var = new List<submissionservice2.Var>();
        submissionservice2.Var LadingSFID = new submissionservice2.Var();
        LadingSFID.attribute = 'FromName';
        LadingSFID.simpleValue = IdHeader;
        objTransport.vars.Var.add(LadingSFID); 
        submissionservice2.Var BookingRecName = new submissionservice2.Var();
        BookingRecName.attribute = 'FromCompany';
        BookingRecName.simpleValue = bookingName;
        objTransport.vars.Var.add(BookingRecName);
		system.debug('objTransportreqqq------->>>>>'+objTransport);
        
        submissionservice2.SubmissionResult objSubmissionResult = objSubmissionService.SubmitTransport(objTransport);
            system.debug('objSubmissionResult------->>>>>'+objSubmissionResult);
        
    }
    // Dummy function to hit the api
    @AuraEnabled 
    public static void softShipApi(){
        
        String strURL='https://stage.api.crowley.com/v1/voyages?polcode=USJAX&podcode=PRSJU&arrival=09-01-2019&departure=08-21-2019&days=10&count=10&onlyWebPorts=FALSE&dateFormat=MM-dd-yyyy&includeTerminals=TRUE&useETBandETD=TRUE&service=SAL&includeTransshipment=TRUE';
        HttpRequest request = new HttpRequest();
        request.setEndpoint(strURL);
        request.setMethod('GET');
        request.setHeader('client_id','2f065d7451c749bd8cb1ce08c45f721e');
        request.setHeader('Client_secret','d1BbC072682c4185956d4a461156870e');
                              HttpResponse ratesResponse = new HttpResponse();
                              Http http=new Http();
                              ratesResponse = http.send(request);
        
        //String response = CustomerCommunity_APICallouts.getAPIResponse(URL, requestbody, Null);
        System.debug('response-->'+ratesResponse.getBody());
    }
    
    @AuraEnabled
    public static Id createShippingInstructionRecord(Id bookingRecordId, String bookingName){
        List<Header__c> listHeaderRecords = new List<Header__c>();
        List<Booking__c> listBookingRecords = new List<Booking__c>();
        try{
            Id shippingInstRecordTypeId = getRecordTypeId('Header__c:Shipping_Instruction');
            listBookingRecords = [SELECT Id, Name FROM Booking__c WHERE Id = :bookingRecordId];
            listHeaderRecords = [SELECT Id, Booking__c FROM Header__c WHERE Booking__c = :bookingRecordId AND RecordTypeId = :shippingInstRecordTypeId];
            if(!listBookingRecords.isEmpty()){
                Header__c headerRecord = new Header__c();
                headerRecord.Name = bookingName + ' - Shipping Instruction v' + String.valueOf(listHeaderRecords.size()+1);
                headerRecord.Status__c = 'In Process';
                headerRecord.Booking__c = bookingRecordId;
                headerRecord.RecordTypeId = shippingInstRecordTypeId;
                insert headerRecord;
                listBookingRecords[0].Shorten_URL__c = '';
                listBookingRecords[0].shippingStatus__c = 'In Process';
                if(!listBookingRecords.isEmpty() && listBookingRecords!=NULL){
                    update listBookingRecords;
                }
                return headerRecord.Id;
            } 
            return Null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    @AuraEnabled
    public static Id createHazardousDocumentRecord(Id bookingRecordId, String bookingName){
        List<Header__c> listHazardousDocRecords = new List<Header__c>();
        List<Booking__c> listBookingRecords = new List<Booking__c>();
        try{
            Id hazardousDocRecordTypeId = getRecordTypeId('Header__c:Hazardous_Document');
            listBookingRecords = [SELECT Id, Name FROM Booking__c WHERE Id = :bookingRecordId];
            listHazardousDocRecords = [SELECT Id, Booking__c FROM Header__c WHERE Booking__c = :bookingRecordId AND RecordTypeId =:hazardousDocRecordTypeId];
            if(!listBookingRecords.isEmpty()){
                Header__c headerRecord = new Header__c();
                headerRecord.Name = bookingName + ' - Hazardous Document v' + String.valueOf(listHazardousDocRecords.size()+1);
                headerRecord.Status__c = 'In Process';
                headerRecord.Booking__c = bookingRecordId;
                headerRecord.RecordTypeId = hazardousDocRecordTypeId;
                insert headerRecord;
                return headerRecord.Id;
            } 
            return Null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    public static Id createCustomerDocumentRecord(Id bookingRecordId,String documentType, String bookingName){
        
        List<CustomerDocument__c> listCustomerDocument = new List<CustomerDocument__c>();
        List<Booking__c> listBookingRecords = new List<Booking__c>();
        try{
            listBookingRecords = [SELECT Id, Name FROM Booking__c WHERE Id = :bookingRecordId];
            listCustomerDocument = [SELECT Id, Booking__c FROM CustomerDocument__c WHERE Booking__c = :bookingRecordId];
            if(!listBookingRecords.isEmpty()){
                CustomerDocument__c objCustomerDocument = new CustomerDocument__c();
                objCustomerDocument.Name = bookingName + '- '+ documentType + ' v' + String.valueOf(listCustomerDocument.size()+1);
                objCustomerDocument.Booking__c = bookingRecordId;
                objCustomerDocument.Document_Type__c = documentType;
                insert objCustomerDocument;
                listBookingRecords[0].Shorten_URL__c = '';
                listBookingRecords[0].shippingStatus__c = 'In Process';
                if(!listBookingRecords.isEmpty() && listBookingRecords!=NULL){
                    update listBookingRecords;
                }  
                return objCustomerDocument.Id;
            }  
            return Null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        } 
    }
    
    @AuraEnabled
    public static ContentVersion getFileType(Id contentDocumentId){
        List<ContentVersion> listContentVersionRecords = new List<ContentVersion>();
        try{
            listContentVersionRecords = [SELECT Id, ContentDocumentId, Title, FileType, VersionData FROM ContentVersion WHERE ContentDocumentId = :contentDocumentId];
            if(!listContentVersionRecords.isEmpty())
                return listContentVersionRecords[0];
            return Null;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return Null;
        }
    }
    
    @AuraEnabled
    public static List<BoL_Excel_Parser__mdt> getExcelFieldLabels(){
        List<BoL_Excel_Parser__mdt> listExcelParser = [SELECT MasterLabel, Ending_Label__c, Traverse_Path__c, Active__c, Object_API_Name__c, Parent_Object_API_Name__c, Sorting_Sequence__c,
                                                       Field_Data__c, Sequence__c, Field_API_Name__c, Field_Data_Type__c, Type__c, Multiple_Records__c, Matching_Labels__c, Check_Field_Range__c, 
                                                       Field_Validations__c, Dependent_Field_API_Name__c, Dependent_Field_DataType__c, Dependent_Field_Value__c
                                                       FROM BoL_Excel_Parser__mdt WHERE Active__c = True Order By Sorting_Sequence__c ASC];
        if(listExcelParser != Null && !listExcelParser.isEmpty()){
            return listExcelParser;
        }
        else
            return Null;
    }
    
    @AuraEnabled
    public static Map<String, List<BoL_Excel_Parser_Validations__mdt>> getExcelFieldValidations(){
        Map<String, List<BoL_Excel_Parser_Validations__mdt>> mapFieldValidations = new Map<String, List<BoL_Excel_Parser_Validations__mdt>>();
        List<BoL_Excel_Parser_Validations__mdt> listExcelFieldValidation = [SELECT MasterLabel, Blank_Check__c, Includes_Substring__c, Matches_Substring__c, Null_Check__c, Alpha_Numeric_Value__c,
                                                                           Numeric_Value__c, Substring__c, Substring_Error_Message__c FROM BoL_Excel_Parser_Validations__mdt 
                                                                           WHERE Active__c = True];
        if(listExcelFieldValidation != Null && !listExcelFieldValidation.isEmpty()){
            for(BoL_Excel_Parser_Validations__mdt fieldValidation : listExcelFieldValidation){
                List<BoL_Excel_Parser_Validations__mdt> listTempValidations = new List<BoL_Excel_Parser_Validations__mdt>();
                if(!mapFieldValidations.isEmpty() && mapFieldValidations.containsKey(fieldValidation.MasterLabel))
                    listTempValidations = mapFieldValidations.get(fieldValidation.MasterLabel);
                listTempValidations.add(fieldValidation);
                if(!listTempValidations.isEmpty())
                    mapFieldValidations.put(fieldValidation.MasterLabel, listTempValidations);
            }
            return mapFieldValidations;
        }
        else
            return Null;
    }
    
    @AuraEnabled
    public static void createBLRecords(List<BoL_Excel_Parser__mdt> listFormData, Id headerRecordId){
        Map<String, List<BoL_Excel_Parser__mdt>> mapTypeFieldDetails = new Map<String, List<BoL_Excel_Parser__mdt>>();
        Map<String, List<BoL_Excel_Parser__mdt>> mapSequenceFieldDetails = new Map<String, List<BoL_Excel_Parser__mdt>>();
        Map<String, Id> mapParentRecordIds = new Map<String, Id>();
        List<sObject> listObject = new List<sObject>();
        List<sObject> listObjectUpdate = new List<sObject>();
        Boolean updateRecord = False;
        if(headerRecordId != Null)
            mapParentRecordIds.put('Header__c',headerRecordId);
        for(BoL_Excel_Parser__mdt fieldData : listFormData){
            List<BoL_Excel_Parser__mdt> listTypeFieldDetails = new List<BoL_Excel_Parser__mdt>();
            List<BoL_Excel_Parser__mdt> listSequenceFieldDetails = new List<BoL_Excel_Parser__mdt>();
            if(fieldData.Type__c != Null && fieldData.Type__c != '' && !mapTypeFieldDetails.isEmpty() && mapTypeFieldDetails.containsKey(fieldData.Type__c)){
                listTypeFieldDetails = mapTypeFieldDetails.get(fieldData.Type__c);
                listTypeFieldDetails.add(fieldData);
            }
            else if(fieldData.Type__c != Null && fieldData.Type__c != '')
                listTypeFieldDetails.add(fieldData);
            if(!listTypeFieldDetails.isEmpty())
                mapTypeFieldDetails.put(fieldData.Type__c, listTypeFieldDetails);
            if(fieldData.Sequence__c != Null && fieldData.Object_API_Name__c != 'Addresses__c' && !mapSequenceFieldDetails.isEmpty() && mapSequenceFieldDetails.containsKey(String.valueOf(fieldData.Sequence__c))){
                listSequenceFieldDetails = mapSequenceFieldDetails.get(String.valueOf(fieldData.Sequence__c));
                listSequenceFieldDetails.add(fieldData);
            }
            else if(fieldData.Sequence__c != Null && fieldData.Object_API_Name__c != 'Addresses__c')
                listSequenceFieldDetails.add(fieldData);
            if(!listSequenceFieldDetails.isEmpty())
                mapSequenceFieldDetails.put(String.valueOf(fieldData.Sequence__c), listSequenceFieldDetails);
        }
        
        if(!mapSequenceFieldDetails.isEmpty()){
            listObject = new List<sObject>();
            for(String sequenceNumber : mapSequenceFieldDetails.keySet()){
                sObject objectRecord = Schema.getGlobalDescribe().get(mapSequenceFieldDetails.get(sequenceNumber)[0].Object_API_Name__c).newSObject();
                if(mapSequenceFieldDetails.get(sequenceNumber)[0].Object_API_Name__c == 'Header__c'){
                    objectRecord.put('Id', headerRecordId);
                    objectRecord.put('Status__c', 'Available for Review');
                    updateRecord = True;
                }
                if(mapSequenceFieldDetails.get(sequenceNumber)[0].Parent_Object_API_Name__c == 'TLIEquipType__c' && !mapParentRecordIds.containsKey('TLIEquipType__c')){
                    TLIEquipType__c TLIRec = new TLIEquipType__c();
                    TLIRec.Name = 'TLI Equipment';
                    TLIRec.Header__c = headerRecordId;
                    insert TLIRec;
                    mapParentRecordIds.put('TLIEquipType__c', TLIRec.Id);
                }
                for(BoL_Excel_Parser__mdt fieldDetail : mapSequenceFieldDetails.get(sequenceNumber)){
                    if(fieldDetail != Null && fieldDetail.Field_Data__c != Null && fieldDetail.Field_Data__c != '0' && fieldDetail.Field_Data__c != ''){
                        System.debug('@@@ fieldDetail - ' + fieldDetail.MasterLabel + ' - ' + fieldDetail.Field_Data__c + ' - ' + fieldDetail);
                        if(fieldDetail.Description__c != Null && fieldDetail.Description__c != '' && objectRecord.getSobjectType().getDescribe().fields.getMap().containsKey('Cargo_Description__c'))
                            objectRecord.put('Cargo_Description__c', fieldDetail.Description__c);
                        if(fieldDetail.Field_Data__c.contains('\n'))
                            fieldDetail.Field_Data__c = fieldDetail.Field_Data__c.replaceAll('\n',' ');
                        if(fieldDetail.Field_Data_Type__c == 'Text')
                            objectRecord.put(fieldDetail.Field_API_Name__c, String.valueOf(fieldDetail.Field_Data__c));
                        if(fieldDetail.Field_Data_Type__c == 'Number')
                            objectRecord.put(fieldDetail.Field_API_Name__c, Integer.valueOf(fieldDetail.Field_Data__c));
                        if(fieldDetail.Dependent_Field_API_Name__c != Null && fieldDetail.Dependent_Field_API_Name__c != '' && fieldDetail.Dependent_Field_DataType__c == 'Text' && fieldDetail.Dependent_Field_Value__c != Null && fieldDetail.Dependent_Field_Value__c != '')
                            objectRecord.put(fieldDetail.Dependent_Field_API_Name__c, String.valueOf(fieldDetail.Dependent_Field_Value__c));
                        if(fieldDetail.Dependent_Field_API_Name__c != Null && fieldDetail.Dependent_Field_API_Name__c != '' && fieldDetail.Dependent_Field_DataType__c == 'Number' && fieldDetail.Dependent_Field_Value__c != Null && fieldDetail.Dependent_Field_Value__c != '')
                            objectRecord.put(fieldDetail.Dependent_Field_API_Name__c, Integer.valueOf(fieldDetail.Dependent_Field_Value__c));
                        if(fieldDetail.Parent_Object_API_Name__c != Null && fieldDetail.Parent_Object_API_Name__c != '' && !mapParentRecordIds.isEmpty() && mapParentRecordIds.containsKey(fieldDetail.Parent_Object_API_Name__c))
                            objectRecord.put(fieldDetail.Parent_Object_API_Name__c, mapParentRecordIds.get(fieldDetail.Parent_Object_API_Name__c));
                    }
                }
                if(updateRecord){
                    listObjectUpdate.add(objectRecord);
                    updateRecord = False;
                }
                else
                    listObject.add(objectRecord);
            }
            if(!listObjectUpdate.isEmpty())
                update listObjectUpdate;
            if(!listObject.isEmpty())
                insert listObject;
            mapParentRecordIds = addParentIdInMap(listObject, mapParentRecordIds);
        }
        
        //insert address records
        if(!mapTypeFieldDetails.isEmpty()){
            listObject = new List<sObject>();
            for(String addressType : mapTypeFieldDetails.keyset()){
                sObject objectRecord = Schema.getGlobalDescribe().get(mapTypeFieldDetails.get(addressType)[0].Object_API_Name__c).newSObject();
                for(BoL_Excel_Parser__mdt fieldDetail : mapTypeFieldDetails.get(addressType)){
                    objectRecord.put('Type__c', String.valueOf(fieldDetail.Type__c));
                    if(fieldDetail.Field_Data__c != Null){
                        String fieldValue = fieldDetail.Field_Data__c;
                        if(fieldValue.contains('\n') && fieldValue.indexOf('\n',0) == 0)
                            fieldValue = fieldValue.substring(1, fieldValue.length());
                        if(fieldDetail.Field_API_Name__c == 'Address__c'){
                            if(fieldValue.contains('\n')){
                                objectRecord.put('Name', fieldValue.substring(0,fieldValue.indexOf('\n', 0)));   
                                objectRecord.put(fieldDetail.Field_API_Name__c, fieldValue.substring(fieldValue.indexOf('\n', 0)));
                            }
                            else
                                objectRecord.put(fieldDetail.Field_API_Name__c, fieldValue);
                        }
                        else if(fieldDetail.Field_Data_Type__c == 'Text')
                            objectRecord.put(fieldDetail.Field_API_Name__c, String.valueOf(fieldValue));
                        else if(fieldDetail.Field_Data_Type__c == 'Number')
                            objectRecord.put(fieldDetail.Field_API_Name__c, Integer.valueOf(fieldValue));
                    
                    if(fieldDetail.Parent_Object_API_Name__c != Null && fieldDetail.Parent_Object_API_Name__c != '' && !mapParentRecordIds.isEmpty() && mapParentRecordIds.containsKey(fieldDetail.Parent_Object_API_Name__c))
                        objectRecord.put(fieldDetail.Parent_Object_API_Name__c, mapParentRecordIds.get(fieldDetail.Parent_Object_API_Name__c));
                    }
                    }
                listObject.add(objectRecord);
            }
            if(!listObject.isEmpty())
                insert listObject;
            mapParentRecordIds = addParentIdInMap(listObject, mapParentRecordIds);
        }
    }
    public static Map<String, Id> addParentIdInMap(List<sObject> listsObjects, Map<String, Id> mapParentRecordIds){
        if(!listsObjects.isEmpty()){
            for(sObject objRecord : listsObjects){
                String sObjName = objRecord.Id.getSObjectType().getDescribe().getName();
                if(mapParentRecordIds != Null && !mapParentRecordIds.containsKey(sObjName))
                    mapParentRecordIds.put(sObjName, objRecord.Id);
            }
        }
        return mapParentRecordIds;
    }
    @AuraEnabled
    public static void createFileRecord(String fileName, String base64Data, String contentType, Id headerRecordId){
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        ContentVersion cv = new ContentVersion();
        cv.Title = fileName;
        cv.VersionData = EncodingUtil.base64Decode(base64Data); 
        cv.PathOnClient = fileName;
        insert cv;
        
        List<ContentVersion> listcv = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ID = :cv.Id];
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = listcv[0].ContentDocumentId;
        cdl.LinkedEntityId = headerRecordId;
        cdl.ShareType = 'V';
        insert cdl;
    }
    
    public static Id getRecordTypeId(String recTypeName){
        if (recordTypesMap!= null && recordTypesMap.containsKey(recTypeName)){
            return recordTypesMap.get(recTypeName);
        }
        return null;
    }
    
    private static Map<String, ID> recordTypesMap{
        get{
            if (recordTypesMap == null ){
                recordTypesMap = new Map<String, Id>();
                for (RecordType aRecordType : [SELECT SobjectType, DeveloperName FROM RecordType WHERE isActive = true]){
                    recordTypesMap.put(aRecordType.SobjectType + ':' + aRecordType.DeveloperName, aRecordType.Id);
                }
            }
            return recordTypesMap;
        }
        set;
    }
    
}