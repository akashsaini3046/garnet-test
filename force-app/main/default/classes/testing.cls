public class testing {
    public static void convert(){
        StaticResource sr = [select Body from StaticResource where Name = 'temp' ];

        String jsonStr = sr.Body.toString();
        Attachment attach = new Attachment();
        attach.contentType = 'application/pdf';
        attach.name = 'ShippingInstruction5.pdf';
        attach.parentId = 'a1n0t000000fRKC';
        attach.body = EncodingUtil.base64Decode(jsonStr);
        insert attach;
    }
}