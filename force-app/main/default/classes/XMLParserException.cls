/*
* Name: XMLParserException
* Purpose: This is custom exception class.
* Author: Nagarro
* Created Date: 04-Jult-2019
* Modification History
*  Modification #   Story/Defect#      Modified By     Date   Description
*/
public class XMLParserException extends Exception {}