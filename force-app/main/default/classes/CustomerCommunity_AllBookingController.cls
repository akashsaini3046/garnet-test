public class CustomerCommunity_AllBookingController {
    
    @AuraEnabled
    public static List<Booking__c> fetchBookingData(Integer recordLimit, Integer recordOffset, String fieldName, String order){
        Integer intLimit = Integer.valueof(recordLimit);
        Integer intOffset = Integer.valueof(recordOffset);
        Id bookingRecordTypeId = getRecordTypeId('Booking__c:Booking_Record');
        String query = 'SELECT Id, Name, Account__c, Account__r.Name, Booking_Number__c, Origin_Type__c, Destination_Type__c, Status__c, CreatedDate FROM Booking__c WHERE RecordTypeId = :bookingRecordTypeId ORDER BY '+fieldName+' '+ order + ' LIMIT ' +intLimit + ' OFFSET '+ intOffset ;
        List<Booking__c> bookings = Database.query(query);
        return bookings;
    }
    
    @AuraEnabled
    public static Integer getTotalBookings(){
        AggregateResult results = [SELECT Count(Id) TotalBookings  From Booking__c];
        Integer totalBookings = (Integer)results.get('TotalBookings') ; 
        return totalBookings;
    }
    
    @AuraEnabled
    public static String fetchIframeUrl(String bookingId){
        String bookingUrl;
        Booking__c booking = [SELECT Id, Name FROM Booking__c WHERE Id = :bookingId LIMIT 1];
        String communityUrlPathPrefix = getCommunityUrlPathPrefix();
        if(Network.getNetworkId() != null){
           bookingUrl = '/'+communityUrlPathPrefix + '/apex/BookingPDFMain?Id='+booking.Id+'&bookingName='+String.valueOf(booking.Name);
        }
        else{
            bookingURL = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/BookingPDFMain?Id='+booking.Id+'&bookingName='+String.valueOf(booking.Name);
        }
        return bookingUrl;
    }
    		 
    private static String getCommunityUrlPathPrefix(){
        String nwid = Network.getNetworkId();
        if (!String.isBlank(nwid)) {
            String communityUrlPathPrefix  = [SELECT urlPathPrefix from Network where id =: nwid][0].urlPathPrefix ;
            return communityUrlPathPrefix;
        }
        else{
            return null;
        }
    }
    
    @AuraEnabled
    public static void sendEmailPDF(Id bookingId, String emailAddress){
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
		Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
		PageReference pref = page.BookingRecordPDF;
        pref.getParameters().put('Id',bookingId);
		Blob b = pref.getContent();
		attach.setFileName('BookDetails.pdf');
		attach.setBody(b);
		semail.setSubject('Booking Details');
        List<String> emailsAddrs = emailAddress.split(';');
        String[] emailAddressArray = new String[emailsAddrs.size()];
        for(Integer i = 0 ; i < emailsAddrs.size(); i++){
            emailAddressArray[i] = emailsAddrs.get(i);
            System.debug('EMAIL ADDRESS : '+emailsAddrs.get(i));
        }
        String[] sendTo = emailAddressArray;
        semail.setToAddresses(sendTo);
        OrgWideEmailAddress owea = new OrgWideEmailAddress();
        owea = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress WHERE DisplayName='NagarroCrowley'];
        if ( owea != null) {
            semail.setOrgWideEmailAddressId(owea.Id);
        }
        semail.setHtmlBody('Hi,<br><br>Please find the attached booking confirmation document. <br><br>For more booking details <a href="https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/booking/Booking__c">Click Here.</a><br><br>Regards<br>Crowley');
		semail.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
		Messaging.sendEmail(new Messaging.SingleEmailMessage[]{semail});
    }
    
    public static Map<String,String> fetchReceiptDeliveryTermsPickListValues(String fieldName){
        Map<String,String> pickLabelValueMap = new Map<String,String>();
        
        Map<String,List<Schema.PicklistEntry>> picklistSchema= new Map<String,List<Schema.PicklistEntry>>();
        Schema.sObjectType objType = New_Booking_Fields__mdt.getSObjectType();
        //Schema.sObjectType objType = objectName.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        //List<String> fieldsToGetValues=new List<String>();
        //String fieldName ='Receipt_Delivery_Terms__c' ;
        
        
        //fieldsToGetValues.add('Receipt_Delivery_Terms__c');
        
        
        
        if(fieldMap.containsKey(fieldName)){
            list<Schema.PicklistEntry> values =
                fieldMap.get(fieldName).getDescribe().getPickListValues();
            for(Schema.PicklistEntry p : values){
                
                pickLabelValueMap.put(p.getLabel(),p.getValue());
            }
            
        }
        System.debug(fieldName+' values :');
        System.debug(pickLabelValueMap);
        return pickLabelValueMap; 
    }
    
    @AuraEnabled
    @future(callout=True)
    public static void getRates(Id IdBooking){
        
        System.debug('Entered inside GetRates method');
        System.debug('Booking id is : '+IdBooking);
        Booking__c bookingRec = new Booking__c();
        Shipment__c shipmentRec = new Shipment__c();
        Commodity__c commodityRec = new Commodity__c();
        
        FreightDetail__c freightDetailRec = new FreightDetail__c();
        Requirement__c requirementRec = new Requirement__c();
        Party__c partyRec = new Party__c();
        Voyage__c voyageRec = new Voyage__c();
        List<Voyage__c> voyageRecList = new List<Voyage__c>();
        Map<String, String> mapCommodityNameCategory = new Map<String, String>();
        // List<New_Booking_Commodities__mdt> listCommoditiesRecords = new List<New_Booking_Commodities__mdt>();
        
        
        
        bookingRec = [Select Name,Destination_Type__c,Origin_Type__c,Customer_Destination_Code__c,Date_of_Discharge__c,Total_Weight__c,	Total_Volume__c,
                      Total_Volume_Unit__c,Total_Weight_Unit__c,Contact__c,Customer_Origin_Code__c,Container_Mode__c from Booking__c 
                      where id = :IdBooking];
        
        shipmentRec = [Select Origin_Code__c,Destination_Code__c from Shipment__c where Booking__c = :bookingRec.Id];
        
        voyageRec = [Select Estimate_Sail_Date__c from Voyage__c where Shipment__c = :shipmentRec.Id Limit 1];
        voyageRecList=[Select Estimate_Sail_Date__c,Loading_Sequence__c from Voyage__c where Shipment__c = :shipmentRec.Id order By Loading_Sequence__c];
        
        freightDetailRec = [Select id from FreightDetail__c where Shipment__c  = :shipmentRec.Id];
        
        System.debug('Before commodity');
        commodityRec = [Select Name from Commodity__c where Freight__c = :freightDetailRec.Id Limit 1];
        System.debug('After commodity');
        
        requirementRec = [Select Container_Type__c,Category__c from Requirement__c where Freight__c = :freightDetailRec.Id];
        
        partyRec = [Select CVIF__c,Party_Id__c,Type__c from Party__c where Booking__c = :bookingRec.Id and Type__c='CUST'];
        
        System.debug('Party Data :');
        System.debug(partyRec);
        
        
        
        
        //requirementRec.Category__c = 'Cargo, NOS';
        /* listCommoditiesRecords = [SELECT MasterLabel, Commodity_Name__c, Category__c FROM New_Booking_Commodities__mdt WHERE Category__c != Null AND Category__c != ''];
if(!listCommoditiesRecords.isEmpty()){
for(New_Booking_Commodities__mdt currentCommodity : listCommoditiesRecords){
if(currentCommodity.Commodity_Name__c != Null && currentCommodity.Commodity_Name__c != '' && currentCommodity.Category__c != NULL && currentCommodity.Category__c != '')
mapCommodityNameCategory.put(currentCommodity.Category__c, currentCommodity.Commodity_Name__c); 
}
}    */
        
        System.debug('@@@');
        
        List<Commodity__c> listCommodity = new List<Commodity__c>();
        List<CustomerCommunity_BookingRecord.ContainerCargoDetail> listCargoDetail = new List<CustomerCommunity_BookingRecord.ContainerCargoDetail>();
        CustomerCommunity_BookingRecord bookingRecordValues = new CustomerCommunity_BookingRecord();
        
        bookingRecordValues.customerId = partyRec.CVIF__c;
        bookingRecordValues.bookingRequestNumber = bookingRec.Name;
        
        //bookingRecordValues.bookingType = 'FCL';
        bookingRecordValues.bookingType = bookingRec.Container_Mode__c;
        
        Map<String,String> mapValueNameReceiptDeliveryTerms= fetchReceiptDeliveryTermsPickListValues('Receipt_Delivery_Terms__c');
        //List<New_Booking_Fields__mdt> listReceiptDeliveryTerms = new List<New_Booking_Fields__mdt>();
        
        // bookingRecordValues.deliveryTermVal = 'P';
        //bookingRecordValues.receiptTermVal = 'D';
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Origin_Type__c))
            bookingRecordValues.receiptTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Origin_Type__c);
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Destination_Type__c))
            bookingRecordValues.deliveryTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Destination_Type__c); 
        
        System.debug('receiptTermVal  : '+bookingRecordValues.receiptTermVal+' deliveryTermVal : '+bookingRecordValues.deliveryTermVal);
        bookingRecordValues.estSailingDate = voyageRec.Estimate_Sail_Date__c;
        bookingRecordValues.originCode = shipmentRec.Origin_Code__c;
        bookingRecordValues.destinationCode = shipmentRec.Destination_Code__c;
        bookingRecordValues.totalVolume = bookingRec.Total_Volume__c;
        bookingRecordValues.totalWeight = bookingRec.Total_Weight__c;
        bookingRecordValues.totalVolumeUnit = bookingRec.Total_Volume_Unit__c;
        bookingRecordValues.totalWeightUnit = bookingRec.Total_Weight_Unit__c;
        CustomerCommunity_BookingRecord.ContainerCargoDetail containerCargoRecordValues = new CustomerCommunity_BookingRecord.ContainerCargoDetail();
        containerCargoRecordValues.sequenceId = 1;
        containerCargoRecordValues.UniqueId = 'commodity1';
        containerCargoRecordValues.isHazardous = false;
        containerCargoRecordValues.freightRecord = freightDetailRec;
        containerCargoRecordValues.requirementRecord = requirementRec;
        listCommodity.add(commodityRec);
        containerCargoRecordValues.listCommodityRecords = listCommodity; 
        listCargoDetail.add(containerCargoRecordValues);             
        
        bookingRecordValues.listCargoDetails = listCargoDetail;
        String bookingRecordString = JSON.serialize(bookingRecordValues);
        System.debug('bookingRecordString --->'+ bookingRecordString);
        String jsonResponseData = CustomerCommunity_NewBookingsController.getSoftshipRates(bookingRecordString,'ChargeLineItem1');
        System.debug('Response value is-----> '+jsonResponseData);
        
        
    } 
    
    @AuraEnabled
    public static String validateIMDG(Id IdBooking){
        System.debug('Entered inside validateIMDG()');
        System.debug('Booking Id is : '+IdBooking);
        CustomerCommunity_BookingRecord bookingRecordValues=new CustomerCommunity_BookingRecord();
        Booking__c bookingRec = new Booking__c();
        Shipment__c shipmentRec = new Shipment__c();
        FreightDetail__c freightDetailRec = new FreightDetail__c();
        Requirement__c requirementRec = new Requirement__c();
        Voyage__c voyageRec = new Voyage__c();
        List<Party__c> partyList = new List<Party__c>();
        List<Commodity__c> commodityList = new List<Commodity__c>();
        List<Contact> contactList = new List<Contact>();
        Set<String> partyCodes = new Set<String>();
        Set<Id> setAddressId = new Set<Id>();
        Map<Id, Address__c> mapAddressRecords;
        Map<String,Id> mapCVIFAddressId = new Map<String,Id>();
        Map<String,Contact> mapCVIFContactRecords = new Map<String,Contact>();
        
        bookingRec = [Select Name,Destination_Type__c,Status__c, Origin_Type__c,Customer_Destination_Code__c,Date_of_Discharge__c,Total_Weight__c,	Total_Volume__c,
                      Total_Volume_Unit__c,Total_Weight_Unit__c,Contact__c,Customer_Origin_Code__c,Container_Mode__c from Booking__c 
                      where id = :IdBooking];
        partyList = [Select id ,CVIF__c,Party_Id__c,Type__c from Party__c where Booking__c = :bookingRec.Id];
        shipmentRec = [Select Origin_Code__c,Destination_Code__c from Shipment__c where Booking__c = :bookingRec.Id];
        freightDetailRec = [Select id from FreightDetail__c where Shipment__c  = :shipmentRec.Id];
        requirementRec = [Select Container_Type__c,Category__c from Requirement__c where Freight__c = :freightDetailRec.Id];
        commodityList = new List<Commodity__c>([Select Name,Emergency_Contact_Name__c,Emergency_Contact_Number__c,Number__c,Package_Group__c,Technical_Name__c,Suffix__c,Primary_Class__c,IMO_Class__c,Phone_Number__c,Name__c from Commodity__c where Is_Hazardous__c=true AND Freight__c = :freightDetailRec.Id]);
        voyageRec = [Select Estimate_Sail_Date__c from Voyage__c where Shipment__c = :shipmentRec.Id Limit 1];
        
        
        for(Party__c partyRecs : partyList)
            partyCodes.add(partyRecs.CVIF__c);
        
        contactList = [SELECT Id, Name, Phone, MobilePhone, Fax, Address__c, Account.CVIF__c FROM Contact WHERE Account.CVIF__c IN :partyCodes];
        for(Contact con: contactList){
            mapCVIFContactRecords.put(con.Account.CVIF__c,con);
            mapCVIFAddressId.put(con.Account.CVIF__c,con.Address__c);
            setAddressId.add(con.Address__c);
        }
        mapAddressRecords= new Map<Id, Address__c>([SELECT Id, Name, Address_Line_2__c, Address_Line_3__c, City__c, State_Picklist__c, Postal_Code__c, Country__c, 
                                                    CVIF_Location_Id__c FROM Address__c WHERE Id IN :setAddressId]);
        
        for(Party__c partyRecord : partyList){
            if(partyRecord.Type__c=='CUST'){
                bookingRecordValues.customerContactId=partyRecord.CVIF__c;
                bookingRecordValues.customerAddressRecord= mapAddressRecords.get(mapCVIFAddressId.get(partyRecord.CVIF__c));
                bookingRecordValues.customerContactRecord= mapCVIFContactRecords.get(partyRecord.CVIF__c);
                bookingRecordValues.customerId= partyRecord.CVIF__c;
            }
            else if(partyRecord.Type__c=='SHP'){
                bookingRecordValues.shipperContactId=partyRecord.CVIF__c;
                bookingRecordValues.shipperAddressRecord=mapAddressRecords.get(mapCVIFAddressId.get(partyRecord.CVIF__c));
                bookingRecordValues.shipperContactRecord= mapCVIFContactRecords.get(partyRecord.CVIF__c);
                bookingRecordValues.shipperId= partyRecord.CVIF__c;
            }
            else if(partyRecord.Type__c=='CON'){
                bookingRecordValues.consigneeContactId=partyRecord.CVIF__c;
                bookingRecordValues.shipperAddressRecord=mapAddressRecords.get(mapCVIFAddressId.get(partyRecord.CVIF__c));
                bookingRecordValues.consigneeContactRecord= mapCVIFContactRecords.get(partyRecord.CVIF__c);
                bookingRecordValues.consigneeId= partyRecord.CVIF__c;
            }
        }
        
        
        Map<String,String> mapValueNameReceiptDeliveryTerms= fetchReceiptDeliveryTermsPickListValues('Receipt_Delivery_Terms__c');
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Origin_Type__c))
            bookingRecordValues.receiptTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Origin_Type__c);
        if(mapValueNameReceiptDeliveryTerms.containsKey(bookingRec.Destination_Type__c))
            bookingRecordValues.deliveryTermVal = mapValueNameReceiptDeliveryTerms.get(bookingRec.Destination_Type__c);
        
        bookingRecordValues.estSailingDate = voyageRec.Estimate_Sail_Date__c;
        bookingRecordValues.originCode = shipmentRec.Origin_Code__c;
        bookingRecordValues.destinationCode = shipmentRec.Destination_Code__c;
        bookingRecordValues.totalVolume = bookingRec.Total_Volume__c;
        bookingRecordValues.totalWeight = bookingRec.Total_Weight__c;
        bookingRecordValues.totalVolumeUnit = bookingRec.Total_Volume_Unit__c;
        bookingRecordValues.bookingRequestNumber = bookingRec.Name;
        bookingRecordValues.bookingType = bookingRec.Container_Mode__c;
        
        
        //List<Commodity__c> listCommodity = new List<Commodity__c>();
        //Either comment the below line or listCommodity.add(commodityRec);
        // listCommodity=[Select Name from Commodity__c where Freight__c = :freightDetailRec.Id];
        List<CustomerCommunity_BookingRecord.ContainerCargoDetail> listCargoDetail = new List<CustomerCommunity_BookingRecord.ContainerCargoDetail>();
        
        CustomerCommunity_BookingRecord.ContainerCargoDetail containerCargoRecordValues = new CustomerCommunity_BookingRecord.ContainerCargoDetail();
        
        containerCargoRecordValues.sequenceId = 1;
        containerCargoRecordValues.UniqueId = 'commodity1';
        containerCargoRecordValues.isHazardous = true;
        containerCargoRecordValues.freightRecord = freightDetailRec;
        containerCargoRecordValues.requirementRecord = requirementRec;
        //listCommodity.add(commodityRec);
        containerCargoRecordValues.listCommodityRecords = commodityList; 
        
        listCargoDetail.add(containerCargoRecordValues); 
        
        bookingRecordValues.listCargoDetails = listCargoDetail;
        
        String bookingRecordString = JSON.serialize(bookingRecordValues);
        System.debug('bookingRecordString --->'+ bookingRecordString);
        String jsonResponseData = CustomerCommunity_NewBookingsController.validateHazardousBooking(IdBooking,bookingRecordString);
        System.debug('Response value is-----> '+jsonResponseData);
        return jsonResponseData;
    }
    
    public static Id getRecordTypeId(String recTypeName){
        if (recordTypesMap!= null && recordTypesMap.containsKey(recTypeName)){
            return recordTypesMap.get(recTypeName);
        }
        return null;
    }
    
    private static Map<String, ID> recordTypesMap{
        get{
            if (recordTypesMap == null ){
                recordTypesMap = new Map<String, Id>();
                for (RecordType aRecordType : [SELECT SobjectType, DeveloperName FROM RecordType WHERE isActive = true]){
                    recordTypesMap.put(aRecordType.SobjectType + ':' + aRecordType.DeveloperName, aRecordType.Id);
                }
            }
            return recordTypesMap;
        }
        set;
    }
}