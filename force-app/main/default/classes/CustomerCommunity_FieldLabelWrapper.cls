public class CustomerCommunity_FieldLabelWrapper {
    @AuraEnabled
    public String fieldLabel {get;set;}
    
    @AuraEnabled
    public String fieldType {get;set;}
    
    @AuraEnabled
    public Boolean fieldVisibility {get;set;}
    
    @AuraEnabled
    public String fieldApiName {get;set;}
     
    @AuraEnabled
    public String record {get;set;}
    
    @AuraEnabled
    public String recordId {get;set;}
}