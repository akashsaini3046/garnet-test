public class AttachmentDAOCls {
    
    @AuraEnabled
    public static List<Attachment> getAllAttachments(String parentId){
        return [SELECT Id,Name, ParentId, ContentType, Body, Description FROM Attachment WHERE ParentId=:parentId  ORDER BY CreatedDate ASC];
    }
}