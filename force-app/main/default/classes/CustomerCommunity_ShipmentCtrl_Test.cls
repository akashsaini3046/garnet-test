@isTest
public class CustomerCommunity_ShipmentCtrl_Test {
    
    @testSetup static void setup() {
		Booking__c b=new Booking__c(Shorten_URL__c='https://sforce.co/2oNgW2Z',shippingStatus__c='In Process');
        insert b;
        Header__c h=new Header__c(Shorten_URL__c='https://sforce.co/2MdWlg4');
        insert h;       
    }
    static testMethod void getShipmentInfoTestMethod(){
        CustomerCommunity_MockResponses.ShipmentControllergetShipmentInfoMockResponse mock=new CustomerCommunity_MockResponses.ShipmentControllergetShipmentInfoMockResponse();
        Test.setMock(HttpCalloutMock.class, mock);
        List<String> listTrackingIDs = new List<String>();
        listTrackingIDs.add('CMCU4554958');
        listTrackingIDs.add('CMCU4554959');
        listTrackingIDs.add('CMCU4554951');
        
        Test.startTest();
        List<CustomerCommunity_ShipmentInformation> si=new List<CustomerCommunity_ShipmentInformation>();
        si=CustomerCommunity_ShipmentController.getShipmentInfo(listTrackingIDs);
        Test.stopTest();
        
        system.assertEquals('Not booked', si[0].currentStatus);
    }
    
    static testMethod void getCWShipmentInfoTestMethod(){
        CustomerCommunity_MockResponses.ShipmentControllergetCWShipmentInfoMockResponse mock=new CustomerCommunity_MockResponses.ShipmentControllergetCWShipmentInfoMockResponse();
        Test.setMock(HttpCalloutMock.class, mock);
        String trackId='SUCA278342';
        String shipment='ForwardingShipment';
        
        Test.startTest();
        CustomerCommunity_ShipmentController.getCWShipmentInfo(shipment,trackId);
        Test.stopTest();
    }
    
    static testmethod void updateBookingFieldsTestMethod(){
        List<Header__c> hl=new List<Header__c>();
		//Header__c head=new Header__c(Name='BRN-000001- Shipping Instruction v1',Booking__c='a060t000002k2vsAAA',Shorten_URL__c='https://sforce.co/2oNgW2Z',Status__c='In Process');
		//hl.add(head);
        Header__c head1=new Header__c(Name='BRN-000001- Shipping Instruction v2',Shorten_URL__c='https://sforce.co/2oNgW2Z',Status__c='In Process');
		hl.add(head1);
        Test.startTest();
        CustomerCommunity_ShipmentController.updateBookingFields(hl);
        Test.stopTest();
    }
    
    static testmethod void updateHeaderStatusTestMethod(){
        Attachment attach=new Attachment();   	
    	attach.Name='Unit Test Attachment.xml';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
        List<Attachment> att=new List<Attachment>();
        att.add(attach);
        Test.startTest();
        CustomerCommunity_ShipmentController.updateHeaderStatus(att);
        Test.stopTest();
    }
    
    static testmethod void updateShortenURLTestMethod(){
        CustomerCommunity_MockResponses.ShipmentControllerupdateShortenURLMockResponse mock=new CustomerCommunity_MockResponses.ShipmentControllerupdateShortenURLMockResponse();
        Test.setMock(HttpCalloutMock.class, mock);
        set<ID> hl=new set<ID>();
		//ID head = 'a090t000009AH6TAAW';
		//hl.add(head);
        ID head1 = 'a090t000009AH6sAAG';
        hl.add(head1);
        Test.startTest();
        CustomerCommunity_ShipmentController.updateShortenURL(hl);
        Test.stopTest();
    }
}