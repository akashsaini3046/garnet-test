public class BookingFCLRatesService {

    public static String callingSource = '';
    public static CustomerCommunity_RatingAPIResponse fetchUpdateBookingWrapper(BookingWrapper bookingWrap){
        CustomerCommunity_RatingAPIResponse responseWrapper = new CustomerCommunity_RatingAPIResponse();
        CustomerCommunity_RatingAPIRequest requestWrapper = createRequestWrapper(bookingWrap);
        System.debug('@@@@@@ Rating API Request : '+JSON.serialize(requestWrapper));
        if(requestWrapper != null){
            responseWrapper = sendRatingRequest(requestWrapper);
            if(responseWrapper != null){
                return responseWrapper; 
            }
        }
        return null;
    }

    public static String softshipRatingRequest(BookingWrapper bookingWrap){       
        CustomerCommunity_RatingAPIRequest requestWrapper = createRequestWrapper(bookingWrap);   
        System.debug('@@@@@@ Rating API Request : '+JSON.serialize(requestWrapper,true));
        return JSON.serialize(requestWrapper,true);
    }

    public static CustomerCommunity_RatingAPIRequest createRequestWrapper(BookingWrapper bookingWrap){//
        CustomerCommunity_RatingAPIRequest requestWrapper = new CustomerCommunity_RatingAPIRequest();
        requestWrapper = getWrapper(bookingWrap);
        return requestWrapper;
    }
	
    public static CustomerCommunity_RatingAPIResponse sendRatingRequest(CustomerCommunity_RatingAPIRequest request){
        CustomerCommunity_RatingAPIResponse responseWrapper = new CustomerCommunity_RatingAPIResponse();
        String requestString = JSON.serialize(request, true);
        String responseString = CustomerCommunity_APICallouts.getAPIResponse('RatingAPINew', requestString);
        responseString = responseString.replace('"Number":','"NumberX":');
        responseString = responseString.replace('"From":','"FromX":');
        //String responseString = CustomerCommunity_APICallouts.getCustomAuthRes('callout:RatingAPINew',requestString);
        //System.debug('responseString-->'+responseString);
        responseWrapper = (CustomerCommunity_RatingAPIResponse) JSON.deserialize(responseString, CustomerCommunity_RatingAPIResponse.class);
        System.debug('responseWrapper '+responseWrapper);
        return responseWrapper;
    }
    private static CustomerCommunity_RatingAPIRequest getWrapper(BookingWrapper bookingWrap){
        CustomerCommunity_RatingAPIRequest requestWrapper = new CustomerCommunity_RatingAPIRequest();
        requestWrapper.Booking = getBooking(bookingWrap);
        requestWrapper.Action = 'CalculateRate';
        requestWrapper.CalcRule = 'N';        
        requestWrapper.MaxNumberOfSchedules = 5;
        requestWrapper.maxRoutes = 5;
        requestWrapper.DefaultOptionalServices = 'Y';
        requestWrapper.SelectedRoute = bookingWrap.selectedRouteId;
        return requestWrapper;
    }
    
    private static CustomerCommunity_RatingAPIRequest.Booking getBooking(BookingWrapper bookingWrapper){
        CustomerCommunity_RatingAPIRequest.Booking booking = new CustomerCommunity_RatingAPIRequest.Booking();
        String bookingCargoType = bookingWrapper.shipment.listCargo[0].cargoType;
       /* if(bookingCargoType.equalsIgnoreCase('breakbulk')){
            booking.BookingAgency = getBookingAgency();
        }*/
        booking.BookingAgency = getBookingAgency();
        booking.Customer = getCustomer(bookingWrapper);
        booking.RequestedBookingRoute = getRequestedBookingRoute(bookingWrapper) ;
        booking.BookingCargo = getBookingCargo(bookingWrapper);
        booking.TransCurrency = getTransCurrency() ; // HC
        booking.Contract = getContract(bookingWrapper) ;
        booking.ShipmentType = 'FF'; // HC
        booking.BookingDate ='';
        booking.ShipmentCondition = bookingWrapper.booking.Payment_Terms__c;
        /*if(bookingWrapper.booking.Ready_Date__c != null){
            //leg.ReadyDate = String.valueOf(bookingWrapper.booking.Ready_Date__c.year())+'-'+String.valueOf(bookingWrapper.booking.Ready_Date__c.month())+'-'+String.valueOf(bookingWrapper.booking.Ready_Date__c.day());
            booking.ReadyDate = String.valueOf(bookingWrapper.booking.Ready_Date__c.year())+'-'+String.valueOf(bookingWrapper.booking.Ready_Date__c.month())+'-'+String.valueOf(bookingWrapper.booking.Ready_Date__c.day())+'T00:00:00Z';
        }else{
            //leg.ReadyDate = String.valueOf(date.today().year())+'-'+String.valueOf(date.today().month())+'-'+String.valueOf(date.today().day());
            booking.ReadyDate = String.valueOf(date.today().year())+'-'+String.valueOf(date.today().month())+'-'+String.valueOf(date.today().day())+'T00:00:00Z';
        }*/
        return booking;
    }
    
    private static CustomerCommunity_RatingAPIRequest.BookingAgency getBookingAgency(){
        CustomerCommunity_RatingAPIRequest.BookingAgency bookingAgency = new CustomerCommunity_RatingAPIRequest.BookingAgency();
        bookingAgency.Code = 'HQ';
        return bookingAgency;
    }
    private static CustomerCommunity_RatingAPIRequest.Customer getCustomer(BookingWrapper bookingWrapper){
        CustomerCommunity_RatingAPIRequest.Customer customer = new CustomerCommunity_RatingAPIRequest.Customer();
        Account account;
        if(bookingWrapper.booking.Account__c != null){
            account = [SELECT Id, CVIF__c, Name FROM Account WHERE Id = :bookingWrapper.booking.Account__c];
            customer.CustomerCode = getCustomerCode(account.CVIF__c); 
            customer.CustomerName1 = account.Name;
        }else{
            customer.CustomerCode = getCustomerCode('');
            customer.CustomerName1 = '';
        }
        /*if(bookingCargoType.equalsIgnoreCase('BreakBulk')){
            customer.CustomerMatchCode = getCustomerMatchCode();
        }*/
        return customer;
    }
    private static CustomerCommunity_RatingAPIRequest.RequestedBookingRoute getRequestedBookingRoute(BookingWrapper bookingWrapper) {
        CustomerCommunity_RatingAPIRequest.RequestedBookingRoute requestedBookingRoute = new CustomerCommunity_RatingAPIRequest.RequestedBookingRoute();
        requestedBookingRoute.MaxTransshipments = 3;
        requestedBookingRoute.Legs = getLegsList(bookingWrapper);
        return requestedBookingRoute;
    }
    private static CustomerCommunity_RatingAPIRequest.BookingCargo getBookingCargo(BookingWrapper bookingWrapper) {
        CustomerCommunity_RatingAPIRequest.BookingCargo bookingCargo = new CustomerCommunity_RatingAPIRequest.BookingCargo();
        String bookingCargoType = bookingWrapper.shipment.listCargo[0].cargoType;
        if(bookingCargoType.equalsIgnoreCase('Container')){
            if(bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper != null 
                && bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper.size() > 0){
                bookingCargoType = 'MultiContainerCargoType';
            } else {
                bookingCargoType = 'SingleContainerCargoType';
            }
        }
        if(bookingCargoType.equalsIgnoreCase('RoRo')){
            bookingCargoType='RoRo';
        }
        if(bookingCargoType.equalsIgnoreCase('BreakBulk')){
            bookingCargoType='BreakBulk';
        }
        bookingCargo.type = bookingCargoType;
        bookingCargo.values = getValuesList(bookingWrapper, bookingCargoType);
        return bookingCargo;
    }
    private static CustomerCommunity_RatingAPIRequest.TransCurrency getTransCurrency() {
        CustomerCommunity_RatingAPIRequest.TransCurrency transCurrency = new CustomerCommunity_RatingAPIRequest.TransCurrency();
        transCurrency.Code = 'USD';
        return transCurrency;
    }
    private static CustomerCommunity_RatingAPIRequest.Contract getContract(BookingWrapper bookingWrapper) {
        CustomerCommunity_RatingAPIRequest.Contract contract = new CustomerCommunity_RatingAPIRequest.Contract();
        if(String.isNotBlank(bookingWrapper.booking.Contract_Number__c)){
            contract.ContractNumber = bookingWrapper.booking.Contract_Number__c;
        }else{
            contract.ContractNumber = '';
        }        
        return contract;
    }
    private static CustomerCommunity_RatingAPIRequest.CustomerCode getCustomerCode(String code) {
        CustomerCommunity_RatingAPIRequest.CustomerCode customerCode = new CustomerCommunity_RatingAPIRequest.CustomerCode();
        customerCode.Code = code; 
        return customerCode;
    }
    /*private static CustomerCommunity_RatingAPIRequest.CustomerMatchCode getCustomerMatchCode() {
        CustomerCommunity_RatingAPIRequest.CustomerMatchCode customerMatchCode = new CustomerCommunity_RatingAPIRequest.CustomerMatchCode();
        customerMatchCode.Code = '0296708'; 
        return customerMatchCode;
    }*/
    
    private static String checkNull(String strValue) {
        return (strValue!=null) ? strValue : '';
    }
    private static List<CustomerCommunity_RatingAPIRequest.Legs> getLegsList(BookingWrapper bookingWrapper) {
        List<CustomerCommunity_RatingAPIRequest.Legs> legs = new List<CustomerCommunity_RatingAPIRequest.Legs>();
       /* Map<String, Ports_Information__c> portNameVsPortInfo = new Map<String, Ports_Information__c>();
        Set<String> setLocationCodes = new Set<String>();
        List<Ports_Information__c> portInformationList = new List<Ports_Information__c>();
        
        if(bookingWrapper.booking.Origin_Type__c != 'P') {
            setLocationCodes.add(bookingWrapper.booking.Customer_Origin_Code__c );
        }   
        if(bookingWrapper.booking.Destination_Type__c != 'P') {
            setLocationCodes.add(bookingWrapper.booking.Customer_Destination_Code__c);
        }
        if(!setLocationCodes.isEmpty()){
            portInformationList = [SELECT Name, Zip_Code__c, Location_Code__c, Softship_Zipcodes__c FROM Ports_Information__c WHERE Name IN :setLocationCodes ORDER BY Name];
        }
        if(!portInformationList.isEmpty()){
            for(Ports_Information__c port : portInformationList){
                portNameVsPortInfo.put(port.Name, port);
            }
        }
        */
        CustomerCommunity_RatingAPIRequest.Legs leg = new CustomerCommunity_RatingAPIRequest.Legs();
        leg.LegSequence = 1;
        if(bookingWrapper.booking.Ready_Date__c != null){
            leg.ReadyDate = String.valueOf(bookingWrapper.booking.Ready_Date__c.year())+'-'+String.valueOf(bookingWrapper.booking.Ready_Date__c.month())+'-'+String.valueOf(bookingWrapper.booking.Ready_Date__c.day())+'T00:00:00Z';
        }else{
            leg.ReadyDate = String.valueOf(date.today().year())+'-'+String.valueOf(date.today().month())+'-'+String.valueOf(date.today().day())+'T00:00:00Z';
        }
        //leg.ReceiptTermCode = bookingWrapper.booking.Origin_Type__c;
		//leg.DeliveryTermCode = bookingWrapper.booking.Destination_Type__c;
		leg.ReceiptTermCode = bookingWrapper.booking.Description__c.substring(0, 1);
		leg.DeliveryTermCode = bookingWrapper.booking.Description__c.substring(1);
        //leg.StartLocation = getLocation(bookingWrapper.booking.Customer_Origin_Code__c, bookingWrapper.booking.Origin_Type__c, portNameVsPortInfo); 
        //leg.EndLocation = getLocation(bookingWrapper.booking.Customer_Destination_Code__c, bookingWrapper.booking.Destination_Type__c, portNameVsPortInfo); 
         leg.StartLocation = bookingWrapper.booking.Customer_Origin_Code__c; 
        leg.EndLocation = bookingWrapper.booking.Customer_Destination_Code__c;         
        
      // leg.StartSubLocationCode = bookingWrapper.booking.From_Sub_Location__c;
       // leg.EndSubLocationCode = bookingWrapper.booking.To_Sub_Location__c;
       	leg.StartSubLocationCode = bookingWrapper.booking.Pickup_Location__c;
        leg.EndSubLocationCode = bookingWrapper.booking.Delivery_Location__c;      
        //leg.StartSubLocationCode ='';
        //leg.EndSubLocationCode='';
        //leg.ReceiptTermBehavior ='';
        leg.ReceiptTermBehavior =bookingWrapper.booking.Description__c.substring(0, 1);
        leg.ReceiptStopoverCode ='';
        //leg.ReceiptTypeCode =bookingWrapper.booking.Transportation_Management_System_Origin__c;
        leg.ReceiptTypeCode = checkNull(bookingWrapper.booking.Transportation_Management_System_Origin__c);
         leg.ReceiptDrayage = '';
        //leg.DeliveryTermBehavior ='';
        leg.DeliveryTermBehavior =bookingWrapper.booking.Description__c.substring(1);
        leg.DeliveryStopoverCode ='';
        //leg.DeliveryTypeCode =bookingWrapper.booking.Transportation_Management_System_Destina__c;
        leg.DeliveryTypeCode = checkNull(bookingWrapper.booking.Transportation_Management_System_Destina__c);
		leg.DeliveryDrayage = '';
        legs.add(leg);
        
        return legs;
    }
    
    private static List<CustomerCommunity_RatingAPIRequest.Values> getValuesList(BookingWrapper bookingWrapper, String bookingCargoType){
        List<CustomerCommunity_RatingAPIRequest.Values> values = new List<CustomerCommunity_RatingAPIRequest.Values>();
        List<New_Booking_Container_Type__mdt> containerTypeList = new  List<New_Booking_Container_Type__mdt>();
        List<New_Booking_Commodities__mdt> commoditiesList = new List<New_Booking_Commodities__mdt>();
        Map<String, String> containerTypeVsLength = new Map<String, String>();
        Map<String, New_Booking_Commodities__mdt> commodityVsMetaData = new Map<String, New_Booking_Commodities__mdt>();
        
        containerTypeList = [SELECT Id, MasterLabel, Category__c, Length__c, Type__c FROM New_Booking_Container_Type__mdt ORDER BY MasterLabel];
        if(!containerTypeList.isEmpty()){
            for(New_Booking_Container_Type__mdt bookingContainerType : containerTypeList){
                String containerValue = '';
                if(bookingContainerType.Length__c != Null && bookingContainerType.Length__c != ''){
                    containerValue = containerValue + bookingContainerType.Length__c + ' ' + bookingContainerType.MasterLabel;
                    containerTypeVsLength.put(containerValue, bookingContainerType.Length__c);
                }else{
                    containerValue = containerValue + bookingContainerType.MasterLabel;
                    containerTypeVsLength.put(containerValue, bookingContainerType.Length__c);
                }
            }
        }
        
        commoditiesList = [SELECT MasterLabel, Commodity_Name__c, Category__c FROM New_Booking_Commodities__mdt WHERE Category__c != Null AND Category__c != ''  ORDER BY MasterLabel];
        if(!commoditiesList.isEmpty()){
            for(New_Booking_Commodities__mdt commodity : commoditiesList){
                if(commodity.Commodity_Name__c != Null && commodity.Commodity_Name__c != ''){
                    commodityVsMetaData.put(commodity.Commodity_Name__c, commodity);
                }   
            }
        }
        List<BookingWrapper.RequirementWrapper> allRequirementWrappers = new List<BookingWrapper.RequirementWrapper>();
        for(BookingWrapper.FreightDetailWrapper freightWrapper : bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper){
            allRequirementWrappers.addAll(freightWrapper.listRequirementWrapper);
        }
        CustomerCommunity_RatingAPIRequest.Values value = new CustomerCommunity_RatingAPIRequest.Values();
        Integer i=1;
        if(bookingCargoType.equalsIgnoreCase('RoRo') || bookingCargoType.equalsIgnoreCase('BreakBulk')){
            for(BookingWrapper.FreightDetailWrapper freightWrapper :  bookingWrapper.shipment.listCargo[0].listFreightDetailWrapper){
                value = new CustomerCommunity_RatingAPIRequest.Values();
                System.debug('-------------->>>>>'+freightWrapper);
                value.Cargo = getCargoList(freightWrapper, bookingCargoType);
                value.CargoItemId= new Map<String,Integer>{'ItemRunningNumber'=>i}; 
                values.add(value);
                i++;
            }
            
        }else{
            for(BookingWrapper.RequirementWrapper requirementWrapper : allRequirementWrappers){
                value = new CustomerCommunity_RatingAPIRequest.Values();
                //value.Cargo = getCargoList(requirementWrapper, containerTypeVsLength, commodityVsMetaData, bookingCargoType);
                //values.add(value);
                value.Cargo = getContainerCargoListContainer(requirementWrapper);
                value.CargoItemId= new Map<String,Integer>{'ItemRunningNumber'=>i};
                values.add(value);
                i++;
            }
            
        }
        //values.add(value);
        return values;
    }
     private static List<CustomerCommunity_RatingAPIRequest.Cargo> getContainerCargoListContainer(BookingWrapper.RequirementWrapper requirementWrapper){
        System.debug('---->>'+requirementWrapper);
        Integer runningNumber=1;
        List<CustomerCommunity_RatingAPIRequest.Cargo> cargoList = new List<CustomerCommunity_RatingAPIRequest.Cargo>();
        while(runningNumber<= requirementWrapper.requirement.Quantity__c ){
            CustomerCommunity_RatingAPIRequest.Cargo cargo = new CustomerCommunity_RatingAPIRequest.Cargo();
            cargo.Id= new Map<String,Integer>{'ContainerRunningNumber'=>runningNumber};
            
            CustomerCommunity_RatingAPIRequest.KindOfPackage kindOfPackage = new CustomerCommunity_RatingAPIRequest.KindOfPackage();
            kindOfPackage.Code = requirementWrapper.containerType;
            cargo.KindOfPackage = kindOfPackage;
            cargo.Commodity = getCommodity(requirementWrapper);
            cargo.ContainerType = getContainerType(requirementWrapper);
            cargo.LoadingType = getLoadingType(requirementWrapper); 
            cargo.NumberOfPackage = getNumberOfPackage(requirementWrapper);
            cargo.OperationalStatus = getOperationalStatus();
            cargo.DescriptionOfGoods = '';
            cargo.IsEmpty = requirementWrapper.requirement.Is_Empty__c;
            cargo.IsShippersOwn = requirementWrapper.requirement.IsShippersOwn__c;
            if(cargo.IsShippersOwn){
                cargo.ContainerNumber= requirementWrapper.requirement.Shipper_Owned_Equipment_Number__c;
            }else{
                cargo.ContainerNumber= '';
            }
            cargo.IsHeavy = requirementWrapper.requirement.IsHeavy__c;
            CustomerCommunity_RatingAPIRequest.Reefer reefer = new CustomerCommunity_RatingAPIRequest.Reefer();
            reefer.IsNonOperative = requirementWrapper.requirement.IsNonOperativeReefer__c;
            cargo.Reefer = reefer;

            CustomerCommunity_RatingAPIRequest.ValueUnit weightKilogram = new CustomerCommunity_RatingAPIRequest.ValueUnit();
            weightKilogram.Value = 0.00;
            weightKilogram.Unit = 0;
            cargo.WeightKilogram =weightKilogram;

            CustomerCommunity_RatingAPIRequest.ValueUnit grossValueUnit = new CustomerCommunity_RatingAPIRequest.ValueUnit();
            grossValueUnit.Value = 0.00;
            grossValueUnit.Unit = 0;
            cargo.CargoGrossWeight = grossValueUnit;

            CustomerCommunity_RatingAPIRequest.ValueUnit netValueUnit = new CustomerCommunity_RatingAPIRequest.ValueUnit();
            netValueUnit.Value = 0.00;
            netValueUnit.Unit = 0;
            cargo.CargoNetWeight =netValueUnit;
            if(requirementWrapper.requirement.OutOfGauge__c){
                cargo.OutOfGauge = getOutOfGauge(requirementWrapper);
            }
            else{
                cargo.OutOfGauge=null;
            }      

            //cargo.Imdg=getImdg();
            cargoList.add(cargo);
            runningNumber++;
        }
        return cargoList;
    }
    
     private static CustomerCommunity_RatingAPIRequest.OutOfGauge getOutOfGauge(BookingWrapper.RequirementWrapper requirementWrapper){
        CustomerCommunity_RatingAPIRequest.OutOfGauge outOfGauge = new CustomerCommunity_RatingAPIRequest.OutOfGauge();
        outOfGauge.RequestNumber='';
        outOfGauge.LostSlots=0;
        outOfGauge.Top = getOutOfGaugeUnitValue(0,0);
        outOfGauge.Left = getOutOfGaugeUnitValue(0,0);
        outOfGauge.Right = getOutOfGaugeUnitValue(0,0);
        outOfGauge.Front = getOutOfGaugeUnitValue(0,0);
        outOfGauge.Door = getOutOfGaugeUnitValue(1,3);
        return outOfGauge;
    }

    private static CustomerCommunity_RatingAPIRequest.ValueUnit getOutOfGaugeUnitValue(Decimal value, Integer unit){
        CustomerCommunity_RatingAPIRequest.ValueUnit valueUnit = new CustomerCommunity_RatingAPIRequest.ValueUnit();
        valueUnit.Value = value;
        valueUnit.Unit = Unit;
        return valueUnit;
    }


    private static List<CustomerCommunity_RatingAPIRequest.Cargo> getCargoList(BookingWrapper.FreightDetailWrapper freightWrapper,  String bookingCargoType)
    {
        List<CustomerCommunity_RatingAPIRequest.Cargo> cargoList = new List<CustomerCommunity_RatingAPIRequest.Cargo>();
       // for(BookingWrapper.FreightDetailWrapper freightWrapper : allFreightDetailWrapper){
            CustomerCommunity_RatingAPIRequest.Cargo cargo = new CustomerCommunity_RatingAPIRequest.Cargo();
            cargoList.add(getCargo(freightWrapper, bookingCargoType));
        //}
         return cargoList;
    }

    private static CustomerCommunity_RatingAPIRequest.Cargo getCargo(BookingWrapper.FreightDetailWrapper freightDetailWrapper,  String bookingCargoType)
    {
        CustomerCommunity_RatingAPIRequest.Cargo cargo = new CustomerCommunity_RatingAPIRequest.Cargo();
        CustomerCommunity_RatingAPIRequest.Commodity commodity = new CustomerCommunity_RatingAPIRequest.Commodity();
        commodity.Code = freightDetailWrapper.commodityDesc;
        commodity.DescriptionOfGoods = '';
        cargo.Commodity = commodity;
       
        if(bookingCargoType.equalsIgnoreCase('RoRo')){
            cargo.Vin = '';
            cargo.CarModelId = getCarModelId(freightDetailWrapper.freightDetail);	
            cargo.ModelYear = 0;
        }
        
        cargo.LengthCentimeter = getLengthCentimeter(freightDetailWrapper.freightDetail);	
        cargo.WidthCentimeter = getWidthCentimeter(freightDetailWrapper.freightDetail);	
        cargo.HeightCentimeter = getHeightCentimeter(freightDetailWrapper.freightDetail);	
        cargo.WeightKilogram = getWeightKilogram(freightDetailWrapper.freightDetail);

        CustomerCommunity_RatingAPIRequest.KindOfPackage kindOfPackage = new CustomerCommunity_RatingAPIRequest.KindOfPackage();
        if(bookingCargoType.equalsIgnoreCase('RoRo')){
            kindOfPackage.Code = freightDetailWrapper.freightDetail.Privately_Owned_Dealer_Owned_Vehicle__c;
           
        }else{
            kindOfPackage.Code = freightDetailWrapper.typeOfPackage;
        }
        cargo.KindOfPackage = kindOfPackage;
        CustomerCommunity_RatingAPIRequest.LoadingType loadingType = new CustomerCommunity_RatingAPIRequest.LoadingType();
        loadingType.Code ='F';
        cargo.LoadingType = loadingType;       
        Map<String, Integer> numberOfPackage = new Map<String, Integer>();
        numberOfPackage.put('Number', 1);
        cargo.NumberOfPackage = numberOfPackage; //HC
        Map<String, Integer> qtyOfPackage = new Map<String, Integer>();
        qtyOfPackage.put('Number', Integer.valueOf(freightDetailWrapper.freightDetail.Freight_Quantity__c));
        
        cargo.QuantityOfPackage = qtyOfPackage; //HC
        cargo.OperationalStatus = getOperationalStatus(); // HC

        CustomerCommunity_RatingAPIRequest.Code imdgcode = new CustomerCommunity_RatingAPIRequest.Code();
        imdgcode.Code ='';
        CustomerCommunity_RatingAPIRequest.Imdg imdg = new CustomerCommunity_RatingAPIRequest.Imdg();
        imdg.ClassCode =imdgcode;
        cargo.Imdg = imdg;

        return cargo;
    }
    


    /*private static List<CustomerCommunity_RatingAPIRequest.Cargo> getCargoList(BookingWrapper.RequirementWrapper requirementWrapper, 
                                            Map<String, String> containerTypeVsLength, Map<String, New_Booking_Commodities__mdt> commodityVsMetaData, String bookingCargoType)
    {
        System.debug('---->>'+requirementWrapper);
        List<CustomerCommunity_RatingAPIRequest.Cargo> cargoList = new List<CustomerCommunity_RatingAPIRequest.Cargo>();
        //for(BookingWrapper.RequirementWrapper requirementWrapper : allRequirementWrappers){
            CustomerCommunity_RatingAPIRequest.Cargo cargo = new CustomerCommunity_RatingAPIRequest.Cargo();
            cargo.Commodity = getCommodity(requirementWrapper);
            cargo.ContainerType = getContainerType(requirementWrapper);   
            cargo.KindOfPackage = getKindOfPackage(requirementWrapper);
            cargo.LoadingType = getLoadingType(requirementWrapper); 
            cargo.NumberOfPackage = getNumberOfPackage(requirementWrapper); //HC
            cargo.QuantityOfPackage = getQtyOfPackage(requirementWrapper);
            cargo.OperationalStatus = getOperationalStatus(); // HC
            System.debug('cargo------>'+cargo);
            cargoList.add(cargo);
       // }
       System.debug('cargoList------>'+cargoList);
        return cargoList;
    }*/
    private static CustomerCommunity_RatingAPIRequest.Commodity getCommodity(BookingWrapper.RequirementWrapper requirementWrapper){
        CustomerCommunity_RatingAPIRequest.Commodity commodity = new CustomerCommunity_RatingAPIRequest.Commodity();
        /*if(requirementWrapper != null && requirementWrapper.requirement != Null &&  requirementWrapper.commodityDesc != Null && requirementWrapper.commodityDesc != '' 
           && !commodityVsMetaData.isEmpty() && commodityVsMetaData.containsKey(requirementWrapper.commodityDesc) 
           && commodityVsMetaData.get(requirementWrapper.commodityDesc).MasterLabel != Null)
        {
            commodity.Code = commodityVsMetaData.get(requirementWrapper.commodityDesc).MasterLabel;
            String description = requirementWrapper.commodityDesc;
            commodity.DescriptionOfGoods = (description != null && description != '' ? description : '');
        }else{
            commodity.Code = '';
            commodity.DescriptionOfGoods = '';
        }*/
        
        commodity.Code = (requirementWrapper.commodityCode != null && requirementWrapper.commodityCode != '') ? requirementWrapper.commodityCode : '';
        commodity.DescriptionOfGoods = (requirementWrapper.commodityDesc != null && requirementWrapper.commodityDesc != '') ? requirementWrapper.commodityDesc : '';        

        return commodity;
    }
    private static CustomerCommunity_RatingAPIRequest.ContainerType getContainerType(BookingWrapper.RequirementWrapper requirementWrapper){
        CustomerCommunity_RatingAPIRequest.ContainerType containerType = new CustomerCommunity_RatingAPIRequest.ContainerType();
       /* containerType.Code = '';
        containerType.Description = '';
        if(callingSource.equalsIgnoreCase('ShippingInstruction') && requirementWrapper != null && requirementWrapper.requirement!= null && requirementWrapper.requirement.Container_Type__c != null){
            containerType.Code = requirementWrapper.requirement.Container_Type__c;
            containerType.Description = '';
            return containerType;
        }
        containerType.Code = requirementWrapper.containerType;
        containerType.Description = requirementWrapper.containerDesc; */
        containerType.IsReefer = requirementWrapper.requirement.Running_Reefer__c;
        containerType.ContainerTypeCode = getContainerTypeCode(requirementWrapper);
        
        return containerType;
    }
     private static CustomerCommunity_RatingAPIRequest.ContainerTypeCode getContainerTypeCode(BookingWrapper.RequirementWrapper requirementWrapper){
        CustomerCommunity_RatingAPIRequest.ContainerTypeCode containerTypeCode = new CustomerCommunity_RatingAPIRequest.ContainerTypeCode();
        containerTypeCode.Code = requirementWrapper.containerType;
        containerTypeCode.Description = requirementWrapper.containerDesc;        
        return containerTypeCode;
    }
    
    private static CustomerCommunity_RatingAPIRequest.KindOfPackage getKindOfPackage(BookingWrapper.FreightDetailWrapper freightDetailWrapper){
        CustomerCommunity_RatingAPIRequest.KindOfPackage kindOfPackage = new CustomerCommunity_RatingAPIRequest.KindOfPackage();
      /*  if(callingSource.equalsIgnoreCase('ShippingInstruction') && requirementWrapper != null && requirementWrapper.requirement!= null && requirementWrapper.requirement.Container_Type__c != null){
            kindOfPackage.Code = requirementWrapper.requirement.Container_Type__c;
            return kindOfPackage;
        } */
         kindOfPackage.Code = freightDetailWrapper.typeOfPackage;
        kindOfPackage.Description =freightDetailWrapper.packageDesc;
        return kindOfPackage;
    }
    private static CustomerCommunity_RatingAPIRequest.CarModelId getCarModelId(FreightDetail__c freightDetail){
        CustomerCommunity_RatingAPIRequest.CarModelId carModelId = new CustomerCommunity_RatingAPIRequest.CarModelId();
        carModelId.Manufacturer = freightDetail.Manufacturer__c;
        carModelId.Model = freightDetail.Model__c;
        carModelId.Type = freightDetail.Type__c;
        return carModelId;
    }
    private static CustomerCommunity_RatingAPIRequest.LengthCentimeter getLengthCentimeter(FreightDetail__c freightDetail){
        CustomerCommunity_RatingAPIRequest.LengthCentimeter lengthCentimeter = new CustomerCommunity_RatingAPIRequest.LengthCentimeter();
        lengthCentimeter.Unit = 0;
        lengthCentimeter.Amount = freightDetail.Length_Major__c;
        return lengthCentimeter;
    }
    private static CustomerCommunity_RatingAPIRequest.WidthCentimeter getWidthCentimeter(FreightDetail__c freightDetail){
        CustomerCommunity_RatingAPIRequest.WidthCentimeter widthCentimeter = new CustomerCommunity_RatingAPIRequest.WidthCentimeter();
        widthCentimeter.Unit = 0;
        widthCentimeter.Amount = freightDetail.Width_Major__c;
        return widthCentimeter;
    }
    private static CustomerCommunity_RatingAPIRequest.HeightCentimeter getHeightCentimeter(FreightDetail__c freightDetail){
        CustomerCommunity_RatingAPIRequest.HeightCentimeter heightCentimeter = new CustomerCommunity_RatingAPIRequest.HeightCentimeter();
        heightCentimeter.Unit = 0;
        heightCentimeter.Amount = freightDetail.Height_Major__c;
        return heightCentimeter;
    }
    private static CustomerCommunity_RatingAPIRequest.ValueUnit getWeightKilogram(FreightDetail__c freightDetail){
        CustomerCommunity_RatingAPIRequest.ValueUnit weightKilogram = new CustomerCommunity_RatingAPIRequest.ValueUnit();
        //weightKilogram.Value = Integer.valueOf(freightDetail.Declared_Weight_Value__c);
        weightKilogram.Value = freightDetail.Declared_Weight_Value__c;
        weightKilogram.Unit = 0;
        return weightKilogram;
    }
    private static CustomerCommunity_RatingAPIRequest.LoadingType getLoadingType(BookingWrapper.RequirementWrapper requirementWrapper){
        CustomerCommunity_RatingAPIRequest.LoadingType loadingType = new CustomerCommunity_RatingAPIRequest.LoadingType();
        loadingType.Code = requirementWrapper.requirement.Is_Empty__c != null && requirementWrapper.requirement.Is_Empty__c ? 'E' : 'F';
        return loadingType;
    }

    private static Map<String, Integer> getQtyOfPackage(BookingWrapper.RequirementWrapper requirementWrapper){
        Map<String, Integer> numberOfPackage = new Map<String, Integer>();
        numberOfPackage.put('Number', Integer.valueOf(requirementWrapper.requirement.Quantity__c));
        return numberOfPackage;
    }

    private static Map<String, Integer> getNumberOfPackage(BookingWrapper.RequirementWrapper requirementWrapper){
        Map<String, Integer> numberOfPackage = new Map<String, Integer>();
        numberOfPackage.put('Number', 1);
        return numberOfPackage;
    }

    private static CustomerCommunity_RatingAPIRequest.OperationalStatus getOperationalStatus(){
        CustomerCommunity_RatingAPIRequest.OperationalStatus operationalStatus = new CustomerCommunity_RatingAPIRequest.OperationalStatus();
        operationalStatus.Status  = 'A';
        return operationalStatus;
    }
    private static String getLocation(String originDestinationCode, String termValue, Map<String, Ports_Information__c> portNameVsPortInfo){
        if(originDestinationCode != null && originDestinationCode != ''){
            if(termValue.equals('P')){
                return originDestinationCode.toUpperCase();
            } else if (termValue.equals('CFS') && !portNameVsPortInfo.isEmpty() && portNameVsPortInfo.containsKey(originDestinationCode)) {
                String locationCode = portNameVsPortInfo.get(originDestinationCode).Location_Code__c ;
                return (locationCode != null && locationCode != '') ? locationCode : ''; 
            } else if (termValue.equals('D') && !portNameVsPortInfo.isEmpty() && portNameVsPortInfo.containsKey(originDestinationCode)) {
                String zipCode = portNameVsPortInfo.get(originDestinationCode).Softship_Zipcodes__c ;
                return (zipCode != null && zipCode != '') ? zipCode : '';
            } else {
                return '';
            }
        }
        return '';
    }
}