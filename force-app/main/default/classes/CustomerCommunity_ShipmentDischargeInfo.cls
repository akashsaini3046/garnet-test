public class CustomerCommunity_ShipmentDischargeInfo {
   	@AuraEnabled public String dateInfo{get; set;}
    @AuraEnabled public String flag{get; set;}
    @AuraEnabled public String location{get; set;}
}