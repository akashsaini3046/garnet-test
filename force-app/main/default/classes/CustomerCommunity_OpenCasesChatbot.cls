public class CustomerCommunity_OpenCasesChatbot {
    @InvocableMethod(label='Get Open Cases')
    public static List<List<String>> getOpenCases(List<String> listContactId) {
        List<Case> listCases = new List<Case>();
        List<Contact> listContact = new List<Contact>();
        List<String> listOpenCases = new List<String>();
        List<List<String>> listCaseResponse = new List<List<String>>();
        try{
            listContact = [SELECT Id, AccountId FROM Contact WHERE Id = :listContactId[0]];
            if(!listContact.isEmpty()){
                listCases = [SELECT Id, CaseNumber, ContactId FROM Case 
                             WHERE (ContactId = :listContact[0].Id OR AccountId = :listContact[0].AccountId ) AND IsClosed = False ORDER BY CreatedDate ASC LIMIT 4];
                if(!listCases.isEmpty()){
                    if(listCases.size() >= 1)
                    	listOpenCases.add(listCases[0].CaseNumber);
                    if(listCases.size() >= 2)
                    	listOpenCases.add(listCases[1].CaseNumber);
                    if(listCases.size() >= 3)
                    	listOpenCases.add(listCases[2].CaseNumber);
                    if(listCases.size() >= 4)
                    	listOpenCases.add(listCases[3].CaseNumber);
                    listCaseResponse.add(listOpenCases);
                }
            }
            return listCaseResponse;
        }
        catch(Exception ex){
            ExceptionHandler.logApexCalloutError(ex);
            return listCaseResponse;
        }
    }
}