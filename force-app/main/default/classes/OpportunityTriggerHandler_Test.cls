@isTest
public class OpportunityTriggerHandler_Test {
    @testSetup static void setup()
    {
        Account accObj1 = new Account();
        accObj1.Name = 'Account 123';
        insert accObj1;
        
        DescribeFieldResult describe = Address__c.Country__c.getDescribe();
        List<PicklistEntry> availableValues = describe.getPicklistValues();
        
        Address__c businessLocationObj = new Address__c();
        businessLocationObj.Account__c = accObj1.Id;
        businessLocationObj.Address_Line_1__c = 'address1';
        businessLocationObj.City__c = 'City1';
        if(availableValues.size()>0)
            businessLocationObj.Country__c = availableValues[0].getValue();
        businessLocationObj.Postal_Code__c = '1111111';
        businessLocationObj.Phone__c = '88888888888';
        businessLocationObj.State__c = 'State1';
        businessLocationObj.Name ='BL1';
        insert businessLocationObj;
        
        Contact con= new Contact();
        con.LastName='Test con1';
        con.Phone = '99999999999';
        con.Email = 'contact@email.com';
        con.Address__c = businessLocationObj.Id;
        con.Contact_Created_From_Lead__c = TRUE;
        insert con;
        
        Product2 pro1 = new Product2();
        pro1.Name='CrowleyFresh';
        pro1.ProductCode = 'CrowleyFresh';
        pro1.isActive=true;
        insert pro1;
        
        Product2 pro2 = new Product2();
        pro2.Name='Air';
        pro2.ProductCode = 'Air';
        pro2.isActive=true;
        insert pro2;
        
        Pricebook2 standardPricebook = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        Update standardPricebook;
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Pricebook2Id = standardPricebook.Id;
        pbEntry.Product2Id = pro1.Id;
        pbEntry.UnitPrice = 100.00;
        pbEntry.IsActive = true;
        insert pbEntry;
        
        CountryRegionMapping__c crm = new CountryRegionMapping__c();
        crm.Name='1';
        crm.Country_Name__c = 'Anguilla';
        crm.Region__c = 'Caribbean';
        insert crm;
        
        ProfileNames__c p1 = new ProfileNames__c();
        p1.Name = 'Sales Leader';
        insert p1;
        
        ProfileNames__c p2 = new ProfileNames__c();
        p2.Name = 'System Administrator';
        insert p2;
        
        Opportunity opp1 = new Opportunity();
        opp1.AccountId = accObj1.Id;
        opp1.Name = 'Opp1';
        opp1.CloseDate = System.today();
        opp1.StageName=ConstantClass.PROSPECTING_OPP_STAGE;
        opp1.Proposal_Submission_Date__c = System.today();
        opp1.Est_Business_Start_Date__c = System.today();
        opp1.Service_Type__c = 'CrowleyFresh;';
        opp1.Contact__c = con.Id;
        
        opp1.Opportunity_Created_from_Lead__c = TRUE;
        insert opp1;
        
        Contract contractObj = new Contract();
        contractObj.Opportunity__c = opp1.Id;
        contractObj.AccountId = accObj1.Id;
        contractObj.StartDate = System.today();
        contractObj.Status = 'Draft';
        contractObj.ContractTerm = 2;
        contractObj.Pricebook2Id = standardPricebook.Id;
        insert contractObj;
        
        OpportunityStageField.getOpportunityStageValue(opp1.Id);
        OpportunityStageField.getQuotedPriceOfOpportunity(opp1.Id);
    }
    
    static testMethod void InsertOpportunityTestMethod()
    {
        Opportunity opp = [Select Id,Service_Type__c, StageName from Opportunity];
        Test.startTest();
        System.assertNotEquals(NULL, opp);
        System.assertEquals(opp.StageName, ConstantClass.PROSPECTING_OPP_STAGE);
        Test.stopTest();
    }
    
    static testMethod void InsertOpportunityLineItemTestMethod()
    {
        Opportunity opp = [Select Id,Service_Type__c from Opportunity];
        System.assertNotEquals(NULL, opp);
        
        PriceBookEntry pbEntry = [Select Id, UnitPrice from PriceBookEntry];
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.OpportunityId = opp.Id;
        oppLineItem.Quantity = 5;
        oppLineItem.PricebookEntryId = pbEntry.Id;
        oppLineItem.TotalPrice = oppLineItem.Quantity * pbEntry.UnitPrice;
        insert oppLineItem;
        System.assertNotEquals(NULL, oppLineItem);
    }
    
    static testMethod void UpdateOpportunityTestMethod()
    {
        Opportunity opp = [Select Id,Service_Type__c,StageName from Opportunity];
        Test.startTest();
        System.assertNotEquals(NULL, opp);
        System.assertEquals(opp.StageName, ConstantClass.PROSPECTING_OPP_STAGE);
        
        opp.Service_Type__c = 'Air;';
        update opp;
        Test.stopTest();
    }
    
    static testMethod void DeleteOpportunityTestMethod()
    {   
        Opportunity opp = [Select Id from Opportunity];
        System.assertNotEquals(NULL, opp);
        List<Opportunity> oppListToDelete = new List<Opportunity>();
        oppListToDelete.add(opp);
        
        test.startTest();
        try
        {
            delete oppListToDelete;
        }
        catch(Exception e)
        {
            System.debug('Exception in Opportunity Test Class'+e.getMessage());
        }
        
        test.stopTest();
    }
    
    static testMethod void DeleteOpportunityLineItemTestMethod2()
    {   
        OpportunityLineItem oppLineItem = [Select Id from OpportunityLineItem];
        System.assertNotEquals(NULL, oppLineItem);
        List<OpportunityLineItem> oppLineItemListToDelete = new List<OpportunityLineItem>();
        oppLineItemListToDelete.add(oppLineItem);
        
        test.startTest();
        
        try
        {
            delete oppLineItemListToDelete;
        }
        catch(Exception e)
        {
            System.debug('Exception in Opportunity Test Class'+e.getMessage());
        }
        test.stopTest();
    }
    
}