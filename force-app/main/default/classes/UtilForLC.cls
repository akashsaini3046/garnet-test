public with sharing class UtilForLC {

    @AuraEnabled
    public static List<sObject> fetchRecords(String jsonString){
        try{
            QueryMetaData queryMetaData = (QueryMetaData) JSON.deserialize(jsonString, QueryMetaData.class);
            return processQuery(queryMetaData);
        }catch(Exception e){
            System.debug(logginglevel.ERROR, 'MESSAGE:'+ e.getMessage() +'STACK TRACE:'+e.getStackTraceString());
        }
        return null;
    }

    private static List<sObject> processQuery(QueryMetaData queryMetaData){
        Map<String, List<sObject>> refIdVsValuesList = new Map<String, List<sObject>>();
        String whereClause = '';
        String limitClause = '';
        String sortClause = '';
        if(queryMetaData.filterLogic != null){
            whereClause = getWhereClause(queryMetaData);
        }
        if(queryMetaData.limitRecords != null){
            limitClause = ' LIMIT '+ queryMetaData.limitRecords;
        }
        if(queryMetaData.sortBy != null){
            sortClause = ' ORDER BY ';
            List<String> sortByList = new List<String>();
            for(SortByWrapper sortBy : queryMetaData.sortBy){
                sortByList.add(sortBy.fieldName + ' ' + sortBy.order);
            }
            sortClause += String.join(sortByList, ',');
        }
        String fieldsToQuery = getFields(queryMetaData);
        String finalQuery = 'SELECT '+ fieldsToQuery + ' FROM ' + queryMetaData.sObjectApiName + whereClause + sortClause + limitClause;
        System.debug(finalQuery);
        return Database.query(finalQuery);
    }

    private static String getFields(QueryMetadata queryMetadata){
        String fieldsToQuery = 'Id';
        List<FieldWrapper> fieldWrapperList = queryMetaData.fieldsToFetch; 
        fieldWrapperList.sort();
        for(FieldWrapper fieldWrapper : fieldWrapperList){
            if(fieldWrapper.dataType.equalsIgnoreCase('picklist')){
                fieldsToQuery += ', toLabel('+ fieldWrapper.fieldName + ') ';
            }else{
                fieldsToQuery += ', ' + fieldWrapper.fieldName;
            }
        }
        return fieldsToQuery;
    }

    private static String getWhereClause(QueryMetaData queryMetaData){
        Map<String, String> refIdVsClause = new Map<String, String>();
        String whereClause = ' WHERE ' + (queryMetaData.filterLogic != null ? queryMetaData.filterLogic : '');
        SObjectType ref = ((SObject)(Type.forName('Schema.'+queryMetaData.sObjectApiName).newInstance())).getSObjectType();
        DescribeSObjectResult describe = ref.getDescribe();
        for(FilterWrapper filter : queryMetaData.filters){
            String value = '';
            Schema.DisplayType fieldType = describe.fields.getMap().get(filter.fieldName).getDescribe().getType();
            if(fieldType == Schema.DisplayType.Double || fieldType == Schema.DisplayType.Integer){
                value = '(' + String.join(filter.values, ',') + ')';
            }else{
                value = '(\'' + String.join(filter.values, '\',\'') + '\')';
            }
            String clause = filter.fieldName + ' ' + filter.operator + ' ' + value;
            refIdVsClause.put('{'+ filter.refId + '}', clause);
        }
        for(String refId : refIdVsClause.keySet()){
            whereClause = whereClause.replace(refId, refIdVsClause.get(refId));
        }
        return whereClause;
    }

    public class QueryMetaData{
        @AuraEnabled public String sObjectApiName;
        @AuraEnabled public List<FieldWrapper> fieldsToFetch;
        @AuraEnabled public List<FilterWrapper> filters;
        @AuraEnabled public String filterLogic;
        @AuraEnabled public Integer limitRecords;
        @AuraEnabled public Integer offset;
        @AuraEnabled public List<SortByWrapper> sortBy;
    }
    public class FieldWrapper implements Comparable{

        @AuraEnabled public String label;
        @AuraEnabled public String fieldName;
        @AuraEnabled public String dataType;
        @AuraEnabled public Boolean isTableColumn;
        @AuraEnabled public Integer order;
        @AuraEnabled public String redirectUrl;
        @AuraEnabled public String dateFormat;

        public Integer compareTo(Object compareTo) {
            FieldWrapper other = (FieldWrapper) compareTo;
            if (this.order > other.order){
                return 1;
            }
            else if (this.order == other.order){
                return 0;
            }
            return -1; 
        }

    }
    public class FilterWrapper{
        @AuraEnabled public String refId;
        @AuraEnabled public String fieldName;
        @AuraEnabled public List<String> values;
        @AuraEnabled public String operator;
    }
    public class SortByWrapper{
        @AuraEnabled public String fieldName;
        @AuraEnabled public String order;
    }

}