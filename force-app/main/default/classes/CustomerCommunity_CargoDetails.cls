public class CustomerCommunity_CargoDetails {
    @AuraEnabled public String containerType{get; set;}
    @AuraEnabled public String containerQuantity{get; set;}
    @AuraEnabled public CustomerCommunity_KeyValueRecord commodity{get; set;}
    @AuraEnabled public String grossWeight{get; set;}
}