trigger EquipmentBoLTrigger on Equipment_BOL__c (before insert) {
    if(Trigger.isBefore && Trigger.isInsert)
        EquipmentBoLTriggerHandler.populateFieldData(Trigger.new);
}