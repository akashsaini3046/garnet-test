trigger LeadTrigger on Lead (before insert, before update, before delete, after insert, after update) {
    new LeadTriggerHandler().run();
}