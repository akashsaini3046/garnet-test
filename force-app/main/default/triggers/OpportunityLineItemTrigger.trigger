trigger OpportunityLineItemTrigger on OpportunityLineItem (after insert, after delete) {
    new OpportunityLineItemTriggerHandler().run();
}