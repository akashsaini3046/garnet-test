({
    doInit : function(component, event, helper) {
      helper.fetchpdf(component,event);     
    },
    handleRangeChange : function(component, event, helper) {
        var sz = component.get('v.value');
        component.set('v.imgvalue', sz*6);
        var imgcmp = component.find('imagep');
        console.log(imgcmp.style);
        imgcmp.set("v.style", 'min-width:'+ component.get('v.value'));
    },
    onZoomIn: function(component, event, helper) {
        var sz = component.get('v.value');
        if(parseInt(sz)<=195){
            var size = parseInt(sz)+5;
            helper.setSizeofImage(component, event, helper,size);
        }
        else{
            helper.setSizeofImage(component, event, helper,200);
        }
    },
    onZoomOut: function(component, event, helper) {
        var sz = component.get('v.value');
        if(parseInt(sz)>=55){
            var size = parseInt(sz)-5;
            helper.setSizeofImage(component, event, helper,size);
        }
        else{
            helper.setSizeofImage(component, event, helper,50);
        }
    },
    onPagechange : function(component, event, helper) {
        var index = event.target.id;
        var pdfPublicURL = component.get("v.pdfPublicURL");
        pdfPublicURL = pdfPublicURL+ index;
        component.set("v.displayPdfPublicURL",pdfPublicURL);
        component.set("v.pageNumber",parseInt(index)+1);
        
    },
    changePageRight: function(component, event, helper) {
        var currentPageNumber = component.get("v.pageNumber");
        var pageNumbersList = component.get("v.pageNumbersList");
        if(currentPageNumber<pageNumbersList.length){
            var pdfPublicURL = component.get("v.pdfPublicURL");
            pdfPublicURL = pdfPublicURL+ currentPageNumber;
            component.set("v.displayPdfPublicURL",pdfPublicURL);
            component.set("v.pageNumber",parseInt(currentPageNumber)+1);
        }
    },
    changePageLeft: function(component, event, helper) {
        var currentPageNumber = component.get("v.pageNumber");
        if(currentPageNumber>1){
            var pdfPublicURL = component.get("v.pdfPublicURL");
            pdfPublicURL = pdfPublicURL+ (parseInt(currentPageNumber)-2);
            component.set("v.displayPdfPublicURL",pdfPublicURL);
            component.set("v.pageNumber",parseInt(currentPageNumber)-1);
        }
    },
    loadpdf : function(component, event, helper) {
        console.log('load pdf');
		//helper.loadpdf(component,event);
        $(document).ready(function () {
            var native_width = 0;
            var native_height = 0;
            $('.large').css('background', 'url('+$("#pageImgId").attr("src")+') no-repeat');
            if (!native_width && !native_height) {
                var image_object = new Image();
                image_object.src = $("#pageImgId").attr("src");
                native_width = image_object.width;
                native_height = image_object.height;
            }
            $("#close").mouseover(function (e) {
                $(".large").hide();
            });
            $(".cCustomerCommunity_PDFViewerV1").click(function (e) {
                var magnify_offset = $(this).offset();        
                var mx = e.pageX - magnify_offset.left;
                var my = e.pageY - magnify_offset.top;
                
                if (mx < $(this).width() && my < $(this).height() && mx > 0 && my > 0) {
                    $('.large').fadeIn(100);
                    console.log($('.large').fadeIn(100));
                } else {
                    $('.large').fadeOut(100);
                }
                if ($('.large').is(":visible")) {
                    var rx = Math.round(mx / $("#pageImgId").width() * native_width - $(".large").width() / 2) * -1;
                    var ry = Math.round(my / $("#pageImgId").height() * native_height - $(".large").height() / 2) * -1;
                    var bgp = rx + "px " + ry + "px";
                    var px = mx - native_width - $(".large").width() / 2;
                    var py = my - native_height - $(".large").height() / 2;
                    console.log(px);
                    console.log(py);   		
                    $('.large').css({
                        left: e.offsetX-80,
                        top: e.offsetY-50,
                        backgroundPosition: bgp
                    });
                }
            })
        })
    }
    
})