({
    searchHelper : function(component, event, getInputkeyWord) {
        var functionality = component.get("v.functionality");
        if(functionality === "BusinessLocation:Address"){
            this.findAddresses(component, event, getInputkeyWord);
        }else if(functionality === "Account:CompanyName"){
            this.findAccounts(component,event,getInputkeyWord)
        }else if(functionality === "BusinessLocation:Country"){
            this.findCountries(component,event,getInputkeyWord);   
        }else if(functionality === "BusinessLocation:State"){
            this.findStates(component,event,getInputkeyWord);        
        }else if(functionality === "linerBooking:Accounts"){
            this.findAccountsBooking(component, event, getInputkeyWord);
        }else if(functionality === "linerBooking:FromToLocations"){
            this.findFromToLocations(component, event, getInputkeyWord);
        }
    },
    findFromToLocations : function(component,event,getInputkeyWord){
        var locationTerm = component.get("v.locationTerm");
        console.log("Location Term : ",locationTerm)
        var termType = component.get("v.termType");
        var data = component.get("v.listOfSearchRecordsFromParent");
        var searchRecords = [];
        if (typeof locationTerm == "undefined" || locationTerm == '') {
            component.set("v.resultMessage", "Please select a "+(termType === "receiptTerm"? "Receipt" : "Delivery")+" Term and then enter "+(termType === "receiptTerm"? "Origin" : "Destination")+" Code");
        } else if (getInputkeyWord == '' || getInputkeyWord == null) {
            component.set("v.resultMessage", "Please enter "+(termType === "receiptTerm"? "Origin" : "Destination")+" Zip Code");
        }else if (locationTerm == 'CFS') {
            for (var i = 0; i < data.length && searchRecords.length < 5 ; i++) {
                if (data[i].Name.includes(getInputkeyWord) ) {
                    var result = {
                        name : "",
                        location : "",
                        termType : termType,
                        code : ""
                    };
                    console.log('locationCode : ' + getInputkeyWord);
                    if (data[i].Street__c != '' && data[i].Street__c != null)
                        result.location = result.location + data[i].Street__c + ' ';
                    if (data[i].Name != '' && data[i].Name != null)
                        result.location = result.location + data[i].Name + ' ';
                    if (data[i].State_Abbreviation__c != '' && data[i].State_Abbreviation__c != null)
                        result.location = result.location + data[i].State_Abbreviation__c + ' ';
                    if (data[i].Zip_Code__c != '' && data[i].Zip_Code__c != null)
                        result.location = result.location + data[i].Zip_Code__c + ' ';
                    if (data[i].Country__c != '' && data[i].Country__c != null)
                        result.location = result.location + data[i].Country__c + ' ';
                    result.name = data[i].Name;
                    result.code = data[i].Name;
                    searchRecords.push(result);
                }
            }
            console.log("searchRecords : ", searchRecords);
            component.set("v.listOfSearchRecords", searchRecords);
        } else {
            component.set("v.resultMessage", "");
            if (locationTerm == 'P')
                this.getLocationDetails(component, event, locationTerm, getInputkeyWord, termType);
            else if (locationTerm == 'D')
                this.getLocationDetails(component, event, locationTerm, getInputkeyWord, termType);
            else {
                component.set("v.resultMessage", "Entered code not found. Please re-check the "+(termType === "receiptTerm"? "Origin" : "Destination")+" Code.");
            }
        }
        component.set("v.loading", false);
    },
    getLocationDetails: function (component, event, locationTerm, getInputkeyWord, termType) {
        var searchRecords = [];
        var action = component.get("c.getLocationDetails");
        action.setParams({
            termCode: locationTerm,
            locCode: getInputkeyWord
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var data = response.getReturnValue();
                for (var i = 0; i < data.length && searchRecords.length < 5 ; i++) {
                    var result = {
                        name : "",
                        location : "",
                        termType : termType,
                        code : ""
                    };
                    console.log('locationCode : ' + getInputkeyWord);
                    if (data[i].Place_Name__c != '' && data[i].Place_Name__c != null)
                        result.location = result.location + data[i].Place_Name__c + ' ';
                    if (locationTerm === 'D') {
                        result.name = data[i].Zip_Code__c;
                        result.code = data[i].Softship_Zipcodes__c;
                        if (data[i].State__c != '' && data[i].State__c != null && data[i].Place_Name__c != data[i].State__c)
                            result.location = result.location + data[i].State_Abbreviation__c + ' ';
                    }else if (locationTerm === 'P'){
                        result.name = data[i].Location_Code__c;
                        result.code = data[i].Location_Code__c;
                    }else{
                        result.name = data[i].Place_Name__c;
                        result.code = data[i].Location_Code__c;
                    }
                    if (data[i].Country__c != '' && data[i].Country__c != null)
                        result.location = result.location + data[i].Country__c + ' ';
                    searchRecords.push(result);
                }
                console.log("searchRecords : ", searchRecords);
                component.set("v.listOfSearchRecords", searchRecords);
            } else {
                component.set("v.resultMessage", "Entered code not found. Please re-check the "+(termType === "receiptTerm"? "Origin" : "Destination")+" Code.");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action);
    },
    findAccountsBooking : function(component,event,getInputkeyWord){
        var action = component.get("c.fetchAccountsBooking");
        action.setParams({
            searchKeyWord : getInputkeyWord
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                component.set("v.resultMessage",'Result');
                if (responseData.length == 0) {
                    component.set("v.resultMessage",'No Results Found.');
                }
                component.set("v.listOfSearchRecords", responseData);
            }else if(state === "ERROR"){
                console.log("Error in fetching Accounts !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action);
    },
    findAddresses : function (component, event, getInputkeyWord){
        var action = component.get("c.fetchBusinessLocation");
        var selectedCompanyName = component.get("v.companyName");
        action.setParams({
            searchKeyWord : getInputkeyWord,
            companyName : selectedCompanyName 
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                if (responseData.length == 0) {
                    component.set("v.resultMessage",'No Results Found.');
                    component.set("v.Message", 'The Location you are searching for is unavailable please add it as a new location');
                    component.set("v.showAddDialog",true);
                } else {      
                    component.set("v.showAddDialog",false);
                    component.set("v.resultMessage",'Search Result...');
                }
                component.set("v.listOfSearchRecords", responseData);
            }else if(state === "ERROR"){
                console.log("Error in fetching Addresses !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action);
    },
    findAccounts : function(component,event,getInputkeyWord){
        var action = component.get("c.fetchAccounts");
        action.setParams({
            searchKeyWord : getInputkeyWord
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                if (responseData.length == 0) {
                    component.set("v.resultMessage",'No Results Found.');
                    component.set("v.Message", 'The Account you are searching for is unavailable please add it as a new company');
                    component.set("v.showAddDialog",true);
                } else {
                    component.set("v.resultMessage",'Search Result...');
                    component.set("v.showAddDialog",false);
                }
                component.set("v.listOfSearchRecords", responseData);
            }else if(state === "ERROR"){
                console.log("Error in fetching Accounts !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action);
    },
    findCountries : function(component,event,getInputkeyWord){
        var action = component.get("c.fetchCountries");
        action.setParams({
            searchKeyWord : getInputkeyWord.toUpperCase()
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                if(responseData.length == 0){
                    component.set("v.resultMessage", 'No Result Found');  
                }
                else{
                    component.set("v.resultMessage",'Search Result...');
                    component.set("v.listOfSearchRecords", responseData);
                }
            }else if(state === "ERROR"){
                console.log("Error in fetching Countries !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action); 
    },
    findStates : function(component,event,getInputkeyWord){
        var action = component.get("c.fetchStates");
        action.setParams({
            searchKeyWord : getInputkeyWord.toUpperCase()
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                if(responseData.length == 0){
                    component.set("v.resultMessage", 'No Result Found'); 
                }
                else{
                    component.set("v.resultMessage",'Search Result...');
                    component.set("v.listOfSearchRecords", responseData);
                }    
            }else if(state === "ERROR"){
                console.log("Error in fetching States !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action); 
    },
    setLocationDetails : function(component,event,locationName,newLocation){
        var action = component.get("c.fetchLocationDetails");
        action.setParams({
            searchKeyWord : locationName
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                this.showLocationFields(component,event,true,responseData,newLocation);
            }else if(state === "ERROR"){
                console.log("Error in Setting Details !");
            }    
        });
        $A.enqueueAction(action);     
    },
    showLocationFields : function(component,event,value,responseData,newLocation){ 
        var locationEvent = component.getEvent("DisplayLocationFields");
        locationEvent.setParams({
            "makeLocationFieldsVisible" : true,
            "makeLocationFieldReadOnly" : value,
            "addressRecord" : responseData,
            "addNewLocation" :newLocation
        });
        locationEvent.fire();
    },
    hideLocationFields : function(component,event){
        var locationEvent = component.getEvent("DisplayLocationFields");
        locationEvent.setParams({
            "makeLocationFieldsVisible" : false     
        });
        locationEvent.fire();
    },
    addAccountEvent : function(component,event,addCompany,selectedItemId, cvifId){
        var accountEvent = component.getEvent("addAccount");
        accountEvent.setParams({
            "newAccount" :  addCompany,
            "selectedItemID" : selectedItemId,
            "accountType" : component.get("v.accountType"),
            "cvifId" : component.get("v.cvifId"),
            "index" : component.get("v.index")
        });
        accountEvent.fire(); 
    },
    setTermLocationEvent : function(component, event, location, termType, selectedItemGotFromEvent, code){
        var setTermLocationEvent = component.getEvent("setTermLocation");
        setTermLocationEvent.setParams({
            "selectedCode" :  selectedItemGotFromEvent,
            "location" : location,
            "termType" : termType,
            "code" : code
        });
        setTermLocationEvent.fire(); 
    },
    toggleStateField : function(component,event,value){
        var stateEvent = component.getEvent("showState");
        stateEvent.setParams({
            "showStates" : value
        });
        stateEvent.fire();   
    }
})