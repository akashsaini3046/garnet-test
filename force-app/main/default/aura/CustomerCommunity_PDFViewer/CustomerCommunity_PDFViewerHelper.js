({
    loadpdf:function(component,event){
        try{
            var pdfData = component.get('v.pdfData');
            console.log(pdfData);
            var pdfjsframe = component.find('pdfFrame')
            if(typeof pdfData != 'undefined'){
                //pdfjsframe.getElement().contentWindow.postMessage(pdfData,'*');	
            }
        }catch(e){
            alert('Error: ' + e.message);
        }
    },
    fetchpdf:function(component,event,helper){
        var shipmentId = component.get("v.recordId");
        var action = component.get("c.getPublicURL");
        action.setParams({
            "ShippingInstructionId" :shipmentId,
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var pdfPublicUrl = response.getReturnValue();
                component.set("v.pdfPublicURL",pdfPublicUrl['publicURL']);
                if(pdfPublicUrl['publicURL'] != null)
                	this.getNumberOfPages(component,event,helper,pdfPublicUrl['Base64Data']);               
            } 
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getNumberOfPages:function(component,event,helper,base64Data){
        const byteCharacters = atob(base64Data);
        var count = byteCharacters.match(/\/Type[\s]*\/Page[^s]/g).length;
            var pageNumbersList =[];
            for(let j = 0 ; j< count ; j++ ){
                pageNumbersList[j]=j;
            }
            component.set("v.pageNumbersList",pageNumbersList);
    }
    
    
})