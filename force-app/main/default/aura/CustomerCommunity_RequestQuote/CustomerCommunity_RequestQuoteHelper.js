({
    fireHighlightEvent: function (component, event) {
        var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({
            "selectedMenu": compname
        });
        appEvent.fire();
    },
    navigateToBooking : function (component, event, helper) {
        window.open('/CrowleyCustomerCommunity/s/booking/Booking__c/Default?tabset-45306=1','_blank');
	},
    fetchDestinationPortList : function(component){
        var action = component.get("c.getDestinationPorts");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.DestinationPortList", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    }
    , FCLBtnClick: function (component) {
        component.set("v.freightType", "FCL");
        var FCLbutton = component.find("fclbtn");
        $A.util.addClass(FCLbutton, 'btn-blue');
        $A.util.removeClass(FCLbutton, 'btn-grey');
        var LCLbutton = component.find("lclbtn");
        $A.util.addClass(LCLbutton, 'btn-grey');
        $A.util.removeClass(LCLbutton, 'btn-blue');
    }
    , LCLBtnClick: function (component) {
        component.set("v.freightType", "LCL");
        var FCLbutton = component.find("fclbtn");
        $A.util.addClass(FCLbutton, 'btn-grey');
        $A.util.removeClass(FCLbutton, 'btn-blue');
        var LCLbutton = component.find("lclbtn");
        $A.util.addClass(LCLbutton, 'btn-blue');
        $A.util.removeClass(LCLbutton, 'btn-grey');
    },
     modifyPlaceholders: function (component) {
        var currentUnit = document.getElementById("measurmentUnit").checked;
        component.find("cargoVolume").set("v.value", "");
        component.find("cargoWeight").set("v.value", "");
        component.set("v.chargeableWeight", 0);
        if (currentUnit) {
            component.set("v.volumePlaceholder", "Total cubic metre");
            component.set("v.weightPlaceholder", "Total kg");
            component.set("v.weightUnit", " kg");
            component.set("v.volumeUnit", "cubic metre");
        } else {
            component.set("v.volumePlaceholder", "Total cubic feet");
            component.set("v.weightPlaceholder", "Total lb");
            component.set("v.weightUnit", " lb");
            component.set("v.volumeUnit", "cubic feet");
        }
    }, 
    modifyChargeableWeight: function (component) {
        var currentVolume = component.find("cargoVolume")
            .get("v.value");
        var currentWeight = component.find("cargoWeight")
            .get("v.value");
        var measurmentUnit = component.get("v.weightUnit");
        if (currentWeight == '') currentWeight = 0;
        if (currentVolume == '') currentVolume = 0;
        if (measurmentUnit == ' kg') {
            console.log(currentWeight);
            if ((currentVolume * 167) > currentWeight) component.set("v.chargeableWeight", currentVolume * 167);
            else component.set("v.chargeableWeight", currentWeight);
        }
        if (component.get("v.weightUnit") == " lb") {
            if (currentVolume * 5 > currentWeight * 0.5) component.set("v.chargeableWeight", currentVolume * 5);
            else component.set("v.chargeableWeight", currentWeight * 0.5);
        }
    },
    getQuotation: function(component) {
		var currentUnit = document.getElementById("measurmentUnit").checked;
        var destCountry = component.find("destinationCount").get("v.value");
        var cargoVolume = component.find("cargoVolume").get("v.value");
        var cargoWeight = component.find("cargoWeight").get("v.value");
        var origCountry = component.find("originCountry").get("v.value");
        var origPostalCode = component.find("originCode").get("v.value");
        var destinationCountry = component.find("destinationCount").get("v.value");
        var showZipMessage = component.get("v.showZipErrorMessage");
        component.set("v.destination", destinationCountry);
        console.log('origPostalCode11--->'+origPostalCode+'showZipMessage-->'+showZipMessage);
        if(origPostalCode==''){
            console.log('origPostalCode11222--->'+origPostalCode);
            component.set("v.showZipErrorMessage", true);
            component.set("v.errorMessage", 'Please Enter the Origin Zip Code'); 
        }
		else if (origPostalCode!='') {
			var keyword = component.find("originCode").get("v.value");
			var actionGetPorts = component.get("c.getPorts");
			actionGetPorts.setParams({ zipCode: keyword });
			actionGetPorts.setCallback(this, function(response) {
				var state = response.getState();
				if(state === "SUCCESS" && response.getReturnValue()!=null){  
					var portObject = response.getReturnValue();
					console.log('originCityState-->'+response.getReturnValue());
					component.set("v.originCityState",portObject.Place_Name__c+' ,'+portObject.State_Code__c);
					component.set("v.showZipErrorMessage",false);
					component.set("v.wrongZipcode",false);
					if(destinationCountry=='' || destinationCountry=='-- None --'){
						component.set("v.showErrorMessage", true);
						component.set("v.errorMessage", 'Please Select Destination'); 
					}
					else if(cargoWeight=='' && cargoVolume==''){
						component.set("v.showErrorMessage", true);
						component.set("v.errorMessage", 'Please Enter the Cargo Volume or Cargo Weight'); 
					}
					else if(cargoWeight>999999 || cargoVolume>999999){
						component.set("v.showErrorMessage", true);
						component.set("v.errorMessage", 'Please Enter the Cargo Volume/Cargo Weight less than or equal to 999999'); 
					}                    
					else{
						var actionGetRates = component.get("c.getQuotationRates");
							actionGetRates.setParams({
								destinationCountry: destCountry
								, weight: cargoWeight
								, volume: cargoVolume
								, originCountry: origCountry
								, originPostalCode: origPostalCode
								, currentUnit: currentUnit
						   
							});
							actionGetRates.setCallback(this, function (response) {
								var state = response.getState();
								console.log('state--->'+state+'body-->'+response.getReturnValue());
								if (state === "SUCCESS" && response.getReturnValue()!=null) {
									component.set("v.showErrorMessage", false);
									component.set("v.quoteData", response.getReturnValue());
								}
								else{
								   component.set("v.showErrorMessage", true);
								   component.set("v.quoteData", null);
								   component.set("v.errorMessage", 'Rates Currently not available for this Destination.');
								}
							});
						$A.enqueueAction(actionGetRates);
					}
				}
				else{
					component.set("v.fullPortsList",null); 
					component.set("v.showZipErrorMessage",true);
					component.set("v.errorMessage", 'Please Enter a valid Zip Code');
					component.set("v.wrongZipcode",true);
				}
			});
			$A.enqueueAction(actionGetPorts);
		}
},
    clearValues: function (component) {
        component.find("originCode").set("v.value","");
        component.find("cargoWeight").set("v.value","");
        component.find("cargoVolume").set("v.value","");
        component.find("originState").set("v.value","");
        component.find("destinationCount").set("v.value",'-- None --');
        component.set("v.showErrorMessage",false);
        component.set("v.showZipErrorMessage",false);
        component.set("v.quoteData",null); 
        
    },
    fetchPortsInformation : function(component,event) {
        var action = component.get("c.getPorts");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('value-->'+response.getReturnValue());
            if(response.getReturnValue()!=null){
                
                component.set("v.fullPortsList",response.getReturnValue());
                component.set("v.showErrorMessage",false);
            }
            else{
                component.set("v.fullPortsList",null); 
                component.set("v.showErrorMessage",true);
            } 
        });
        $A.enqueueAction(action);
    },
    searchCityByZipcode : function(component) {
        var keyword = component.find("originCode").get("v.value");
        console.log('keyword-->'+keyword);
        var action = component.get("c.getPorts");
         action.setParams({
            zipCode: keyword
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('value-->'+response.getReturnValue());
            if(response.getReturnValue()!=null){  
                var portObject = response.getReturnValue();
                console.log('originCityState-->'+response.getReturnValue());
                component.set("v.originCityState",portObject.Place_Name__c+' ,'+portObject.State_Code__c);
                component.set("v.showZipErrorMessage",false);
                component.set("v.wrongZipcode",false);
            }
            else{
                component.set("v.fullPortsList",null); 
                component.set("v.showZipErrorMessage",true);
                component.set("v.errorMessage", 'Please Enter a valid Zip Code');
                component.set("v.wrongZipcode",true);
            } 
        });
        $A.enqueueAction(action);
    }
})