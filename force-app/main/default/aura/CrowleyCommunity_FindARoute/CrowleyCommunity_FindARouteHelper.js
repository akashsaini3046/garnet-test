({
    //Custom labels to be preloaded
    // $Label.c.CustomerCommunity_FARErrorMessage
     fireHighlightEvent : function(component, event){
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
	},
    fetchFCLPorts : function(component){
        var action = component.get("c.getFCLPortsMap");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.portListToDisplayMap", a.getReturnValue());
                component.set("v.fclPortMap", a.getReturnValue());
            }else if(state === "ERROR"){
                console.log("Error in fetchFCLPorts !");
            } 
        });
        $A.enqueueAction(action);
    },
    fetchLCLPorts : function(component){
        var action = component.get("c.getLCLPortsMap");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.lclPortMap", a.getReturnValue());
            } else if(state === "ERROR"){
                console.log("Error in fetchLCLPorts !");
            }
        });
        $A.enqueueAction(action);
    },
    fetchSailingWeeksPicklist : function(component){
        component.set("v.sailingWeeks", null);
        var action = component.get("c.getSailingWeeks");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.sailingWeeks", a.getReturnValue());
            } else if(state === "ERROR"){
                console.log("Error in fetchSailingWeeksPicklist !");
            }
        });
        $A.enqueueAction(action);
    },
    getRouteList : function(component){
        var shipmentTypeValue = component.get("v.shipmentType");
        var originValue = component.get("v.selectedOrigin");
        var destinationValue = component.get("v.selectedDestination");        
        var sailingWeeksValue = component.find("sailingWeeksId").get("v.value");
        console.log(shipmentTypeValue+' '+originValue+' '+' '+destinationValue+' '+sailingWeeksValue);
        if(originValue == "" || destinationValue == "" || sailingWeeksValue == "" || sailingWeeksValue == "Select"){
            component.set("v.routeList",null);
            component.set("v.errorMessage",$A.get("$Label.c.CustomerCommunity_FARInputMessage"));
            component.set("v.showErrorMessage", true);
        } 
        else {
            var portsMap = component.get("v.portListToDisplayMap");
            var routeRequestWrapper={originPort:portsMap[originValue], destPort:portsMap[destinationValue], FclLclData:shipmentTypeValue, displayRoutes:sailingWeeksValue, cargoReadyDate:new Date()};
            console.log('routeRequestWrapper '+JSON.stringify(routeRequestWrapper));
            component.set('v.loading', true);
            var action = component.get("c.getRoutesTest");
            action.setParams({ 
                routeRequestWrapper : routeRequestWrapper
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                component.set("v.showErrorMessage", false);
                component.set("v.shipmentType", shipmentTypeValue);                
                component.set("v.destinationSelected", destinationValue);
                component.set("v.originSelected",originValue);
                if(response.getReturnValue()!=null && response.getReturnValue()!=''){
                    console.log('RESPONSE --> '+response.getReturnValue());
                    component.set("v.routeList",response.getReturnValue());
                    var responseRoutes = component.get("v.routeList");
                    component.set("v.originCode", responseRoutes[0].originPortAbbr);
                    component.set("v.destinationCode", responseRoutes[0].destinationPortAbbr); 
                    component.set("v.NoOfRoutes", responseRoutes[0].routeCount);                                        
                } 
                else {
                    component.set("v.errorMessage",$A.get("$Label.c.CustomerCommunity_FARErrorMessage"));
                    component.set("v.showErrorMessage", true);
                    component.set("v.routeList",null);
                }               
                this.handleSortingCriteria(component,event);
                component.set('v.loading', false);               
            });
            $A.enqueueAction(action);
        }
    },
    FCLBtnClick : function(component){
        component.set("v.routeList", null);
        component.set("v.selectedOrigin","");
        component.set("v.selectedDestination","");
        component.set("v.shipmentType", "FCL");
        component.set("v.FCLButtonVariant","Brand");
        component.set("v.LCLButtonVariant","Base");
        component.set("v.portListToDisplayMap", component.get("v.fclPortMap"));
        component.find("sailingWeeksId").set("v.value", "");
    },
    LCLBtnClick : function(component){
        component.set("v.routeList", null);
        component.set("v.selectedOrigin","");
        component.set("v.selectedDestination","");
        component.set("v.shipmentType", "LCL");
        component.set("v.FCLButtonVariant","Base");
        component.set("v.LCLButtonVariant","Brand");
        component.set("v.portListToDisplayMap", component.get("v.lclPortMap"));  
        component.find("sailingWeeksId").set("v.value", "");
    },
    changeSectionState : function(component) {
        var noOfRoutes = component.get("v.NoOfRoutes");
        var fucnRequired = component.get("v.ExpandCollapseButton");
        var collapseLabel = 'Collapse All'; var expandLabel = 'Expand All'; var i = 1;
        if(fucnRequired == collapseLabel) {
            while (i <= noOfRoutes ) {
                var bodyId = "collapseOne" + i;
                var sectionId = "collapseSection" + i;
                var bodyElement = document.getElementById(bodyId);
                var sectionElement = document.getElementById(sectionId);
                sectionElement.setAttribute('aria-expanded', false);
                $A.util.removeClass(bodyElement, 'show');
                $A.util.addClass(sectionElement, 'collapsed');
                i++;
            }
            component.set("v.ExpandCollapseButton", expandLabel);
        }
        else if(fucnRequired == expandLabel) {
            while (i <= noOfRoutes ) {
                var bodyId = "collapseOne" + i;
                var sectionId = "collapseSection" + i;
                var bodyElement = document.getElementById(bodyId);
                var sectionElement = document.getElementById(sectionId);  
                sectionElement.setAttribute('aria-expanded', true);
                $A.util.addClass(bodyElement, 'show');
                $A.util.removeClass(sectionElement, 'collapsed');
                i++;
            }
            component.set("v.ExpandCollapseButton", collapseLabel);
        }
    },

    printFullList : function(component, event) {
        var noOfRoutes = component.get("v.NoOfRoutes");
        var i = 1;
        var collapseLabel = 'Collapse All';
        while (i <= noOfRoutes ) {
            var bodyId = "collapseOne" + i;
            var sectionId = "collapseSection" + i;
            var bodyElement = document.getElementById(bodyId);
            var sectionElement = document.getElementById(sectionId);
            sectionElement.setAttribute('aria-expanded', true);
            $A.util.addClass(bodyElement, 'show');
            $A.util.removeClass(sectionElement, 'collapsed');
            i++;
        }
        component.set("v.ExpandCollapseButton", collapseLabel);
    },
    printIndividualSection : function(component, event) {
        var sectionId = event.currentTarget.id;
        var IntSectionId = parseInt(sectionId, 10);
        var noOfRoutes = component.get("v.NoOfRoutes");
        var i = 1;
        var expandLabel = 'Expand All';
        while (i <= noOfRoutes ) {
            var bodyId = "collapseOne" + i;
            var sectionId = "collapseSection" + i;
            var bodyElement = document.getElementById(bodyId);
            var sectionElement = document.getElementById(sectionId);
            console.log(i == IntSectionId);
            if(i != IntSectionId && $A.util.hasClass(bodyElement, 'show')) {
                sectionElement.setAttribute('aria-expanded', false);
                $A.util.removeClass(bodyElement, 'show');
                $A.util.addClass(sectionElement, 'collapsed');
            }
            if(i == IntSectionId && !($A.util.hasClass(bodyElement, 'show'))) {
                sectionElement.setAttribute('aria-expanded', true);
                $A.util.addClass(bodyElement, 'show');
                $A.util.removeClass(sectionElement, 'collapsed');
            }
            i++;
        }
        component.set("v.ExpandCollapseButton", expandLabel);
    },
    
    
    handleSortingCriteria : function(component, event){
        var orderList = component.get("v.routeList");
        if(orderList !=null){ 
            var criteria = component.get("v.filter");  
            console.log('criteria : '+criteria);
            orderList.sort(function(a,b){
                if(criteria=='Arriving Date'){                             
                    var criteriaA = new Date(a.arrivalDate);
                    var criteriaB = new Date(b.arrivalDate);
                }
                else if(criteria=='Starting Date'){
                    var criteriaA = new Date(a.startDate);
                    var criteriaB = new Date(b.startDate);                   
                }                                   
                else if(criteria=='Number of Stops'){
                    var criteriaA = parseInt(a.stopCount) ;
                    var criteriaB = parseInt(b.stopCount) ;         
                }                  
                else if(criteria =='Number of Days'){
                     var criteriaA = parseInt(a.transitTime);
                     var criteriaB = parseInt(b.transitTime); 
                 }
                if(criteriaA < criteriaB)
                    return -1;
                if(criteriaA > criteriaB)
                    return 1;
                return 0;  
            });                                   
                       
            const months = ["January" , "February" , "March" , "April" , "May" , "June" , "July" , "August" , "September" , "October" , "November" , "December"];
            
            for(var o in orderList){
                var start = months[(parseInt(orderList[o].startDate.substring(0,2)))-1] + ' ' +  orderList[o].startDate.substring(3,5) ;
                orderList[o].startDateExpectedFormat = start ;            
                orderList[o].arrivalDateExpectedFormat=months[(parseInt(orderList[o].arrivalDate.substring(0,2)))-1] + ' ' +  orderList[o].arrivalDate.substring(3,5) ;	         
            }
            component.set("v.routeList",orderList);
        }  
    },
    
    scrollToResultArea: function(component,event){  
        var expectedElement= document.getElementById('scrollUpId');       
        let elementHeight = Math.max(
            expectedElement.clientHeight, expectedElement.offsetHeight
        );   
        window.scrollBy(0,elementHeight);
    },
    
    handleOnRender : function(component,event){
        var routeList = component.get("v.routeList");      
        if(routeList!=null)
            this.scrollToResultArea(component,event);
    },
    
    handleComponentEvent : function(component,event,helper){
       this.getRouteList(component);      
    }
})