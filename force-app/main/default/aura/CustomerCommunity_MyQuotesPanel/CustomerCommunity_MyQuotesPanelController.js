({
    doInit : function(component, event, helper){
        var action = component.get("c.fetchCaseRecords");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.quoteRecords", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
     }
})