({	
    doInit : function(component){
        localStorage.clear();
        sessionStorage.clear();
    },
    handleOnMouseDown : function(component){
        var getSelectItem = component.get("v.item");
        console.log('selected Item '+getSelectItem);
        var compEvent = component.getEvent("selectedItemEvent");
        compEvent.setParams({
            "selectedItem" : getSelectItem 
        });
        compEvent.fire();
       // component.set("v.itemCopy",component.get("v.item"));
    }
})