({
    createBookingRecord : function(component, event) {
        var readyDateVal = component.get("v.ReadyDate");
        var recordVal = component.get("v.recordId");
        var action = component.get("c.cloneBookingRecords"); 
        action.setParams({ bookingRecordId :recordVal,
                          readyDate : readyDateVal,
                          contactRecordId : null});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.bookingRequestRecord",responseData);
                
                if(responseData.Status == "HazCheck Required")
                    this.getHazCheckerResponse(component, event);
                else
                    this.getBookingNumber(component, event);
            }
            else{
                component.set("v.PageMsg","We encounterd some error while processing your request. Please try again.");
                component.set("v.isMsgVisible",true);
                component.set("v.bookingRecordCreated", false);
            } 
        });
        $A.enqueueAction(action);
    },
    getHazCheckerResponse : function(component, event){
        var bookingRec = component.get("v.bookingRequestRecord");
        if(bookingRec != null){
            var action = component.get("c.validateHazardousBooking"); 
            action.setParams({ bookingId : bookingRec.Id});
            action.setCallback(this, function(response){
                var state = response.getState();
                if(response.getReturnValue()!=null){
                    var responseData = response.getReturnValue();
                    if(responseData == 'True'){
                        this.getBookingNumber(component, event);
                    }
                    else{
                        component.set("v.PageMsg","Hazardous Goods could not be validated at the moment. Please use following Booking Request Number for future references - " + bookingRec.Name);
                        component.set("v.isMsgVisible",true);
                		component.set("v.bookingRecordCreated", true);
                    }
                }
                else{
                    component.set("v.PageMsg","Hazardous Goods could not be validated at the moment. Please use following Booking Request Number for future references - " + bookingRec.Name);
                    component.set("v.isMsgVisible",true);
                    component.set("v.bookingRecordCreated", true);
                } 
            });
            $A.enqueueAction(action);
        }
    },
    getBookingNumber : function(component, event){
        var bookingRec = component.get("v.bookingRequestRecord");
        if(bookingRec != null){
            var action = component.get("c.getCICSBookingNumber"); 
            action.setParams({ bookingId : bookingRec.Id});
            action.setCallback(this, function(response){
                var state = response.getState();
                if(response.getReturnValue()!=null){
                    var responseData = response.getReturnValue();
                    component.set("v.bookingRequestRecord", responseData);
                    component.set("v.PageMsg","A new Booking Record has been created with Confirmed Booking Number - " + responseData.Booking_Number__c + " and Booking Request NUmber - " + responseData.Name);
                    component.set("v.isMsgVisible",true);
                    component.set("v.bookingRecordCreated", true);
                }
                else{
                    component.set("v.PageMsg","Booking could not be confirmed at the moment. Please use following Booking Request Number for future references - " + bookingRec.Name);
                    component.set("v.isMsgVisible",true);
                    component.set("v.bookingRecordCreated", true);
                } 
            });
            $A.enqueueAction(action);
        }
    }
})