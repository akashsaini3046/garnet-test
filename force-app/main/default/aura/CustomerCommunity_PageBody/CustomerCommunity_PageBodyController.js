({
    handlecloseModal: function (component, event, helper) {
        var isModalVisible = event.getParam("isModalVisible");
        if (isModalVisible === true) {
            component.set("v.isModalOpen", false);
        }
    },
    closeModalBox: function (component, event, helper) {
        component.set("v.isModalOpen", false);
    },
    openRegisterModal: function (component, event, helper) {
        component.set("v.isModalOpen", true);
    }
});