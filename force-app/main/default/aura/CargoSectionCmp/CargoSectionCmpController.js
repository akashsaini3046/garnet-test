({
    doInit : function(component, event, helper) {
        var bookingWrapperObj = component.get("v.BookingWrapperObj"); 
        console.log(bookingWrapperObj);      
        if(bookingWrapperObj.length>0){
            helper.editCargoToContainer(component, event, helper);
        }else{
            helper.addCargoToContainer(component, event, helper);
        }
        helper.fetchContainerTypeRecords(component);
        helper.columnSubstances(component);
        helper.getVehiclesData (component, event, helper);
    },
    
    addCargo : function(component, event, helper) {
        helper.addCargo(component, event, helper);
    },

    next : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.fetchRates(component, event, helper);
    },
    completeLater : function(component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction" : "completeLater"
        });
        bookingRecordIdEvent.fire();

    },
    renderCargoSection : function(component, event, helper) {
        helper.addCargoToContainer(component, event, helper);
       
    },
    addItem : function(component, event, helper) {
        helper.addItem(component, event, helper);
       
    },
    removeItem : function(component, event, helper) {
        helper.removeItem(component, event, helper);
       
    },
    openReeferModal : function(component, event, helper) {
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        console.log(id_str);        
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var reeferData = BookingWrapperObj[0].listFreightDetailWrapper[0].listRequirementWrapper[id_str].requirement;
        component.set("v.reeferData",reeferData);
        component.set("v.showReeferModal",true);
       
    },
    closeReeferModal : function(component, event, helper) {    
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        console.log(BookingWrapperObj);    
        component.set("v.showReeferModal",false);
       
    },
    openHazardousModal : function(component, event, helper) {
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var listCommodityWrapper = BookingWrapperObj[0].listFreightDetailWrapper[0].listCommodityWrapper.push({commodity:{}});
        console.log(listCommodityWrapper);
        var hazardousData = BookingWrapperObj[0].listFreightDetailWrapper[0].listCommodityWrapper[listCommodityWrapper-1].commodity;
        //console.log(hazardousData);
        component.set("v.hazardousData",hazardousData);
        component.set("v.showIMDGModal",true);
       
    },
    closeHazardousModal : function(component, event, helper) {
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var listCommodityWrapper = BookingWrapperObj[0].listFreightDetailWrapper[0].listCommodityWrapper;
        listCommodityWrapper.splice(listCommodityWrapper.length-1, 1);
        console.log(BookingWrapperObj);    
        component.set("v.showIMDGModal",false);
       
    },
    saveAndCloseHazardousModal : function(component, event, helper) {
        //var BookingWrapperObj = component.get("v.BookingWrapperObj");
        //var hazardousData = component.get("v.hazardousData");
        //BookingWrapperObj[0].listFreightDetailWrapper[0].listCommodityWrapper[0] = hazardousData;
        //console.log(BookingWrapperObj); 
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        console.log(BookingWrapperObj); 
        component.set("v.BookingWrapperObj",BookingWrapperObj);  
        component.set("v.showIMDGModal",false);
       
    }, 

    openOutOfGaugeModal : function(component, event, helper) {
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        console.log(id_str);
        component.set("v.showOutofGaugeModal",true);
       
    },
    closeOutOfGaugeModal : function(component, event, helper) {        
        component.set("v.showOutofGaugeModal",false);
       
    },
    handleUploadFinished : function(component, event, helper) {        
       
    },
    editCommodity : function(component, event, helper) {
        var selectedItem = event.currentTarget; // Get the target object
        var index = selectedItem.dataset.record; // Get its value i.e. the index
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var listCommodityWrapper = BookingWrapperObj[0].listFreightDetailWrapper[0].listCommodityWrapper;
        var hazardousData = listCommodityWrapper[index].commodity;
        component.set("v.hazardousData",hazardousData);
        component.set("v.showIMDGModal",true);
    },
    deleteCommodity : function(component, event, helper) {
        var selectedItem = event.currentTarget; // Get the target object
        var index = selectedItem.dataset.record; // Get its value i.e. the index
        console.log(index);
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var listCommodityWrapper = BookingWrapperObj[0].listFreightDetailWrapper[0].listCommodityWrapper;
        listCommodityWrapper.splice(index, 1);
        component.set("v.BookingWrapperObj",BookingWrapperObj);
    },
    openSubstancesModal : function(component, event, helper) {
        component.set("v.showSubstancesModal",true)
    },
    closeSubstancesModal : function(component, event, helper) {
        component.set("v.showSubstancesModal",false)
    },
    updateRowSelection : function(component, event, helper) {
        var selectedRows = event.getParam("selectedRows");
        var hazardousData = component.get("v.hazardousData");
        hazardousData.Prefix__c = selectedRows[0].Prefix__c;
        hazardousData.Suffix__c = selectedRows[0].Suffix__c;
        hazardousData.Number__c = selectedRows[0].Name;
        hazardousData.IMO_Class__c = selectedRows[0].Primary_Class__c;
        hazardousData.Primary_Class__c = selectedRows[0].Primary_Class__c;        
        hazardousData.Package_Group__c = selectedRows[0].Packing_Group__c;
        hazardousData.Technical_Name__c = selectedRows[0].Substance_Name__c;
        hazardousData.Secondary_Class__c = selectedRows[0].Secondary_Class__c;
        hazardousData.Is_Marine_Pollutant__c = (selectedRows[0].Marine_Pollutant__c=='Yes');
        hazardousData.Is_Hazardous__c = true;
        component.set("v.hazardousData",hazardousData);
        component.set("v.showSubstancesModal",false)
    },
    searchUNSubstances : function(component, event, helper){
        component.set("v.imdgSpinner", true);
        helper.searchUNSubstances(component, event);
    },

    getModel : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var selectedId = event.getSource().get("v.value");
        helper.getModel(component, event, selectedId, index);
    },
    populateVehicleDetail : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var selectedId = event.getSource().get("v.value");
        helper.populateVehicleDetail(component, event, selectedId, index);
    },

    populateContainer : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var selectedId = event.getSource().get("v.value");
        helper.populateContainerData(component, event, selectedId, index);
    },
    hazardous : function(component, event, helper){
        var bookingWrapper = component.get("v.BookingWrapper");
        bookingWrapper.booking.Is_Hazardous__c = !component.get("v.IsHazardous");
        component.set("v.BookingWrapper",bookingWrapper);        
    },
        
})