({
    doInit : function(component, event, helper) {
        var bookingWrapper = component.get("v.BookingWrapper");
        component.find("containerbtn").set("v.checked",bookingWrapper.shipmentMap.CONTAINER.isSelected);
        component.find("rorobtn").set("v.checked",bookingWrapper.shipmentMap.RORO.isSelected);
        component.find("breakbulkbtn").set("v.checked",bookingWrapper.shipmentMap.BREAKBULK.isSelected);       
        //bookingWrapper.shipmentMap.CONTAINER.isSelected;
    },
    
    addCargo : function(component, event, helper) {
        helper.addCargo(component, event, helper);
    },

    next : function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.fetchRates(component, event, helper);
    },
    completeLater : function(component, event, helper) {
        var bookingRecordIdEvent = component.getEvent("bookingEventData");
        bookingRecordIdEvent.setParams({
            "selectedAction" : "completeLater"
        });
        bookingRecordIdEvent.fire();
    },
    getToggleContainer : function(component,event,helper){
        var bookingWrapper = component.get("v.BookingWrapper");
        var checkCmp = component.find("containerbtn").get("v.checked");
        if(helper.checkCargoType(component)){
            bookingWrapper.shipmentMap.CONTAINER.isSelected = checkCmp;
            component.set("v.BookingWrapper",bookingWrapper);
            if(checkCmp){
                component.find("tabs").set("v.selectedTabId", "1");
            }else{
                var currentTab = component.find("tabs").get("v.selectedTabId");
                helper.selectTab(component, currentTab, bookingWrapper);
            }
            
        }else{
            component.find("containerbtn").set("v.checked",true);
        }
        
    },
    getToggleRORO : function(component,event,helper){
        var bookingWrapper = component.get("v.BookingWrapper");
        var checkCmp = component.find("rorobtn").get("v.checked");
        if(helper.checkCargoType(component)){
            bookingWrapper.shipmentMap.RORO.isSelected = checkCmp;
            component.set("v.BookingWrapper",bookingWrapper);
            if(checkCmp){
                component.find("tabs").set("v.selectedTabId", "2");
            }else{
                var currentTab = component.find("tabs").get("v.selectedTabId");
                helper.selectTab(component, currentTab, bookingWrapper);
            }
            
        }else{
            component.find("rorobtn").set("v.checked",true);
        }
    },
    getToggleBreaakbulk : function(component,event,helper){
        var bookingWrapper = component.get("v.BookingWrapper");
        var checkCmp = component.find("breakbulkbtn").get("v.checked");
        if(helper.checkCargoType(component)){
            bookingWrapper.shipmentMap.BREAKBULK.isSelected = checkCmp;
            component.set("v.BookingWrapper",bookingWrapper);
            if(checkCmp){
                component.find("tabs").set("v.selectedTabId", "3");
            }else{
                var currentTab = component.find("tabs").get("v.selectedTabId");
                helper.selectTab(component, currentTab, bookingWrapper);
            }

        }else{
            component.find("breakbulkbtn").set("v.checked",true);
        }
    },

    
})