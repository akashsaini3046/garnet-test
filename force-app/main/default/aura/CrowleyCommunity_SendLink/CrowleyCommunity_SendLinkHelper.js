({
	resendLink : function(component,event) {   
        var action = component.get("c.sentRegistrationEmail");
        action.setParams({
            "username": component.get("v.userEmail")
        });
        action.setCallback(this, function (a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                if (a.getReturnValue() == "True") { 
                    component.set("v.linkMessage", "A link has been sent to Again to your Email. Please verify for the next step.");
                    component.set("v.showLinkMessage", true);
                } else if (a.getReturnValue() == "False") { 
                    component.set("v.linkMessage", "Please check your username. If you still face issues, contact your Crowley administrator");
                    component.set("v.showLinkMessage", true);
                } else if (a.getReturnValue() == "Error") {
                    component.set("v.showLinkMessage", "Something went wrong. Please contact Crowley Administrator.");
                    component.set("v.linkMessage", true);
                }
            } else {
                component.set("v.showLinkMessage", "Something went wrong. Please contact Crowley Administrator.");
                component.set("v.linkMessage", true);
            }
        });
        $A.enqueueAction(action);
   }
})