({
    handleTest1: function (component, event, helper) {
        var action = component.get("c.handleTest11");
        action.setCallback(this, function(a) {
            var state = a.getState();
            console.log('aaa1--->'+ a.getReturnValue());
            if (state === "SUCCESS"){
                console.log('SUCCESS');
            }else{
                console.log('ERROR');
            }
        });
        $A.enqueueAction(action);
    },
    handleTest2: function (component, event, helper) {
        var action = component.get("c.handleTest22");
        action.setCallback(this, function(a) {
            var state = a.getState();
            console.log('aaa2--->'+ a.getReturnValue());
            if (state === "SUCCESS"){
                console.log(a.getReturnValue().Id);
                console.log('SUCCESS');
            }else{
                console.log('ERROR');
            }
        });
        $A.enqueueAction(action);
    },
  	fetchFCLPorts : function(component){
        var action = component.get("c.getFCLPorts");
        action.setCallback(this, function(a) {
            var state = a.getState();
            console.log('aaa--->'+ a.getReturnValue());
            if (state === "SUCCESS"){
                component.set("v.portListToDisplay", a.getReturnValue());
                component.set("v.fclPortList", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    
    fetchLCLPorts : function(component){
        var action = component.get("c.getLCLPorts");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.lclPortList", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    
    fetchSailingWeeksPicklist : function(component){
        var action = component.get("c.getSailingWeeks");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.sailingWeeks", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    fireHighlightEvent : function(component, event){
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
	},
    getRouteList : function(component){
        var shipmentTypeValue = component.get("v.shipmentType");
        var originValue = component.find("originId").get("v.value");
        var destinationValue = component.find("destinationId").get("v.value");
        var sailingWeeksValue = component.find("sailingWeeksId").get("v.value");
        
        if(originValue == "" || destinationValue == "" || sailingWeeksValue == "" || originValue == "--Select--" || destinationValue == "--Select--" || sailingWeeksValue == "--Select--"){
            component.set("v.routeList",null);
            component.set("v.errorMessage",$A.get("$Label.c.CustomerCommunity_FARInputMessage"));
            component.set("v.showErrorMessage", true);
        }
        else {
            var action = component.get("c.getRoutes");
            action.setParams({ originPort :originValue,destinationPort:destinationValue,
                              shipmentType:shipmentTypeValue,sailingWeeks:sailingWeeksValue});
            action.setCallback(this, function(response){
                var state = response.getState();
                component.set("v.showErrorMessage", false);
                component.set("v.originSelected", originValue);
                component.set("v.destinationSelected", destinationValue);
                
                if(response.getReturnValue()!=null){
                    component.set("v.routeList",response.getReturnValue());
                    component.set("v.showErrorMessage", false);
                    
                    var responseRoutes = component.get("v.routeList");
                    component.set("v.originCode", responseRoutes[0].originPortAbbr);
                    component.set("v.destinationCode", responseRoutes[0].destinationPortAbbr); 
                    component.set("v.NoOfRoutes", responseRoutes[0].routeCount);
                }
                else{
                    component.set("v.errorMessage",$A.get("$Label.c.CustomerCommunity_FARErrorMessage"));
                    component.set("v.showErrorMessage", true);
                    component.set("v.routeList",null);
                } 
            });
            $A.enqueueAction(action);
        }
    },
    changeSectionState : function(component) {
        var noOfRoutes = component.get("v.NoOfRoutes");
        var fucnRequired = component.get("v.ExpandCollapseButton");
        var collapseLabel = 'Collapse All'; var expandLabel = 'Expand All'; var i = 1;
        if(fucnRequired == collapseLabel) {
            while (i <= noOfRoutes ) {
                var bodyId = "collapseOne" + i;
                var sectionId = "collapseSection" + i;
                var bodyElement = document.getElementById(bodyId);
                var sectionElement = document.getElementById(sectionId);
                sectionElement.setAttribute('aria-expanded', false);
                $A.util.removeClass(bodyElement, 'show');
                $A.util.addClass(sectionElement, 'collapsed');
                i++;
            }
            component.set("v.ExpandCollapseButton", expandLabel);
        }
        else if(fucnRequired == expandLabel) {
            while (i <= noOfRoutes ) {
                var bodyId = "collapseOne" + i;
                var sectionId = "collapseSection" + i;
                var bodyElement = document.getElementById(bodyId);
                var sectionElement = document.getElementById(sectionId);  
                sectionElement.setAttribute('aria-expanded', true);
                $A.util.addClass(bodyElement, 'show');
                $A.util.removeClass(sectionElement, 'collapsed');
                i++;
            }
            component.set("v.ExpandCollapseButton", collapseLabel);
        }
    },
    printIndividualSection : function(component, event) {
    	var sectionId = event.currentTarget.id;
        var IntSectionId = parseInt(sectionId, 10);
    	var noOfRoutes = component.get("v.NoOfRoutes");
        var i = 1;
        var expandLabel = 'Expand All';
        while (i <= noOfRoutes ) {
            var bodyId = "collapseOne" + i;
            var sectionId = "collapseSection" + i;
            var bodyElement = document.getElementById(bodyId);
            var sectionElement = document.getElementById(sectionId);
            console.log(i == IntSectionId);
            if(i != IntSectionId && $A.util.hasClass(bodyElement, 'show')) {
                sectionElement.setAttribute('aria-expanded', false);
                $A.util.removeClass(bodyElement, 'show');
                $A.util.addClass(sectionElement, 'collapsed');
            }
            if(i == IntSectionId && !($A.util.hasClass(bodyElement, 'show'))) {
                sectionElement.setAttribute('aria-expanded', true);
                $A.util.addClass(bodyElement, 'show');
                $A.util.removeClass(sectionElement, 'collapsed');
            }
            i++;
        }
        component.set("v.ExpandCollapseButton", expandLabel);
	},
    clearFormValues : function(component, event) {
        var originList = component.get("v.originPort");
        var destinationList = component.get("v.destinationPort");
        var weekList = component.get("v.sailingWeeks");
        component.find("originId").set("v.value","");
        component.find("destinationId").set("v.value","");
        component.find("sailingWeeksId").set("v.value","");
        component.set("v.originPort","");
        component.set("v.originPort",originList);
        component.set("v.destinationPort","");
        component.set("v.destinationPort",destinationList);
        component.set("v.sailingWeeks","");
        component.set("v.sailingWeeks",weekList);
        component.set("v.shipmentType", "FCL"); 
        component.set("v.routeList","");  
        component.set("v.showErrorMessage",false);
    },
    FCLBtnClick : function(component){
        component.set("v.shipmentType", "FCL");
        component.set("v.portListToDisplay", component.get("v.fclPortList"));
        var FCLbutton = component.find("fclbtn");
        $A.util.addClass(FCLbutton, 'btn-blue');
        $A.util.removeClass(FCLbutton, 'btn-grey');
        var LCLbutton = component.find("lclbtn");
        $A.util.addClass(LCLbutton, 'btn-grey');
        $A.util.removeClass(LCLbutton, 'btn-blue');
    },
    LCLBtnClick : function(component){
        console.log('list-->'+component.get("v.lclPortList"));
        component.set("v.shipmentType", "LCL");
        component.set("v.portListToDisplay", component.get("v.lclPortList"));
        var FCLbutton = component.find("fclbtn");
        $A.util.addClass(FCLbutton, 'btn-grey');
        $A.util.removeClass(FCLbutton, 'btn-blue');
        var LCLbutton = component.find("lclbtn");
        $A.util.addClass(LCLbutton, 'btn-blue');
        $A.util.removeClass(LCLbutton, 'btn-grey');
    },
    printFullList : function(component, event) {
        var noOfRoutes = component.get("v.NoOfRoutes");
        var i = 1;
        var collapseLabel = 'Collapse All';
        while (i <= noOfRoutes ) {
            var bodyId = "collapseOne" + i;
            var sectionId = "collapseSection" + i;
            var bodyElement = document.getElementById(bodyId);
            var sectionElement = document.getElementById(sectionId);
            sectionElement.setAttribute('aria-expanded', true);
            $A.util.addClass(bodyElement, 'show');
            $A.util.removeClass(sectionElement, 'collapsed');
            i++;
        }
        component.set("v.ExpandCollapseButton", collapseLabel);
    }
})