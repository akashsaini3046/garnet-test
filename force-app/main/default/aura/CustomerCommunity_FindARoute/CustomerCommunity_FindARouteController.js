({
	doInit : function(component, event, helper) {
        component.find("sailingWeeksId").set("v.value","2 Weeks");
        helper.fireHighlightEvent(component, event);
        helper.fetchFCLPorts(component);
        helper.fetchLCLPorts(component);
        helper.fetchSailingWeeksPicklist(component);
	},
    findRoutes : function(component, event,helper) {
        helper.getRouteList(component);
    },
    handleFCLClick : function(component, event, helper) {
        helper.FCLBtnClick(component);
    },
    handleLCLClick : function(component, event, helper) {
        helper.LCLBtnClick(component);
    },
    ExpandCollapseSection : function(component, event, helper) {
        helper.changeSectionState(component);
    },
    clearForm : function(component, event, helper) {
        helper.clearFormValues(component, event);
    },
    PrintFullSchedule : function(component, event, helper) {
        helper.printFullList(component, event);
        window.print();
    },
    PrintSection : function (component, event, helper) {
        helper.printIndividualSection(component, event);
        window.print();
    },
    handleTest1: function (component, event, helper) {
        helper.handleTest1(component, event, helper);
    },
    handleTest2: function (component, event, helper) {
        helper.handleTest2(component, event, helper);
    }
})