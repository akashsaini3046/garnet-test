({
    createUserRecord : function(component,event) {
        this.sendRegistrationLink(component,event);     
        /*var action = component.get("c.saveUserDetail");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseServer = response.getReturnValue();
                
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);*/
        
    },
    sendRegistrationLink : function(component,event){
        var navService = component.find("navService");
        var pageReference = {
            type: "standard__webPage",
            attributes: {
                url: "https://comdev-crowleydev.cs77.force.com/CrowleyMobile/s/sendlink"
            }
        };
        navService.navigate(pageReference);   
    },
    getPicklistValues: function (component, event) {
        var action = component.get("c.getCountries");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseServer = response.getReturnValue();
                component.set("v.CountriesList", responseServer);
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    }
})