({ 
	handleActive: function (cmp, event) {
        var tab = event.getSource();
        var tabName = tab.get('v.id');
        if(tabName.includes("tab-new-booking")){
            this.injectComponent(cmp, 'c:CustomerCommunity_NewBookings', tab);
        }else if(tabName.includes("tab-fcl-booking")){            
            this.injectComponent(cmp, 'c:LinerBookingSelection', tab);
        }else if (tabName.includes("tab-all-bookings")) {
            this.injectComponent(cmp, 'c:CustomerCommunity_AllBookings', tab);
        } else if (tabName.includes("tab-view-details")) {
            this.injectComponent(cmp, 'c:CustomerCommunity_BookingDetail', tab);
        }
    },
    
    injectComponent: function (cmp, name, target) {
        var attr = {};
        if(name === 'c:CustomerCommunity_BookingDetail'){
            attr = {
                bookingId : target.get('v.id').slice(17)  
            };
        }
        $A.createComponent(name, attr, function (contentComponent, status, error) {
            console.log(JSON.stringify(contentComponent));
            if (status === "SUCCESS") {
                target.set('v.body', contentComponent);
            } else {
                $A.createComponent(name, attr, function (contentComponent, status, error) {
                    console.log(JSON.stringify(contentComponent));
                    if (status === "SUCCESS") {
                        target.set('v.body', contentComponent);
                    } else {
                        throw new Error(error);
                    }
                });
                throw new Error(error);
            }
        });
    }
})