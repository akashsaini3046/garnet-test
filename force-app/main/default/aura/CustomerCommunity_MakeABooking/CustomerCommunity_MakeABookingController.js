({ 
	handleActive: function (cmp, event, helper) {
        var sourceValue = event.getSource();
        console.log(sourceValue);
        helper.handleActive(cmp, event);
    },
    
    handleComponentEvent : function(cmp, event) {
        var bookingId = event.getParam("bookingId");
        var bookingName = event.getParam("bookingName");
        var selectedTabId = event.getParam("selectedTabId");
        var detail = cmp.get("v.moretabs");
        var newlst =[];
        var bookingOpenIdsVsName = new Map();
        if(cmp.get("v.bookingOpenIdsVsName") != null){
            bookingOpenIdsVsName = cmp.get("v.bookingOpenIdsVsName");
        }
        var alreadyOpened = false;
        for(var key of bookingOpenIdsVsName.keys()){
            if(key === bookingId){
                alreadyOpened = true;
            }
        }
        if(alreadyOpened){
            cmp.find("tabs").set("v.selectedTabId", "tab-view-details-"+ bookingId);
        } else {
            bookingOpenIdsVsName.set(bookingId, bookingName);
            cmp.set("v.moretabs", []);
            cmp.set("v.bookingOpenIdsVsName", bookingOpenIdsVsName);
            for(var key of bookingOpenIdsVsName.keys()){
                $A.createComponent("lightning:tab", {
                    "label": "Booking ("+bookingOpenIdsVsName.get(key)+")",
                    "id": "tab-view-details-"+key,
                    "onactive": cmp.getReference("c.handleActive")
                }, function (newTab, status, error) {
                    if (status === "SUCCESS") {
                        newlst.push(newTab);
                        cmp.set("v.moretabs", newlst);
                        if(key === bookingId){
                            cmp.set("v.bookingId", key);
                            cmp.find("tabs").set("v.selectedTabId", "tab-view-details-"+ key);
                        }
                    } else {
                        throw new Error(error);
                    }
                });
            }
        }
    }
})