({
	getAllSubstances : function(component, event, helper) {
		var action = component.get("c.getAllSubstancesRecord");
        action.setCallback(this, function(response) {
            console.log(response.getState())
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log(response.getReturnValue())
                component.set("v.mapSubstances",allValues);
                var optionObj = component.get("v.options");
                for (var key in allValues){
                    optionObj.push(allValues[key]);
                }
                component.set("v.options",optionObj); 
            }
        });
        $A.enqueueAction(action);
	}
})