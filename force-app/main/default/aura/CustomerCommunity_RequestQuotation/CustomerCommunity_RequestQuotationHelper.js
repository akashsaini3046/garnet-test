({
    fireHighlightEvent: function (component, event) {
        var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({
            "selectedMenu": compname
        });
        appEvent.fire();
    },
    clearValues: function (component, event) {
        component.set("v.quoteDataToDisplay", null);
        component.set("v.showErrorMessage", false);
        component.set("v.chargeLineItems", null);
        component.set("v.fclRatingResponse", null);        
        component.set("v.showCargoerror", false);
    },
    fetchDestinationPortList : function(component){
        var action = component.get("c.getDestinationPorts");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS"){
                component.set("v.DestinationPortList", a.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    fetchCommodityRecords : function(component){
        var action = component.get("c.fetchCommodityList");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.CommodityPicklist",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    fetchContainerTypeRecords : function(component){
        var action = component.get("c.fetchContainerTypeList");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.ContainerList",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    formatDate : function(component){
        var currentDate = new Date();
        var minDate = $A.localizationService.formatDate(currentDate.setDate(currentDate.getDate()), "YYYY-MM-DD");
        var maxDate = $A.localizationService.formatDate(currentDate.setDate(currentDate.getDate()+365), "YYYY-MM-DD");
        component.set('v.minDate', minDate);
        component.set("v.maxDate", maxDate);
    },
    createBlankQuotation : function(component, event){ 
        var action = component.get("c.createBlankQuotationRecord");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.quoteRequest",responseData);
                this.fetchContactDetails(component, event);
            } 
        });
        $A.enqueueAction(action);
    },
    FCLBtnClick: function (component) {
        component.set("v.freightType", "FCL");
        component.set("v.quoteRequest.ContainerMode", "FCL");
        var FCLbutton = component.find("fclbtn");
        $A.util.addClass(FCLbutton, 'btn-blue');
        $A.util.removeClass(FCLbutton, 'btn-grey');
        var LCLbutton = component.find("lclbtn");
        $A.util.addClass(LCLbutton, 'btn-grey');
        $A.util.removeClass(LCLbutton, 'btn-blue');
    },
    LCLBtnClick: function (component) {
        component.set("v.freightType", "LCL");
        component.set("v.quoteRequest.ContainerMode", "LCL");
        var FCLbutton = component.find("fclbtn");
        $A.util.addClass(FCLbutton, 'btn-grey');
        $A.util.removeClass(FCLbutton, 'btn-blue');
        var LCLbutton = component.find("lclbtn");
        $A.util.addClass(LCLbutton, 'btn-blue');
        $A.util.removeClass(LCLbutton, 'btn-grey');
    },
    modifyPlaceholders: function (component) {
        var currentUnit = document.getElementById("measurmentUnit").checked;
        //component.find("cargoVolume").set("v.value", "");
        //component.find("cargoWeight").set("v.value", "");
        component.set("v.chargeableWeight", 0);
        if (currentUnit) {
            component.set("v.volumePlaceholder", "cubic metre");
            component.set("v.weightPlaceholder", "kg");
            component.set("v.weightUnit", "KG");
            component.set("v.volumeUnit", "M3");
            
        } else {
            component.set("v.volumePlaceholder", "cubic feet");
            component.set("v.weightPlaceholder", "lb");
            component.set("v.weightUnit", "LB");
            component.set("v.volumeUnit", "CF");
        }
    }, 
    modifyChargeableWeight: function (component) {
        var currentVolume = component.find("cargoVolume").get("v.value");
        var currentWeight = component.find("cargoWeight").get("v.value");
        var measurmentUnit = component.get("v.weightUnit");
        if (currentWeight == '') currentWeight = 0;
        if (currentVolume == '') currentVolume = 0;
        if (measurmentUnit == ' kg') {
            console.log(currentWeight);
            if ((currentVolume * 167) > currentWeight) component.set("v.chargeableWeight", currentVolume * 167);
            else component.set("v.chargeableWeight", currentWeight);
        }
        if (component.get("v.weightUnit") == " lb") {
            if (currentVolume * 5 > currentWeight * 0.5) component.set("v.chargeableWeight", currentVolume * 5);
            else component.set("v.chargeableWeight", currentWeight * 0.5);
        }
    },
    checkValidations : function(component){
        
        var validationArray = [];  
        var receiptTerm = component.find('ReceiptTerm');
        var readyDate = component.find('ReadyDate');
        var deliveryTerm = component.find('DeliveryTerm'); 
        var cargoVolume = component.find('cargoVolume');
        var cargoWeight = component.find('cargoWeight');
        
        
        var totalDimensionCheck = component.get("v.totalDimensions");
        var packageDetailCheck = component.get("v.packageDetails");
        var ArrayValid,packageDetails,FormValPart  
        
        validationArray.push(receiptTerm);
        validationArray.push(readyDate);
        validationArray.push(deliveryTerm);
        
        
        if(totalDimensionCheck == true){  
            validationArray.push(cargoVolume);
            validationArray.push(cargoWeight);
        }
        
        ArrayValid = validationArray.reduce(function (validFields, inputCmp) {	  
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);  
        
        FormValPart = component.find('FormValuePart').reduce(function (validFields, inputCmp) {	  
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        
        if(packageDetailCheck == true){
            FormValPart = component.find('FormVal').reduce(function (validFields, inputCmp) {	  
                inputCmp.showHelpMessageIfInvalid();
                return validFields && inputCmp.get('v.validity').valid;
            }, true);			
        }
        if(ArrayValid && FormValPart)
            return true;
        else
            return false; 
    },
    getQuotationFCL : function(component){
        console.log("inhelperfcllllll--->");
        var isFormValid = this.checkValidationsFCL(component);
        if(isFormValid){
            component.set("v.hasResults", true);
            console.log("isFormValidfclllll--->"+isFormValid);
            var quoteRequestValues = component.get("v.quoteRequest");
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var quoteRequestValuesString=JSON.stringify(quoteRequestValues);
            var createSoftshipRequest = component.get("c.sendSoftShipRequestFCL"); 
            createSoftshipRequest.setParams({quoteWrapperString:quoteRequestValuesString,cargodetailList:cargoDetailsValues});
            createSoftshipRequest.setCallback(this, function(response) {
                var state = response.getState();
                console.log("statefclll--->"+state+'resssfclll--->'+response.getReturnValue());
                if (state === "SUCCESS" && response.getReturnValue()!=null && response.getReturnValue()["resultOld"]) 
                {  
                    component.set("v.hasResults", false);
                    console.log("fclresposneeee--->"+JSON.stringify(response.getReturnValue()));
                    var res=response.getReturnValue();
                    component.set("v.fclRatingResponse", response.getReturnValue());
                    if(typeof res["resultOld"] !== "undefined" && typeof res["resultOld"][0]["CalculatedContributionResult"] !== null){
                        component.set("v.lineItems", res.resultOld[0].CalculatedContributionResult.ItemValues[0].ValuesDataRevenue[0].ValuesGroup[0].DocValuesData);
                    }
                    console.log(component.get("v.lineItems"));
                }
                else{
                    
                    component.set("v.fclRatingResponse",null);
                    component.set("v.hasResults", false);
                    component.set("v.showCargoerror", true);
                    component.set("v.CargoerrorMsg","Something went Wrong.Please try Again");
                }
            });
            $A.enqueueAction(createSoftshipRequest);
        }
    },
    checkValidationsFCL : function(component){
        
        var validationArray = [];  
        var receiptTerm = component.find('ReceiptTerm');
        var readyDate = component.find('ReadyDate');
        var deliveryTerm = component.find('DeliveryTerm');    
        
        var ArrayValid,FormValPart  
        
        validationArray.push(receiptTerm);
        validationArray.push(readyDate);
        validationArray.push(deliveryTerm);
        
        ArrayValid = validationArray.reduce(function (validFields, inputCmp) {	  
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);  
        
        FormValPart = component.find('FormValPart').reduce(function (validFields, inputCmp) {	  
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        
        if(ArrayValid && FormValPart)
            return true;
        else
            return false; 
    },
    
    createQuoteRecord : function(component){
        var freightType = component.get("v.freightType");
        if(freightType=='LCL'){
            var isFormValid = this.checkValidations(component);
            if(isFormValid){
                console.log("createQuoteRecord isFormValid--->"+isFormValid);
                component.set("v.quoteRequest.ContainerMode",component.get("v.freightType"));
                var cargoDetailsValues = component.get("v.cargoDetailsValues");
                var quoteRequestValues = component.get("v.quoteRequest");
                var totalDimensions = component.get("v.totalDimensions");
                var packageDetails = component.get("v.packageDetails");
                var currentUnit = document.getElementById("measurmentUnit").checked;
                component.set("v.chargeableWeight", 0);
                if (currentUnit) {
                    quoteRequestValues.totalWeightUnit = 'KG';
                    quoteRequestValues.totalVolumeUnit = 'M3';
                    
                } else {
                    quoteRequestValues.totalWeightUnit = 'LB';
                    quoteRequestValues.totalVolumeUnit = 'CF'; 
                }
                
                if(component.get("v.totalDimensions")){
                    var currentVolume = component.find("cargoVolume").get("v.value");
                    var currentWeight = component.find("cargoWeight").get("v.value");
                    if(currentVolume != null)
                        quoteRequestValues.totalVolume = currentVolume;
                    if(currentWeight != null)
                        quoteRequestValues.totalWeight = currentWeight;
                }
                else{
                    quoteRequestValues.totalVolume = 0;
                    quoteRequestValues.totalWeight = 0;
                }
                var cargoDetailsValuesString=JSON.stringify(cargoDetailsValues);
                var quoteRequestValuesString=JSON.stringify(quoteRequestValues);
                var createQuoteRequest = component.get("c.createQuoteRecord");
                createQuoteRequest.setParams({
                    quoteWrapperString : quoteRequestValuesString,
                    cargodetailList : cargoDetailsValues,
                    freightType : freightType,
                    totalDimensions : totalDimensions,
                    packageDetails : packageDetails,
                    reQuoteId : component.get("v.reQuoteRecordId")
                });
                createQuoteRequest.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log("state--->"+state+'resss--->'+response.getReturnValue());
                    if (state === "SUCCESS" && response.getReturnValue()!=null) 
                    {  
                        component.set("v.newQuoteRecord", response.getReturnValue());
                    }
                    else if(state === 'ERROR'){
                        component.set("v.showCargoerror", true);
                        component.set("v.CargoerrorMsg","Something went Wrong while saving the Quote .Please try Again");
                    }
                    
                });
                $A.enqueueAction(createQuoteRequest);
            }
        }else if (freightType=='FCL'){
            var isFormValid = this.checkValidationsFCL(component);
            if(isFormValid){
                var quoteRequestValues = component.get("v.quoteRequest");
                var cargoDetailsValues = component.get("v.cargoDetailsValues");
                var totalDimensions = component.get("v.totalDimensions");
                var packageDetails = component.get("v.packageDetails");
                var currentUnit = document.getElementById("measurmentUnit").checked;
                component.set("v.chargeableWeight", 0);
                if (currentUnit) {
                    quoteRequestValues.totalWeightUnit = 'KG';
                    quoteRequestValues.totalVolumeUnit = 'M3';
                } else {
                    quoteRequestValues.totalWeightUnit = 'LB';
                    quoteRequestValues.totalVolumeUnit = 'CF'; 
                }
                quoteRequestValues.totalVolume = 0;
                quoteRequestValues.totalWeight = 0;
                var quoteRequestValuesString = JSON.stringify(quoteRequestValues);
                var createQuoteRequest = component.get("c.createQuoteRecord");
                console.log(quoteRequestValuesString);
                console.log(JSON.stringify(component.get("v.cargoDetailsValues")));
                createQuoteRequest.setParams({
                    quoteWrapperString : quoteRequestValuesString,
                    cargodetailList : cargoDetailsValues,
                    freightType : freightType,
                    totalDimensions : false,
                    packageDetails : false,
                    reQuoteId : component.get("v.reQuoteRecordId")
                });
                createQuoteRequest.setCallback(this, function(response) {
                    var state = response.getState();
                    console.log("state--->"+state+'resss--->'+response.getReturnValue());
                    if (state === "SUCCESS" && response.getReturnValue()!=null) 
                    {  
                        component.set("v.newQuoteRecord", response.getReturnValue());
                    }
                    else if(state === 'ERROR'){
                        component.set("v.showCargoerror", true);
                        component.set("v.CargoerrorMsg","Something went Wrong while saving the Quote .Please try Again");
                    }
                    
                });
                $A.enqueueAction(createQuoteRequest);
            }
        }
	},
    getQuotationCFStoCFS : function(component){
        console.log("inhelper--->");
        var isFormValid = this.checkValidations(component); 
        if(isFormValid){
            console.log("isFormValid--->"+isFormValid);
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var quoteRequestValues = component.get("v.quoteRequest");
            var currentUnit = document.getElementById("measurmentUnit").checked;
            component.set("v.chargeableWeight", 0);
            if (currentUnit) {
                quoteRequestValues.totalWeightUnit = 'KG';
                quoteRequestValues.totalVolumeUnit = 'M3';
                
            } else {
                quoteRequestValues.totalWeightUnit = 'LB';
                quoteRequestValues.totalVolumeUnit = 'CF'; 
            }
            
            if(component.get("v.totalDimensions")){
                var currentVolume = component.find("cargoVolume").get("v.value");
                var currentWeight = component.find("cargoWeight").get("v.value");
                if(currentVolume != null)
                    quoteRequestValues.totalVolume = currentVolume;
                if(currentWeight != null)
                    quoteRequestValues.totalWeight = currentWeight;
            }
            else{
                quoteRequestValues.totalVolume = 0;
                quoteRequestValues.totalWeight = 0;
            }
            var cargoDetailsValuesString=JSON.stringify(cargoDetailsValues);
            var quoteRequestValuesString=JSON.stringify(quoteRequestValues);
            var recTerm = component.find("ReceiptTerm").get("v.value");
            var deliveryTerm = component.find("DeliveryTerm").get("v.value");
            var createCargoWiseRequest = component.get("c.sendCargoWiseRequest"); 
            component.set("v.hasResults", true);
            component.set("v.chargeLineItems", null);
            component.set("v.showCargoerror",false);
            createCargoWiseRequest.setParams({quoteWrapperString:quoteRequestValuesString,cargodetailList:cargoDetailsValues});
            createCargoWiseRequest.setCallback(this, function(response) {
                var state = response.getState();
                console.log("state--->"+state+'resss--->'+response.getReturnValue());
                if (state === "SUCCESS" && response.getReturnValue()!=null) 
                {  
                    console.log("success--->");
                    var newItems=[];
                    var chargeLineString = '';  
                    var responseVal = response.getReturnValue();
                    component.set("v.chargeLineBody", responseVal);
                    console.log("From server: " + responseVal);
                    if(responseVal.ChargeLine!=null){
                        for(var i = 0; i < responseVal.ChargeLine.length; i++){
                            var dataString = responseVal.ChargeLine[i].ChargeCodeDesc + '-' + responseVal.ChargeLine[i].SellOSAmount;
                            console.log("dataString--->"+dataString+"respp-->"+responseVal.ChargeLine[i].ChargeCodeDesc);
                            if(!dataString.includes('Error')){
                                newItems.push(dataString);
                            }	
                        }
                        
                        for(var i=0;i<newItems.length;i++){
                            chargeLineString = chargeLineString + newItems[i] + ';';  
                        }
                        component.set("v.chargeLineItems", newItems);
                        component.set("v.showCargoerror", false);
                        component.set("v.chargeLineStr",chargeLineString);
                        component.set("v.hasResults", false);
                    }
                    
                    else if(recTerm=='CFS' && deliveryTerm=='CFS'){
                        component.set("v.hasResults", false);
                        component.set("v.showCargoerror", true);
                        component.set("v.CargoerrorMsg","No Rates found between the locations");
                        component.set("v.chargeLineItems", null);
                    }
                    if(recTerm=='D' && deliveryTerm=='CFS'){
                        console.log('indoorrrrrrtocfssssssloves-->');
                        this.getQuotationDoorToCFS(component);
                        component.set("v.chargeLineItems", null);
                    }
                    
                }
                else if(state === 'ERROR'){
                    component.set("v.hasResults", false);
                    component.set("v.showCargoerror", true);
                    component.set("v.CargoerrorMsg","Something went Wrong.Please try Again");
                    component.set("v.chargeLineItems", null);     
                }
                
            });
            $A.enqueueAction(createCargoWiseRequest);
        }
    },
    
    getQuotationCFStoCFSClone : function(component){
        console.log("inhelper--->");
        var isFormValid = this.checkValidations(component); 
        if(isFormValid){
            console.log(component.get("v.freightType")+"isFormValid--->"+isFormValid);
            component.set("v.quoteRequest.ContainerMode",component.get("v.freightType"));
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var quoteRequestValues = component.get("v.quoteRequest");
            var currentUnit = document.getElementById("measurmentUnit").checked;
            component.set("v.chargeableWeight", 0);
            if (currentUnit) {
                quoteRequestValues.totalWeightUnit = 'KG';
                quoteRequestValues.totalVolumeUnit = 'M3';
                
            } else {
                quoteRequestValues.totalWeightUnit = 'LB';
                quoteRequestValues.totalVolumeUnit = 'CF'; 
            }
            
            if(component.get("v.totalDimensions")){
                var currentVolume = component.find("cargoVolume").get("v.value");
                var currentWeight = component.find("cargoWeight").get("v.value");
                if(currentVolume != null)
                    quoteRequestValues.totalVolume = currentVolume;
                if(currentWeight != null)
                    quoteRequestValues.totalWeight = currentWeight;
            }
            else{
                quoteRequestValues.totalVolume = 0;
                quoteRequestValues.totalWeight = 0;
            }
            var cargoDetailsValuesString=JSON.stringify(cargoDetailsValues);
            var quoteRequestValuesString=JSON.stringify(quoteRequestValues);
            var recTerm = component.find("ReceiptTerm").get("v.value");
            var deliveryTerm = component.find("DeliveryTerm").get("v.value");
            var createCargoWiseRequest = component.get("c.sendCargoWiseRequest"); 
            component.set("v.hasResults", true);
            component.set("v.chargeLineItems", null);
            component.set("v.showCargoerror",false);
            createCargoWiseRequest.setParams({quoteWrapperString:quoteRequestValuesString,
                                              cargodetailList:cargoDetailsValues});
            createCargoWiseRequest.setCallback(this, function(response) {
                var state = response.getState();
                console.log("state--->"+state+'resss--->'+response.getReturnValue());
                if (state === "SUCCESS" && response.getReturnValue()!=null) 
                {  
                    console.log("success--->");
                    var newItems=[];
                    var chargeLineString = '';  
                    var responseVal = response.getReturnValue();
                    component.set("v.chargeLineResults", responseVal);
                    //component.set("v.chargeLineBody", responseVal);
                    console.log("From server: " + responseVal);
                    if(responseVal.length){
                        for(var j= 0; j < responseVal.length; j++){
                            console.log("From server: " + responseVal[j]); 
                            if(responseVal[j].ChargeLine!=null){
                                component.set("v.chargeLineBody", responseVal[j]);
                                for(var i = 0; i < responseVal[j].ChargeLine.length; i++){
                                    var dataString = responseVal[j].ChargeLine[i].ChargeCodeDesc + '-' + responseVal[j].ChargeLine[i].SellOSAmount;
                                    console.log("dataString--->"+dataString+"respp-->"+responseVal[j].ChargeLine[i].ChargeCodeDesc);
                                    if(!dataString.includes('Error')){
                                        newItems.push(dataString);
                                    }	
                                }                                
                            }
                            for(var i=0;i<newItems.length;i++){
                                chargeLineString = chargeLineString + newItems[i] + ';';  
                            }
                            component.set("v.chargeLineItems", newItems);
                            component.set("v.showCargoerror", false);
                            component.set("v.chargeLineStr",chargeLineString);
                            component.set("v.hasResults", false);
                        }
                        
                        if(recTerm=='CFS' && deliveryTerm=='CFS' && newItems.length==0){
                            component.set("v.hasResults", false);
                            component.set("v.showCargoerror", true);
                            component.set("v.CargoerrorMsg","No Rates found between the locations");
                            component.set("v.chargeLineItems", null);
                        }
                        if(recTerm=='D' && deliveryTerm=='CFS'){
                            console.log('indoorrrrrrtocfssssssloves-->');
                            //this.getQuotationDoorToCFS(component);
                            this.getQuotationDoorToCFSRates(component);
                            component.set("v.chargeLineItems", null);
                            
                        }
                    }
                }
                else if(state === 'ERROR'){
                    component.set("v.hasResults", false);
                    component.set("v.showCargoerror", true);
                    component.set("v.CargoerrorMsg","Something went Wrong.Please try Again");
                    component.set("v.chargeLineItems", null);     
                }
                
            });
            $A.enqueueAction(createCargoWiseRequest);
        }
    },
    saveRateOnQuote : function(component, event){
        var buttonValue = event.getSource().get("v.value");
        if(buttonValue === 'FCLRate'){
            buttonValue = 0;
        }
        var lfsRatesResponse = [];
        var recTerm = component.find("ReceiptTerm").get("v.value");
        var deliveryTerm = component.find("DeliveryTerm").get("v.value");
        var freightType = component.get("v.freightType");
        if(freightType === 'LCL' && recTerm === 'D' && deliveryTerm === 'CFS'){ 
            for(var i = 0; i < component.get("v.quoteDataToDisplay").length; i++){
                lfsRatesResponse.push(component.get("v.quoteDataToDisplay")[i].Body);
            }
        }else if(freightType === 'LCL' && recTerm === 'CFS' && deliveryTerm === 'CFS'){
            var body = component.get("v.chargeLineBody");
            lfsRatesResponse.push(body);
        }
        var action = component.get("c.saveQuoteRates");
        var fclRatingResponse = component.get("v.fclRatingResponse");
        console.log("fclRatingResponse : " + JSON.stringify(component.get("v.fclRatingResponse")));
        console.log(buttonValue);
        console.log(JSON.stringify(lfsRatesResponse));
        action.setParams({
            fclRatingResponseString : JSON.stringify(component.get("v.fclRatingResponse")),
            lfsRatesBody: lfsRatesResponse,
            receiptTerm : recTerm,
            deliveryTerm : deliveryTerm,
            freightType : freightType,
            rateId : buttonValue,
            quote : component.get("v.newQuoteRecord"),
            isInitiatedForBooking : false
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue()!=null){
                event.getSource().set("v.label", "Saved");
                event.getSource().set("v.disabled", true);
                event.getSource().set("v.class", "search-form-btn font-weight-bold button-disable");
                console.log("Rates saved " + response.getReturnValue());
            }
            else if(state === 'ERROR'){
                console.log("Error occured while saving the rates");
            }else if(response.getReturnValue() == null){
                console.log("Error occured !!");
            }
        });
        $A.enqueueAction(action);
    },
    getQuotationDoorToCFSRates: function(component) {      
        var isFormValid = this.checkValidations(component); 
        if(isFormValid){
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var quoteRequestValues = component.get("v.quoteRequest");
            if(component.get("v.totalDimensions")){
                var currentVolume = component.find("cargoVolume").get("v.value");
                var currentWeight = component.find("cargoWeight").get("v.value");
                if(currentVolume != null)
                    quoteRequestValues.totalVolume = currentVolume;
                if(currentWeight != null)
                    quoteRequestValues.totalWeight = currentWeight;
            }
            else{
                quoteRequestValues.totalVolume = 0;
                quoteRequestValues.totalWeight = 0;
            }
            var cargoDetailsValuesString=JSON.stringify(cargoDetailsValues);
            var recTerm = component.find("ReceiptTerm").get("v.value");
            var deliveryTerm = component.find("DeliveryTerm").get("v.value"); 
            component.set("v.quoteDataToDisplay",null);
            component.set("v.showErrorMessage", false);
            component.set("v.hasResults", true); 
            var quoteRequestValues = component.get("v.quoteRequest");
            var quoteStringData=JSON.stringify(quoteRequestValues);
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var chargeLineBodyValue =component.get("v.chargeLineBody");
            console.log('chargeLineBodyValue--->'+chargeLineBodyValue);			
            var actionGetRates = component.get("c.getAllCarrierRates");
            actionGetRates.setParams({listOfDisplayedQuotes: component.get("v.quoteData"),
                                      quoteString:quoteStringData,
                                      cargodetailList:cargoDetailsValues,
                                      chargeLineValue:component.get("v.chargeLineBody"),
                                      listChargeLine:component.get("v.chargeLineResults")});
            actionGetRates.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && response.getReturnValue()!=null) 
                {
                    var newItems=[];
                    var responseVal = response.getReturnValue();
                    component.set("v.quoteData", responseVal);
                    console.log("From server: " + JSON.stringify(responseVal));
                    for(var i = 0; i < responseVal.length; i++){
                        var dataString=JSON.stringify(responseVal[i]);
                        console.log("dataString--->"+dataString+"respp-->"+response[i]);
                        if(!dataString.includes('Error')){
                            console.log('@@@ - Test going on');
                            newItems.push(responseVal[i]);
                        }	
                    }
                    newItems.sort(function (a, b) {
                        console.log('@@@ TotalRate - ' + b.Body.TotalRate);
                        return a.Body.TotalRate - b.Body.TotalRate;
                    });
                    console.log('newItems1111--->'+newItems);
                    component.set("v.quoteDataToDisplay", newItems);
                    //console.log('newItems1111--->'+newItems);
                    component.set("v.showErrorMessage", false);
                    component.set("v.hasResults", false);
                    component.set("v.hasMoreResults", false);
                }
                else{
                    component.set("v.showErrorMessage", true);
                    component.set("v.errorMessage","Something went Wrong Please try Again."); 
                }
                if(state === 'ERROR'){
                    component.set("v.showErrorMessage", true);
                    component.set("v.errorMessage","Something went Wrong Please try Again.");  
                }        
            });
            $A.enqueueAction(actionGetRates);
        }
    }, 
    getDoorToCFSRates: function(component) {      
        var isFormValid = this.checkValidations(component); 
        if(isFormValid){
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var quoteRequestValues = component.get("v.quoteRequest");
            if(component.get("v.totalDimensions")){
                var currentVolume = component.find("cargoVolume").get("v.value");
                var currentWeight = component.find("cargoWeight").get("v.value");
                if(currentVolume != null)
                    quoteRequestValues.totalVolume = currentVolume;
                if(currentWeight != null)
                    quoteRequestValues.totalWeight = currentWeight;
            }
            else{
                quoteRequestValues.totalVolume = 0;
                quoteRequestValues.totalWeight = 0;
            }
            var cargoDetailsValuesString=JSON.stringify(cargoDetailsValues);
            component.set("v.quoteDataToDisplay",null);
            component.set("v.showErrorMessage", false);
            component.set("v.hasResults", true); 
            var quoteRequestValues = component.get("v.quoteRequest");
            var quoteString=JSON.stringify(quoteRequestValues);
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var chargeLineBodyValue =component.get("v.chargeLineBody");
            console.log('chargeLineBodyValue--->'+chargeLineBodyValue);			
            var actionGetRates = component.get("c.createLFSCWRequest");
            actionGetRates.setParams({quoteWrapper:quoteRequestValues,
                                      quoteStringData:quoteString,
                                      cargoWrapper:cargoDetailsValuesString});
            actionGetRates.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && response.getReturnValue()!=null) 
                {
                    var newItems=[];
                    var responseVal = response.getReturnValue();
                    component.set("v.quoteData", responseVal);
                    console.log("From server: " + responseVal);
                    for(var i = 0; i < responseVal.length; i++){
                        var dataString=JSON.stringify(responseVal[i]);
                        console.log("dataString--->"+dataString+"respp-->"+response[i]);
                        if(!dataString.includes('Error')){
                            console.log('@@@ - Test going on');
                            newItems.push(responseVal[i]);
                        }	
                    }
                    newItems.sort(function (a, b) {
                        console.log('@@@ TotalRate - ' + b.Body.TotalRate);
                        return a.Body.TotalRate - b.Body.TotalRate;
                    });
                    console.log('newItems1111--->'+newItems);
                    component.set("v.quoteDataToDisplay", newItems);
                    //console.log('newItems1111--->'+newItems);
                    component.set("v.showErrorMessage", false);
                    component.set("v.hasResults", false);
                    component.set("v.hasMoreResults", false);
                }
                else{
                    component.set("v.showErrorMessage", true);
                    component.set("v.errorMessage","Something went Wrong Please try Again."); 
                }
                if(state === 'ERROR'){
                    component.set("v.showErrorMessage", true);
                    component.set("v.errorMessage","Something went Wrong Please try Again.");  
                }        
            });
            $A.enqueueAction(actionGetRates);
        }
    },
    getQuotationDoorToCFS: function(component) {      
        var isFormValid = this.checkValidations(component); 
        if(isFormValid){
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var quoteRequestValues = component.get("v.quoteRequest");
            if(component.get("v.totalDimensions")){
                var currentVolume = component.find("cargoVolume").get("v.value");
                var currentWeight = component.find("cargoWeight").get("v.value");
                if(currentVolume != null)
                    quoteRequestValues.totalVolume = currentVolume;
                if(currentWeight != null)
                    quoteRequestValues.totalWeight = currentWeight;
            }
            else{
                quoteRequestValues.totalVolume = 0;
                quoteRequestValues.totalWeight = 0;
            }
            var cargoDetailsValuesString=JSON.stringify(cargoDetailsValues);
            var quoteStringData=JSON.stringify(quoteRequestValues);
            var recTerm = component.find("ReceiptTerm").get("v.value");
            var deliveryTerm = component.find("DeliveryTerm").get("v.value"); 
            console.log('cargoDetailsValues-->'+cargoDetailsValues);
            console.log('quoteRequestValues-->'+quoteRequestValues);
            console.log('cargoDetailsValuesString-->'+cargoDetailsValuesString);
            console.log('recTerm-->'+recTerm);
            console.log('deliveryTerm-->'+deliveryTerm);
            console.log('deliveryTerm-->'+component.get("v.chargeLineBody"));
            component.set("v.quoteDataToDisplay",null);    
            var actionGetCarriers = component.get("c.getCarriers");
            actionGetCarriers.setCallback(this, function (response) {
                var state = response.getState();
                console.log('state--->'+state+'body-->'+response.getReturnValue());
                if (state === "SUCCESS" && response.getReturnValue()!=null) {
                    component.set("v.showErrorMessage", false);
                    component.set("v.hasResults", true); 
                    component.set("v.carrierData", response.getReturnValue());
                    var quoteRequestValues = component.get("v.quoteRequest");
                    var quoteStringData=JSON.stringify(quoteRequestValues);
                    var cargoDetailsValues = component.get("v.cargoDetailsValues");
                    var chargeLineBodyValue =component.get("v.chargeLineBody");
                    console.log('chargeLineBodyValue--->'+chargeLineBodyValue);
                    var actionGetRates = component.get("c.getCarrierRates");
                    actionGetRates.setParams({listOfAllCarrierCodes : component.get("v.carrierData") ,listOfDisplayedQuotes: component.get("v.quoteData"),quoteString:quoteStringData,cargodetailList:cargoDetailsValues,chargeLineValue:component.get("v.chargeLineBody")});
                    actionGetRates.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS" && response.getReturnValue()!=null) 
                        {
                            var newItems=[];
                            var responseVal = response.getReturnValue();
                            component.set("v.quoteData", responseVal);
                            console.log("From server: " + responseVal);
                            for(var i = 0; i < responseVal.length; i++){
                                var dataString=JSON.stringify(responseVal[i]);
                                console.log("dataString--->"+dataString+"respp-->"+response[i]);
                                if(!dataString.includes('Error')){
                                    console.log('@@@ - Test going on');
                                    newItems.push(responseVal[i]);
                                }	
                            }
                            newItems.sort(function (a, b) {
                                console.log('@@@ Rate - ' + b.Body.Rate);
                                return a.Body.Rate - b.Body.Rate;
                            });
                            console.log('newItems1111--->'+newItems);
                            component.set("v.quoteDataToDisplay", newItems);
                            //console.log('newItems1111--->'+newItems);
                            component.set("v.showErrorMessage", false);
                            
                            var responseCarrier=component.get("v.carrierData")
                            console.log("responseVal---->" + responseVal.length+"responseCarrier--->"+responseCarrier.length);
                            if(responseVal.length<responseCarrier.length){
                                if(newItems.length>0){
                                    component.set("v.hasResults", false);
                                    component.set("v.hasMoreResults", true);   
                                }
                                
                                console.log("callinggetMoreQuotations---->");
                                this.getMoreQuotations(component);		
                            }
                            else{
                                component.set("v.hasResults", false);
                                component.set("v.hasMoreResults", false);
                            }
                        }
                        else{
                            component.set("v.showErrorMessage", true);
                            component.set("v.errorMessage","Something went Wrong Please try Again."); 
                        }
                        if(state === 'ERROR'){
                            component.set("v.showErrorMessage", true);
                            component.set("v.errorMessage","Something went Wrong Please try Again.");  
                        }        
                    });
                    $A.enqueueAction(actionGetRates);
                }
            });
            $A.enqueueAction(actionGetCarriers);
        }
    },
    getMoreQuotations: function(component) {
        console.log("inmoreeee---->");
        var cargoDetailsValues = component.get("v.cargoDetailsValues");
        var quoteRequestValues = component.get("v.quoteRequest");
        var quoteStringData=JSON.stringify(quoteRequestValues);
        var actionGetRates = component.get("c.getCarrierRates");
        actionGetRates.setParams({ listOfAllCarrierCodes : component.get("v.carrierData") ,listOfDisplayedQuotes: component.get("v.quoteData"),
                                  quoteString:quoteStringData,cargodetailList:cargoDetailsValues,chargeLineValue:component.get("v.chargeLineBody")});
        
        //actionGetRates.setParams({ listOfAllCarrierCodes : component.get("v.carrierData") ,listOfDisplayedQuotes: component.get("v.quoteData")});
        actionGetRates.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue()!=null) 
            {
                var newItems=[];
                var responseVal = response.getReturnValue();
                console.log("From server moree: " + responseVal+'length-->'+responseVal.length);
                for(var i = 0; i < responseVal.length; i++){
                    var dataString=JSON.stringify(responseVal[i]);
                    console.log("dataString--->"+dataString+"respp-->"+response[i]);
                    if(!dataString.includes('Error')){
                        newItems.push(responseVal[i]);
                    }	
                }
                newItems.sort(function (a, b) {
                    console.log('@@@ Rate - ' + b.Body.Rate);
                    return a.Body.Rate - b.Body.Rate;
                });
                if(newItems.length>0){
                    component.set("v.hasResults", false);
                    component.set("v.hasMoreResults", true);   
                }
                console.log('newItems1111--->'+newItems);
                component.set("v.quoteData", response.getReturnValue());
                component.set("v.quoteDataToDisplay", newItems);
                component.set("v.showErrorMessage", false);
                var quoteData=component.get("v.quoteData");	
                var AllCodes=component.get("v.carrierData");
                console.log('quoteDatamoreee--->'+quoteData.length);
                if(quoteData.length<AllCodes.length){
                    console.log('quoteDisplayed--->'+quoteData.length);
                    this.getMoreQuotations(component);	  
                }
                if(quoteData.length==AllCodes.length){
                    console.log('quoteDisplayedequallll--->'+quoteData.length);
                    component.set("v.hasResults", false);
                    component.set("v.hasMoreResults", false);	  
                }
            }
            else{
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage","Something went Wrong Please try Again."); 
            }       
        });
        $A.enqueueAction(actionGetRates);	
    }, 
    getQuotationDuplicate: function(component) {
        var isFormValid = this.checkValidations(component); 
        if(isFormValid){
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var quoteRequestValues = component.get("v.quoteRequest");
            var cargoDetailsValuesString=JSON.stringify(cargoDetailsValues);
            var quoteRequestValuesString=JSON.stringify(quoteRequestValues);
            var recTerm = component.find("ReceiptTerm").get("v.value");
            var deliveryTerm = component.find("DeliveryTerm").get("v.value"); 
            console.log('cargoDetailsValues-->'+cargoDetailsValues);
            console.log('quoteRequestValues-->'+quoteRequestValues);
            console.log('cargoDetailsValuesString-->'+cargoDetailsValuesString);
            console.log('quoteRequestValuesString-->'+quoteRequestValuesString);
            console.log('recTerm-->'+recTerm);
            console.log('deliveryTerm-->'+deliveryTerm);
            component.set("v.showErrorMessage", false);
            component.set("v.hasResults", true); 
            var actionGetRates = component.get("c.getCarrierRates");
            actionGetRates.setParams({ listOfAllCarrierCodes : component.get("v.carrierData") ,listOfDisplayedQuotes: component.get("v.quoteData"),
                                      quoteWrapper:quoteRequestValues,cargodetailList:cargoDetailsValues});
            actionGetRates.setCallback(this, function(response) {
            }	
                                      )
            $A.enqueueAction(actionGetRates);
            
        }
    },
    getQuotationDetails: function(component, currentCarrier) {
        console.log("inmoreeee---->");
        component.set("v.hasMoreResults", true);
        
        var actionGetRates = component.get("c.getCarrierRatesDuplicate");
        actionGetRates.setParams({ carrierCode : currentCarrier});
        actionGetRates.setCallback(this, function(response) {
            alert('incalback---->');
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue()!=null){ 
                component.set("v.hasResults", false);
                var listCarrierCode = component.get("v.carrierData");
                var listQuotesResponse = component.get("v.quoteData");
                console.log("From server moree: " + response.getReturnValue());
                listQuotesResponse.push(response.getReturnValue());
                if(listQuotesResponse.length == listCarrierCode.length-1)
                    component.set("v.hasMoreResults", false); 
                component.set("v.quoteData", listQuotesResponse);
                component.set("v.showErrorMessage", false);
                console.log('Rate length - ' + listQuotesResponse.length);
            }
            else{
                component.set("v.showErrorMessage", true);
            }
        });
        $A.enqueueAction(actionGetRates);
    },
    clearValues: function (component) {
        /*component.find("cargoQuantity").set("v.value","");
        component.find("cargoType").set("v.value","--None--");
        component.find("cargoTotalWeight").set("v.value","");
        component.find("cargoLength").set("v.value","");
        component.find("cargoWidth").set("v.value","");
        component.find("cargoHeight").set("v.value","");
        component.find("hazmat").set("v.value",false);
        component.find("totalVolume").set("v.value",false); 
        component.find("ReceiptTerm").set("v.value",'-- None --');
        component.find("DeliveryTerm").set("v.value",'-- None --');
        component.set("v.destinationCode",null);
        component.set("v.destinationCode",null);
        component.set("v.destinationLocation",null);
        component.set("v.showErrorMessage",false);
        component.find("originCode").set("v.value","");
        component.find("destinationCode").set("v.value","");*/
        component.set("v.quoteDataToDisplay",null); 
        
        component.set("v.OriginLocation",null); 
        component.set("v.destinationLocation",null); 
        
        
        component.set("v.originCodePlaceholder","Select Receipt term");
        component.set("v.destinationCodePlaceholder","Select Delivery term");
        
        component.set("v.disableOriginCode",true);
        component.set("v.disableDestinationCode",true);
        
        
        component.set("v.originPlaceholder","Select Receipt Term and then enter Origin Code");
        component.set("v.destinationPlaceholder","Select Delivery Term and then enter Destination Code"); 
        
        component.set("v.quoteData",null); 
        component.set("v.fclRatingResponse",null);
        
        component.set("v.quoteRequest.originCode",null);
        component.set("v.quoteRequest.destinationCode",null);
        component.find("ReceiptTerm").set("v.value",'-- None --');
        component.find("DeliveryTerm").set("v.value",'-- None --');
        component.find("ReadyDate").set("v.value","");
        
        component.set("v.showCargoerror", false);
        var newArray = []; 
        var cargoArray = component.get("v.cargoDetailsValues");
        console.log('cargoArray-->'+cargoArray);
        for(var i = 0; i < cargoArray.length; i++){
            console.log('forrrr-->'+cargoArray);
            cargoArray[i].sequenceId=i;
            cargoArray[i].quantity=null;
            cargoArray[i].type="--None--";
            cargoArray[i].totalWeight=null;
            cargoArray[i].length=null;
            cargoArray[i].width=null;
            cargoArray[i].height=null;
            cargoArray[i].totalVolume=null;
            cargoArray[i].isHazardous=false;
            cargoArray[i].unNumber=null;
            newArray.push(cargoArray[i]);
        }	
        
        component.set("v.cargoDetailsValues",newArray);
        component.set("v.chargeLineItems",null);
    },
    saveRateAndNavigateBooking : function (component, event) {
        var buttonValue = event.getSource().get("v.value");
        if(buttonValue === 'FCLRate'){
            buttonValue = 0;
        }
        var lfsRatesResponse = [];
        var recTerm = component.find("ReceiptTerm").get("v.value");
        var deliveryTerm = component.find("DeliveryTerm").get("v.value");
        var freightType = component.get("v.freightType");
        if(freightType === 'LCL' && recTerm === 'D' && deliveryTerm === 'CFS'){ 
            for(var i = 0; i < component.get("v.quoteDataToDisplay").length; i++){
                lfsRatesResponse.push(component.get("v.quoteDataToDisplay")[i].Body);
            }
        }else if(freightType === 'LCL' && recTerm === 'CFS' && deliveryTerm === 'CFS'){
            var body = component.get("v.chargeLineBody");
            lfsRatesResponse.push(body);
        }
        var action = component.get("c.saveQuoteRates");
        var fclRatingResponse = component.get("v.fclRatingResponse");
        action.setParams({
            fclRatingResponseString : JSON.stringify(fclRatingResponse),
            lfsRatesBody: lfsRatesResponse,
            receiptTerm : recTerm,
            deliveryTerm : deliveryTerm,
            freightType : freightType,
            rateId : buttonValue,
            quote : component.get("v.newQuoteRecord"),
            isInitiatedForBooking : true
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue()!=null){
                console.log("Rates Booked " + response.getReturnValue());
                this.navigateToBooking(component, event, response.getReturnValue());
            }
            else if(state === 'ERROR'){
                console.log("Error occured while saving the rates");
            }
        });
        $A.enqueueAction(action);
    },
    navigateToBooking : function (component, event, quoteRateId) {
        /*var cargoDetails = component.get("v.cargoDetailsValues");
        component.set("v.quoteRequest.commodityRecords", cargoDetails);
        var quoteRequestValues = component.get("v.quoteRequest");
        console.log(component.get("v.cargoDetailsValues"));
        var quoteRequestValuesString = JSON.stringify(quoteRequestValues);
        var currentUserId = $A.get("$SObjectType.CurrentUser.Id");
        var action = component.get("c.createCacheRecord");
        console.log("quoteRequestValuesString : "+quoteRequestValuesString);*/
        var quoteRequestValues = component.get("v.quoteRequest");
        quoteRequestValues.quoteRecord = component.get("v.newQuoteRecord");
        quoteRequestValues.cargodetailWrapperList = component.get("v.cargoDetailsValues");
        var quoteRequestValuesString = JSON.stringify(quoteRequestValues);
        var currentUserId = $A.get("$SObjectType.CurrentUser.Id");
        var action = component.get("c.createCacheRecord");
        console.log("quoteRequestValuesString : "+quoteRequestValuesString);
        action.setParams({
            quoteWrapperString:quoteRequestValuesString,
            userId : currentUserId,
            quoteRateId : quoteRateId
        }); 
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue() != null && response.getReturnValue() == 'TRUE'){
                console.log('Successfully stored in cache!');  
                component.set("v.quoteRequest.commodityRecords", null);
                window.open('/CrowleyCustomerCommunity/s/booking/Booking__c/Default?tabset-45306=1&param='+'book','_blank');  
            }
        });
        $A.enqueueAction(action);
    },
    fetchPortsInformation : function(component,event) {
        var action = component.get("c.getPorts");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('value-->'+response.getReturnValue());
            if(response.getReturnValue()!=null){
                
                component.set("v.fullPortsList",response.getReturnValue());
                component.set("v.showErrorMessage",false);
            }
            else{
                component.set("v.fullPortsList",null); 
                component.set("v.showErrorMessage",true);
            } 
        });
        $A.enqueueAction(action);
    },
    searchCityByZipcode : function(component) {
        var keyword = component.find("originCode").get("v.value");
        console.log('keyword-->'+keyword);
        var action = component.get("c.getPorts");
        action.setParams({
            zipCode: keyword
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('value-->'+response.getReturnValue());
            if(response.getReturnValue()!=null){  
                var portObject = response.getReturnValue();
                console.log('originCityState-->'+response.getReturnValue());
                component.set("v.originCityState",portObject.Place_Name__c+' ,'+portObject.State_Code__c);
                component.set("v.showZipErrorMessage",false);
                component.set("v.wrongZipcode",false);
            }
            else{
                component.set("v.fullPortsList",null); 
                component.set("v.showZipErrorMessage",true);
                component.set("v.errorMessage", 'Please Enter a valid Zip Code');
                component.set("v.wrongZipcode",true);
            } 
        });
        $A.enqueueAction(action);
    },
    fetchReceiptDeliveryTerms : function(component){
        var action = component.get("c.fetchReceiptDeliveryTermsList");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.ReceiptDeliveryTermsList",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    createBlankCargoDetail : function(component){ 
        var cargoRec = component.get("v.cargoDetailsValues"); 
        var action = component.get("c.createBlankCargoRecord");
        console.log('reqArrLimit11-->'+cargoRec.length);
        action.setParams({ cargoRecordList :cargoRec});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                console.log('responseData-->'+responseData.length);
                component.set("v.cargoDetailsValues",responseData);
            }
        });
        $A.enqueueAction(action);
    },
    getOriginLocationDetails : function(component, event){
        var recTerm = component.find("ReceiptTerm").get("v.value");  
        var originCode = component.get("v.quoteRequest.originCode");
        console.log('recTerm-->'+recTerm+'originCode-->'+originCode);
        var action = component.get("c.getLocationDetails");
        action.setParams({termCode :recTerm,
                          locCode :originCode});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.OriginLocation",responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                component.set("v.OriginLocation","");
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered Origin code not found. Please re-check the Origin Code.");
            } 
        });
        $A.enqueueAction(action);
    },
    getDestinationLocationDetails : function(component, event){
        var delTerm = component.find("DeliveryTerm").get("v.value");  
        var destCode = component.get("v.quoteRequest.destinationCode");
        console.log('delTerm-->'+delTerm+'destCode-->'+destCode);
        var action = component.get("c.getLocationDetails");
        action.setParams({termCode :delTerm,
                          locCode :destCode});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.destinationLocation",responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                component.set("v.destinationLocation","");
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered Destination code not found. Please re-check the Destination Code.");
            } 
        });
        $A.enqueueAction(action);
    },
    fetchCFSLocations : function(component, event){
        var action = component.get("c.getCFSLocations");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                responseData.sort(); 
                component.set("v.cfsLocations",responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                component.set("v.cfsLocations",null);
            } 
        });
        $A.enqueueAction(action);
    },
    getOriginLocation : function(component, event){
        var recTerm = component.find("ReceiptTerm").get("v.value");
        var originCode = component.get("v.quoteRequest.originCode");
        var cfsPorts = component.get("v.cfsLocations");
        if(recTerm == ''){
            component.set("v.showErrorMessage",true);
            component.set("v.errorMessage","Please select a Receipt Term and then enter Origin Code");
            component.set("v.OriginLocation","");
        }
        else if(originCode == '' || originCode == null){
            component.set("v.showErrorMessage",true);
            component.set("v.errorMessage","Please enter Origin Zip Code");
            component.set("v.OriginLocation","");
        }
            else if(recTerm == 'CFS'){
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
                var originLoc = '';
                for(var i=0;i<cfsPorts.length; i++){
                    if(cfsPorts[i].Name == originCode){
                        if(cfsPorts[i].Street__c != '' && cfsPorts[i].Street__c != null)
                            originLoc = originLoc + cfsPorts[i].Street__c + ' ';
                        if(cfsPorts[i].Name != '' && cfsPorts[i].Name != null)
                            originLoc = originLoc + cfsPorts[i].Name + ' ';
                        if(cfsPorts[i].State_Abbreviation__c != '' && cfsPorts[i].State_Abbreviation__c != null)
                            originLoc = originLoc + cfsPorts[i].State_Abbreviation__c + ' ';
                        if(cfsPorts[i].Zip_Code__c != '' && cfsPorts[i].Zip_Code__c != null)
                            originLoc = originLoc + cfsPorts[i].Zip_Code__c + ' ';
                        if(cfsPorts[i].Country__c != '' && cfsPorts[i].Country__c != null)
                            originLoc = originLoc + cfsPorts[i].Country__c + ' ';
                        component.set("v.OriginLocation",originLoc);    
                    }
                }
            }
                else{
                    component.set("v.showErrorMessage",false);
                    component.set("v.errorMessage","");
                    if(recTerm == 'P' && originCode.length > 4)
                        this.getOriginLocationDetails(component, event);
                    else if(recTerm == 'D' && originCode.length > 3)
                        this.getOriginLocationDetails(component, event);
                        else{
                            component.set("v.OriginLocation","");
                            component.set("v.showErrorMessage",true);
                            component.set("v.errorMessage","Entered Origin code not found. Please re-check the Origin Code.");
                        }
                }
    },
    getDestinationLocation : function(component, event){
        var delTerm = component.find("DeliveryTerm").get("v.value");
        var destinationCode = component.get("v.quoteRequest.destinationCode");
        var cfsPorts = component.get("v.cfsLocations");
        if(delTerm == ''){
            component.set("v.showErrorMessage",true);
            component.set("v.errorMessage","Please select a Delivery Term and then enter Destination Code");
            component.set("v.destinationLocation","");
        }
        else if(destinationCode == '' || destinationCode == null){
            component.set("v.showErrorMessage",true);
            component.set("v.errorMessage","Please enter Destination Code");
            component.set("v.destinationLocation","");
        }
            else if(delTerm == 'CFS'){
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
                var destinationLoc = '';
                for(var i=0;i<cfsPorts.length; i++){
                    if(cfsPorts[i].Name == destinationCode){
                        if(cfsPorts[i].Street__c != '' && cfsPorts[i].Street__c != null)
                            destinationLoc = destinationLoc + cfsPorts[i].Street__c + ' ';
                        if(cfsPorts[i].Name != '' && cfsPorts[i].Name != null)
                            destinationLoc = destinationLoc + cfsPorts[i].Name + ' ';
                        if(cfsPorts[i].State_Abbreviation__c != '' && cfsPorts[i].State_Abbreviation__c != null)
                            destinationLoc = destinationLoc + cfsPorts[i].State_Abbreviation__c + ' ';
                        if(cfsPorts[i].Zip_Code__c != '' && cfsPorts[i].Zip_Code__c != null)
                            destinationLoc = destinationLoc + cfsPorts[i].Zip_Code__c + ' ';
                        if(cfsPorts[i].Country__c != '' && cfsPorts[i].Country__c != null)
                            destinationLoc = destinationLoc + cfsPorts[i].Country__c + ' ';
                        component.set("v.destinationLocation",destinationLoc);    
                    }
                }
            }
                else{
                    component.set("v.showErrorMessage",false);
                    component.set("v.errorMessage","");
                    if(delTerm == 'P' && destinationCode.length > 4)
                        this.getDestinationLocationDetails(component, event);
                    else if(delTerm == 'D' && destinationCode.length > 3)
                        this.getDestinationLocationDetails(component, event);
                        else{
                            component.set("v.destinationLocation","");
                            component.set("v.showErrorMessage",true);
                            component.set("v.errorMessage","Entered Destination code not found. Please re-check the Destination Code.");
                        }
                }
    },
    getCustomerRecords : function(component, event, parsedResponse){
        if (typeof parsedResponse === 'undefined') {
            var custRec = document.getElementById("customerId").value;
            var shipRec = document.getElementById("shipperId").value;
            var consigRec = document.getElementById("consigneeId").value;
        }else{
            var custRec = parsedResponse['customerId'];
            var shipRec = parsedResponse['shipperId'];
            var consigRec = parsedResponse['consigneeId'];
        }
        
        var action = component.get("c.fetchContactsAndAddresses");
        var bookingRec = component.get("v.quoteRequest");
        bookingRec.shipperId = shipRec;
        bookingRec.consigneeId=consigRec;
        bookingRec.customerId=custRec;
        var autofillFields = component.get("v.autoFillCustomerFields");
        action.setParams({ partyCode :custRec});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                console.log('responseData-->'+responseData);
                if(responseData != null){
                    component.set("v.EnableCustomerRecords",true);
                    component.set("v.CustomerContactRecords",responseData);
                }
                
                for(var i=0; i<responseData.length; i++){
                    var contactRecord = component.get("v.contactRecordDetail");
                    if(responseData[i].ContactRecord.Id == contactRecord.Id){
                        bookingRec.customerAddressRecord = responseData[i].AddressRecord;
                        bookingRec.customerContactRecord = responseData[i].ContactRecord;
                        bookingRec.customerContactId = contactRecord.Id;
                        
                    }
                }
                if(typeof parsedResponse !== 'undefined'){
                    component.find("customerContact").set('v.value', parsedResponse["customerContactId"]);
                    component.set("v.quoteRequest.customerAddressRecord", parsedResponse["customerAddressRecord"]);
                    component.set("v.quoteRequest.customerContactRecord", parsedResponse["customerContactRecord"]);
                    bookingRec.customerAddressRecord = parsedResponse["customerAddressRecord"];
                    bookingRec.customerContactRecord = parsedResponse["customerContactRecord"];
                    bookingRec.customerContactId = parsedResponse["customerContactId"];
                    console.log(parsedResponse["customerContactRecord"]);
                    //component.set("v.contactRecordDetail", parsedResponse["customerContactRecord"]);
                }
                component.set("v.quoteRequest", bookingRec);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                bookingRec.customerAddressRecord = null;
                bookingRec.customerContactRecord = null;
                bookingRec.customerContactId = '';
                component.set("v.quoteRequest", bookingRec);
                component.set("v.EnableCustomerRecords",false);
                component.set("v.CustomerContactRecords",null);
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered code not found. Please re-check the Customer Code.");
            } 
        });
        $A.enqueueAction(action);
    },
    fetchContactDetails : function(component, event){
        var action = component.get("c.getContactDetails");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.contactRecordDetail",responseData);
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
                this.populateCustomerRecord(component, event);
            }
            else{
                component.set("v.contactRecordDetail",null);
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Contact Details could not be fetched. Please try again.");
            } 
        });
        $A.enqueueAction(action);
    },
    populateCustomerRecord : function(component, event){
        var contactDetails = component.get("v.contactRecordDetail");
        if(contactDetails != null){ 
            var action = component.get("c.getCustomerCVIF");
            action.setParams({ contactRecord :contactDetails});
            action.setCallback(this, function(response){
                var state = response.getState();
                if(response.getReturnValue()!=null){
                    var responseData = response.getReturnValue();
                    document.getElementById("customerId").value = responseData;
                    this.getCustomerRecords(component, event);
                    component.set("v.showErrorMessage",false);
                    component.set("v.errorMessage","");
                }
                else{
                    component.set("v.showErrorMessage",true);
                    component.set("v.errorMessage","Customer Details could not be fetched automatically. Please enter the Customer Details.");
                } 
            });
            $A.enqueueAction(action);
        }
    },
    getShipperRecords : function(component, event, parsedResponse){
        if (typeof parsedResponse === 'undefined') {
            var custRec = document.getElementById("shipperId").value;
        }else{
            var custRec = parsedResponse['shipperId'];
        }
        var action = component.get("c.fetchContactsAndAddresses");
        var bookingRec = component.get("v.quoteRequest"); 
        bookingRec.shipperId = custRec;
        action.setParams({ partyCode :custRec});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                if(responseData != null){
                    component.set("v.EnableShipperRecords",true);
                    component.set("v.ShipperContactRecords",responseData);
                    if(typeof parsedResponse !== 'undefined'){
                        component.find("shipperContact").set('v.value', parsedResponse["shipperContactId"]);
                        component.set("v.quoteRequest.shipperAddressRecord", parsedResponse["shipperAddressRecord"]);
                        component.set("v.quoteRequest.shipperContactRecord", parsedResponse["shipperContactRecord"]);
                    }
                    component.set("v.quoteRequest", bookingRec);
                }
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                bookingRec.shipperAddressRecord = null;
                bookingRec.shipperContactRecord = null;
                bookingRec.shipperContactId = '';
                component.set("v.quoteRequest", bookingRec);
                component.set("v.EnableShipperRecords",false);
                component.set("v.ShipperContactRecords",null);
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered code not found. Please re-check the Shipper Code.");
            } 
        });
        $A.enqueueAction(action);
    },
    updateAddressField : function(component, event){
        var partyType = event.getSource().getLocalId();
        var bookingRec = component.get("v.quoteRequest"); 
        if(partyType == "shipperContact"){
            var partylist = component.get("v.ShipperContactRecords");
            for(var i=0; i<partylist.length; i++){
                if(partylist[i].ContactRecord.Id == bookingRec.shipperContactId){
                    bookingRec.shipperAddressRecord = partylist[i].AddressRecord;
                    bookingRec.shipperContactRecord = partylist[i].ContactRecord;
                }
            }
        }
        if(partyType == "customerContact"){
            var partylist = component.get("v.CustomerContactRecords");
            for(var i=0; i<partylist.length; i++){
                if(partylist[i].ContactRecord.Id == bookingRec.customerContactId){
                    bookingRec.customerAddressRecord = partylist[i].AddressRecord;
                    bookingRec.customerContactRecord = partylist[i].ContactRecord;
                }
            }
        }
        if(partyType == "consigneeContact"){
            var partylist = component.get("v.ConsigneeContactRecords");
            for(var i=0; i<partylist.length; i++){
                if(partylist[i].ContactRecord.Id == bookingRec.consigneeContactId){
                    bookingRec.consigneeAddressRecord = partylist[i].AddressRecord;
                    bookingRec.consigneeContactRecord = partylist[i].ContactRecord;
                }
            }
        }
        component.set("v.quoteRequest", bookingRec);
    },
    
    getConsigneeRecords : function(component, event, parsedResponse){
    	if (typeof parsedResponse === 'undefined') {
            var custRec = document.getElementById("consigneeId").value;
        }else{
            var custRec = parsedResponse['consigneeId'];
        }
        var action = component.get("c.fetchContactsAndAddresses");
        var bookingRec = component.get("v.quoteRequest"); 
        bookingRec.consigneeId = custRec;
        action.setParams({ partyCode :custRec});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                if(responseData != null){
                    component.set("v.EnableConsigneeRecords",true);
                    component.set("v.ConsigneeContactRecords",responseData);
                    if(typeof parsedResponse !== 'undefined'){
                        component.find("consigneeContact").set('v.value', parsedResponse["consigneeContactId"]);
                        component.set("v.quoteRequest.consigneeAddressRecord", parsedResponse["consigneeAddressRecord"]);
                        component.set("v.quoteRequest.consigneeContactRecord", parsedResponse["consigneeContactRecord"]);
                    }
                    component.set("v.quoteRequest", bookingRec);
                }
                component.set("v.showErrorMessage",false);
                component.set("v.errorMessage","");
            }
            else{
                bookingRec.consigneeAddressRecord = null;
                bookingRec.consigneeContactRecord = null;
                bookingRec.consigneeContactId = '';
                component.set("v.quoteRequest", bookingRec);
                component.set("v.EnableConsigneeRecords",false);
                component.set("v.ConsigneeContactRecords",null);
                component.set("v.showErrorMessage",true);
                component.set("v.errorMessage","Entered code not found. Please re-check the Consignee Code.");
            } 
        });
        $A.enqueueAction(action);
    },    
    getDoorToCFSRatesClone: function(component) {      
        var isFormValid = this.checkValidations(component); 
        var mapChargeLineItems = component.get("v.mapChargeLineItems");
        var mapCharge = new Map();
        if(isFormValid){
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var quoteRequestValues = component.get("v.quoteRequest");
            component.set("v.quoteRequest.ContainerMode", "LCL");
            var currentUnit = document.getElementById("measurmentUnit").checked;
            component.set("v.chargeableWeight", 0);
            if (currentUnit) {
                quoteRequestValues.totalWeightUnit = 'KG';
                quoteRequestValues.totalVolumeUnit = 'M3';
                
            } else {
                quoteRequestValues.totalWeightUnit = 'LB';
                quoteRequestValues.totalVolumeUnit = 'CF'; 
            }
            
            if(component.get("v.totalDimensions")){
                var currentVolume = component.find("cargoVolume").get("v.value");
                var currentWeight = component.find("cargoWeight").get("v.value");
                if(currentVolume != null)
                    quoteRequestValues.totalVolume = currentVolume;
                if(currentWeight != null)
                    quoteRequestValues.totalWeight = currentWeight;
            }
            else{
                quoteRequestValues.totalVolume = 0;
                quoteRequestValues.totalWeight = 0;
            }
            var cargoDetailsValuesString=JSON.stringify(cargoDetailsValues);
            var recTerm = component.find("ReceiptTerm").get("v.value");
            var deliveryTerm = component.find("DeliveryTerm").get("v.value"); 
            component.set("v.quoteDataToDisplay",null);
            component.set("v.quoteData", null);
            component.set("v.showErrorMessage", false);
            component.set("v.hasResults", true); 
            var quoteRequestValues = component.get("v.quoteRequest");
            var quoteStringData=JSON.stringify(quoteRequestValues);
            var cargoDetailsValues = component.get("v.cargoDetailsValues");
            var chargeLineBodyValue =component.get("v.chargeLineBody");
            console.log('chargeLineBodyValue--->'+chargeLineBodyValue);	
            //this.getDoorToCFSCWRatesClone(component,quoteStringData,cargoDetailsValues); 
            var actionGetRates = component.get("c.createLFSCWRequest");
            actionGetRates.setStorable();
            actionGetRates.setParams({quoteString:quoteStringData,
                                      cargoDetailList:cargoDetailsValues
                                     });
            actionGetRates.setCallback(this, function(response) {
                var state = response.getState();
                var responseVal = response.getReturnValue();
                if (state === "SUCCESS" && responseVal!=null && responseVal.length>0) 
                {
                    var responseVal = response.getReturnValue();
                    var newItems=[];
                    component.set("v.quoteData", responseVal);
                    console.log(JSON.stringify(responseVal));
                    //var mapChargeLineResultData=JSON.parse(JSON.stringify(component.get("v.mapChargeLineResult")));
                    //if(mapChargeLineResultData!=null){
                    //this.getCombinedData(component);
                    //}
                    for(var i = 0; i < responseVal.length; i++){
                        var dataString=JSON.stringify(responseVal[i]);
                        console.log("dataString--->"+dataString+"respp-->"+response[i]);
                        if(!dataString.includes('Error')){
                            console.log('@@@ - Test going on');
                            mapCharge.set(i.toString(),responseVal[i].Body.ChargeLine); 
                            newItems.push(responseVal[i]);
                        }	
                    }
                    
                    newItems.sort(function (a, b) {
                        console.log('@@@ TotalRate - ' + b.Body.TotalRate);
                        return a.Body.TotalRate - b.Body.TotalRate;
                    });
                    component.set("v.mapChargeLineItems",mapCharge);
                    component.set("v.quoteDataToDisplay", newItems);          
                    component.set("v.showErrorMessage", false);
                    component.set("v.hasResults", false);
                    component.set("v.hasMoreResults", false);
                    
                }else{
                    component.set("v.hasResults", false);
                    component.set("v.showErrorMessage", true);
                    component.set("v.errorMessage","Something went Wrong Please try Again."); 
                }
                /*if (state === "SUCCESS" && response.getReturnValue()!=null) 
				{
					var newItems=[];
					var responseVal = response.getReturnValue();
					component.set("v.quoteData", responseVal);
					console.log("From server: " + responseVal);
					for(var i = 0; i < responseVal.length; i++){
						var dataString=JSON.stringify(responseVal[i]);
						console.log("dataString--->"+dataString+"respp-->"+response[i]);
						if(!dataString.includes('Error')){
							console.log('@@@ - Test going on');
							newItems.push(responseVal[i]);
						}	
					}
					newItems.sort(function (a, b) {
						console.log('@@@ TotalRate - ' + b.Body.TotalRate);
						return a.Body.TotalRate - b.Body.TotalRate;
					});
					console.log('newItems1111--->'+newItems);
					component.set("v.quoteDataToDisplay", newItems);
					//console.log('newItems1111--->'+newItems);
					component.set("v.showErrorMessage", false);
                    component.set("v.hasResults", false);
                    component.set("v.hasMoreResults", false);
				}
				else{
					component.set("v.showErrorMessage", true);
					component.set("v.errorMessage","Something went Wrong Please try Again."); 
				}
				if(state === 'ERROR'){
					component.set("v.showErrorMessage", true);
					component.set("v.errorMessage","Something went Wrong Please try Again.");  
				}*/        
            });
            $A.enqueueAction(actionGetRates);
        }
    },
    getDoorToCFSCWRatesClone: function(component,quoteStringData,cargoDetailsValues) {
        var actionGetCWRates = component.get("c.createCWRequest");
        actionGetCWRates.setStorable();
        actionGetCWRates.setParams({quoteString:quoteStringData,
                                    cargoDetailList:cargoDetailsValues
                                   });
        actionGetCWRates.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue()!=null) 
            {
                var quoteData=component.get("v.quoteData");
                var newQuote = [];
                var responseVal = response.getReturnValue();                    
                component.set("v.mapChargeLineResult",responseVal);
                if(quoteData!=null && quoteData.length>0){
                    this.getCombinedData(component);
                    //var quoteData=JSON.parse(JSON.stringify(component.get("v.quoteData")));
                    /*for (var i in quoteData) {
                            var chargeLineValue = responseVal[quoteData[i].Body.FromToLocation];                            
                            if(chargeLineValue.length>0){                                
                                if(chargeLineValue.CWTransitDays != null){
                                    if(chargeLineValue.TotalAmount == null){
                                        chargeLineValue.TotalAmount = '0';
                                    }
                                    if(chargeLineValue.CWTransitDays == null){
                                        chargeLineValue.CWTransitDays = 0;
                                    }
                                    if(quoteData[i].Body.TransitDays == null || quoteData[i].Body.TransitDays == 'Error'){
                                        quoteData[i].Body.TransitDays = '0'; 
                                    }
                                    if(quoteData[i].Body.Rate == null ){
                                        quoteData[i].Body.Rate = 0; 
                                    }
                                    quoteData[i].Body.chargeLineValue = chargeLineValue.ChargeLine;
                            	    quoteData[i].Body.CWTransitDays = chargeLineValue.CWTransitDays;
                                    quoteData[i].Body.TotalDays=Integer.valueOf(chargeLineValue.CWTransitDays)+Integer.valueOf(quoteData[i].Body.TransitDays); 
                                    quoteData[i].Body.TotalRate=Decimal.valueOf(Integer.valueOf(Decimal.valueOf(chargeLineValue.TotalAmount))+Integer.valueOf(quoteData[i].Body.Rate));
                                    
                    			}
                            }
                            newQuote.push(quoteData);
                        }*/
                        //console.log(newQuote);
                        //component.set("v.quoteData", quoteData);                        
                    }                    
                }
            });
        $A.enqueueAction(actionGetCWRates);
    },
    getCombinedData: function(component) {
        var actionAllCombinedData = component.get("c.getAllCombinedData");
        actionAllCombinedData.setParams({listOfQuoteRatesResponseString:JSON.stringify(component.get("v.quoteData")),
                                         mapChargeLine:JSON.stringify(component.get("v.mapChargeLineResult"))
                                        });
        actionAllCombinedData.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS" && response.getReturnValue()!=null) 
            {
                var responseVal = response.getReturnValue();
                var newItems=[];
                component.set("v.quoteData", responseVal);
                console.log(responseVal);
                for(var i = 0; i < responseVal.length; i++){
                    var dataString=JSON.stringify(responseVal[i]);
                    console.log("dataString--->"+dataString+"respp-->"+response[i]);
                    if(!dataString.includes('Error')){
                        newItems.push(responseVal[i]);
                    }	
                }
                component.set("v.quoteDataToDisplay", newItems);
                component.set("v.showErrorMessage", false);
                component.set("v.hasResults", false);
                component.set("v.hasMoreResults", false);
            }
            
        });
        $A.enqueueAction(actionAllCombinedData);
    },
    setReQuoteValues : function(component, event){
        var quoteId = component.get("v.reQuoteRecordId");
        if(quoteId != null && quoteId !== ""){
            var action = component.get("c.getReQuoteData");
            action.setParams({ 
                quoteId : quoteId
            });
            console.log("quoteId :"+quoteId);
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log(state + " : "+ JSON.stringify(response.getReturnValue()));
                if(state === 'SUCCESS'){
                    if(response.getReturnValue()!=null){
                        var parsedResponse = response.getReturnValue();
                        var quoteRecord = component.get("v.quoteRequest");
                        console.log("response Quote :"+ JSON.stringify(response.getReturnValue()));
                        quoteRecord.ContainerMode = parsedResponse["ContainerMode"];
                        quoteRecord.consigneeId = parsedResponse["consigneeId"];
                        quoteRecord.customerId = parsedResponse["customerId"];
                        quoteRecord.shipperId = parsedResponse["shipperId"];
                        quoteRecord.receiptTermVal = parsedResponse["receiptTermVal"];
                        quoteRecord.deliveryTermVal = parsedResponse["deliveryTermVal"];
                        quoteRecord.originCode = parsedResponse["originCode"];
                        quoteRecord.destinationCode = parsedResponse["destinationCode"];
                        quoteRecord.estSailingDate = parsedResponse["estSailingDate"];
                        component.set("v.contactRecordDetail", parsedResponse["customerContactRecord"]);
                        component.set("v.quoteRequest", quoteRecord);
                        
                        if(parsedResponse["totalVolumeUnit"] !== null && parsedResponse["totalVolumeUnit"] === "M3"){
                            component.find("measurmentUnit").set("v.checked", true);
            				component.set("v.volumeUnit", parsedResponse["totalVolumeUnit"]);
                        }else if(parsedResponse["totalVolumeUnit"] !== null && parsedResponse["totalVolumeUnit"] === "CF"){
                            component.set("v.volumeUnit", parsedResponse["totalVolumeUnit"]);
                            component.find("measurmentUnit").set("v.checked", false);
                        }
                        if(parsedResponse["totalWeightUnit"] !== null && parsedResponse["totalWeightUnit"] === "KG"){
                            component.find("measurmentUnit").set("v.checked", true);
            				component.set("v.weightUnit", parsedResponse["totalWeightUnit"]);
                        }else if(parsedResponse["totalWeightUnit"] !== null && parsedResponse["totalWeightUnit"] === "LB"){
                            component.set("v.weightUnit", parsedResponse["totalWeightUnit"]);
                            component.find("measurmentUnit").set("v.checked", false);
                        }
                        
                        if(parsedResponse["totalVolume"] !== null && parsedResponse["totalVolume"] !== 0){
                            component.set("v.totalDimensions", true);
                            component.set("v.packageDetails", false);
                            component.find("cargoWeight").set("v.value", parsedResponse["totalWeight"]);
                            component.find("cargoVolume").set("v.value", parsedResponse["totalVolume"]);
                        }else{
                            component.set("v.totalDimensions", false);
                            component.set("v.packageDetails", true);
                        }
                        
                        if(parsedResponse["cargodetailWrapperList"] != null){
                            component.set("v.cargoDetailsValues", parsedResponse["cargodetailWrapperList"]);
                        }
                        this.getQuoteRates(component,event, quoteId);
                        this.getShipperRecords(component,event, parsedResponse);
                        this.getConsigneeRecords(component,event, parsedResponse);
                        this.getCustomerRecords(component,event, parsedResponse);
                        this.getOriginLocation(component,event);
                        this.getDestinationLocation(component,event);
                        component.set("v.disableOriginCode", false);
                        component.set("v.disableDestinationCode", false);
                        if(parsedResponse["ContainerMode"] === 'LCL'){
                            var test = [];
                            test.push({"isHazardous":false,"sequenceId":1,"type":""});
                            component.set("v.cargoDetailsValues",test);
                            this.LCLBtnClick(component);
                        }
                        if(parsedResponse["ContainerMode"] === 'FCL'){
                            this.FCLBtnClick(component);
                        }
                    }
                }else if(state === 'ERROR'){
                    console.log("Error11");
                }else{
                    console.log("Unknown error!");
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    getQuoteRates : function(component, event, quoteId){
        var action = component.get("c.getQuoteRates");
        action.setParams({ 
            quoteId : quoteId
        });
        console.log(quoteId);
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state --->  "+state);
            if(state === 'SUCCESS'){
                if(response.getReturnValue()!=null){
                    var parsedResponse = response.getReturnValue();
                    console.log(JSON.stringify(parsedResponse));
                    if(parsedResponse["quoteStatus"] === 'Saved'){
                        console.log("parsedResponsequoteRec :"+JSON.stringify(parsedResponse["quoteRec"]));
                        component.set("v.newQuoteRecord", parsedResponse["quoteRec"]);
                    }
                    if(typeof parsedResponse["fclRates"] !== 'undefined'){
                        component.set("v.fclRatingResponse", parsedResponse["fclRates"]);
                        if(typeof parsedResponse["fclRates"]["result"] !== "undefined" && 
                           typeof parsedResponse["fclRates"]["result"]["Revenue"] !== "undefined" &&
                           typeof parsedResponse["fclRates"]["result"]["Revenue"]["Positions"] !== "undefined"){
                            console.log();
                            component.set("v.lineItems", parsedResponse["fclRates"]["result"]["Revenue"]["Positions"]);
                        }
                    }
                    if(typeof parsedResponse["lfsRates"] !== 'undefined' ){
                        if(parsedResponse["originType"] === 'CFS' && parsedResponse["destinationType"] === 'CFS'){
                            var body = parsedResponse["lfsRates"][0];
                            console.log("body" + JSON.stringify(body));
                            console.log("body - Charge Item :" + JSON.stringify(body["Body"]["ChargeLine"]));
                            if(typeof body["Body"]["ChargeLine"] !== 'undefined' ){
                                console.log(JSON.stringify(parsedResponse["lfsRates"][0]["Body"]));
                                component.set("v.chargeLineBody", parsedResponse["lfsRates"][0]["Body"]);
                                var newItems = [];
                                var chargeLineString = ""
                                for(var i = 0; i < body["Body"]["ChargeLine"].length; i++){
                                    var dataString = body["Body"]["ChargeLine"][i].ChargeCodeDesc + '-' + body["Body"]["ChargeLine"][i].SellOSAmount;
                                    if(!dataString.includes('Error')){
                                        newItems.push(dataString);
                                    }	
                                }
                                for(var i=0;i<newItems.length;i++){
                                    chargeLineString = chargeLineString + newItems[i] + ';';  
                                }
                                console.log("newItems :"+ JSON.stringify(newItems));
                                component.set("v.chargeLineItems", newItems);
                                component.set("v.chargeLineStr",chargeLineString);
                            }
                        }else if(parsedResponse["originType"] === 'D Door' && parsedResponse["destinationType"] === 'CFS'){
                            var body = parsedResponse["lfsRates"];
                            console.log("[lfsRates] :"+JSON.stringify(parsedResponse["lfsRates"]));
                            component.set("v.quoteDataToDisplay", body);
                        }
                    }
                }
            }else if(state === 'ERROR'){
                console.log("Error112");
            }else{
                console.log("Unknown error!");
            }
        });
        $A.enqueueAction(action);
    }
    
})