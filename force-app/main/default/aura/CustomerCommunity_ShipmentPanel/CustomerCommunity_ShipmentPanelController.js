({
    doInit : function(component, event, helper){
        helper.getBookingRecords(component);
     },
    changeSelection : function(component, event, helper) {
        var SelectAllCheckboxValue =  component.find("SelectAllCheckbox").get("v.value");
        var checkbox = component.find("myCheckbox");
        if(SelectAllCheckboxValue === true)
        for(var i=0;i<checkbox.length;i++){
            checkbox[i].set("v.value",true);
        }
        else{
            for(var i=0;i<checkbox.length;i++){
                checkbox[i].set("v.value",false);
            }
        }
    }
})