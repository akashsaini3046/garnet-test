({
    fetchBolAndRelatedData: function (component, event, helper) {

        this.showSpinner(component);
        var bolId = component.get("v.bolId");

        var action = component.get("c.getBolDetails");
        action.setParams({
            bolId: bolId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseData = response.getReturnValue();
                //console.log('### ---> Stringify Response');
                //console.log(JSON.stringify(responseData));
                this.setSectionData(component, responseData, "v.bolFieldNameValue", "Bol");
                this.setSectionData(component, responseData, "v.bolLocationsFieldNameValue", "Locations");
                this.setSectionData(component, responseData, "v.bolRoutingsFieldNameValue", "Routing");
                this.setSectionData(component, responseData, "v.billItemsFieldNameValue", "BillItems");
                this.setSectionData(component, responseData, "v.itemTextFieldNameValue", "ItemTexts");
                this.setSectionData(component, responseData, "v.subItemsFieldNameValue", "SubItems");
                this.setSectionData(component, responseData, "v.commoditiesFieldNameValue", "Commodity");
                this.setSectionData(component, responseData, "v.freeTextFieldNameValue", "FreeText");
                this.setSectionData(component, responseData, "v.voyagesFieldNameValue", "Voyage");
                this.setSectionData(component, responseData, "v.equipmentsFieldNameValue", "Equipment");
                var voyagesFieldNameValue=component.get("v.voyagesFieldNameValue");
      			var count = 1;
                console.log('voyage val');
                for(var v in voyagesFieldNameValue){
                    
                    if(count == voyagesFieldNameValue.length){
                        component.set("v.voyagesFieldNameValue",voyagesFieldNameValue[v]);
                        console.log(JSON.stringify(voyagesFieldNameValue[v]));
                    }
                    count++;
                }
                this.hideSpinner(component);
            } else if (state === "ERROR") {
                console.log("Error in controller");
            } else {
                console.log("Error Unknown !");
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    setSectionData: function (component, responseData, attribute, sectionName) {
        var sectionRecords = responseData[sectionName];
        var sectionList = [];
        for (var s in sectionRecords) {
            var sectionFieldNameValue = [];
            var sectionRec = sectionRecords[s];
            for (var key in sectionRec) {
                sectionFieldNameValue.push({ label: key, value: sectionRec[key] });
            }
            sectionList.push(sectionFieldNameValue);
        }
        component.set(attribute, sectionList);
    },

    setLocations: function (component) {
        var bolLocationsFieldNameValue = component.get('v.bolLocationsFieldNameValue');
        //var bolLocationsFieldNameValue = bolLocationsFieldNameValue.map(());
    },

    showSpinner: function (component) {
        component.set("v.spinner", true);
    },
    hideSpinner: function (component) {
        component.set("v.spinner", false);
    }

})