({
    doInit : function(component, event, helper){
        helper.createShippingRecord(component, event);
    },
    handleUploadFinished: function (component, event) {
        component.set("v.PageMsg", "File Uploaded Successfully. We will notify you once it is available for review.");
        component.set("v.isMsgVisible", true);
    }
})