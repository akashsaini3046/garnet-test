({
    getBookingId : function(component,event){ 
        var action = component.get("c.getValidBookingId");
        var name = component.find('booking_Id').get('v.value');
        action.setParams({bookingName:name});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var Idbooking = response.getReturnValue();
                if(Idbooking != null && Idbooking != "FALSE"){
                   component.set("v.bookingRecordId", Idbooking);
                   component.set("v.disableUploadButton", false);
                   this.isBookingValid(component,event);
                } else if(Idbooking === "FALSE"){
                   component.set("v.bookingRecordId", "");
                   component.set("v.showFieldErrorMsg",true);
                   component.set("v.ErrorMsg","Please enter a Valid Booking Number.");
                   component.set("v.disableUploadButton", true);
                }
            } else if (state === "ERROR") {
                self.ErrorHandler();
            }
        });
        $A.enqueueAction(action); 
    },
    isBookingValid : function(component,event){ 
        var action = component.get("c.getValidBooking");
        var name = component.find('booking_Id').get('v.value');
        var typeDocument = component.get("v.SelectedDocumentType");
        action.setParams({bookingName:name,documentType:typeDocument});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var Idheader = response.getReturnValue();
                if(Idheader != null && Idheader != "FALSE"){
                   component.set("v.recordId", Idheader);
                   component.set("v.disableUploadButton", false);
                } else if(Idheader === "FALSE"){
                   component.set("v.showFieldErrorMsg",true);
                   component.set("v.ErrorMsg","Please enter a Valid Booking Number.1");
                   component.set("v.disableUploadButton", true);
                }
            } else if (state === "ERROR") {
                self.ErrorHandler();
            }
        });
        $A.enqueueAction(action); 
    },
    doInitHandler: function (component, event, helper) {
        var action = component.get("c.getAllBookings");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var documents = response.getReturnValue();
                if(documents != null){
                component.set("v.totalPages", Math.ceil(response.getReturnValue().length / component.get("v.pageSize")))
                component.set("v.documentRecords", documents);
                component.set("v.currentPageNumber", 1);
                this.buildData(component, event);
                }
            } else if (state === "ERROR") {
                self.ErrorHandler();
            }
        });
        $A.enqueueAction(action);
    },
    buildData: function (component, event) {
        var data = [];
        var pageNumber = component.get("v.currentPageNumber");
        var pageSize = component.get("v.pageSize");
        var allData = component.get("v.documentRecords");
        var x = (pageNumber - 1) * pageSize;
        //creating data-table data
        for (; x <= (pageNumber) * pageSize; x++) {
            if (allData[x]) {
                data.push(allData[x]);
            }
        }
        component.set("v.documentRecordsFiltered", data);
        this.generatePageList(component, pageNumber);
    },
    generatePageList: function (component, pageNumber) {
        pageNumber = parseInt(pageNumber);
        var pageList = [];
        var totalPages = component.get("v.totalPages");
        if (totalPages > 1) {
            if (totalPages <= 10) {
                var counter = 2;
                for (; counter < (totalPages); counter++) {
                    pageList.push(counter);
                }
            } else {
                if (pageNumber < 5) {
                    pageList.push(2, 3, 4, 5, 6);
                } else {
                    if (pageNumber > (totalPages - 5)) {
                        pageList.push(totalPages - 5, totalPages - 4, totalPages - 3, totalPages - 2, totalPages - 1);
                    } else {
                        pageList.push(pageNumber - 2, pageNumber - 1, pageNumber, pageNumber + 1, pageNumber + 2);
                    }
                }
            }
        }
        component.set("v.pageList", pageList);
    },
    deleteDocumentHelper: function (component, event, indexId, documentType) {
        var action = component.get("c.deleteDocumentRecord");
        action.setParams({
            "recordId": indexId,
            "documentType": documentType
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var message = response.getReturnValue();
                if (message === 'True') {
                    alert("Record Deleted Successfully !");
                    this.doInitHandler(component, event);
                } else if (message == null || message == 'False') {
                    alert("Something went Wrong Please try Again");
                }
            } else if (state === "ERROR") {
                alert("Something went Wrong Please try Again! ")
                self.ErrorHandler(component, response);
            }
        })
        $A.enqueueAction(action);
    },
    viewDocumentHelper: function (component, event, indexId) {
        var action = component.get("c.readFileContent");
        action.setParams({
            "recordId": indexId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var self = this;
            if (state === "SUCCESS") {
                var recordId = response.getReturnValue();
                component.set("v.contentId", recordId);
                component.set("v.hasModalOpen", true);
            } else if (state === "ERROR") {
                self.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    },
    getExcelFieldLabelList : function(component){
        var action = component.get("c.getExcelFieldLabels");
        action.setCallback(this, function(response){
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.listExcelFieldLabels",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    getExcelFieldValidationList : function(component){
        var action = component.get("c.getExcelFieldValidations");
        action.setCallback(this, function(response){
            if(response.getReturnValue()!=null){
                var responseData = response.getReturnValue();
                component.set("v.listExcelFieldValidations",responseData);
            } 
        });
        $A.enqueueAction(action);
    },
    importTable: function(component,event,helper) {
        event.stopPropagation();
        event.preventDefault();
        try {
            const file = helper.validateFile(component, event);
            helper.readExcelFile(event, file)
            .then($A.getCallback(excelFile => {
                this.createRecords(component, excelFile);
            	helper.createFileRecord(component, event, helper);
            }))
                .catch($A.getCallback(exceptionMessage => {
            }))
                .finally($A.getCallback(() => {
            }))
            } catch (exceptionMessage) {
            }
    },
    validateFile: function(component, event) {
        const files = event.getSource().get("v.files");
        if (!files || files.length === 0 || $A.util.isUndefinedOrNull(files[0])) {
            throw 'No file specified';
        }
        const file = files[0];
        if (file.size > 10000000) {
            throw 'File size exceeded : 10mb';
        }
        return file;
    },

    readExcelFile: function(event, file) {
        return new Promise(function (resolve, reject) {
            const fileReader = new FileReader();
            fileReader.onload = event => {
                let filename = file.name;
                let binary = "";
                new Uint8Array(event.target.result).forEach(function (byte) {
                    binary += String.fromCharCode(byte);
                });

                try {
                    resolve({
                        "fileName": filename,
                        "xlsx": XLSX.read(binary, {type: 'binary', header: 1})
                    });
                } catch (error) {
                    reject(error);
                }
            };
            fileReader.readAsArrayBuffer(file);
        });
    },
    
    createRecords : function(component, parsedFile){    
        const get = (p, o) => p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o);
        var headerId = component.get("v.recordId");
        var fieldList = component.get("v.listExcelFieldLabels");
        var cellRange = parsedFile.xlsx.Sheets.Sheet1["!ref"]; 
        var startRange = cellRange.substring(0,cellRange.indexOf(":")).split(/([0-9]+)/);
        var endRange = cellRange.substring(cellRange.indexOf(":")+1,cellRange.length).split(/([0-9]+)/);
		var containerStartingRange = []; var containerEndingRange = [];
        var validationMessage = "";
        if(fieldList != null && fieldList.length>0){
            for(var i=0;i<fieldList.length;i++){
                if(fieldList[i].Active__c){
                    var cellLocation = this.findCellLocation(parsedFile,fieldList[i].MasterLabel, startRange, endRange);
                    if(cellLocation != null){
                        var fieldDataText = "";
                        var fieldRange = cellLocation.split(/([0-9]+)/);
                        if(fieldList[i].Traverse_Path__c == "Row"){
                            for(var row = parseInt(fieldRange[1], 10)+1; row <= endRange[1]; row++){
                                var cellValue = get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],fieldRange[0]+row,'v'], parsedFile);
                                if(cellValue != null && cellValue != ""){
                                    cellValue = cellValue.toString(); var currentSequenceCount = parseInt(fieldList[i].Sequence__c, 10);
                                    if(fieldList[i].Ending_Label__c != null && fieldList[i].Ending_Label__c !="" && fieldList[i].Ending_Label__c == cellValue)
                                        break;
                                    else if(fieldList[i].Multiple_Records__c){
                                        var count = 1; containerEndingRange[0] = row-1;
                                        while(cellValue != fieldList[i].Ending_Label__c){
                                            if(cellValue != null && cellValue !=""){
                                                cellValue = cellValue.toString();
                                                var matchingLabelArr = [];
                                                if(fieldList[i].Matching_Labels__c != null && fieldList[i].Matching_Labels__c != ""){
                                                    var matchingLabels = fieldList[i].Matching_Labels__c;
                                                    if(matchingLabels.includes(','))
                                                    	matchingLabelArr = matchingLabels.split(',');
                                                    else
                                                        matchingLabelArr[0] = matchingLabels;
                                                }
												if(fieldList[i].Field_Validations__c == true)
													validationMessage += this.checkFieldValidations(component, fieldList[i].Field_API_Name__c, cellValue, fieldRange[0]+row);
                                                if(fieldList[i].Field_Data__c == null || fieldList[i].Field_Data__c == ""){
                                                    fieldList[i].Field_Data__c = cellValue;
													if(matchingLabelArr != null && matchingLabelArr.length > 0 && this.checkContainerNumber(matchingLabelArr, cellValue) == true){
														fieldList[i].Field_Data__c = cellValue;
														var rowCount = this.calculateRowCounts(component, parsedFile, row, endRange[1], fieldRange[0], matchingLabelArr, fieldList[i].Ending_Label__c);
														fieldList[i].Description__c = this.getDescriptionRow(component, rowCount, parsedFile, fieldRange[0], row);
														containerStartingRange[count] = row; containerEndingRange[count] = row + rowCount;
                                                    }
													if(fieldList[i].Check_Field_Range__c == true){
														var fieldRowCount = this.getDataRange(component, containerStartingRange, containerEndingRange, row);
														if(fieldRowCount != -1){
															if(fieldRowCount  != 1)
																fieldList[i].Sequence__c = currentSequenceCount + fieldRowCount;
															if(fieldList[i].Dependent_Field_API_Name__c != null &&  fieldList[i].Dependent_Field_API_Name__c != '' && cellValue.match(/(\d+)/) != null){
																fieldList[i].Field_Data__c = cellValue.match(/(\d+)/)[0];
																fieldList[i].Dependent_Field_Value__c = cellValue.replace(/[0-9 ]/g, '').toUpperCase();
															}
															else
																fieldList[i].Field_Data__c = cellValue;
														}
													}
                                                }
                                                else if(fieldList[i].Field_Data__c != null && fieldList[i].Field_Data__c != ""){
													var newFieldData = new Object();
                                                    if(fieldList[i].Check_Field_Range__c == true){
														var fieldRowCount = this.getDataRange(component, containerStartingRange, containerEndingRange, row);
														if(fieldRowCount != -1){
															if(fieldList[i].Dependent_Field_API_Name__c != null &&  fieldList[i].Dependent_Field_API_Name__c != '' && cellValue.match(/(\d+)/) != null){
																newFieldData.Field_Data__c = cellValue.match(/(\d+)/)[0];
																newFieldData.Dependent_Field_Value__c = cellValue.replace(/[0-9 ]/g, '').toUpperCase();
															}
															else
																newFieldData.Field_Data__c = cellValue;
															newFieldData = this.cloneRelatedRecord(component, newFieldData, fieldList[i], currentSequenceCount, fieldRowCount);
														}
													}
													else if(fieldList[i].Check_Field_Range__c == false && (matchingLabelArr == null || matchingLabelArr.length == 0 || (matchingLabelArr != null && matchingLabelArr.length > 0 && this.checkContainerNumber(matchingLabelArr, cellValue) == true))){
                                                        newFieldData.Field_Data__c = cellValue;
														count++;	
                                                        newFieldData = this.cloneRelatedRecord(component, newFieldData, fieldList[i], currentSequenceCount, count);
														var rowCount = this.calculateRowCounts(component, parsedFile, row, endRange[1], fieldRange[0], matchingLabelArr, fieldList[i].Ending_Label__c);
														newFieldData.Description__c = this.getDescriptionRow(component, rowCount, parsedFile, fieldRange[0], row);
														containerStartingRange[count] = row; containerEndingRange[count] = row + rowCount;
                                                    }
													
													fieldList.push(newFieldData);
                                                }
                                            }
                                            row = row+1;
                                            cellValue = get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],fieldRange[0]+row,'v'], parsedFile);
                                        }
                                        break;
                                    }
                                    else
                                    	fieldDataText = fieldDataText + '\n'+ cellValue;
                                }
                            }
                            if(fieldDataText != null && fieldDataText != "")
                            	fieldList[i].Field_Data__c = fieldDataText.toString();
                        }
                        if(fieldList[i].Traverse_Path__c == "Column"){
                            for(var col = fieldRange[0].charCodeAt(0)+1; col <= endRange[0].charCodeAt(0); col++){
                                var cellValue = get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],String.fromCharCode(col)+fieldRange[1],'v'], parsedFile);
                                if(cellValue != null && cellValue != ""){
                                    if(fieldList[i].Ending_Label__c != null && fieldList[i].Ending_Label__c !="" && 
                                       fieldList[i].Ending_Label__c == cellValue)
                                        break;
                                    else
                                        fieldDataText = fieldDataText + '\n' + cellValue;
                                }
                            }
                            fieldList[i].Field_Data__c = fieldDataText.toString();
                        }
                    }
                }
            }
            if(validationMessage != null && validationMessage != ''){
                alert('Field Validations - \n' + validationMessage);
                this.doInitHandler(component);
                throw new Error("Field Validations - \n" + validationMessage);
            }
            else{
                var action = component.get("c.createBLRecords");
                action.setParams({ listFormData :fieldList,
                                  headerRecordId : headerId});
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        this.doInitHandler(component);
                        this.getExcelFieldLabelList(component);
                    } else if (state === "ERROR") {
                        alert("Something went Wrong Please try Again! ");
                    }
                });
                $A.enqueueAction(action);
            }
        }
    },
        
    findCellLocation : function(parsedFile, fieldLabel, startRange, endRange){
        const get = (p, o) => p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o);
        for(var col = startRange[0].charCodeAt(0); col <= endRange[0].charCodeAt(0); col++){
            for(var row = startRange[1]; row <= endRange[1]; row++){
                if(fieldLabel == get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],String.fromCharCode(col)+row,'v'], parsedFile))
                    return String.fromCharCode(col)+row;
            }
        }
        return null;
    }, 
    createFileRecord : function(component, event, helper){
        var files = event.getSource().get("v.files");
        var file = files[0];
        var reader = new FileReader();
        reader.onloadend = function() {
            var dataURL = reader.result;
            var content = dataURL.match(/,(.*)$/)[1];
            helper.uploadFile(component, file, content);
        }
        reader.readAsDataURL(file);
    },
    uploadFile: function(component, file, base64Data) {
        var headerId = component.get("v.recordId");
        var action = component.get("c.createFileRecord");
        action.setParams({
            fileName: file.name,
            base64Data: base64Data,
            contentType: file.type,
            headerRecordId : headerId
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                alert("Your Document has been Uploaded Successfully !"); 
            } else if (state === "ERROR") {
                alert("Something went Wrong Please try Again! ");
            }
            component.set("v.bookingId","");
            component.set("v.disableUploadButton",true);
            this.doInitHandler(component);
        });
        $A.enqueueAction(action);
    },
    checkContainerNumber : function(matchingLabelArr, cellValue) {
        var isContainerNumber = false;
        cellValue = cellValue.toString();
        for(var i=0;i<matchingLabelArr.length;i++)
            if(cellValue.includes(matchingLabelArr[i]))
                isContainerNumber = true;
        return isContainerNumber;
    },
    checkFieldValidations : function(component, fieldAPIName, cellValue, cellLocation){
        var validationMessage = '';
        var mapFieldValidations = component.get('v.listExcelFieldValidations');
        var listFieldValidations = mapFieldValidations[fieldAPIName];
        if(listFieldValidations != null && listFieldValidations.length > 0){
            for(var i=0; i<listFieldValidations.length; i++){
                if(listFieldValidations[i].Null_Check__c && cellValue == null){
                    validationMessage += 'Value required at cell location - ' + cellLocation + '\n';
                }
                if(listFieldValidations[i].Blank_Check__c && cellValue == ''){
                    validationMessage += 'Value required at cell location - ' + cellLocation + '\n';
                }
                if(listFieldValidations[i].Numeric_Value__c && (cellValue.match(/(\d+)/) == null || cellValue.replace(/[0-9 ]/g, '') != '')){
                    validationMessage += 'Number value required at cell location - ' + cellLocation + '\n';
                }
				if(listFieldValidations[i].Alpha_Numeric_Value__c && (cellValue.match(/(\d+)/) == null || cellValue.replace(/[0-9 ]/g, '') == null || cellValue.replace(/[0-9 ]/g, '') == '')){
						validationMessage += 'Alpha numeric value required at cell location - ' + cellLocation + '\n';
				}
                if(listFieldValidations[i].Matches_Substring__c || listFieldValidations[i].Includes_Substring__c){
                    var substringVal = listFieldValidations[i].Substring__c; var subStringArray = []; var errorFound = true;
                    if(substringVal.includes(','))
                        subStringArray = substringVal.split(',');
                    else
                        subStringArray[0] = substringVal;
                    for(var arrayCount=0; arrayCount<subStringArray.length; arrayCount++){
                        if(listFieldValidations[i].Matches_Substring__c && cellValue.replace(/[0-9 ]/g, '').toUpperCase() == subStringArray[arrayCount].toUpperCase()){
                            errorFound = false;
                            break;
                        }
                        if(listFieldValidations[i].Includes_Substring__c && cellValue.replace(/[0-9]/g, '').toUpperCase().includes(subStringArray[arrayCount].toUpperCase())){
                            errorFound = false;
                            break;
                        }
                    }
                    if(errorFound)
                    	validationMessage += listFieldValidations[i].Substring_Error_Message__c + ' at cell location - ' + cellLocation + '. Possible values - ' + substringVal + '.\n';
                }
            }
        }
        return validationMessage;
    },
	cloneRelatedRecord : function(component, newRecord, existingRecord, currentSequenceCount, count){
		newRecord.Sequence__c = currentSequenceCount + count;
		newRecord.Active__c = false;
		newRecord.Field_API_Name__c = existingRecord.Field_API_Name__c;
		newRecord.Field_Data_Type__c = existingRecord.Field_Data_Type__c;
		newRecord.Object_API_Name__c = existingRecord.Object_API_Name__c;
		newRecord.Multiple_Records__c = existingRecord.Multiple_Records__c;
		newRecord.Parent_Object_API_Name__c = existingRecord.Parent_Object_API_Name__c;
		newRecord.Matching_Labels__c = existingRecord.Matching_Labels__c;
		newRecord.Type__c = existingRecord.Type__c;
		newRecord.Field_Validations__c = existingRecord.Field_Validations__c;
		newRecord.Dependent_Field_API_Name__c = existingRecord.Dependent_Field_API_Name__c;
		newRecord.Dependent_Field_DataType__c = existingRecord.Dependent_Field_DataType__c;
		return newRecord;
	},
	calculateRowCounts : function(component, parsedFile, row, endRange, fieldRange, matchingLabelArr, endingLabel){
		var rowCount = 1;
		const get = (p, o) => p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o);
		var nextCellValue = get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],fieldRange+(row+rowCount),'v'], parsedFile);
		while(nextCellValue != null && (row+rowCount) <= endRange && !this.checkContainerNumber(matchingLabelArr, nextCellValue) && nextCellValue != endingLabel){
			rowCount++;
			nextCellValue = get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],fieldRange+(row+rowCount),'v'], parsedFile);
		}
		return rowCount;
	},
	getDataRange : function(component, containerStartingRange, containerEndingRange, row){
		var count = -1;
		for(var dataRowCount = 0; dataRowCount <= containerEndingRange.length; dataRowCount++){
			if(row >= containerStartingRange[dataRowCount] && row <= containerEndingRange[dataRowCount]){
				count = dataRowCount;
				break;
			}
		}
		return count;
	},
	getDescriptionRow : function(component, rowCount, parsedFile, fieldRange, row){
		const get = (p, o) => p.reduce((xs, x) => (xs && xs[x]) ? xs[x] : null, o);
		var description = "";
		for(var descriptionRow = 0; descriptionRow <= rowCount; descriptionRow++){
			var descriptionVal = get(['xlsx', 'Sheets', parsedFile.xlsx.SheetNames[0],String.fromCharCode(fieldRange.charCodeAt(0)+3)+(row+descriptionRow),'v'], parsedFile);
			if(descriptionVal != null && descriptionVal != "")
				description = description + descriptionVal + '\n';
		}
		return description;
	}
})