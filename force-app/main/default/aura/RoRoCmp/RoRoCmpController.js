({
    doInit : function(component, event, helper) {
        var bookingWrapperObj = component.get("v.BookingWrapperObj"); 
        if(bookingWrapperObj.length>0){
            helper.editCargoToContainer(component, event);
        }else{
            helper.addCargoToContainer(component, event);
        }
        helper.getVehiclesData (component, event);
    },
    addItem : function(component, event, helper) {
        helper.addItem(component, event);
       
    },
    removeItem : function(component, event, helper) {
        helper.removeItem(component, event);
       
    },
    getModel : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var selectedId = event.getSource().get("v.value");
        helper.getModel(component, event, selectedId, index);
    },
    populateVehicleDetail : function(component, event, helper){
        var index = event.getSource().get("v.name");
        var selectedId = event.getSource().get("v.value");
        helper.populateVehicleDetail(component, event, selectedId, index);
    },
})