({
    addCargoToContainer: function (component, event) {       
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var objcargo = {};
        objcargo["cargoType"] = 'roro';
        objcargo["listFreightDetailWrapper"] = [{ commodityDesc: '', freightDetail: {}, listCommodityWrapper: [], listRequirementWrapper: [{ requirement: {}, commodityDesc: '' }] }];
        component.set("v.BookingWrapperObj", objcargo);
        console.log(component.get("v.BookingWrapperObj"));
    },

    editCargoToContainer: function (component, event) {
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        BookingWrapperObj[0].cargoType = 'roro';
        if (BookingWrapperObj[0].listFreightDetailWrapper[0].listCommodityWrapper.length > 0) {
            component.set("v.IsHazardous", true);
        }
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
    addItem: function (component, event) {       
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var objcargo = {};
        BookingWrapperObj[0].listFreightDetailWrapper.push({ freightDetail: {}, commodityDesc: '' });
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
    removeItem: function (component, event) {       
        var index = event.target.id;
        console.log(index);
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        BookingWrapperObj[0].listFreightDetailWrapper.splice(index, 1);
        console.log(BookingWrapperObj);
        component.set("v.BookingWrapperObj", BookingWrapperObj);
    },
    getVehiclesData: function (component, event) {
        let manufacture = new Set()
        var action = component.get("c.getVehicles");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var vehicles = response.getReturnValue();
                console.log(vehicles);
                component.set("v.listVehicles",vehicles);
                for (var i = 0; i < vehicles.length; i++) {
                    manufacture.add(vehicles[i].Name);
                 }
                 const manufactureVal= manufacture.values();
                 var listManufacture= component.get("v.listManufacture");
                 for (var i = 0; i < manufacture.size; i++) {
                    listManufacture.push((manufactureVal.next().value));
                 }
                component.set("v.listManufacture",listManufacture);
                
            } else if (state === "ERROR") {
                console.log("error");
            }
            component.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },

    getModel: function (component, event, selectedId, index) {
        var listVehicles = component.get("v.listVehicles");
        var model = [];
        for (var i = 0; i < listVehicles.length; i++) {
            if(listVehicles[i].Name===selectedId){
                model.push(listVehicles[i]);
            }            
        }
        console.log(model);
        var modelData = component.get("v.modelData");
        modelData[index] = model;
        component.set("v.modelData", modelData);
    },
    populateVehicleDetail: function (component, event, selectedId, index) {
        var modelData =  component.get("v.modelData")[index];
        var BookingWrapperObj = component.get("v.BookingWrapperObj");
        var freightDetail = BookingWrapperObj[0].listFreightDetailWrapper[index].freightDetail;
        for (var i = 0; i < modelData.length; i++) {
            if(modelData[i].Model__c.toString()==selectedId){
                freightDetail.Length_Major__c =  modelData[i].Length__c;
                freightDetail.Width_Major__c = modelData[i].Width__c;
                freightDetail.Height_Major__c = modelData[i].Height__c;
                freightDetail.Declared_Weight_Value__c = modelData[i].Weight__c;
                freightDetail.Type__c = modelData[i].Type__c;
                break;
            }   
            
        }
        component.set("v.BookingWrapperObj",BookingWrapperObj);
    },
})