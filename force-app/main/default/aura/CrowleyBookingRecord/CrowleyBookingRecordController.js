({
    init: function (cmp, event, helper) {
        console.log('INIT OF BOOKING RECORD');
        helper.showBookingDetails(cmp);
    },
    openEmailAddressModal : function(component, event, helper){
        component.set("v.isSendEmail", true);
    },
    handleGetRates : function(component, event, helper){
		helper.handleGetRates(component, event, helper);  
    },
    handleValidateIMDG : function(component, event, helper){       
        helper.handleValidateIMDG(component, event, helper);
    },
    closeModel : function(component,event,helper){
        component.set("v.isSendEmail", false);
    },
    sendEmailWithPDF : function(component,event,helper){
        var inputField = component.find('emailIds');
        var allValid = inputField.get('v.validity').valid;
        if (allValid) {
            helper.sendEmailPDF(component, event, helper);
        } else {
            inputField.set('v.validity', {valid:false, badInput :true});
            inputField.showHelpMessageIfInvalid();
        }
    },
})