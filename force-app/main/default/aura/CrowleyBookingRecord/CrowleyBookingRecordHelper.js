({
    sendEmailPDF : function (component, event, helper){
        var action = component.get("c.sendEmailPDF");
        var emailAddress = component.find('emailIds').get('v.value');
        var bookingId = component.get("v.recordId");
        component.set("v.showSpinnerModal", true);
        action.setParams({
            bookingId : bookingId,
            emailAddress : emailAddress
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            if (state === "SUCCESS"){
                toastEvent.setParams({
                    message: 'Email Sent Successfully',
                    type:'success'
                });               
            }else{
                toastEvent.setParams({
                    message: 'Error sending Email',
                    type:'error'
                }); 
            }
            component.set("v.showSpinnerModal", false);
            toastEvent.fire();  
            component.set("v.isSendEmail", false);
        });
        $A.enqueueAction(action);
        console.log("sendEmailPDF Call");
    },  
    
    showBookingDetails : function(cmp) {
        console.log('showBookingDetails()');
        var bookingId = cmp.get("v.recordId");
        cmp.set("v.openModal",true);
        var action = cmp.get("c.fetchIframeUrl");
        action.setParams({
            bookingId : bookingId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.bookingUrl", response.getReturnValue());
                console.log('response.getReturnValue()',response.getReturnValue());
            } else if (state === "ERROR") {
                console.log("error");
                let errors = response.getError();
                let message = '';
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                console.error(message);
            }
            cmp.set('v.isLoading', false);
        });
        $A.enqueueAction(action);
    },   
    
    handleGetRates : function(component, event, helper){
        console.log('Entered handleGetRates');	       
        console.log('Record id : '+component.get('v.bookingId'));       
        
        if(component.get('v.displayRates')==true)
            component.set('v.displayRates',false);
        else if(component.get('v.displayRates')==false)
            component.set('v.displayRates',true);  
        var action =component.get("c.getRates");
        
        action.setParams({
            IdBooking: component.get('v.bookingId')
        });
        
        console.log('Before setCallback');
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                console.log('start res @@@');
                console.log(responseData);
                console.log('stop res @@@');
                component.set("v.ratingData",responseData);
            }
            else if(state === "ERROR"){
                console.log("Error is : ");
                console.log(response.getError());
            }
        });
        console.log('After setCallback');
        $A.enqueueAction(action);
    }, 
    
    handleValidateIMDG : function(component, event, helper){
        
        this.showSpinner(component);
        console.log('Entered handleValidateIMDG in helper');
        var action= component.get("c.validateIMDG");
        action.setParams({
            IdBooking: component.get('v.bookingId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('State is : '+state)
            if (state === "SUCCESS"){                  
                
                var responseData = response.getReturnValue();
                if(responseData=="true"){
                    
                    this.allAttachments(component, event, helper);
                    
                }
                //$A.get('e.force:refreshView').fire();
                //console.log('Response Data : '+responseData);
                var childCom =component.find("bookingDetailComp");
                childCom.set("v.recordId",component.get('v.bookingId'));
                childCom.reInit();
            }
            else if(state=="ERROR"){
                console.log("Error is : ");
                console.log(response.getError());
            }	
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);   
    },  
    
    showSpinner : function(component){
        component.set("v.spinner",true);
    },
    
    hideSpinner : function(component){
        component.set("v.spinner",false);
    }
})