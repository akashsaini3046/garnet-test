({
	getUser : function(component, helper){
        var action = component.get("c.getUser");
        action.setCallback(this, function(response)
                           {
                               var state = response.getState();
                               console.log("user information -> ", response.getReturnValue());
                               if (component.isValid() && state === "SUCCESS")
                               {
                                   component.set("v.user", response.getReturnValue());
                               }
                               else if (state === "ERROR") {
                                   var errors = response.getError();
                                   if (errors) {
                                       if (errors[0] && errors[0].message) {
                                           console.log("Error message: " + 
                                                       errors[0].message);
                                       }
                                   } else {
                                       console.log("Unknown error");
                                   }
                               }
                           });
        $A.enqueueAction(action);
    }
})