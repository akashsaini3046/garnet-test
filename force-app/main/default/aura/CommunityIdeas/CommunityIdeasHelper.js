({
    getURLParameter : function(key) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;
        
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.
            console.log('sParameterName -> ',sParameterName);
            if (sParameterName[0] == key) {
                return sParameterName[1];
            }
        }
        return '';
        //return '087M00000008klqIAA';
    },
    getIdeaDetails : function(cmp, helper, ideaId){
        console.log('inside idea detail')
        
        var action = cmp.get("c.getIdeaDetails");
        console.log(ideaId);
        action.setParams({
            ideaId: ideaId
        });
        console.log(ideaId);
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state'+state);
            if (state === "SUCCESS") {
                console.log('ideaRecord -> ',response.getReturnValue());
                cmp.set('v.ideaRecord', response.getReturnValue());
                cmp.set("v.displayIdeaDetail", true);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
     checkIsUpvotedorDownvoted: function(cmp, helper){
        var ideaRecord = cmp.get('v.ideaRecord');
        var userRecord = cmp.get('v.currUser');
        cmp.set("v.isUpvoted", false);
        cmp.set("v.isDownvoted", false);
        if(ideaRecord && ideaRecord.Votes && ideaRecord.Votes.length>0){
            for(var i=0; i<ideaRecord.Votes.length; i++){
                if(ideaRecord.Votes[i].CreatedById == userRecord.Id){
                    if(ideaRecord.Votes[i].Type=='Up')
                        cmp.set("v.isUpvoted",true);
                    else
                        cmp.set("v.isDownvoted",true);
                }
            }
        }
         console.log('yes');
         cmp.set("v.displayIdeasList",false);
         cmp.set("v.displayIdeaDetail",true);
         cmp.set("v.displayEditIdea",false);
         cmp.set("v.displayNewIdea",false);
    },
    
})