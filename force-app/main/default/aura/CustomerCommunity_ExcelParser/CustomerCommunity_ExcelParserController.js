({
    doInit : function(component, event, helper){
        helper.getFieldLabelList(component);
    },
    onTableImport: function (cmp, evt, helper) {
        helper.disableExcelInput(cmp);
        helper.importTableAndThrowEvent(cmp, evt, helper);
    }
})