({
    selectItem : function(component, event, helper){  
        
        var compEvent = component.getEvent("selectedItemEvent");
        if(component.get("v.buttonText") == "Location" || component.get("v.buttonText") == "Company"){
            var getSelectItem = component.get("v.item.Name"); 
            compEvent.setParams({
                "selectedItem" : getSelectItem, 
                "selectedItemID" : component.get("v.item.Id")
            });
            compEvent.fire();
        }else if(component.get("v.buttonText") == "CompanyBooking"){
            var getSelectItem = component.get("v.item.Name"); 
            compEvent.setParams({
                "selectedItem" : getSelectItem,
                "cvifId" : component.get("v.item.CVIF__c"),
                "selectedItemID" : component.get("v.item.Id")
            });
            compEvent.fire();
        }else if(component.get("v.buttonText") == "TermLocation"){
            var getSelectItem = component.get("v.item.name"); 
            compEvent.setParams({
                "selectedItem" : getSelectItem,
                "selectedItemID" : component.get("v.item.location"),
                "termType" : component.get("v.item.termType"),
                "code" : component.get("v.item.code")
            });
            compEvent.fire();
        }else{
            var getSelectItem = component.get("v.item"); 
            compEvent.setParams({
                "selectedItem" : getSelectItem, 
                "selectedItemID" : null
            });
            compEvent.fire(); 
        }
    }
})