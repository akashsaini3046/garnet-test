({
    handleActive: function (cmp, event) {
        var tab = event.getSource();
        console.log("tab.get('v.id')" + tab.get('v.id'));
        switch (tab.get('v.id')) {
            case 'tab-new-quote':
                this.injectComponent(cmp, 'c:CustomerCommunity_RequestQuotation', tab);
                break;
            case 'tab-all-quote':
                this.injectComponent(cmp, 'c:CustomerCommunity_AllQuotation', tab);
                break;
            case 'tab-view-details':
                this.injectComponent(cmp, 'c:CustomerCommunity_QuoteDetail', tab);
                break;
            case 'tab-new-quote-ui':
                this.injectComponent(cmp, 'c:CustomerCommunity_RequestQuotationNew', tab);
                break;
        }
    },
    injectComponent: function (cmp, name, target) {
        var attr = {};
        if (name === 'c:CustomerCommunity_RequestQuotation' || name === 'c:CustomerCommunity_RequestQuotationNew' ) {
            attr = {
                reQuoteRecordId: cmp.get("v.reQuoteRecordId")
            };
        }
        if (name === 'c:CustomerCommunity_QuoteDetail') {
            attr = {
                quoteId: cmp.get("v.quoteId")
            };
        }
        $A.createComponent(name, attr, function (contentComponent, status, error) {
            console.log(JSON.stringify(contentComponent));
            if (status === "SUCCESS") {
                target.set('v.body', contentComponent);
            } else {
                throw new Error(error);
            }
        });
    }
})