({
	setUserSocialDetails: function (component) {
        var action = component.get("c.fetchUserSocialDetails");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var userSocialDetails = response.getReturnValue();
                if (userSocialDetails != null) {
                    component.set("v.connectedSocialAccounts", userSocialDetails);
                }else{
                    component.set("v.connectedSocialAccounts", null);
                }
            } else if (state === "ERROR") {
                console.log("error");
            }
        });
        $A.enqueueAction(action);
    },
    
    handlerDisconnect : function (component, event, helper){
        var socialId = event.target.id;
        var action = component.get("c.disconnectSocialAccount");
        console.log(socialId);
        action.setParams({ 
            socialId : socialId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                this.setUserSocialDetails(component, event, helper);
            } else if (state === "ERROR") {
                console.log("error");
            }
        });
        $A.enqueueAction(action);
    }
})