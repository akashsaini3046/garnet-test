({
	 doInit : function(component, event, helper) {
        helper.highlightListItems(component, event);
        var pageReference = component.get("v.pageReference");
        var selectedTab= decodeURIComponent(window.location.search.substring(1));
        console.log('-->'+selectedTab);
		component.set("v.selectedSequence", selectedTab);
        //helper.FetchListItems(component);
    },
    changeHighlightSection : function(component, event, helper) {
        helper.highlightListItems(component, event);
    }
})