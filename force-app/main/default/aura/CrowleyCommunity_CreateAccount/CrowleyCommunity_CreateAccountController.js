({
    validateUser : function(component,event,helper){
        helper.validateUser(component,event);
    },
    insertAccount : function(component,event,helper){
        var newAccount = event.getParam("newAccount");
        var selectedItemId = event.getParam("selectedItemID");
        component.set("v.addNewAccount",newAccount);  
        component.set("v.selectedAccountId",selectedItemId);
    },
    doInit : function(component, event, helper){
        if(component.get('v.companyName') != null){
            component.set("v.search",component.get('v.companyName'));
        } 
        if(component.get("v.userEmail") != null){
            component.set("v.userName",component.get("v.userEmail"));
        }
        if(component.get("v.newContact") == false){
            helper.setContactDetails(component,event);
            component.set("v.disableFields",true);
        }
    },
    navigateToOtpScreen : function(component,event,helper){
        helper.navigateToOtpScreen(component,event);
    },
    keyPressController : function(component,event,helper){
        //helper.keyPressController(component,event);
    },
    handleOnRender :  function(component,event,helper){
         var elem= component.find("getDeviceHeight").getElement();
        var winHeight = window.innerHeight;
        elem.style.height = winHeight + "px";
    }
})