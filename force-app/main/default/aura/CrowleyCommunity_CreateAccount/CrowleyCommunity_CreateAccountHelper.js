({
    keyPressController : function(component,event){
        var userName = component.get("v.userName");
        if(userName == ""){
           component.set("v.showCheckIcon",false);  
        }
        var action = component.get("c.checkforDuplicate");
        action.setParams({
            userName : userName
        })
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                if(response == 'True'){
                   component.find("emailUser").set('v.validity', {valid:false, badInput :true}); 
                   component.find("emailUser").showHelpMessageIfInvalid(); 
                   
                 }
                else if(response == 'False'){
                    if(component.get("v.userName") != ""){ 
                        //component.set("v.showCheckIcon",true); 
                    }
                }
                else if(reponse == 'Error'){
                   component.set("v.ErrorMessage","Something went Wrong. Please try Again !"); 
                   component.set("v.showErrorMessage",true);
                }
                else{
                    component.set("v.ErrorMessage","Something went Wrong. Please try Again !"); 
                    component.set("v.showErrorMessage",true);     
                } 
             }    
             else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);  
    },
    navigateToAddressScreen : function(component,event){
        var isValid = this.checkValidations(component,event)
        if(isValid){
            component.set("v.showerrorMessage",false);
            component.set("v.accountLookUpValidity",false);
            var homeScreenEvent = component.getEvent("HomeScreenEvent");
            homeScreenEvent.setParams({
                "HomeScreenComponent": "c:CrowleyCommunity_CreateBusinessLocation",
                'companyName' : component.get('v.search'),
                'firstName' : component.get('v.userFirstName'),
                'lastName' : component.get('v.userLastName'),
                'userName' : component.get('v.userName'),
                'phoneNumber' : component.get('v.userPhoneNumber'),
                'addNewAccount' : component.get('v.addNewAccount'),
                'newContact' : component.get('v.newContact'),
                'selectedAccountId' : component.get('v.selectedAccountId'),
                'selectedContactId' : component.get('v.selectedContactId')
            });
            homeScreenEvent.fire();
        }
    },
    setContactDetails : function(component,event){    
        var action = component.get("c.getContactDetails");
        var contactEmail = component.get("v.userEmail")
        action.setParams({
            conEmail : contactEmail
        })
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                if(response != null ){
                    component.set("v.selectedContactId",response['Id']);
                    component.set("v.selectedAccountId",response['Account']['Id'])
                    console.log('selectedContactId' + component.get('v.selectedContactId'));
                    component.set("v.userFirstName",response['FirstName']);
                    component.set("v.userLastName",response['LastName']);
                    component.set("v.search",response['Account']['Name']); 
                    component.set("v.userPhoneNumber",response['Phone']);
                }
                console.log(response)         
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);   
    }, 
    validateUser: function (component, event) {
        var action = component.get("c.checkforDuplicate");
        var userName = component.get("v.userName");
        action.setParams({
            userName : userName
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseServer = response.getReturnValue();
                if(responseServer == 'True'){
                    component.set("v.errorMessage",'The UserName already Exist. Please try with a Different Name');
                    component.set("v.showerrorMessage",true);
                }
                else if(responseServer == 'False'){
                    this.navigateToAddressScreen(component,event);
                }
                else{
                    component.set("v.errorMessage",'Something Went Wrong. Please try Again.');
                    component.set("v.showerrorMessage",true);       
              }
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    navigateToOtpScreen : function(component,event){
        component.set("v.showerrorMessage",false)
        var homeScreenEvent = component.getEvent("HomeScreenEvent");
        homeScreenEvent.setParams({
            "HomeScreenComponent": "c:CrowleyCommunity_OTPGeneration"      
        });
        homeScreenEvent.fire();  
    },
    checkValidations : function(component,event){ 
        component.set("v.accountLookUpValidity",true);
        var inputField = component.find("emailUser");
        var inputFieldValue = inputField.get('v.value');
        var userNameValid = true;
        if(inputFieldValue == "" || typeof(inputFieldValue) == "undefined"){
            userNameValid = inputField.showHelpMessageIfInvalid();      
        }
        var fieldValidity = component.find('FormVal').reduce(function (validFields, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        if(fieldValidity && userNameValid)
            return true;
        else
            return false;
    },
    validateEmail : function(userName){
        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var result = userName.match(pattern);
        return result;
    },
    ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    },
})