({
    getUserFieldsInit: function (component, event) {
        var action = component.get("c.getUserFields");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseServer = response.getReturnValue();
                component.set("v.userFields", responseServer);
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    getPicklistValues: function (component, event) {
        var action = component.get("c.getCountries");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseServer = response.getReturnValue();
                component.set("v.CountriesList", responseServer);
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    userSignUp: function (component, event) {
        var validForm = component.find('FormVal').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        if (validForm) {
            var userObj = component.get("v.userFields");
            var action = component.get("c.checkIfAccountExists");
            action.setParams({
                "companyName": userObj.CompanyName__c,
                "firstName": userObj.FirstName__c,
                "lastName": userObj.LastName__c,
                "email": userObj.Email__c,
                'title': userObj.Title__c,
                'country': userObj.Country__c
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var contactId = response.getReturnValue();
                    if (contactId == 'EXCEPTION') {
                        component.set("v.UserSuccessMsg", 'User already exists in the system. To reset your password, click on "Forgot Password".');
                        component.set("v.isError", false);
                        component.set("v.OnUserSuccess", true);
                    } else if (contactId == 'EXCEPTION1') {
                        component.set("v.UserSuccessMsg", 'User already exists in the system. To reset your password, click on "Forgot Password".');
                        component.set("v.isError", true);
                        component.set("v.OnUserSuccess", true);
                    } else {
                        component.set("v.NewContactId", contactId);
                        component.set("v.CreateUser", true);
                    }
                } else if (state === "ERROR") {
                    this.ErrorHandler(component, response);
                }
            });
            $A.enqueueAction(action);
        }
    },
    signupNewUser: function (component, event) {
        var userObj = component.get("v.userFields");
        var conId = component.get("v.NewContactId");
        var action = component.get("c.registerUser");

        action.setParams({
            'objUserFilter': {
                'FirstName': userObj.FirstName__c,
                'Email': userObj.Email__c,
                'CompanyName': userObj.CompanyName__c,
                'LastName': userObj.LastName__c,
                'UserName': userObj.UserName__c,
                'Title': userObj.Title__c,
                'PhoneNumber': userObj.PhoneNumber__c,
                'Country': userObj.Country__c
            },
            'contactId': conId
        })
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var IfUserCreated = response.getReturnValue();
                if (IfUserCreated === 'True') {
                    component.set("v.UserSuccessMsg", 'User created successfully!! Activation link has been sent to your Email Id');
                    component.set("v.isError", false);
                    component.set("v.OnUserSuccess", true);
                } else if (IfUserCreated === 'False') {
                    component.set("v.UserSuccessMsg", 'User with this Email Id already exists. To reset password click on "Forgot Password".');
                    component.set("v.isError", true);
                    component.set("v.OnUserSuccess", true);
                } else if (IfUserCreated === 'Exists') {
                    component.set("v.UserSuccessMsg", 'Username already exists. Please enter a unique username.');
                    component.set("v.isError", true);
                    component.set("v.OnUserSuccess", true);
                } else if (IfUserCreated === 'Error') {
                    component.set("v.UserSuccessMsg", 'Something went wrong. Please try Again.');
                    component.set("v.isError", true);
                    component.set("v.OnUserSuccess", true);
                }
            } else if (state === 'ERROR') {
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    },
})