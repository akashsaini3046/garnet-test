({
    changeVisibility : function(component, event, helper) {
        var isVisible = event.getParam("Visibility");
        component.set("v.IsLevel2Customer",isVisible);
         window.setTimeout($A.getCallback(function () {
            if (component.isValid()) {
                 $A.util.addClass(document.getElementById('dashboardDiv'), 'levelup');
            } else {
                console.log('Component is Invalid');
            }
        }), 500);
    }
})