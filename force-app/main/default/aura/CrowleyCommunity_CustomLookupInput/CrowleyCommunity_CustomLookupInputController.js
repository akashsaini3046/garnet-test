({
    onChangeSearchText : function(component){
        var inputValue = component.find("inputSearch").get("v.value");
        component.set("v.selectedItem", inputValue);
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    
    keyPressController : function(component, event, helper) {
        component.set("v.loading", true);
        var getInputkeyWord = component.get("v.SearchKeyWord");
        if( getInputkeyWord !== "" && getInputkeyWord!=null ){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component, event, getInputkeyWord);
        } 
        else {
           /* component.set("v.listOfSearchRecords", Object.keys(component.get("v.portsToDisplayMap")));
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');  */
                
       		component.set("v.listOfSearchRecords", null); 
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open'); 
            component.set("v.Message", 'Search Result...'); 
            
            component.set("v.loading", false);  
        }
    },
    handleComponentEvent : function(component, event, helper) {	 
        var selectedItemGotFromEvent = event.getParam("selectedItem");
        component.set("v.selectedItem" , selectedItemGotFromEvent); 
        component.find("inputSearch").set("v.value", selectedItemGotFromEvent)
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open'); 
              
        var compntEvent = component.getEvent("changeItemEvent");
        compntEvent.fire();
    },      
    onBlurInputSearch : function(component, event, helper) {
        /*component.set("v.selectedItem" , component.get("v.itemCopy")); 
        var compntEvent = component.getEvent("changeItemEvent");
        compntEvent.fire();*/
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        
    }
})