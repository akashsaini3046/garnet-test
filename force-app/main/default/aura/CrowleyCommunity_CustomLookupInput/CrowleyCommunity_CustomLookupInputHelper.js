({
    searchHelper : function(component, event, getInputkeyWord) {
        var functionality = component.get("v.functionality");
        if(functionality === "findARoute:Ports"){
            //this.findARoutePorts(component, event, getInputkeyWord);
           this.findARoutePortsTest(component, event, getInputkeyWord);
        }
    },
    
    findARoutePortsTest : function (component, event, getInputkeyWord){
        var portsList=[];
        var portsMap = component.get("v.portsToDisplayMap");
        for(var ports in portsMap){
            var index =ports.indexOf(getInputkeyWord.toUpperCase());
            if(index !== -1){
                portsList.push(ports);
            }
        }
        if (portsList===null) {
            component.set("v.Message", 'No Result Found...');
        } 
        else {
            component.set("v.Message", 'Search Result...');
        }
        component.set("v.listOfSearchRecords", portsList);
        component.set("v.loading", false);
    },
    
    findARoutePorts : function (component, event, getInputkeyWord){
        var action = component.get("c.fetchPorts");
        action.setParams({
            searchKeyWord : getInputkeyWord,
            shipmentType : component.get("v.shipmentType")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var responseData = response.getReturnValue();
                if (responseData.length == 0) {
                   component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", 'Search Result...');
                }
                component.set("v.listOfSearchRecords", responseData);
            }else if(state === "ERROR"){
                console.log("Error in fetchFCLPorts !");
            }
            component.set("v.loading", false);
        });
        $A.enqueueAction(action);
    }
})