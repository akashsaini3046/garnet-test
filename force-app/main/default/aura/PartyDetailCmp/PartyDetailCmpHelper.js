({
    customerAccount: function (component, event) {
        var action = component.get("c.customerAccount");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                console.log("responseData", JSON.stringify(responseData));
                component.set("v.customerAccount", responseData);
                component.set("v.showErrorMessage", false);
                component.set("v.errorMessage", "");
                //this.populateCustomerRecord(component, event);
                //this.partyAssignment(component, event);
            }
            else {
                component.set("v.contactRecordDetail", null);
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Contact Details could not be fetched. Please try again.");
            }
        });
        $A.enqueueAction(action);
    },
    getPartyRecords: function (component, event, cvifcode, type, contactId) {
        //alert(contactId);
        //alert(cvifcode);
        var action = component.get("c.fetchContactsAndAddresses");
        action.setParams({ partyCode: cvifcode });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                if (responseData != null) {
                    console.log(responseData);
                    if (type == 'customer') {
                        component.set("v.customerContactRecords", responseData);
                        if (contactId == '') {
                            this.setCustomerData(component, event, responseData[0].ContactRecord.Id);
                        }
                    }
                    if (type == 'shipper') {
                        component.set("v.shipperContactRecords", responseData);
                        if (contactId == '') {
                            this.populateShipperData(component, event, responseData[0].ContactRecord.Id);
                        }
                    }
                    if (type == 'consignee') {
                        component.set("v.consigneeContactRecords", responseData);
                        if (contactId == '') {
                            this.populateConsigneeData(component, event, responseData[0].ContactRecord.Id);
                        }
                    }
                }
                component.set("v.showErrorMessage", false);
                component.set("v.errorMessage", "");
            }
            else {
                component.set("v.CustomerContactRecords", null);
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Selected Location not found. Please re-check.");
            }
        });
        $A.enqueueAction(action);
    },
    setCustomerData: function (component, event, selectedContact) {
        var bookingWrapper = component.get("v.BookingWrapper");
        var customer = component.get("v.customer");
        var partylist = component.get("v.customerContactRecords");
        for (var i = 0; i < partylist.length; i++) {
            if (partylist[i].ContactRecord.Id == selectedContact) {
                this.setPartyData(component, bookingWrapper.mapParty.Customer[0], partylist[i].AddressRecord, partylist[i].ContactRecord);
                customer = bookingWrapper.mapParty.Customer[0];
                //console.log(component,bookingWrapper.mapParty.Customer[0]);
                break;
            }
        }
        component.set("v.customer", customer);
        console.log(component.get("v.customer"));
    },
    populateShipperData: function (component, event, selectedContact) {
        var bookingWrapper = component.get("v.BookingWrapper");
        var shipper = component.get("v.shipper");
        var partylist = component.get("v.shipperContactRecords");
        for (var i = 0; i < partylist.length; i++) {
            if (partylist[i].ContactRecord.Id == selectedContact) {
                this.setPartyData(component, bookingWrapper.mapParty.shiper[0], partylist[i].AddressRecord, partylist[i].ContactRecord);
                shipper = bookingWrapper.mapParty.shiper[0];
                break;
            }
        }
        component.set("v.shipper", shipper);
        console.log(component.get("v.shipper"));
    },
    populateConsigneeData: function (component, event, selectedContact) {
        var bookingWrapper = component.get("v.BookingWrapper");
        var consignee = component.get("v.consignee");
        var partylist = component.get("v.consigneeContactRecords");
        for (var i = 0; i < partylist.length; i++) {
            if (partylist[i].ContactRecord.Id == selectedContact) {
                this.setPartyData(component, bookingWrapper.mapParty.consignee[0], partylist[i].AddressRecord, partylist[i].ContactRecord);
                consignee = bookingWrapper.mapParty.consignee[0];
                break;
            }
        }
        component.set("v.consignee", consignee);
    },
    setPartyData: function (component, partydata, addressRecord, contactRecord) {
        if (addressRecord) {
            partydata.Name = addressRecord.Account_Name__c;
            partydata.Address_Line1__c = addressRecord.Name;
            partydata.Address_Line2__c = addressRecord.Address_Line_2__c;
            partydata.City__c = addressRecord.City__c;
            partydata.Country__c = addressRecord.Country__c;
            partydata.State__c = addressRecord.State_Picklist__c;
            partydata.Zip__c = addressRecord.Postal_Code__c;
            partydata.CVIF_Location_Code__c = addressRecord.Location_Code__c;
        }
        if (contactRecord) {
            partydata.Contact_Name__c = contactRecord.Name;
            partydata.CVIF__c = contactRecord.CVIF_Account_Id__c;
            partydata.Email_Address__c = contactRecord.Email;
            partydata.Fax_Number__c = contactRecord.Fax;
            partydata.Phone_Number__c = contactRecord.Phone;
            partydata.Internal_Hidden__c = contactRecord.Id
        }

    },

    partyAssignment: function (component, event) {
        var bookingWrapper = component.get("v.BookingWrapper");
        var customer = component.get("v.customer");
        var shipper = component.get("v.shipper");
        var consignee = component.get("v.consignee");
        customer = bookingWrapper.mapParty.Customer[0];
        customer.CVIF__c = component.get("v.selectedCustomer");
        shipper = bookingWrapper.mapParty.shiper[0];
        consignee = bookingWrapper.mapParty.consignee[0];
        component.set("v.customer", customer);
        component.set("v.shipper", shipper);
        component.set("v.consignee", consignee);
        console.log(customer.Internal_Hidden__c);
        //alert(customer.CVIF__c);
        if (!customer.Internal_Hidden__c) {
            customer.Internal_Hidden__c = '';
            // alert(customer.Internal_Hidden__c);
        }

        this.getPartyRecords(component, event, customer.CVIF__c, 'customer', customer.Internal_Hidden__c);
        this.getPartyRecords(component, event, shipper.CVIF__c, 'shipper', shipper.Internal_Hidden__c);
        this.getPartyRecords(component, event, consignee.CVIF__c, 'consignee', consignee.Internal_Hidden__c);
    },

    getNotifyForwarderRecords: function (component, event, cvifcode, type, index) {
        var action = component.get("c.fetchContactsAndAddresses");
        action.setParams({ partyCode: cvifcode });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var responseData = response.getReturnValue();
                if (responseData != null) {
                    console.log(responseData);
                    if (type == 'notify') {
                        var notifyMap = component.get("v.notifyMap");
                        notifyMap[index] = response.getReturnValue();
                        console.log(notifyMap);
                        component.set("v.notifyMap", notifyMap);
                        //this.setCustomerData(component, event, responseData[0].ContactRecord.Id);                 
                    }
                    if (type == 'forwarder') {
                        var forwarderMap = component.get("v.forwarderMap");
                        forwarderMap[index] = response.getReturnValue();
                        console.log(forwarderMap);
                        component.set("v.forwarderMap", forwarderMap);
                    }
                }
                component.set("v.showErrorMessage", false);
                component.set("v.errorMessage", "");
            }
            else {
                component.set("v.CustomerContactRecords", null);
                component.set("v.showErrorMessage", true);
                component.set("v.errorMessage", "Selected Location not found. Please re-check.");
            }
        });
        $A.enqueueAction(action);
    },
    populateForwarderData: function (component, event, selectedContact, index) {
        //alert(selectedContact);
        var bookingWrapper = component.get("v.BookingWrapper");
        //var forwarder = component.get("v.forwarders");
        var partylist = component.get("v.forwarderMap")[index];
        for (var i = 0; i < partylist.length; i++) {
            if (partylist[i].ContactRecord.Id == selectedContact) {
                this.setPartyData(component, bookingWrapper.mapParty.forwarder[index], partylist[i].AddressRecord, partylist[i].ContactRecord);
                //forwarder[index] = bookingWrapper.mapParty.forwarder[index];
                break;
            }
        }
        component.set("v.BookingWrapper", bookingWrapper);
    },
    populateNotifyData: function (component, event, selectedContact, index) {
        // alert(selectedContact);
        var bookingWrapper = component.get("v.BookingWrapper");
        var partylist = component.get("v.notifyMap")[index];
        for (var i = 0; i < partylist.length; i++) {
            if (partylist[i].ContactRecord.Id == selectedContact) {
                this.setPartyData(component, bookingWrapper.mapParty.notify[index], partylist[i].AddressRecord, partylist[i].ContactRecord);
                //notify[index] = bookingWrapper.mapParty.notify[index];
                break;
            }
        }
        component.set("v.BookingWrapper", bookingWrapper);
    },
})