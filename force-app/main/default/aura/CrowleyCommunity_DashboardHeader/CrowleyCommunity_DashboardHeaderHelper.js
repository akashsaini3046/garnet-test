({
	  abc: function (component, event) {
        var verticalMenuDiv = component.find('verticalMenuDiv');
        var isCollapsed = $A.util.hasClass(verticalMenuDiv, "inactive");
          //alert(isCollapsed);
        if (isCollapsed) {
            $A.util.removeClass(verticalMenuDiv, "inactive");
            $A.util.addClass(verticalMenuDiv, "active");
        } else {
            $A.util.removeClass(verticalMenuDiv, "active");
            $A.util.addClass(verticalMenuDiv, "inactive");
        }
    },
    
     userOptionSelect: function (component, event) {
        var selectedMenuItemValue = event.getParam("value");
        var MenuItem = component.get("v.MenuItemOne");
        if (selectedMenuItemValue === MenuItem) {
            window.location.replace("/CustomerCommunity/secur/logout.jsp");
        }
        if (selectedMenuItemValue === "My Settings") {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                //"url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/settings/" + component.get("v.UserId"),
                "url": "https://comdev-crowleydev.cs77.force.com/CustomerCommunity/s/undermaintenance/"
            });
            urlEvent.fire();
        }
        if (selectedMenuItemValue === "My Profile") {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                //"url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/profile/" + component.get("v.UserId"),
                "url": "https://comdev-crowleydev.cs77.force.com/CustomerCommunity/s/undermaintenance/"
            });
            urlEvent.fire();
        }
        if(selectedMenuItemValue === "My Preferences"){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                //"url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/preferences"
                "url": "https://comdev-crowleydev.cs77.force.com/CustomerCommunity/s/undermaintenance/"
            });
            urlEvent.fire();
        }
    },
    fetchUserDetails: function (component, event) {
        var action = component.get("c.getUserName");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (response.getReturnValue() != null) {
                var userDetail = JSON.parse(response.getReturnValue());
                component.set("v.UserName", userDetail.userName);
                component.set("v.UserId", userDetail.UserId);
            }
        });
        $A.enqueueAction(action);
    },
    handleExpandCollapse: function (component, event) {
        var isCollapsed = $A.util.hasClass(document.getElementById("verticalMenuDiv"), "fliph");
        if (!isCollapsed) {
            $A.util.addClass(document.getElementById("verticalMenuDiv"), "fliph");
            $A.util.addClass(document.getElementById("DashboardContentDiv"), "dasboard-content-lg");
        } else {
            $A.util.removeClass(document.getElementById("verticalMenuDiv"), "fliph");
            if ($A.util.hasClass(document.getElementById("DashboardContentDiv"), "dasboard-content-lg")) {
                $A.util.removeClass(document.getElementById("DashboardContentDiv"), "dasboard-content-lg");
            }
        }
    }
})