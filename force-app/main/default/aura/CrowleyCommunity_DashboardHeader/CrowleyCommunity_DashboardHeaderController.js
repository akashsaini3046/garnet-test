({
    expandCollapseSection: function (component, event, helper) {
        helper.handleExpandCollapse(component, event);
    },
    handleSelect: function (component, event, helper) {
        helper.userOptionSelect(component, event);
    }, 
    doInit : function(component,event,helper){
        helper.fetchUserDetails(component, event);
    },
    abc : function(component,event,helper){
        helper.abc(component, event);
    },
})