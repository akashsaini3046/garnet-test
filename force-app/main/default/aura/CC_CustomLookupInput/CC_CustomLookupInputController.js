({
    onChangeSearchText : function(component){
        var inputValue = component.find("inputSearch").get("v.value");
        if(inputValue.length > 0)
            component.set("v.displayCrossButton", true);
        if(inputValue.length === 0)
            component.set("v.displayCrossButton", false);
        component.set("v.selectedItem", inputValue);
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    
    keyPressController : function(component, event, helper) {
         localStorage.clear();
        sessionStorage.clear();
        component.set("v.loading", true);
        var getInputkeyWord = component.get("v.SearchKeyWord");
        if( getInputkeyWord !== "" && getInputkeyWord!=null ){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component, event, getInputkeyWord);
        } 
        else {
            component.set("v.listOfSearchRecords", null);
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open'); 
            component.set("v.Message", 'Search Result...'); 
            
            component.set("v.loading", false);  
        }
    },
    handleComponentEvent : function(component, event, helper) {	 
        var selectedItemGotFromEvent = event.getParam("selectedItem");
        component.set("v.selectedItem" , selectedItemGotFromEvent); 
        component.find("inputSearch").set("v.value", selectedItemGotFromEvent)
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open'); 
        var compntEvent = component.getEvent("changeItemEvent");
        var parComAtr = component.get("v.parentCompAttribute");
        compntEvent.setParams({
            "parentComponentAttribute" : parComAtr
        });
        compntEvent.fire();
    },      
    onBlurInputSearch : function(component, event, helper) {
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');       
    },

    clearSearch : function(component, event, helper) {
        component.set("v.displayCrossButton", false);
        component.set("v.listOfSearchRecords",null);
        component.set("v.selectedItem" , null); 
         component.set("v.SearchKeyWord" , null); 
        var compEvent = component.getEvent("clearSearchEvent");
        var parComAtr = component.get("v.parentCompAttribute");
        compEvent.setParams({
            "parentComponentAttribute" : parComAtr
        });
        compEvent.fire();
    },
})