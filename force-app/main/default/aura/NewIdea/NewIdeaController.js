({
	doInit : function(cmp, event, helper) {
        helper.getIdeaFieldDescribeResultAction(cmp, helper);
        helper.getIdeaDescribeResultAction(cmp, helper);
		var ideaId = helper.getURLParameter('id');
        if(ideaId){
			cmp.set('v.ideaId', ideaId);
        	helper.getIdeaDetails(cmp, helper, ideaId);
        }else{
            cmp.set('v.ideaRecord',{});
        }
        
        helper.getZonesList(cmp, helper);
       // helper.getIdeaStatuses(cmp, helper);
       // helper.getCategories(cmp, helper);
       // helper.getBenefits(cmp, helper);
    },

    handleCategoriesChange: function (cmp, event) {
        // This will contain an array of the "value" attribute of the selected options
        var selectedOptionValue = event.getParam("value");
        var categories = selectedOptionValue.toString().split(',').join(';');
        console.log('handleCategoriesChange : categories -> ', categories);
        cmp.set('v.ideaRecord.Categories', categories);
        console.log('Categories values -> ',cmp.get('v.categoriesValues'));
    },
    
    saveIdea: function(cmp, event, helper){
        helper.saveIdea(cmp, event, helper);
    },
    
    cancel: function(cmp){
        window.location.href = $A.get("$Label.c.Ideas_List_Page");
    },
    
    onZoneChange: function(cmp, event, helper){
        console.log(cmp.get('v.ideaRecord'));
        console.log('onZoneChange', cmp.find('zones').get('v.value'));
        cmp.set('v.ideaRecord.CommunityId', cmp.find('zones').get('v.value'));
        helper.getRelevantRecordTypeId(cmp,helper,cmp.get("v.ideaRecord.CommunityId"));
    },
    
    findSimilarIdeasKeyupHandler: function(cmp, event, helper){
        console.log('find similar idea for title -> ', cmp.find('title').get('v.value'));
        helper.findSimilarIdeasAction(cmp, helper, cmp.get('v.ideaRecord.CommunityId'), cmp.find('title').get('v.value'));
    },
    
    handleFilesChange: function(cmp, event){
        var fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        cmp.set("v.fileName", fileName);
    },
    
    handleFileUploadEvent: function(cmp, event, helper){
        var state = event.getParam("state");
        if(state=="SUCCESS"){
            helper.showToast("success", "Success", "The record has been updated successfully");
            window.location.href = $A.get("$Label.c.Idea_Detail_Page")+'?id='+cmp.get("v.ideaId");
        }else{
            helper.showToast("error","Failed", event.getParam("message"));
        }
    }
})