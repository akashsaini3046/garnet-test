({
	doInit : function(component, event, helper) {
		helper.fireHighlightEvent(component, event);
        helper.getRecordDetails(component);
	}
})