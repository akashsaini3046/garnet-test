({
	fireHighlightEvent : function(component, event){
		var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({"selectedMenu" : compname });
        appEvent.fire();
	},
    getRecordDetails : function(component) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'), sParameterName, i, knwArticleId;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === 'articleId' && sParameterName[1] != null) {
                component.set("v.articleId",sParameterName[1]);
                knwArticleId = sParameterName[1];
            }
        }
        if(knwArticleId) {
            var action = component.get("c.getArticleDetails");
            action.setParams({ articleId :knwArticleId});
            action.setCallback(this, function(response){
                var state = response.getState();
                if(response.getReturnValue()!=null){
                    component.set("v.article",response.getReturnValue());
                }
                else{
                    component.set("v.article",null);
                } 
            });
            $A.enqueueAction(action);
        }
    }
})