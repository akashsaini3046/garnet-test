({
    init: function (cmp, event, helper) {
        cmp.set('v.columns', helper.getColumnDefinitions());
        helper.getTotalNumberOfBillOfLadings(cmp);
        helper.getDataInDataTable(cmp, 'CreatedDate', 'desc');
        helper.getCommunityUrlPathPrefix(cmp,event,helper);
    },
    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    updateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set('v.selectedRowsCount', selectedRows.length);
    },

    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        console.log(row.Id + " : " + action.name + " : " + JSON.stringify(action));
        switch (action.name) {
            case 'view_details':
                helper.showBillOfLAdingInformation(cmp, row, event);
                break;
        }
    },

    handleLoadMoreBillOfLadings: function (component, event, helper) {
        console.log('handleLoadMoreBillOfLadings');
        component.set('v.isLoading', true);
        helper.getMoreBillOfLadings(component, component.get("v.sortedBy"), component.get("v.sortedDirection"));
    },
})