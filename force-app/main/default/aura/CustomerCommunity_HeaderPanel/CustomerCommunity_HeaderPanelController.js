({
    doInit: function (component, event, helper) {
        helper.fetchUserDetails(component, event);
    },
    goToDashboard: function (component, event, helper) {
        helper.goToDashboardHelper(component, event);
    },
    handleSelect: function (component, event, helper) {
        helper.userOptionSelect(component, event);
    },
    ExpandCollapseSection: function (component, event, helper) {
        helper.handleExpandCollapse(component, event);
    }
})