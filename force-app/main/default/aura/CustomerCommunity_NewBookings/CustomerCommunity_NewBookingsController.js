({
    doInit : function(component, event, helper) {
        helper.fetchIncoTerms(component);
        helper.fetchReceiptDeliveryTerms(component);
        helper.fetchCommodityRecords(component);
        helper.fetchPackageTypeRecords(component);
        helper.fetchContainerTypeRecords(component);
        helper.createBlankBooking(component, event);
        helper.fetchCFSLocations(component, event);
        helper.formatDate(component);
        helper.getRecordBooking(component);
        helper.columnSubstances(component);
    },
    dateUpdate : function(component, event, helper) {
        
     
        /*var dateValue = component.find("ReadyDate").get("v.value");
        if(dateValue == null )
        	component.set("v.dateValidationError" , true);
        else
            component.set("v.dateValidationError",false);*/
    },
    fetchCustomerRecords : function(component, event, helper) {
        //if(event.which == 13){
            component.set("v.autoFillCustomerFields", false);
            helper.getCustomerRecords(component, event);
        //}
    },
    fetchShipperRecords : function(component, event, helper) {
        //if(event.which == 13){
        	helper.getShipperRecords(component, event);
        //}
    },
    fetchConsigneeRecords : function(component, event, helper) {
        //if(event.which == 13){
        	helper.getConsigneeRecords(component, event);
        //}
    },
    updateAddressData : function(component, event, helper){
        helper.updateAddressField(component, event);
    },
    updateOriginValues : function(component, event, helper){
    	var recTerm = component.find("ReceiptTerm").get("v.value");
        component.set("v.originLocation","");
        component.set("v.originCodeValue","");
        component.set("v.OriginLocation","");
        if(recTerm == 'D'){
            component.set("v.disableOriginCode", false);
            component.set("v.originCodePlaceholder","Enter Zip Code");
            component.set("v.originPlaceholder","Enter Zip Code to get location"); 
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
        else if(recTerm == 'P'){
            component.set("v.disableOriginCode", false);
            component.set("v.originCodePlaceholder","Enter Port Code");
            component.set("v.originPlaceholder","Enter Port Code to get location");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
        else if(recTerm == 'CFS'){
            component.set("v.originPlaceholder","Select a CFS Origin Location");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
        else{
            component.set("v.disableOriginCode", true);
            component.set("v.originCodePlaceholder","Select Receipt Term");
            component.set("v.originPlaceholder","Select Receipt Term and then enter Origin Code");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
    },
    getOriginLocation : function(component, event, helper){
        helper.getOriginLocation(component, event);
    },
    updateDestinationValues : function(component, event, helper){
    	var delTerm = component.find("DeliveryTerm").get("v.value"); 
        component.set("v.destinationLocation","");
        component.set("v.destinationCodeValue","");
        component.set("v.destinationLocation","");
        if(delTerm == 'D'){
            component.set("v.disableDestinationCode", false);
            component.set("v.destinationCodePlaceholder","Enter Zip Code");
            component.set("v.destinationPlaceholder","Enter Zip Code to get location");
            component.set("v.showErrorMessage",false); 
            component.set("v.errorMessage","");
        }
        else if(delTerm == 'P'){
            component.set("v.disableDestinationCode", false);
            component.set("v.destinationCodePlaceholder","Enter Port Code");
            component.set("v.destinationPlaceholder","Enter Port Code to get location");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
        else if(delTerm == 'CFS'){
            component.set("v.destinationPlaceholder","Select a CFS Destination Location");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
        else{
            component.set("v.disableDestinationCode", true);
            component.set("v.destinationCodePlaceholder","Select Delivery Term");
            component.set("v.destinationPlaceholder","Select Delivery Term and then enter Origin Code");
            component.set("v.showErrorMessage",false);
            component.set("v.errorMessage","");
        }
    },
    getDestinationLocation : function(component, event, helper){
        helper.getDestinationLocation(component, event);
    },
    createCargoDetailElement : function(component, event, helper) {
        var reqArr = component.get("v.requirementsList");
        var reqArrLimit = component.get("v.requirementsListLimit");
        var bookingRec = component.get("v.bookingRecordValues"); 
        if(bookingRec.listCargoDetails.length < reqArrLimit){
            helper.createBlankFreightDetail(component);
        }
    },
    removeCargoDetailElement : function(component, event) {
        var elementId = event.currentTarget.id;
        var bookingRec = component.get("v.bookingRecordValues"); 
        var isHazardousBooking = false;
        var newCargoArr = []; var count = 1;
        for(var i=0; i<bookingRec.listCargoDetails.length; i++){
            if(bookingRec.listCargoDetails[i].sequenceId != elementId){
                bookingRec.listCargoDetails[i].sequenceId = count++;
                newCargoArr.push(bookingRec.listCargoDetails[i]);
                if(bookingRec.listCargoDetails[i].isHazardous == true)
                    isHazardousBooking = true;
            }
        }
        bookingRec.listCargoDetails = newCargoArr;
        component.set("v.bookingRecordValues",bookingRec);
        component.set("v.isIMDGBooking",isHazardousBooking);
    },
    handleHazardousShipment : function(component, event, helper){
        var elementId = event.currentTarget.id;
        var elementValue = event.currentTarget.checked;
        var bookingRec = component.get("v.bookingRecordValues");
        var isHazardousBooking = false;
        if(elementValue)
        	helper.createBlankCommodityList(component, event);
        else{
            for(var i=0; i<bookingRec.listCargoDetails.length; i++){
                if(bookingRec.listCargoDetails[i].sequenceId == elementId){ 
                    bookingRec.listCargoDetails[i].isHazardous = false;
                    bookingRec.listCargoDetails[i].listCommodityRecords = [];
                }
                if(bookingRec.listCargoDetails[i].isHazardous == true)
                    isHazardousBooking = true;
            }
            component.set("v.bookingRecordValues",bookingRec);
            component.set("v.isIMDGBooking",isHazardousBooking);
        }
    },
    createImdgDetailElement : function(component, event, helper) {
        var commArr = component.get("v.commoditiesList");
        var elementId = event.currentTarget.id;
        var bookingRec = component.get("v.bookingRecordValues");
        var commArrLimit = component.get("v.commoditiesListLimit");
        if(bookingRec.listCargoDetails[elementId-1].listCommodityRecords.length < commArrLimit)
            helper.createBlankCommodityList(component, event);
    },
    removeImdgDetailElement : function(component, event) {
        var elementId = event.currentTarget.id;
        var bookingRec = component.get("v.bookingRecordValues");
        var newBookingCommArr = []; var count = 1;
        for(var i=0; i<bookingRec.listCargoDetails[elementId.charAt(0)-1].listCommodityRecords.length; i++){
            if(bookingRec.listCargoDetails[elementId.charAt(0)-1].listCommodityRecords[i].Name != elementId.charAt(2)){
                bookingRec.listCargoDetails[elementId.charAt(0)-1].listCommodityRecords[i].Name = count++;
                newBookingCommArr.push(bookingRec.listCargoDetails[elementId.charAt(0)-1].listCommodityRecords[i]);
            }
        }
        bookingRec.listCargoDetails[elementId.charAt(0)-1].listCommodityRecords = newBookingCommArr;
        component.set("v.bookingRecordValues",bookingRec);
    },
    createPackageDetailElement : function(component, event, helper) {
        var bookingRec = component.get("v.bookingRecordValues");
        var commArrLimit = component.get("v.commoditiesListLimit");
        if(bookingRec.listPackingLineRecords.length < commArrLimit)
            helper.createPackingLineList(component, event);
    },
    removePackageDetailElement : function(component, event) {
        var elementId = event.currentTarget.id;
        var bookingRec = component.get("v.bookingRecordValues");
        var newPackageDetailArr = []; var count = 1;
        for(var i=0; i<bookingRec.listPackingLineRecords.length; i++){
            if(bookingRec.listPackingLineRecords[i].Name != elementId){
                bookingRec.listPackingLineRecords[i].Name = count++;
                newPackageDetailArr.push(bookingRec.listPackingLineRecords[i]);
            }
        }
        bookingRec.listPackingLineRecords = newPackageDetailArr;
        component.set("v.bookingRecordValues",bookingRec);
    },
    updateMeasurmentUnit : function(component, event, helper) {
        helper.updateMeasurmentUnits(component, event);
    },
    handleDocUpload : function(component, event, helper) {
        var elementValue = event.currentTarget.checked;
        component.set("v.isDocUpload", elementValue);
        component.set("v.isDisabled", elementValue);
    },
    createBooking : function(component, event, helper){
        console.log("isDocUpload",component.get("v.isDocUpload"));
        helper.createBookingRecord(component, event);
    },
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    },
    openHazModelBox: function(component, event, helper) {
        component.set("v.isHazModalBoxOpen", true);
    },
    closeHazModelBox: function(component, event, helper) { 
        component.set("v.isHazModalBoxOpen", false);
    },
    openBookingModelBox : function(component, event, helper){
        component.set("v.isBookingModalBoxOpen", true);
    },
    closeBookingModelBox : function(component, event, helper){
        component.set("v.isBookingModalBoxOpen", false);
    },
    navigateToBookingRecord : function(component, event, helper){
        var bookingRec = component.get("v.bookingRequestRecord");
        if(bookingRec != null){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/CrowleyCustomerCommunity/s/booking/"+bookingRec.Id,
            });
            urlEvent.fire();
        }
        else{
            component.set("v.isBookingModalBoxOpen", false);
        }
    },
    refreshBookingPage : function(component, event, helper){
        component.set("v.isBookingModalBoxOpen", false);
        window.location.reload();
    },
    resetRecordList :function(component, event, helper){
        helper.resetRecordListHelper(component, event);
    },
    handlePackingLine : function(component, event, helper){
        var elementValue = event.currentTarget.checked;
        var bookingRec = component.get("v.bookingRecordValues");
        if(elementValue)
        	helper.createPackingLineList(component, event);
        else{
            bookingRec.listPackingLineRecords = null;
            component.set("v.bookingRecordValues",bookingRec);
        }
    },
    openSubstancesModal : function(component, event, helper){ 
        component.set("v.showSubstancesModal", true);
        var ctarget = event.currentTarget;
        var id_str = ctarget.dataset.value;
        var indexes = id_str.split("-");
        var index =indexes[0]; 
        var innerIndex =indexes[1]; 
        console.log(JSON.stringify(id_str));
        var bookingCargo = component.get("v.bookingRecordValues");
        var uncode =  bookingCargo.listCargoDetails[index].listCommodityRecords[innerIndex].Number__c;
        component.set("v.searchSubsString",uncode);        
        component.set("v.keyStringModal",id_str);
        //console.log(bookingCargo[0].listCommodityRecords[id_str]);
        
        console.log(JSON.stringify(bookingCargo));
    },
    closeSubstancesModal : function(component, event, helper){
    	component.set("v.showSubstancesModal", false);
	},
    searchUNSubstances : function(component, event, helper){
        helper.searchUNSubstances(component, event, helper);
    },
    updateRowSelection: function (component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        var bookingCargo = component.get("v.bookingRecordValues");
        console.log(JSON.stringify(bookingCargo));
        var id_str = component.get("v.keyStringModal");
        var indexes = id_str.split("-");
        var index =indexes[0]; 
        var innerIndex =indexes[1];
        bookingCargo.listCargoDetails[index].listCommodityRecords[innerIndex].Number__c = selectedRows[0].Name;
        bookingCargo.listCargoDetails[index].listCommodityRecords[innerIndex].Suffix__c = selectedRows[0].Suffix__c;
        bookingCargo.listCargoDetails[index].listCommodityRecords[innerIndex].IMO_Class__c = selectedRows[0].Primary_Class__c;
        bookingCargo.listCargoDetails[index].listCommodityRecords[innerIndex].Technical_Name__c = selectedRows[0].Substance_Name__c;
        bookingCargo.listCargoDetails[index].listCommodityRecords[innerIndex].Package_Group__c = selectedRows[0].Variation__c;
        component.set("v.bookingRecordValues",bookingCargo);
        console.log(JSON.stringify(component.get("v.bookingRecordValues")));
        component.set("v.showSubstancesModal", false);
        
    },
    navigateToIMOUploadDoc : function (component, event, helper) {
        var bookingRec = component.get("v.bookingRequestRecord");
        if(bookingRec != null){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "https://comdev-crowleydev.cs77.force.com/CrowleyCustomerCommunity/s/documents?bookingNumber="+bookingRec.Name+"&tabId=tab-hazardous",
            });
            urlEvent.fire();
        } else {
            component.set("v.isBookingModalBoxOpen", false);
        }
    },
    
    navigateToHazardousDoc : function (component, event, helper) {
        var bookingRec = component.get("v.bookingRequestRecord");
        helper.getHazardousDoc(component, event, bookingRec);
    }
})