({
    getPortOfLoadingAndDischarge: function(component) {
        var action = component.get("c.getPortOfLoadingAndDischarge");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                component.set("v.portOfLoadingAndDischarge", a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    formatDate : function(component){
        var currentDate = new Date();
        var minDate = $A.localizationService.formatDate(currentDate.setDate(currentDate.getDate()), "YYYY-MM-DD");
        var maxDate = $A.localizationService.formatDate(currentDate.setDate(currentDate.getDate()+365), "YYYY-MM-DD");
        component.set('v.minDate', minDate);
        component.set("v.maxDate", maxDate);
    },
    setTodayDate: function(component) {
        var today = new Date();
        component.set('v.depToday', today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate());
    },
    getVesselScheduleList: function(component) {
        var polcode = component.find("loadingPortId").get("v.value");
        var podcode = component.find("dischargePortId").get("v.value");
        var departure = component.find("depDate").get("v.value");
        var arrival = component.find("arrivalDate").get("v.value");
        var range = component.find("rangeId").get("v.value");
        var includeTransshipment = component.find("transShipCheck").get("v.value");
          
        if (polcode == "" || polcode == "--Select--") {
            component.set("v.scheduleList", null);
            component.set("v.errorMessage", 'Please provide Port of Loading, Port of Discharge and Date of Departure to find Vessel Schedule');
            component.set("v.showErrorMessage", true);
            component.set("v.showAPIErrorMessage", false);
            
        } else if (podcode == "" || podcode == "--Select--") {
            component.set("v.scheduleList", "");
            component.set("v.errorMessage", 'Please provide Port of Loading, Port of Discharge and Date of Departure to find Vessel Schedule');
            component.set("v.showErrorMessage", true);
            component.set("v.showAPIErrorMessage", false);
        } else if (polcode == podcode) {
            component.set("v.scheduleList", null);
            component.set("v.errorMessage", 'Please provide Port of Loading, Port of Discharge and Date of Departure to find Vessel Schedule');
            component.set("v.showErrorMessage", true);
            component.set("v.showAPIErrorMessage", false);
        } else if ((departure == "" || departure == null) && (arrival == "" || arrival == null)) {
            component.set("v.scheduleList", null);
            component.set("v.errorMessage", 'Please provide Port of Loading, Port of Discharge and Date of Departure to find Vessel Schedule');
            component.set("v.showErrorMessage", true);
            component.set("v.showAPIErrorMessage", false);
        } else {
            component.set("v.portOfLoading", polcode);
            component.set("v.portOfDischarge", podcode);
            
            var action = component.get("c.getVesselSchedulesList");
            action.setParams({
                polcode: polcode,
                podcode: podcode,
                departure: departure,
                arrival: arrival,
                range: range,
                includeTransshipment: includeTransshipment
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                component.set("v.showErrorMessage", false);
                component.set("v.showAPIErrorMessage",false);
                
                if (response.getReturnValue() != null) {
                    component.set("v.scheduleList", response.getReturnValue());
                    console.log('list-->' + component.get("v.scheduleList"));
                    component.set("v.showErrorMessage", false);
                    component.set("v.showAPIErrorMessage", false);
                    
                } else {
                    component.set("v.errorMessage",$A.get("$Label.c.CustomerCommunity_VesselErrorMessage"));
                    component.set("v.errorMessage1",$A.get("$Label.c.CustomerCommunity_VesselErrorMessage_Germany"));
                    component.set("v.errorMessage2",$A.get("$Label.c.CustomerCommunity_VesselErrorMessage_Singapore"));
                    component.set("v.showAPIErrorMessage", true);
                    component.set("v.showErrorMessage", false);
                    component.set("v.scheduleList", null);
                }
            });
            $A.enqueueAction(action);
        }
    },
    fireHighlightEvent: function(component, event) {
        var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({
            "selectedMenu": compname
        });
        appEvent.fire();
    }
})