({
    setDownFlag : function(component, event, helper) {
        component.set('v.isMouseDown', true);
    },
    setDownFlagFalse : function(component, event, helper) {
        component.set('v.isMouseDown', false);
    },
	onmousemove : function(component, event, helper) {
        if(component.get('v.isMouseDown') == true) {
            const clientX = event.clientX;
            const resizer = event.target;
            const deltaX = clientX - (resizer._clientX || clientX);
            resizer._clientX = clientX;
            const l = resizer.previousElementSibling;
            const r = resizer.nextElementSibling;
            // LEFT
            if (deltaX < 0) {
              const w = Math.round(parseInt(getComputedStyle(l).width) + deltaX);
              l.style.flex = `0 ${w < 10 ? 0 : w}px`;
              r.style.flex = "1 0";
            }
            // RIGHT
            if (deltaX > 0) {
              const w = Math.round(parseInt(getComputedStyle(r).width) - deltaX);
              r.style.flex = `0 ${w < 10 ? 0 : w}px`;
              l.style.flex = "1 0";
        	}
        }
	},
    ontouchmove : function(component, e, helper) {
        const clientX = e.touches[0].clientX;
        const deltaX = clientX - (resizer._clientX || clientX);
        resizer._clientX = clientX;
        const l = resizer.previousElementSibling;
        const r = resizer.nextElementSibling;
        // LEFT
        if (deltaX < 0) {
          const w = Math.round(parseInt(getComputedStyle(l).width) + deltaX);
          l.style.flex = `0 ${w < 10 ? 0 : w}px`;
          r.style.flex = "1 0";
        }
        // RIGHT
        if (deltaX > 0) {
          const w = Math.round(parseInt(getComputedStyle(r).width) - deltaX);
          r.style.flex = `0 ${w < 10 ? 0 : w}px`;
          l.style.flex = "1 0";
        }
      },
    
    calculateWidth : function(component, event, helper) {
            var childObj = event.target
            var parObj = childObj.parentNode;
            var count = 1;
            //parent element traversing to get the TH
            /*while(parObj.tagName != 'TH') {
                parObj = parObj.parentNode;
                count++;
            }
            console.log('final tag Name'+parObj.tagName);*/
            //to get the position from the left for storing the position from where user started to drag
            var mouseStart=event.clientX; 
            component.set("v.mouseStart",mouseStart);
            component.set("v.oldWidth",parObj.offsetWidth);
        
    },
    setNewWidth : function(component, event, helper) {
        	const resizer = event.target;
            var childObj = event.target
            var parObj = childObj.parentNode;
            var count = 1;
            /*//parent element traversing to get the TH
            while(parObj.tagName != 'TH') {
                parObj = parObj.parentNode;
                count++;
            }*/
            const l = resizer.previousElementSibling;
            const r = resizer.nextElementSibling;
            var mouseStart = component.get("v.mouseStart");
            var oldWidth = component.get("v.oldWidth");
            //To calculate the new width of the column
            var newWidth = event.clientX- parseFloat(mouseStart)+parseFloat(oldWidth);
            l.style.flex = newWidth+'px';//assign new width to column
        console.log(l.style.width);
    }
    
})