({
    validateUser : function(component,event) {
       
        var inputField = component.find("FormVal");
        var inputFieldValue = inputField.get('v.value');
        if(inputFieldValue == "" || typeof(inputFieldValue) == "undefined"){
            inputField.showHelpMessageIfInvalid(); 
            return;
        }
        else{
            if(!this.validateEmail(inputFieldValue)){
               inputField.showHelpMessageIfInvalid();  
               component.set("v.showErrorMessage", false);
               return;
            }
            var action = component.get("c.validateUserDetail");
            var userEmail = component.get("v.userEmail")
            action.setParams({
                "userEmail": userEmail
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                  var value = response.getReturnValue();  
                  console.log('userIdentifier @'+value);  
                 if (value == "True") {    
                    component.set("v.ErrorMessage", "User already Exists with this Email. Please Enter a different Email.");   
                    component.set("v.showErrorMessage", true);
                }
                else if(value.includes('True:')){
                   var identifier = value.substring(value.lastIndexOf(':')+1);
                   console.log('navigateToOTPScreen @@');
                   this.navigateToOTPScreen(component,event,identifier,false);   
                }
                else if (value == "Error"){
                  component.set("v.ErrorMessage", "Something went wrong. Please contact Crowley Administrator"); 
                  component.set("v.showErrorMessage", true);
                }
                else if(value.includes('False:')){
                    console.log('navigateToOTPScreen @@');
                    var identifier = value.substring(value.lastIndexOf(':')+1);
                    this.navigateToOTPScreen(component,event,identifier,true);    
                }
                
            } else if (state === "ERROR") {
                this.ErrorHandler(component, response);
            }
           });
            $A.enqueueAction(action); 
        }
    },
    navigateToOTPScreen : function(component,event,value,contactValue){
        var homeScreenEvent = component.getEvent("HomeScreenEvent");
        homeScreenEvent.setParams({
            "HomeScreenComponent": "c:CrowleyCommunity_OTPGeneration",
            "identifier" : value,
            "userEmail" : component.get("v.userEmail"),
            "newContact" : contactValue
        });
        homeScreenEvent.fire();  
    },
    validateEmail : function(email) {
    	var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return pattern.test(String(email).toLowerCase());
	}, 
    ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    }
})