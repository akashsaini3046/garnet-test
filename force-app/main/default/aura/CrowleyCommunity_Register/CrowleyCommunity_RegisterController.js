({
    validateUser : function(component, event, helper) {
        helper.validateUser(component,event); 
    },
    navigateToLoginScreen: function(component,event,helper){
        var homeScreenEvent = component.getEvent("HomeScreenEvent");
        homeScreenEvent.setParams({
            "HomeScreenComponent": "c:CrowleyCommunity_Login"      
        });
        homeScreenEvent.fire();
    },
    handleRenderer: function(component, event, helper) {
        var elem = component.find("getDeviceHeight").getElement();
        var winHeight = window.innerHeight;
        elem.style.height = winHeight + "px";
    }
})