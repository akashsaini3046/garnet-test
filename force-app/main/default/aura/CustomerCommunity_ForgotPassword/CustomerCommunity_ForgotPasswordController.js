({
    resetPassword: function (component, event, helper) {
        helper.resetPassword(component);
    },
    handleClose: function (component, event, helper) {
        var closeModalEvent = $A.get("e.c:CustomerCommunity_ForgotPasswordEvent");
        closeModalEvent.setParams({
            "closeModalBox": true
        });
        closeModalEvent.fire();
    },
    closeModal: function (component, helper) {
        var closeEvent = $A.get("e.c:CustomerCommunity_CloseForgetPassword");
        closeEvent.setParams({
            "isFogetModalVisible": true
        });
        closeEvent.fire();
    }
})