({
    doInit: function (component, event, helper) {
        console.log("RatingApiResponseWrapper ", JSON.stringify(component.get("v.RatingApiResponseWrapper")));
        console.log("bookingWrapper ", component.get("v.bookingWrapper"));
        component.set("v.selectedRouteId", null);
        var ratingResponse = component.get("v.RatingApiResponseWrapper");
        if (ratingResponse && ratingResponse.success) {
            helper.parseResponse(component, event, helper);
        }
    },
    next: function (component, event, helper) {
        var routeId = component.get("v.bookingWrapper").selectedRouteId;
        if (routeId) {
            var bookingRecordIdEvent = component.getEvent("bookingEventData");
            bookingRecordIdEvent.setParams({
                "selectedAction": "next"
            });
            bookingRecordIdEvent.fire();
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Route Not Selected !",
                "message": "Please select the Route !",
                "type": "warning"
            });
            toastEvent.fire();
        }
    },

    selectedRateHandler: function (component, event, helper) {
        var routeId = event.getSource().get("v.value");
        var bookingWrapper = component.get("v.bookingWrapper");
        bookingWrapper.selectedRouteId = routeId;
        component.set("v.bookingWrapper", bookingWrapper);
    }
})