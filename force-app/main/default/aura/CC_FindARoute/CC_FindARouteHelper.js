({
    getZipCountries : function(component,event,helper){
        component.set("v.isLoading",true);
        var action = component.get("c.getLocations");
        action.setParams({
            type : 'D'
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state==="SUCCESS"){
                var zipLocations = response.getReturnValue();
                var zipCountriesList =[]
                for(var i in zipLocations){
                    zipCountriesList.push(zipLocations[i].Country_Name__c);
                }
                var zipCountrieSet = new Set(zipCountriesList);
                
                component.set("v.zipCountries",zipCountrieSet);
                console.log(component.get("v.zipCountries"));
            }
            else{
                console.log('Error occured in getZipCountries');
            }
            component.set("v.isLoading",false);
        });
        $A.enqueueAction(action);
    },
    getPortCountries : function(component,event,helper){
        component.set("v.isLoading",true);
        var action = component.get("c.getLocations");
        action.setParams({
            type : 'P'
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state==="SUCCESS"){
                var portLocations = response.getReturnValue();
                var portLocationsList =[]
                for(var i in portLocations){
                    portLocationsList.push(portLocations[i].Country_Name__c);
                }
                var zipCountrieSet = new Set(portLocationsList);
                
                component.set("v.portCountries",zipCountrieSet);
                console.log(component.get("v.portCountries"));
            }
            else{
                console.log('Error occured in getPortCountries');
            }
        });
        $A.enqueueAction(action);
        component.set("v.isLoading",false);
    },
    getRailCountries : function(component,event,helper){
        component.set("v.isLoading",true);
        var action = component.get("c.getLocations");
        action.setParams({
            type : 'R'
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state==="SUCCESS"){
                var railLocations = response.getReturnValue();
                var railLocationsList =[]
                for(var i in railLocations){
                    railLocationsList.push(railLocations[i].Country_Name__c);
                }
                var railCountriesSet = new Set(railLocationsList);
                
                component.set("v.railCountries",railCountriesSet);
                console.log(component.get("v.railCountries"));
            }
            else{
                console.log('Error occured in getRailCountries');
            }
            component.set("v.isLoading",false);
        });
        $A.enqueueAction(action); 
    },
    handleComponentEvent : function(component,event,helper){
        var parentCompAttr = event.getParam("parentComponentAttribute");
        console.log('parentCompAttr '+parentCompAttr);
        if(parentCompAttr === 'selectedOriginCountry'){
            
            if(component.get("v.selectedMovementTypeOrigin")==='D' && component.get("v.selectedOriginCountry")==='United States'){
                var receiptTerms =[];
                receiptTerms.push({value:'',label:'None'});
                receiptTerms.push({value:'A',label:'All Motor'});
                receiptTerms.push({value:'M',label:'Motor/Rail'});
                component.set("v.originReceiptTerms",receiptTerms);
                component.set("v.selectedOriginReceiptTerm",'');
                component.set("v.defaultOriginReceiptTerm",'');
            }
            if(component.get("v.selectedMovementTypeOrigin")==='D' && component.get("v.selectedOriginCountry")!=='United States'){
                var receiptTerms =[];
                receiptTerms.push({value:'',label:'None'});
                receiptTerms.push({value:'A',label:'All Motor'});
                component.set("v.originReceiptTerms",receiptTerms);
                component.set("v.selectedOriginReceiptTerm",'');
                component.set("v.defaultOriginReceiptTerm",'');
                
            }
            
            
            
        }
        
        if(parentCompAttr === 'selectedDestinationCountry'){
            var receiptTerms =[];
            if(component.get("v.selectedMovementTypeDestination")==='D' && component.get("v.selectedDestinationCountry")==='United States'){
                receiptTerms.push({value:'',label:'None'});
                receiptTerms.push({value:'A',label:'All Motor'});
                receiptTerms.push({value:'M',label:'Motor/Rail'});
                component.set("v.destinationReceiptTerms",receiptTerms);
                component.set("v.selectedDestinationReceiptTerm",'');
                component.set("v.defaultDestinationReceiptTerm",'');
            }
            if(component.get("v.selectedMovementTypeDestination")==='D' && component.get("v.selectedDestinationCountry")!=='United States'){
                receiptTerms.push({value:'',label:'None'});
                receiptTerms.push({value:'A',label:'All Motor'});
                component.set("v.destinationReceiptTerms",receiptTerms);
                component.set("v.selectedDestinationReceiptTerm",'');
                component.set("v.defaultDestinationReceiptTerm",'');
            }
            
            
        }
        
        if(parentCompAttr === 'selectedOriginLocation'){
            var locCodeMap = component.get("v.objectFromChild");
            for(var x in locCodeMap){
                if(locCodeMap[x].key === component.get("v.selectedOriginLocation")){
                    console.log(locCodeMap[x].key +'  '+locCodeMap[x].value);
                    component.set("v.originCode",locCodeMap[x].value);
                    var orCode = locCodeMap[x].key;
                    if(component.get("v.selectedMovementTypeOrigin")==='D')
                        component.set("v.originZip",orCode.substring(orCode.indexOf('(')+1,orCode.length - 1));
                }
            }
            
            var action = component.get("c.getSublocations");
            action.setParams({
                type : component.get("v.selectedMovementTypeOrigin"),
                loctionCode : component.get("v.originCode")
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var subLocations = response.getReturnValue();
                    component.set("v.selectedOriginSublocation",'');
                    if(subLocations !== null){                        
                        component.set("v.originSublocationsList",subLocations);
                        component.set("v.displayOriginSublocation",true);  
                    }            
                }
            });
            $A.enqueueAction(action);
        }
        if(parentCompAttr === 'selectedDestinationLocation'){
            var locCodeMap = component.get("v.objectFromChild");
            for(var x in locCodeMap){
                
                if(locCodeMap[x].key === component.get("v.selectedDestinationLocation")){
                    component.set("v.destinationCode",locCodeMap[x].value);
                    var destCode = locCodeMap[x].key;
                    if(component.get("v.selectedMovementTypeDestination")==='D')
                        component.set("v.destinationZip",destCode.substring(destCode.indexOf('(')+1,destCode.length - 1));
                }
            }
            
            var action = component.get("c.getSublocations");
            action.setParams({
                type : component.get("v.selectedMovementTypeDestination"),
                loctionCode : component.get("v.destinationCode")
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var subLocations = response.getReturnValue();
                    component.set("v.selectedDestinationSublocation",'');
                    if(subLocations !== null && subLocations !== undefined){
                        component.set("v.destinationSublocationsList",subLocations);
                        component.set("v.displayDestinationSublocation",true);  
                    }            
                }
            });
            $A.enqueueAction(action);
        }
        
    },
    
    handleClearSearchEvent : function(component,event,helper){
        var parentCompAttr = event.getParam("parentComponentAttribute");
        if(parentCompAttr === 'selectedOriginCountry'){
            component.set("v.selectedOriginCountry",null);
            component.set("v.selectedOriginLocation",null);
            component.set("v.selectedOriginSubLocation",null);
            component.set("v.displayOriginSublocation",false);
            if(component.get("v.selectedMovementTypeOrigin")==='D'){
                var receiptTerms =[];
                receiptTerms.push({value:'',label:'None'});
                receiptTerms.push({value:'A',label:'All Motor'});
                component.set("v.originReceiptTerms",receiptTerms);
                component.set("v.defaultOriginReceiptTerm",'');
            }
        }
        if(parentCompAttr === 'selectedDestinationCountry'){
            component.set("v.selectedDestinationCountry",null);
            component.set("v.selectedDestinationLocation",null);
            component.set("v.selectedDestinationSubLocation",null);
            component.set("v.displayDestinationSublocation",false);
            if(component.get("v.selectedMovementTypeDestination")==='D'){
                var receiptTerms =[];
                receiptTerms.push({value:'',label:'None'});
                receiptTerms.push({value:'A',label:'All Motor'});
                component.set("v.destinationReceiptTerms",receiptTerms);
                component.set("v.defaultDestinationReceiptTerm",'');
            }
        }
        if(parentCompAttr === 'selectedOriginLocation'){
            component.set("v.selectedOriginLocation",null);
            component.set("v.selectedOriginSubLocation",null);
            component.set("v.displayOriginSublocation",false);
        }
        if(parentCompAttr === 'selectedDestinationLocation'){
            component.set("v.selectedDestinationLocation",null);
            component.set("v.selectedDestinationSubLocation",null);
            component.set("v.displayDestinationSublocation",false);
        }
    },
    searchFindRoute : function(component,event,helper){
        component.set("v.isLoading",true);
        var action = component.get("c.getRoutesList");
        console.log('Inside searchFindRoute');
        console.log(component.get("v.originCode"));
        console.log(component.get("v.destinationCode"));
        console.log(component.get("v.selectedMovementTypeOrigin") +' to '+component.get("v.selectedMovementTypeDestination"));
        console.log(component.get("v.readyDate"));
        console.log(component.get("v.selectedOriginReceiptTerm"));
        console.log(component.get("v.selectedDestinationReceiptTerm"));
        console.log(component.get("v.selectedOriginSublocation"));
        console.log(component.get("v.selectedDestinationSublocation"));
        action.setParams({
            originCode : component.get("v.originCode"),
            destinationCode : component.get("v.destinationCode"),
            originType: component.get("v.selectedMovementTypeOrigin"),
            destinationType: component.get("v.selectedMovementTypeDestination"),
            readyDate: component.get("v.readyDate"),	
            receiptTypeCode: component.get("v.selectedOriginReceiptTerm"),
            deliveryTypeCode: component.get("v.selectedDestinationReceiptTerm"),
            toSubLocation: component.get("v.selectedDestinationSublocation"),
            fromSubLocation: component.get("v.selectedOriginSublocation"),
            description: component.get("v.selectedMovementTypeOrigin")+component.get("v.selectedMovementTypeDestination")
            
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.showErrorMessage",false);
                var responseData = response.getReturnValue(); 
                component.set("v.loading", false);
                var numberOfSchedules = 0;
                var countSegmentSequence = 1;
				for(var i in responseData.result){
                    var schedules = responseData.result[i].Schedules;
                    numberOfSchedules = numberOfSchedules+ responseData.result[i].Schedules.length;
                    for(var j in schedules){
                        schedules[j].sequenceNumber = countSegmentSequence;
                        countSegmentSequence ++;
                        var lastSegmentIndex = schedules[j].Segments.length-1;
                        schedules[j].numberOfStops = schedules[j].Segments.length-1; 

                        schedules[j].StartDate.Utc = this.getDateTime(schedules[j].StartDate.Utc);
       
                        var endDate = schedules[j].EndDate.Utc;                        
                        schedules[j].EndDate.Utc = this.getDateTime(endDate);
                        
                        
                        if(schedules[j].Segments[0].FromSubDisplayName !== null && schedules[j].Segments[0].FromSubDisplayName!==undefined && schedules[j].Segments[0].FromSubDisplayName!=='')
                            schedules[j].startLocation = schedules[j].Segments[0].FromSubDisplayName;
                        else
                            schedules[j].startLocation = schedules[j].Segments[0].Leg.StartPosition.DisplayName;
                        
                        schedules[j].startLocationCode = schedules[j].Segments[0].FromLocation.Code;
                        
                        if(schedules[j].Segments[lastSegmentIndex].ToSubDisplayName !== null && schedules[j].Segments[lastSegmentIndex].ToSubDisplayName!==undefined && schedules[j].Segments[lastSegmentIndex].ToSubDisplayName!=='')
                            schedules[j].endLocation = schedules[j].Segments[lastSegmentIndex].ToSubDisplayName;
                        else
                            schedules[j].endLocation = schedules[j].Segments[lastSegmentIndex].Leg.EndPosition.DisplayName;
                        schedules[j].endLocationCode= schedules[j].Segments[lastSegmentIndex].ToLocation.Code;
                        
                 
                        for(var k in schedules[j].Segments){
                            schedules[j].Segments[k].StopSequence = schedules[j].Segments[k].Leg.LegSeqNumber-1;
                            
                            if(k>0){
                                if(!schedules[j].Segments[k-1].IsOcean){                               
                                    schedules[j].Segments[k].arrivalToDisplay = schedules[j].Segments[k-1].ArrivalTime.Utc;
                                }
                                if(schedules[j].Segments[k-1].IsOcean){
                                    schedules[j].Segments[k].arrivalToDisplay = schedules[j].Segments[k-1].To.Berths[0].Arrival.Utc;
                                }
                                
                                var arrivalTime = schedules[j].Segments[k].arrivalToDisplay;
                                schedules[j].Segments[k].arrivalToDisplay = this.getDateTime(arrivalTime);
                                
                                if(schedules[j].Segments[k].IsOcean){
									schedules[j].Segments[k].FromX.Berths[0].Departure.Utc = this.getDateTime(schedules[j].Segments[k].FromX.Berths[0].Departure.Utc);
                                }
                                if(!schedules[j].Segments[k].IsOcean){
                               		schedules[j].Segments[k].DepartureTime.Utc = this.getDateTime(schedules[j].Segments[k].DepartureTime.Utc);
                                }
                            }                                                     
                                
                        }
                        var totalDuration = (schedules[j].TotalDuration).toString();
                        schedules[j].totalDays = (totalDuration.substring(0,totalDuration.indexOf('.')));
                        var hours = parseInt(totalDuration.substring(totalDuration.indexOf('.')+1,totalDuration.indexOf(':')));
                        if(hours>12)
                            schedules[j].totalDays = parseInt(schedules[j].totalDays)+1;
                        var extraString = totalDuration.substring(totalDuration.indexOf(':')+1);
                        schedules[j].extraMinutes = parseInt(extraString.substring(0,extraString.indexOf(':')));
                                                                                               
                    }}
                component.set("v.numberOfSchedules",numberOfSchedules);
                console.log(' searchFindRoute success ');
                component.set("v.routeList",responseData);
            }
            else if(state==='ERROR'){
                 var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.numberOfSchedules",0);
                        component.set("v.errorMessage", 'No Schedules found for the selected date and route, please try again for a different route & date combination');
                        component.set("v.showErrorMessage",true);
                        console.log("Error message: " + errors[0].message);
                      
                    }
                } else {
                    console.log("Unknown error");
                }
                console.log(' searchFindRoute error ');
            }
            component.set("v.isLoading",false);
        });
        $A.enqueueAction(action);
    },
    clearStorages : function(component){
        window.localStorage.clear();
        window.sessionStorage.clear();
    },
    
    getDateTime: function(utcDateTime){
        const timeHourArray = [12,1,2,3,4,5,6,7,8,9,10,11,12,1,2,3,4,5,6,7,8,9,10,11];
        var strDateTime = utcDateTime;
        if(strDateTime!==''){
            var listString = strDateTime.split('T');
            var dateSplit = listString[0].split('-');
            var timeSplit = listString[1].split(':');
            var hours = parseInt(timeSplit[0]);
            var AMPM = (hours>11) ? 'PM' : 'AM';
            strDateTime = listString[0]+' '+ timeHourArray[hours]+':'+timeSplit[1]+' '+AMPM;
        }
        return strDateTime;
    }
    
})