({
    init: function (cmp, event, helper) {
        helper.fireHighlightEvent(cmp, event);
        cmp.set('v.mapMarkers', [
            {
                location: {
                    City: 'San Juan',
                    Country: 'PR'
                },
                icon: 'custom:custom88',
                title: 'CMCU 6030780',
                description: 'IN YARD LOADED FOR DELIVERY \n (01/07/2019 - 12:47)'
            },
            {
                location: {
                    City: 'Port Everglades',
                    Country: 'FL'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4544754',
                description: 'ON STREET EMPTY TO CUSTOMER (01/07/2019 - 16:46)'
            },
            {
                location: {
                    City: 'Jacksonville',
                    Country: 'FL'
                },
                icon: 'custom:custom88',
                title: 'CMCU 6034852',
                description: 'IN YARD LOADED FOR DELIVERY (01/07/2019 - 11:53)'
            },
            {
                location: {
                    City: 'Gulfport',
                    Country: 'MS'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4540511',
                description: 'IN YARD LOADED TO BE SAILED (01/07/2019 - 11:53)'	
            },
            {
                location: {
                    City: 'Villanueva',
                    Country: 'Philippines'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4543399',
                description: 'ON STREET EMPTY TO CUSTOMER (01/07/2019 - 12:55)'	
            },
            {
                location: {
                    City: 'San Salvador',
                    Country: 'El Salvador'
                },
                icon: 'custom:custom88',
                title: 'BSIU 9681617',
                description: 'ON STREET LOADED FOR DELIVERY (01/07/2019 - 12:55)'	
            },
            {
                location: {
                    City: 'Potrerillos',
                    Country: 'Argentina'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4529030',
                description: 'IN YARD LOADED AND DELIVERED (01/07/2019 - 12:55)'	
            },
            {
                location: {
                    City: 'Guatemala City',
                    Country: 'Guatemala'
                },
                icon: 'custom:custom88',
                title: 'CMCG 3532',
                description: 'IN YARD LOADED FOR DELIVERY (01/07/2019 - 12:55)'	
            },
            {
                location: {
                    City: 'Puerto Cortes',
                    Country: 'Honduras'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4956284',
                description: 'IN YARD LOADED TO BE SAILED (01/07/2019 - 12:55)'	
            },
            {
                location: {
                    City: 'Miami',
                    Country: 'FL'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4538197',
                description: 'IN YARD LOADED TO BE SAILED (01/07/2019 - 15:29)'	
            }
        ]);
        cmp.set('v.markersTitle', 'My Shipments');
    },
    updateShipments : function(component, event, helper) {
        helper.updateShipmentsLocation(component, event);
    },
    handleSelectAll: function(component, event, helper) {
        var checkvalue = component.find("selectAll").get("v.value");
        if(checkvalue == true){
            component.find("shipment-1").set("v.value",true);
            component.find("shipment-2").set("v.value",true);
            component.find("shipment-3").set("v.value",true);
        }
        else{ 
            component.find("shipment-1").set("v.value",false);
            component.find("shipment-2").set("v.value",false);
            component.find("shipment-3").set("v.value",false);
        }
        helper.updateShipmentsLocation(component, event);
    },
    searchShipment: function(component,event,helper){
        helper.searchNewShipment(component, event);
    },
    keyCheck: function (component, event, helper) {
        if (event.which == 13) {
            helper.searchNewShipment(component, event);
        }
    }
});