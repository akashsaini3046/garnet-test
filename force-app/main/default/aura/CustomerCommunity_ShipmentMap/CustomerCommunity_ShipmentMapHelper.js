({
	fireHighlightEvent: function(component, event) {
        var appEvent = $A.get("e.c:CustomerCommunity_HighlightedMenu");
        var compname = component.get("v.componentName");
        appEvent.setParams({
            "selectedMenu": compname
        });
        appEvent.fire();
    },
    updateShipmentsLocation : function(component, event) {
        var delayedShipment = component.find("shipment-1").get("v.value");
        var customsHold = component.find("shipment-2").get("v.value");
        var inTransit = component.find("shipment-3").get("v.value");
        var displayShipments = [];
        if(!(delayedShipment && customsHold && inTransit)){
            component.find("selectAll").set("v.value",false);
        }
        if(delayedShipment){
            displayShipments.push({
                location: {
                    City: 'San Juan',
                    Country: 'PR'
                },
                icon: 'custom:custom88',
                title: 'CMCU 6030780',
                description: 'IN YARD LOADED FOR DELIVERY \n (01/07/2019 - 12:47)'
            });
            displayShipments.push({
                location: {
                    City: 'Port Everglades',
                    Country: 'FL'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4544754',
                description: 'ON STREET EMPTY TO CUSTOMER (01/07/2019 - 16:46)'
            });
            displayShipments.push({
                location: {
                    City: 'Jacksonville',
                    Country: 'FL'
                },
                icon: 'custom:custom88',
                title: 'CMCU 6034852',
                description: 'IN YARD LOADED FOR DELIVERY (01/07/2019 - 11:53)'
            });
            displayShipments.push({
                location: {
                    City: 'Villanueva',
                    Country: 'Philippines'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4543399',
                description: 'ON STREET EMPTY TO CUSTOMER (01/07/2019 - 12:55)'	
            });
        }
        if(customsHold){
            displayShipments.push({
                location: {
                    City: 'San Juan',
                    Country: 'PR'
                },
                icon: 'custom:custom88',
                title: 'CMCU 6030780',
                description: 'IN YARD LOADED FOR DELIVERY \n (01/07/2019 - 12:47)'
            });
            displayShipments.push({
                location: {
                    City: 'Port Everglades',
                    Country: 'FL'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4544754',
                description: 'ON STREET EMPTY TO CUSTOMER (01/07/2019 - 16:46)'
            });
            displayShipments.push({
                location: {
                    City: 'Gulfport',
                    Country: 'MS'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4540511',
                description: 'IN YARD LOADED TO BE SAILED (01/07/2019 - 11:53)'	
            });
        }
        if(inTransit){
            displayShipments.push({
                location: {
                    City: 'Guatemala City',
                    Country: 'Guatemala'
                },
                icon: 'custom:custom88',
                title: 'CMCG 3532',
                description: 'IN YARD LOADED FOR DELIVERY (01/07/2019 - 12:55)'	
            });
            displayShipments.push({
                location: {
                    City: 'Miami',
                    Country: 'FL'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4538197',
                description: 'IN YARD LOADED TO BE SAILED (01/07/2019 - 15:29)'	
            });
            displayShipments.push({
                location: {
                    City: 'Puerto Cortes',
                    Country: 'Honduras'
                },
                icon: 'custom:custom88',
                title: 'CMCU 4956284',
                description: 'IN YARD LOADED TO BE SAILED (01/07/2019 - 12:55)'	
            });
        }
        component.set('v.mapMarkers',displayShipments);
    },
    searchNewShipment : function(component, event) {
        var ShipmentId = component.get("v.selectedShipmentId");
        var mapMarkers = component.get("v.mapMarkers"); 
        if(ShipmentId == "" || ShipmentId == null || ShipmentId == undefined){
            component.set('v.mapMarkers', [
                {
                    location: {
                        City: 'San Juan',
                        Country: 'PR'
                    },
                    icon: 'custom:custom88',
                    title: 'CMCU 6030780',
                    description: 'IN YARD LOADED FOR DELIVERY \n (01/07/2019 - 12:47)'
                },
                {
                    location: {
                        City: 'Port Everglades',
                        Country: 'FL'
                    },
                    icon: 'custom:custom88',
                    title: 'CMCU 4544754',
                    description: 'ON STREET EMPTY TO CUSTOMER (01/07/2019 - 16:46)'
                },
                {
                    location: {
                        City: 'Jacksonville',
                        Country: 'FL'
                    },
                    icon: 'custom:custom88',
                    title: 'CMCU 6034852',
                    description: 'IN YARD LOADED FOR DELIVERY (01/07/2019 - 11:53)'
                },
                {
                    location: {
                        City: 'Gulfport',
                        Country: 'MS'
                    },
                    icon: 'custom:custom88',
                    title: 'CMCU 4540511',
                    description: 'IN YARD LOADED TO BE SAILED (01/07/2019 - 11:53)'	
                },
                {
                    location: {
                        City: 'Villanueva',
                        Country: 'Philippines'
                    },
                    icon: 'custom:custom88',
                    title: 'CMCU 4543399',
                    description: 'ON STREET EMPTY TO CUSTOMER (01/07/2019 - 12:55)'	
                },
                {
                    location: {
                        City: 'San Salvador',
                        Country: 'El Salvador'
                    },
                    icon: 'custom:custom88',
                    title: 'BSIU 9681617',
                    description: 'ON STREET LOADED FOR DELIVERY (01/07/2019 - 12:55)'	
                },
                {
                    location: {
                        City: 'Potrerillos',
                        Country: 'Argentina'
                    },
                    icon: 'custom:custom88',
                    title: 'CMCU 4529030',
                    description: 'IN YARD LOADED AND DELIVERED (01/07/2019 - 12:55)'	
                },
                {
                    location: {
                        City: 'Guatemala City',
                        Country: 'Guatemala'
                    },
                    icon: 'custom:custom88',
                    title: 'CMCG 3532',
                    description: 'IN YARD LOADED FOR DELIVERY (01/07/2019 - 12:55)'	
                },
                {
                    location: {
                        City: 'Puerto Cortes',
                        Country: 'Honduras'
                    },
                    icon: 'custom:custom88',
                    title: 'CMCU 4956284',
                    description: 'IN YARD LOADED TO BE SAILED (01/07/2019 - 12:55)'	
                },
                {
                    location: {
                        City: 'Miami',
                        Country: 'FL'
                    },
                    icon: 'custom:custom88',
                    title: 'CMCU 4538197',
                    description: 'IN YARD LOADED TO BE SAILED (01/07/2019 - 15:29)'	
                }
            ]);
            component.set("v.isErrorVisible",false);
            component.set("v.selectedShipmentId","");
            return;
        }
        var setMarker = mapMarkers.find(function(listmarkers){
            return ShipmentId.toUpperCase() == listmarkers.title.toUpperCase();
        });
        if(setMarker == undefined || setMarker == null){
            component.set("v.ErrorMsg","Please enter the valid Shipment Id");
            component.set("v.isErrorVisible",true);
            return;
        }
        component.set("v.mapMarkers",[{
            location: {
                City: setMarker.location.City,
                Country: setMarker.location.Country
            },
            icon: setMarker.icon,
            title: setMarker.title,
            description: setMarker.description
        }]); 
        component.set('v.zoomLevel', 3);
        component.set("v.isErrorVisible",false);
    }
})