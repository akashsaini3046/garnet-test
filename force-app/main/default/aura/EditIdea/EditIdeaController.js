({
	doInit : function(cmp, event, helper) {
        helper.getIdeaDescribeResultAction(cmp, helper);
        helper.getIdeaFieldDescribeResultAction(cmp, helper);
		var ideaId = helper.getURLParameter('id');
        if(ideaId){
            cmp.set('v.ideaId', ideaId);
            helper.getIdeaDetails(cmp, helper, ideaId);
            helper.getZonesList(cmp, helper);
           /* helper.getIdeaStatuses(cmp, helper);
            helper.getCategories(cmp, helper);
            helper.getBenefits(cmp, helper);*/
        }
    },

    handleCategoriesChange: function (cmp, event) {
        // This will contain an array of the "value" attribute of the selected options
        var selectedOptionValue = event.getParam("value");
        var categories = selectedOptionValue.toString().split(',').join(';');
        console.log('handleCategoriesChange : categories -> ', categories);
        cmp.set('v.ideaRecord.Categories', categories);
        console.log('Categories values -> ',cmp.get('v.categoriesValues'));
    },
    
    saveIdea: function(cmp, event, helper){
        helper.saveIdea(cmp, event, helper);
    },
    
    cancel : function(cmp){
        window.location.href = $A.get("$Label.c.Idea_Detail_Page")+'?id='+cmp.get("v.ideaId");
    },
    
    removeAttachment: function(cmp){
        cmp.set("v.ifRemoveAttachment", true);
    },
    
    undoRemoveAttachment: function(cmp){
        cmp.set("v.ifRemoveAttachment", false);
    },
    
    handleFileUploadEvent: function(cmp, event, helper){
        var state = event.getParam("state");
        if(state=="SUCCESS"){
            helper.showToast("success", "Success", "The record has been updated successfully");
            window.location.href = $A.get("$Label.c.Idea_Detail_Page")+'?id='+cmp.get("v.ideaId");
        }else{
            helper.showToast("error","Failed", event.getParam("message"));
        }
    }
})