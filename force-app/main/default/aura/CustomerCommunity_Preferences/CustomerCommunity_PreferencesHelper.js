({
    setUserDetails: function (component) {
        var action = component.get("c.fetchUserDetails");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var userDetails = response.getReturnValue();
                if (userDetails != null) {
                    component.set("v.userEmail", userDetails.Email);
                    this.buildData(component, userDetails);
                }
            } else if (state === "ERROR") {
                this.ErrorHandler();
            }
        });
        $A.enqueueAction(action);
    },
    buildData: function (component, userDetails) {
        var WhatsAppnames = userDetails.HomePhone;
        var SmsNames,smsNameFilter,SmsNamesArr,WhatsAppArr;

       if(userDetails.HomePhone != undefined || userDetails.HomePhone!= null){
           SmsNames =  userDetails.HomePhone+',';
         }
        if(userDetails.MobilePhone != undefined || userDetails.MobilePhone!= null){
            SmsNames = SmsNames+userDetails.MobilePhone+','
        }
        if(userDetails.Phone!= undefined || userDetails.Phone!= null){
            SmsNames = SmsNames+userDetails.Phone+',';   
        }
        if(userDetails.Mobile_Phone_1__c!= undefined || userDetails.Mobile_Phone_1__c!= null){
            SmsNames = SmsNames+userDetails.Mobile_Phone_1__c+',';   
        }
        if(userDetails.Mobile_Phone_2__c!= undefined || userDetails.Mobile_Phone_2__c!= null){
            SmsNames = SmsNames+userDetails.Mobile_Phone_2__c+',';   
        }
         if(userDetails.Mobile_Phone_3__c!= undefined || userDetails.Mobile_Phone_3__c!= null){
            SmsNames = SmsNames+userDetails.Mobile_Phone_3__c+',';   
        }
        if(SmsNames != undefined){
        	smsNameFilter = SmsNames.substring(0,SmsNames.length -1);
        }
        if(WhatsAppnames != null){
        	WhatsAppArr = WhatsAppnames.split(",");
        }
        if(smsNameFilter != null){
            SmsNamesArr = smsNameFilter.split(",");
        }
        var newWhatsAppArr = [];
        var newSmsArr = [];
        var counter1 = 0;
        var counter2 = 0;
        for (var i = 0; i < WhatsAppArr.length; i++) {
            newWhatsAppArr.push({
                "id": counter1++,
                "value": WhatsAppArr[i]
            });
        }
        for (var i = 0; i < SmsNamesArr.length; i++) {
            if(SmsNamesArr[i]!=undefined || SmsNamesArr[i]!=null){  
            newSmsArr.push({
                "id": counter2++,
                "value": SmsNamesArr[i]
            });
          }
        }
        component.set("v.WhatsAppIds", newWhatsAppArr);
        component.set("v.SmsIds", newSmsArr);
    },
    createWhatsAppIDElement: function (component) {
        var WhatsAppArr = component.get("v.WhatsAppIds");
        if (WhatsAppArr.length < 3) {
            WhatsAppArr.push({
                "id": WhatsAppArr.length,
                "value": ""
            });
            component.set("v.WhatsAppIds", WhatsAppArr);
        }
    },
    createWhatsAppIDArray: function (component) {
        var WhatsAppArr = [{
            "id": 0,
            "value": ""
        }];
        component.set("v.WhatsAppIds", WhatsAppArr);
    },
    removeWhatsAppIDElement: function (component, event) {
        var elementId = event.currentTarget.id;
        var WhatsAppArr = component.get("v.WhatsAppIds");
        var newWhatsAppArr = [];
        var count = 0;
        for (var i = 0; i < WhatsAppArr.length; i++) {
            if (WhatsAppArr[i].id != elementId) {
                newWhatsAppArr.push({
                    "id": count++,
                    "value": WhatsAppArr[i].value
                });
            }
        }
        component.set("v.WhatsAppIds", newWhatsAppArr);
    },
    createSmsIDArray: function (component) {
        var SmsIdArr = [{
            "id": 0,
            "value": ""
        }];
        component.set("v.SmsIds", SmsIdArr);
    },
    createSmsIDElement: function (component) {
        var SmsIdsArr = component.get("v.SmsIds");
        if (SmsIdsArr.length < 6) {
            SmsIdsArr.push({
                "id": SmsIdsArr.length,
                "value": ""
            });
            component.set("v.SmsIds", SmsIdsArr);
        }
    },
    removeSmsIDElement: function (component, event) {
        var elementId = event.currentTarget.id;
        var SmsIdArr = component.get("v.SmsIds");
        var newSmsIdArr = [];
        var count = 0;
        for (var i = 0; i < SmsIdArr.length; i++) {
            if (SmsIdArr[i].id != elementId) {
                newSmsIdArr.push({
                    "id": count++,
                    "value": SmsIdArr[i].value
                });
            }
        }
        component.set("v.SmsIds", newSmsIdArr);
    },
    updateUserDetails: function (component, helper) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var userEmail = component.get('v.userEmail')
        var smsIdsArr = component.get("v.SmsIds");
        var whatsAppIdsArr = component.get('v.WhatsAppIds');

        if (userEmail == null || smsIdsArr == null || whatsAppIdsArr == null) {
            component.set("v.showFieldErrorMsg", true);
            component.set("v.ErrorMsg", "Please provide Email,WhatsApp and SMS");
            return;
        }
        var action = component.get("c.updateDetails");
        var smsIds = [];
        var whatsAppIds = [];
        for (var i = 0; i < smsIdsArr.length; i++) {
            if (smsIdsArr[i].value != '') {
                smsIds.push(smsIdsArr[i].value);
            }
        }
        for (var i = 0; i < whatsAppIdsArr.length; i++) {
            if (whatsAppIdsArr[i].value != '') {
                whatsAppIds.push(whatsAppIdsArr[i].value);
            }
        }
        action.setParams({
            userEmail: userEmail,
            idUser: userId,
            listSmsNumbers: smsIds,
            listWhatsAppNumbers: whatsAppIds
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var valueReceived = action.getReturnValue();
                if(valueReceived == null){
                   component.set("v.showFieldErrorMsg", false);
                   alert("Something went wrong please try Again !");
                }
                console.log(valueReceived);
                component.set("v.showFieldErrorMsg", false);
                if (valueReceived == 'TRUE') {
                    alert('User Details has been successfully Updated.');
                    this.setUserDetails(component);
                } else if (valueReceived == undefined || valueReceived == null || valueReceived == 'FALSE') {
                    component.set("v.showFieldErrorMsg", true);
                    component.set("v.ErrorMsg", "Please provide Email,WhatsApp and SMS");
                }
            } else if (state === "ERROR") {
                component.set("v.showFieldErrorMsg", false);
                this.ErrorHandler(component, response);
            }
        });
        $A.enqueueAction(action);
    },
    ErrorHandler: function (component, response) {
        var errors = response.getError();
        if (errors) {
            if (errors[0] && errors[0].message) {
                console.log("Error message: " + errors[0].message);
            }
        } else {
            console.log("Unknown error");
        }
    },
    clearDetails: function (component, event) {
        component.set("v.showFieldErrorMsg", null);
        this.setUserDetails(component);
    }
})