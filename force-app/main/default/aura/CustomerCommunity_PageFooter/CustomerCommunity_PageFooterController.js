({
    doInit: function (component, event, helper) {
        helper.getColumnOneItem(component, event);
        helper.getColumnTwoItem(component, event);
        helper.getColumnThreeItem(component, event);
    },
    gotoURL: function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "http://www.crowley.com/about-us/company-overview/"
        });
        urlEvent.fire();
    },    
    gotoFacebook: function(component,event,helper){
     	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.facebook.com/Crowley/"
        });
        urlEvent.fire();
    },
    gotoTwitter:function(component,event,helper){
     	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://twitter.com/crowleymaritime/"
        });
        urlEvent.fire();
    },
    gotoYoutube:function(component,event,helper){
     	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.youtube.com/user/crowleymaritime/"
        });
        urlEvent.fire();
    },
    gotoInsta:function(component,event,helper){
     	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.instagram.com/crowleymaritime/"
        });
        urlEvent.fire();
    },
    gotoLinkedin:function(component,event,helper){
       var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.linkedin.com/company/crowley-maritime/"
        });
        urlEvent.fire();
    }
})