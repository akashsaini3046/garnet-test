({
    loadValues : function(component, event, helper) {
        
        console.log('Inside loadValues');
        var bookingWrapperData = component.get("v.bookingWrapper");
        
        var bookingData = bookingWrapperData['booking'];
        component.set("v.bookingData",bookingData);
        
        var partyData = bookingWrapperData['mapParty'];       
        var customer = (partyData['Customer'])[0];
        var shipper = (partyData['shiper'])[0];
        var consignee = (partyData['consignee'])[0];
        var notifierList = partyData['notify'];
        var forwarderList = partyData['forwarder'];
        
        component.set("v.customer",customer);
        component.set("v.shipper",shipper);
        component.set("v.consignee",consignee);
        component.set("v.notifierList",notifierList);
        component.set("v.forwarderList",forwarderList);
        
        var cargoWrapperList = (bookingWrapperData['shipment'])['listCargo'];
        component.set("v.cargoList",cargoWrapperList);
        console.log(JSON.stringify(cargoWrapperList));
        
        
		
    }
})