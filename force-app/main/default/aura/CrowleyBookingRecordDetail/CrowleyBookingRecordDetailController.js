({
    doInit: function (component, event, helper) {
        component.set('v.selectedItem', 'details');
        component.set('v.currentContent', 'details');
        helper.fetchBookingAndRelatedData(component, event, helper);
        helper.allAttachments(component, event, helper);
    },
    
    toggleSection : function(component, event, helper) {
        var sectionAuraId = event.target.getAttribute("data-auraId");
        var hasClassCollapse = $A.util.hasClass(component.find(sectionAuraId), "slds-is-open");
        if(!hasClassCollapse){
            $A.util.addClass(component.find(sectionAuraId), 'slds-section slds-is-open');
            $A.util.removeClass(component.find(sectionAuraId), 'slds-is-close');
        }else{
            $A.util.addClass(component.find(sectionAuraId), 'slds-section slds-is-close');
            $A.util.removeClass(component.find(sectionAuraId), 'slds-is-open');
        }
    },
    
    handleSelect: function(component, event, helper) {
        var selected = event.getParam('name');
        component.set('v.currentContent', selected);
        component.set('v.selectedItem', selected);
    },
    handleGetRates: function(component, event, helper){
        helper.handleGetRates(component, event, helper);
    },
    handleValidateIMDG: function(component, event, helper){
        helper.handleValidateIMDG(component, event, helper);
    },
    
    viewPDF : function(component, event, helper){
        helper.viewPDF(component, event, helper);
    },
    openEmailAddressModal : function(component, event, helper){
        component.set("v.isSendEmail", true);
    },
    sendEmailWithPDF : function(component,event,helper){
        var inputField = component.find('emailIds');
        var allValid = inputField.get('v.validity').valid;
        if (allValid) {
            helper.sendEmailPDF(component, event, helper);
        } else {
            inputField.set('v.validity', {valid:false, badInput :true});
            inputField.showHelpMessageIfInvalid();
        }
    },
    closeModel : function(component,event,helper){
        component.set("v.isSendEmail", false);
    }
})