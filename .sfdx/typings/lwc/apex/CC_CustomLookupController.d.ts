declare module "@salesforce/apex/CC_CustomLookupController.getZipLocations" {
  export default function getZipLocations(param: {searchKeyWord: any, countryName: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_CustomLookupController.getPortLocations" {
  export default function getPortLocations(param: {searchKeyWord: any, countryName: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_CustomLookupController.getRailLocations" {
  export default function getRailLocations(param: {searchKeyWord: any, countryName: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_CustomLookupController.getCountries" {
  export default function getCountries(param: {searchKeyWord: any, type: any}): Promise<any>;
}
