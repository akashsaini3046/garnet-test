declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.fetchContactsAndAddresses" {
  export default function fetchContactsAndAddresses(param: {partyCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getHazardousDocument" {
  export default function getHazardousDocument(param: {bookingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getContactDetails" {
  export default function getContactDetails(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getCustomerCVIF" {
  export default function getCustomerCVIF(param: {contactRecord: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.fetchIncoTermList" {
  export default function fetchIncoTermList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.fetchPackageTypeList" {
  export default function fetchPackageTypeList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.fetchReceiptDeliveryTermsList" {
  export default function fetchReceiptDeliveryTermsList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.fetchCommodityList" {
  export default function fetchCommodityList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.fetchContainerTypeList" {
  export default function fetchContainerTypeList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getLocationDetails" {
  export default function getLocationDetails(param: {termCode: any, locCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.createBookingRecord" {
  export default function createBookingRecord(param: {bookingRecValuesString: any, bookingRequestRecord: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.createBlankBookingRecord" {
  export default function createBlankBookingRecord(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.createBlankFreightRecord" {
  export default function createBlankFreightRecord(param: {bookingRecordString: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.createCommodityRecordList" {
  export default function createCommodityRecordList(param: {bookingRecordString: any, sequenceNumber: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.createPackingLineRecords" {
  export default function createPackingLineRecords(param: {bookingRecordString: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.validateHazardousBooking" {
  export default function validateHazardousBooking(param: {bookingId: any, bookingRecValuesString: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getCICSBookingNumber" {
  export default function getCICSBookingNumber(param: {bookingId: any, quoteId: any, quoteRateId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getCFSLocations" {
  export default function getCFSLocations(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getSoftshipRates" {
  export default function getSoftshipRates(param: {bookingRecValuesString: any, sourceType: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.retriveCacheRecord" {
  export default function retriveCacheRecord(param: {userId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getSubstancesRecord" {
  export default function getSubstancesRecord(param: {searchKey: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getVehicles" {
  export default function getVehicles(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getBookingWrapper" {
  export default function getBookingWrapper(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getQuoteBookingWrapper" {
  export default function getQuoteBookingWrapper(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.addNewCargo" {
  export default function addNewCargo(param: {bookingWrapperData: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.processCICSBooking" {
  export default function processCICSBooking(param: {bookingWrapperData: any, book: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.customerAccount" {
  export default function customerAccount(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.bookingDetail" {
  export default function bookingDetail(param: {bookingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.getBookingFromNumber" {
  export default function getBookingFromNumber(param: {bookingNumber: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.fetchFclRates" {
  export default function fetchFclRates(param: {bookingWrapperData: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.fetchRatesForMultiContainer" {
  export default function fetchRatesForMultiContainer(param: {bookingWrapper: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.fetchContainerList" {
  export default function fetchContainerList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.createSoftshipRateAPIWRequest" {
  export default function createSoftshipRateAPIWRequest(param: {bookingWrapper: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_NewBookingsController.processResponse" {
  export default function processResponse(param: {labels: any, state: any}): Promise<any>;
}
