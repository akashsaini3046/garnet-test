declare module "@salesforce/apex/CustomerCommunity_ListViewController.listValues" {
  export default function listValues(param: {objectInfo: any}): Promise<any>;
}
