declare module "@salesforce/apex/IdeaDetailCmpController.getIdeaDetails" {
  export default function getIdeaDetails(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getUser" {
  export default function getUser(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getZonesList" {
  export default function getZonesList(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getBenefits" {
  export default function getBenefits(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getIdeaStatuses" {
  export default function getIdeaStatuses(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getIdeaCategories" {
  export default function getIdeaCategories(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.saveIdeaRecord" {
  export default function saveIdeaRecord(param: {ideaRecord: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.deleteIdeaRecord" {
  export default function deleteIdeaRecord(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.findSimilarIdeas" {
  export default function findSimilarIdeas(param: {communityId: any, title: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getIdeaFieldDescribe" {
  export default function getIdeaFieldDescribe(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getIdeaDescribe" {
  export default function getIdeaDescribe(): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getIdeaComments" {
  export default function getIdeaComments(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.upvoteIdea" {
  export default function upvoteIdea(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.downvoteIdea" {
  export default function downvoteIdea(param: {ideaId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.likeComment" {
  export default function likeComment(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.unlikeComment" {
  export default function unlikeComment(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.deleteComment" {
  export default function deleteComment(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.addComment" {
  export default function addComment(param: {param: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getIdeaCommentRecord" {
  export default function getIdeaCommentRecord(param: {ideaCommentId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.saveIdeaCommentRecord" {
  export default function saveIdeaCommentRecord(param: {comment: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getIdeasList" {
  export default function getIdeasList(param: {communityId: any, statuses: any, searchText: any, categories: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.fetchRecordTypeSpecificPickListvalues" {
  export default function fetchRecordTypeSpecificPickListvalues(param: {zoneId: any}): Promise<any>;
}
declare module "@salesforce/apex/IdeaDetailCmpController.getExpertsGroupMembers" {
  export default function getExpertsGroupMembers(param: {ideaId: any}): Promise<any>;
}
