declare module "@salesforce/apex/TestPDFController.savePDFOnAccount" {
  export default function savePDFOnAccount(): Promise<any>;
}
