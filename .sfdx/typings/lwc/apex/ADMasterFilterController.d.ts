declare module "@salesforce/apex/ADMasterFilterController.getAllChildAccount" {
  export default function getAllChildAccount(param: {idAccountId: any}): Promise<any>;
}
declare module "@salesforce/apex/ADMasterFilterController.getAllChildAndParent" {
  export default function getAllChildAndParent(param: {idAccountId: any}): Promise<any>;
}
declare module "@salesforce/apex/ADMasterFilterController.getAllActiveUsers" {
  export default function getAllActiveUsers(): Promise<any>;
}
declare module "@salesforce/apex/ADMasterFilterController.getAllActiveUsersRelatedToAccount" {
  export default function getAllActiveUsersRelatedToAccount(param: {strAccountId: any}): Promise<any>;
}
declare module "@salesforce/apex/ADMasterFilterController.getPicklistYear" {
  export default function getPicklistYear(): Promise<any>;
}
