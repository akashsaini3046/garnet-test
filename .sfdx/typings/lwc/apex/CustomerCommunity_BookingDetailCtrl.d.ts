declare module "@salesforce/apex/CustomerCommunity_BookingDetailCtrl.getBookingDetail" {
  export default function getBookingDetail(param: {bookingId: any}): Promise<any>;
}
