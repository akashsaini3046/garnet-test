declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.createRequest" {
  export default function createRequest(param: {quotationWrapper: any, cargoWrapperList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.sendRateRequest" {
  export default function sendRateRequest(param: {quoteRequest: any, FromToLocation: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.sendCargoWiseRequest" {
  export default function sendCargoWiseRequest(param: {quoteWrapper: any, cargodetailList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getShipper" {
  export default function getShipper(param: {quoteWrapper: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getConsignee" {
  export default function getConsignee(param: {quoteWrapper: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getCustomer" {
  export default function getCustomer(param: {quoteWrapper: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getListVoyages" {
  export default function getListVoyages(param: {quoteWrapper: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getListPackageLineItem" {
  export default function getListPackageLineItem(param: {quoteWrapper: any, cargodetailList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getListCommodities" {
  export default function getListCommodities(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getListRequirements" {
  export default function getListRequirements(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getShipments" {
  export default function getShipments(param: {quoteWrapper: any, originPort: any, destinationPort: any, cfsPort: any, listVoyages: any, packageLineCollection: any, listFreightDetails: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getQuoteRequest" {
  export default function getQuoteRequest(param: {quoteWrapper: any, party: any, shipment: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsHelper.getRoutesRequestBody" {
  export default function getRoutesRequestBody(param: {originPort: any, destinationPort: any, shipmentType: any, sailingWeeks: any}): Promise<any>;
}
