declare module "@salesforce/apex/CustomerCommunity_ShipmentController.getShipmentInfo" {
  export default function getShipmentInfo(param: {listTrackingIDs: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShipmentController.getCWShipmentInfo" {
  export default function getCWShipmentInfo(param: {shipmentType: any, trackingId: any}): Promise<any>;
}
