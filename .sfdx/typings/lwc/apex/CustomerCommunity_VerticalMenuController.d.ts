declare module "@salesforce/apex/CustomerCommunity_VerticalMenuController.getMenuItems" {
  export default function getMenuItems(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_VerticalMenuController.getCommunityName" {
  export default function getCommunityName(): Promise<any>;
}
