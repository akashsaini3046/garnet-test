declare module "@salesforce/apex/CustomerCommunity_ControlCenterCtrl.fetchBooking" {
  export default function fetchBooking(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ControlCenterCtrl.fetchUser" {
  export default function fetchUser(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ControlCenterCtrl.fetchAccountName" {
  export default function fetchAccountName(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ControlCenterCtrl.countOfCases" {
  export default function countOfCases(): Promise<any>;
}
