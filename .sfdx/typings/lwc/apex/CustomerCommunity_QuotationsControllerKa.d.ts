declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.fetchContactsAndAddresses" {
  export default function fetchContactsAndAddresses(param: {partyCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.getContactDetails" {
  export default function getContactDetails(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.getCustomerCVIF" {
  export default function getCustomerCVIF(param: {contactRecord: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.fetchIncoTermList" {
  export default function fetchIncoTermList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.fetchReceiptDeliveryTermsList" {
  export default function fetchReceiptDeliveryTermsList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.fetchCommodityList" {
  export default function fetchCommodityList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.fetchContainerTypeList" {
  export default function fetchContainerTypeList(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.getLocationDetails" {
  export default function getLocationDetails(param: {termCode: any, locCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.createBlankCargoRecord" {
  export default function createBlankCargoRecord(param: {cargoRecordList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.getCarriers" {
  export default function getCarriers(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.getCarrierRates" {
  export default function getCarrierRates(param: {listOfAllCarrierCodes: any, listOfDisplayedQuotes: any, quoteWrapper: any, cargodetailList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.createRequest" {
  export default function createRequest(param: {newCodeList: any, quotationWrapper: any, cargoWrapperList: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.sendRateRequest" {
  export default function sendRateRequest(param: {quoteRequest: any, carriedCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.getCFSLocations" {
  export default function getCFSLocations(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.createBlankQuotationRecord" {
  export default function createBlankQuotationRecord(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_QuotationsControllerKa.getCarrierRatesDuplicate" {
  export default function getCarrierRatesDuplicate(param: {carrierCode: any}): Promise<any>;
}
