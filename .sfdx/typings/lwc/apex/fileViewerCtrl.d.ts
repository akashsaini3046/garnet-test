declare module "@salesforce/apex/fileViewerCtrl.fetchContentDocument" {
  export default function fetchContentDocument(): Promise<any>;
}
