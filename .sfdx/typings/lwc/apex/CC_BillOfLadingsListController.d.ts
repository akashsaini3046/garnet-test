declare module "@salesforce/apex/CC_BillOfLadingsListController.fetchBillOfLadingData" {
  export default function fetchBillOfLadingData(param: {recordLimit: any, recordOffset: any, fieldName: any, order: any}): Promise<any>;
}
declare module "@salesforce/apex/CC_BillOfLadingsListController.getTotalNumberOfBillOfLadings" {
  export default function getTotalNumberOfBillOfLadings(): Promise<any>;
}
declare module "@salesforce/apex/CC_BillOfLadingsListController.getCommunityUrlPathPrefix" {
  export default function getCommunityUrlPathPrefix(): Promise<any>;
}
