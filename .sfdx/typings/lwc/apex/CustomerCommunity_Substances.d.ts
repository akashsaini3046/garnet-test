declare module "@salesforce/apex/CustomerCommunity_Substances.getAllSubstancesRecord" {
  export default function getAllSubstancesRecord(): Promise<any>;
}
