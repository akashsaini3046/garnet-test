declare module "@salesforce/apex/CrowleyCommunity_OTPVerification.OTPVerification" {
  export default function OTPVerification(param: {Identifier: any, verifyCode: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyCommunity_OTPVerification.OTPResend" {
  export default function OTPResend(param: {userEmail: any}): Promise<any>;
}
