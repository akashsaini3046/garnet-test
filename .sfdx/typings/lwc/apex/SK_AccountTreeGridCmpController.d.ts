declare module "@salesforce/apex/SK_AccountTreeGridCmpController.findHierarchyData" {
  export default function findHierarchyData(param: {recId: any}): Promise<any>;
}
