declare module "@salesforce/apex/CustomerCommunity_ArticleController.getArticles" {
  export default function getArticles(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ArticleController.getArticleDetails" {
  export default function getArticleDetails(param: {articleId: any}): Promise<any>;
}
