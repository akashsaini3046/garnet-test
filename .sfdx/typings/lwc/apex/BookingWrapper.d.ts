declare module "@salesforce/apex/BookingWrapper.addNewCargo" {
  export default function addNewCargo(param: {listCargoWrapper: any}): Promise<any>;
}
declare module "@salesforce/apex/BookingWrapper.getBookingWrapper" {
  export default function getBookingWrapper(): Promise<any>;
}
declare module "@salesforce/apex/BookingWrapper.getQuoteBookingWrapper" {
  export default function getQuoteBookingWrapper(): Promise<any>;
}
