declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getAddresses" {
  export default function getAddresses(param: {idHeader: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getHazardousLines" {
  export default function getHazardousLines(param: {idHeader: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getHeader" {
  export default function getHeader(param: {idHeader: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getHeaderRecordType" {
  export default function getHeaderRecordType(param: {idHeader: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getEquipments" {
  export default function getEquipments(param: {idHeader: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getChargeLineItems" {
  export default function getChargeLineItems(param: {idHeader: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.updateStatusToApprove" {
  export default function updateStatusToApprove(param: {objHeader: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.saveBolRecords" {
  export default function saveBolRecords(param: {listAddress: any, objHeader: any, listEquipment: any, isStatusApprove: any, listHazardousLine: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getBookingRecordValues" {
  export default function getBookingRecordValues(param: {objHeader: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.validateHazchecker" {
  export default function validateHazchecker(param: {objHeader: any, bookingRecordValues: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getBookingId" {
  export default function getBookingId(param: {bookingNumber: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getCICSBookingNumber" {
  export default function getCICSBookingNumber(param: {bookingNumber: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingForm_Ctrl1.getSoftshipRates" {
  export default function getSoftshipRates(param: {bookingRecValuesString: any, sourceType: any}): Promise<any>;
}
