declare module "@salesforce/apex/AttachmentDAOCls.getAllAttachments" {
  export default function getAllAttachments(param: {parentId: any}): Promise<any>;
}
