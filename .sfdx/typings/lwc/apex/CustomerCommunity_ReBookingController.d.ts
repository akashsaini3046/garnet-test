declare module "@salesforce/apex/CustomerCommunity_ReBookingController.cloneBookingRecords" {
  export default function cloneBookingRecords(param: {bookingRecordId: any, readyDate: any, contactRecordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ReBookingController.validateHazardousBooking" {
  export default function validateHazardousBooking(param: {bookingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ReBookingController.getCICSBookingNumber" {
  export default function getCICSBookingNumber(param: {bookingId: any}): Promise<any>;
}
