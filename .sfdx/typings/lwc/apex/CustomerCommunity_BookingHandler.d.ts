declare module "@salesforce/apex/CustomerCommunity_BookingHandler.fetchBookingRecords" {
  export default function fetchBookingRecords(): Promise<any>;
}
