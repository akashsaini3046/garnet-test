declare module "@salesforce/apex/CrowleyCommunity_LoginController.checkPortal" {
  export default function checkPortal(param: {username: any, password: any, currentURL: any}): Promise<any>;
}
