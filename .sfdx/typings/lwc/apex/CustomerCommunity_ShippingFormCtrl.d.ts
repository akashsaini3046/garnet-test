declare module "@salesforce/apex/CustomerCommunity_ShippingFormCtrl.fetchObjects" {
  export default function fetchObjects(param: {shippingInstructionId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_ShippingFormCtrl.saveRecordData" {
  export default function saveRecordData(param: {listWrapper: any}): Promise<any>;
}
