declare module "@salesforce/apex/CrowleyBookingRecordDetailCtrl.sendEmailPDF" {
  export default function sendEmailPDF(param: {bookingId: any, emailAddress: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookingRecordDetailCtrl.getBookingDetail" {
  export default function getBookingDetail(param: {bookingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookingRecordDetailCtrl.getFieldsDefinition" {
  export default function getFieldsDefinition(): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookingRecordDetailCtrl.generatePDF" {
  export default function generatePDF(param: {bookingId: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookingRecordDetailCtrl.getRates" {
  export default function getRates(param: {IdBooking: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookingRecordDetailCtrl.validateIMDG" {
  export default function validateIMDG(param: {IdBooking: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookingRecordDetailCtrl.getAllAttachments" {
  export default function getAllAttachments(param: {parentId: any}): Promise<any>;
}
declare module "@salesforce/apex/CrowleyBookingRecordDetailCtrl.getStringQuery" {
  export default function getStringQuery(param: {mapFieldSet: any, type: any}): Promise<any>;
}
