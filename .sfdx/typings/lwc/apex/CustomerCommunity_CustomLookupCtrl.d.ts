declare module "@salesforce/apex/CustomerCommunity_CustomLookupCtrl.fetchPorts" {
  export default function fetchPorts(param: {searchKeyWord: any, shipmentType: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_CustomLookupCtrl.fetchBusinessLocation" {
  export default function fetchBusinessLocation(param: {searchKeyWord: any, companyName: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_CustomLookupCtrl.fetchAccounts" {
  export default function fetchAccounts(param: {searchKeyWord: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_CustomLookupCtrl.fetchAccountsBooking" {
  export default function fetchAccountsBooking(param: {searchKeyWord: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_CustomLookupCtrl.fetchLocation" {
  export default function fetchLocation(param: {contactId: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_CustomLookupCtrl.fetchLocationDetails" {
  export default function fetchLocationDetails(param: {searchKeyWord: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_CustomLookupCtrl.fetchCountries" {
  export default function fetchCountries(param: {searchKeyWord: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_CustomLookupCtrl.fetchStates" {
  export default function fetchStates(param: {searchKeyWord: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_CustomLookupCtrl.getLocationDetails" {
  export default function getLocationDetails(param: {termCode: any, locCode: any}): Promise<any>;
}
