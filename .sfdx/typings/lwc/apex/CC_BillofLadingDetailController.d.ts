declare module "@salesforce/apex/CC_BillofLadingDetailController.getBolDetails" {
  export default function getBolDetails(param: {bolId: any}): Promise<any>;
}
