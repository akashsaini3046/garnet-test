declare module "@salesforce/apex/ADActualVsNormalisedRevenueController.getActualAndNormalisedRevenue" {
  export default function getActualAndNormalisedRevenue(param: {accountId: any, objMasterFilter: any}): Promise<any>;
}
