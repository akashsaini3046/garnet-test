declare module "@salesforce/apex/TBD_FindARouteController.getRoutes" {
  export default function getRoutes(param: {originPort: any, destinationPort: any, shipmentType: any, sailingWeeks: any}): Promise<any>;
}
