declare module "@salesforce/apex/CustomerCommunity_VesselSchController.getPortOfLoadingAndDischarge" {
  export default function getPortOfLoadingAndDischarge(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_VesselSchController.getVesselSchedulesList" {
  export default function getVesselSchedulesList(param: {polcode: any, podcode: any, departure: any, arrival: any, range: any, includeTransshipment: any}): Promise<any>;
}
