declare module "@salesforce/apex/CustomerCommunity_RegisterController.getUserFields" {
  export default function getUserFields(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_RegisterController.getCountries" {
  export default function getCountries(): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_RegisterController.checkIfAccountExists" {
  export default function checkIfAccountExists(param: {companyName: any, firstName: any, lastName: any, email: any, title: any, country: any}): Promise<any>;
}
declare module "@salesforce/apex/CustomerCommunity_RegisterController.registerUser" {
  export default function registerUser(param: {objUserFilter: any, contactId: any}): Promise<any>;
}
