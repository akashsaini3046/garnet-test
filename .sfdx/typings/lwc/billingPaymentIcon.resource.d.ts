declare module "@salesforce/resourceUrl/billingPaymentIcon" {
    var billingPaymentIcon: string;
    export default billingPaymentIcon;
}